/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100116
 Source Host           : localhost:3306
 Source Schema         : superdelivery

 Target Server Type    : MySQL
 Target Server Version : 100116
 File Encoding         : 65001

 Date: 15/10/2019 07:10:46
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for brand
-- ----------------------------
DROP TABLE IF EXISTS `brand`;
CREATE TABLE `brand`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `image` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `description` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of brand
-- ----------------------------
INSERT INTO `brand` VALUES (1, 0, 'adidas', 'adidas.png', '<p>adidas brand\'s description<br></p>', '2019-06-29 19:27:49', '2019-06-29 20:11:43');
INSERT INTO `brand` VALUES (2, 0, 'armani jeans', 'armani_jeans.png', '<p>armani jeans description</p>', '2019-06-29 19:28:48', '2019-06-29 19:28:48');
INSERT INTO `brand` VALUES (3, 0, 'calvin klein', 'ck.png', '<p>calvin klein\'s description<br></p>', '2019-06-29 19:30:56', '2019-06-29 19:30:56');
INSERT INTO `brand` VALUES (4, 0, 'converse', 'converse.png', '<p>converse\'s description<br></p>', '2019-06-29 19:31:15', '2019-06-29 19:31:15');
INSERT INTO `brand` VALUES (5, 0, 'disney', 'disney.png', '<p>disney\'s description<br></p>', '2019-06-29 19:32:56', '2019-06-29 19:32:56');
INSERT INTO `brand` VALUES (6, 0, 'lee', 'lee.png', '<p>lee\'s description<br></p>', '2019-06-29 19:33:35', '2019-06-29 19:33:35');
INSERT INTO `brand` VALUES (7, 0, 'levis', 'levis.png', '<p>levis\'s description<br></p>', '2019-06-29 19:34:05', '2019-06-29 19:34:05');
INSERT INTO `brand` VALUES (8, 0, 'princess', 'princess.png', '<p>princess\'s description<br></p>', '2019-06-29 19:34:27', '2019-06-29 19:34:27');
INSERT INTO `brand` VALUES (9, 2, 'custom-brand-1', '', NULL, '2019-06-30 12:52:37', '2019-06-30 13:46:24');
INSERT INTO `brand` VALUES (10, 2, 'custom-brand-2', '', NULL, '2019-06-30 12:53:16', '2019-06-30 12:53:16');
INSERT INTO `brand` VALUES (11, 2, 'custom-brand-3', '', NULL, '2019-06-30 12:53:37', '2019-06-30 12:53:50');
INSERT INTO `brand` VALUES (12, 2, 'custom-brand-4', '', NULL, '2019-06-30 12:53:58', '2019-06-30 12:53:58');
INSERT INTO `brand` VALUES (13, 2, 'custom-brand-5', '', NULL, '2019-06-30 12:54:05', '2019-06-30 12:54:05');
INSERT INTO `brand` VALUES (14, 2, 'custom-brand-6', '', NULL, '2019-06-30 12:54:13', '2019-06-30 12:54:13');
INSERT INTO `brand` VALUES (15, 2, 'custom-brand-7', '', NULL, '2019-06-30 12:54:22', '2019-06-30 12:54:22');
INSERT INTO `brand` VALUES (16, 2, 'custom-brand-8', '', NULL, '2019-06-30 12:54:29', '2019-06-30 12:54:29');
INSERT INTO `brand` VALUES (17, 2, 'custom-brand-9', '', NULL, '2019-06-30 12:54:35', '2019-06-30 12:54:35');
INSERT INTO `brand` VALUES (18, 2, 'custom-brand-10', '', NULL, '2019-06-30 12:54:42', '2019-06-30 12:54:42');
INSERT INTO `brand` VALUES (20, 3, 'apple', '', NULL, '2019-07-01 05:38:56', '2019-07-01 05:38:56');
INSERT INTO `brand` VALUES (21, 2, 'aaa', '', NULL, '2019-07-12 08:59:46', '2019-07-12 08:59:46');
INSERT INTO `brand` VALUES (22, 2, 'bbb', '', NULL, '2019-07-12 09:02:35', '2019-07-12 09:02:35');
INSERT INTO `brand` VALUES (23, 2, 'new brand-abc', '', NULL, '2019-07-15 03:25:41', '2019-07-15 03:25:41');

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) NOT NULL DEFAULT 0,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `arrange` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `leaf` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 92 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES (1, 0, 'Category-1', 'Category-1', '0001', 0, '2019-06-10 10:37:03', '2019-07-01 03:12:59');
INSERT INTO `category` VALUES (2, 0, 'Category-2', 'Category-2', '0002', 0, '2019-06-10 10:37:06', '2019-06-10 10:37:06');
INSERT INTO `category` VALUES (3, 0, 'Category-3', 'Category-3', '0003', 0, '2019-06-10 10:37:13', '2019-06-10 10:37:13');
INSERT INTO `category` VALUES (4, 0, 'Category-4', 'Category-4', '0004', 0, '2019-06-10 10:37:15', '2019-06-10 10:37:15');
INSERT INTO `category` VALUES (5, 0, 'Category-5', 'Category-5', '0005', 0, '2019-06-10 10:37:17', '2019-06-10 10:37:17');
INSERT INTO `category` VALUES (6, 1, 'Category-1-1', 'Category-1»Category-1-1', '00010001', 0, '2019-06-10 10:37:24', '2019-07-01 03:11:49');
INSERT INTO `category` VALUES (7, 1, 'Category-1-2', 'Category-1»Category-1-2', '00010002', 0, '2019-06-10 10:37:26', '2019-06-10 10:38:52');
INSERT INTO `category` VALUES (8, 1, 'Category-1-3', 'Category-1»Category-1-3', '00010003', 0, '2019-06-10 10:37:28', '2019-06-10 10:37:28');
INSERT INTO `category` VALUES (9, 1, 'Category-1-4', 'Category-1»Category-1-4', '00010004', 0, '2019-06-10 10:37:30', '2019-06-10 10:37:30');
INSERT INTO `category` VALUES (10, 1, 'Category-1-5', 'Category-1»Category-1-5', '00010005', 0, '2019-06-10 10:37:32', '2019-06-10 10:37:32');
INSERT INTO `category` VALUES (11, 2, 'Category-2-1', 'Category-2»Category-2-1', '00020001', 0, '2019-06-10 10:37:36', '2019-06-10 10:37:36');
INSERT INTO `category` VALUES (12, 2, 'Category-2-2', 'Category-2»Category-2-2', '00020002', 0, '2019-06-10 10:37:38', '2019-06-10 10:37:43');
INSERT INTO `category` VALUES (13, 2, 'Category-2-3', 'Category-2»Category-2-3', '00020003', 0, '2019-06-10 10:37:48', '2019-06-10 10:38:00');
INSERT INTO `category` VALUES (14, 2, 'Category-2-4', 'Category-2»Category-2-4', '00020004', 0, '2019-06-10 10:37:53', '2019-06-10 10:42:04');
INSERT INTO `category` VALUES (15, 2, 'Category-2-5', 'Category-2»Category-2-5', '00020005', 0, '2019-06-10 10:37:55', '2019-06-10 10:37:55');
INSERT INTO `category` VALUES (16, 6, 'Category-1-1-1', 'Category-1»Category-1-1»Category-1-1-1', '000100010001', 0, '2019-06-10 10:38:28', '2019-06-10 10:38:28');
INSERT INTO `category` VALUES (17, 6, 'Category-1-1-2', 'Category-1»Category-1-1»Category-1-1-2', '000100010002', 0, '2019-06-10 10:38:30', '2019-06-14 10:58:41');
INSERT INTO `category` VALUES (18, 6, 'Category-1-1-3', 'Category-1»Category-1-1»Category-1-1-3', '000100010003', 1, '2019-06-10 10:38:31', '2019-06-10 10:38:31');
INSERT INTO `category` VALUES (19, 6, 'Category-1-1-4', 'Category-1»Category-1-1»Category-1-1-4', '000100010004', 1, '2019-06-10 10:38:34', '2019-06-10 10:38:34');
INSERT INTO `category` VALUES (20, 6, 'Category-1-1-5', 'Category-1»Category-1-1»Category-1-1-5', '000100010005', 1, '2019-06-10 10:38:37', '2019-06-10 10:38:37');
INSERT INTO `category` VALUES (21, 7, 'Category-1-2-1', 'Category-1»Category-1-2»Category-1-2-1', '000100020001', 1, '2019-06-10 10:38:56', '2019-06-10 10:38:56');
INSERT INTO `category` VALUES (22, 7, 'Category-1-2-2', 'Category-1»Category-1-2»Category-1-2-2', '000100020002', 1, '2019-06-10 10:38:58', '2019-06-10 10:38:58');
INSERT INTO `category` VALUES (23, 7, 'Category-1-2-3', 'Category-1»Category-1-2»Category-1-2-3', '000100020003', 1, '2019-06-10 10:38:59', '2019-06-10 10:38:59');
INSERT INTO `category` VALUES (24, 7, 'Category-1-2-4', 'Category-1»Category-1-2»Category-1-2-4', '000100020004', 1, '2019-06-10 10:39:01', '2019-06-10 10:39:01');
INSERT INTO `category` VALUES (25, 7, 'Category-1-2-5', 'Category-1»Category-1-2»Category-1-2-5', '000100020005', 1, '2019-06-10 10:39:03', '2019-06-10 10:39:03');
INSERT INTO `category` VALUES (26, 8, 'Category-1-3-1', 'Category-1»Category-1-3»Category-1-3-1', '000100030001', 1, '2019-06-10 10:39:18', '2019-06-10 10:39:18');
INSERT INTO `category` VALUES (27, 8, 'Category-1-3-2', 'Category-1»Category-1-3»Category-1-3-2', '000100030002', 1, '2019-06-10 10:39:21', '2019-06-10 10:39:21');
INSERT INTO `category` VALUES (28, 8, 'Category-1-3-3', 'Category-1»Category-1-3»Category-1-3-3', '000100030003', 1, '2019-06-10 10:39:22', '2019-06-10 10:39:22');
INSERT INTO `category` VALUES (29, 8, 'Category-1-3-4', 'Category-1»Category-1-3»Category-1-3-4', '000100030004', 1, '2019-06-10 10:39:24', '2019-06-10 10:39:24');
INSERT INTO `category` VALUES (30, 8, 'Category-1-3-5', 'Category-1»Category-1-3»Category-1-3-5', '000100030005', 1, '2019-06-10 10:39:25', '2019-06-10 10:39:25');
INSERT INTO `category` VALUES (31, 8, 'Category-1-3-6', 'Category-1»Category-1-3»Category-1-3-6', '000100030006', 1, '2019-06-10 10:39:27', '2019-06-10 10:39:27');
INSERT INTO `category` VALUES (32, 9, 'Category-1-4-1', 'Category-1»Category-1-4»Category-1-4-1', '000100040001', 1, '2019-06-10 10:39:32', '2019-06-10 10:39:32');
INSERT INTO `category` VALUES (33, 9, 'Category-1-4-2', 'Category-1»Category-1-4»Category-1-4-2', '000100040002', 1, '2019-06-10 10:39:33', '2019-06-10 10:39:33');
INSERT INTO `category` VALUES (34, 9, 'Category-1-4-3', 'Category-1»Category-1-4»Category-1-4-3', '000100040003', 1, '2019-06-10 10:39:34', '2019-06-10 10:39:34');
INSERT INTO `category` VALUES (35, 9, 'Category-1-4-4', 'Category-1»Category-1-4»Category-1-4-4', '000100040004', 1, '2019-06-10 10:39:36', '2019-06-10 10:39:36');
INSERT INTO `category` VALUES (36, 10, 'Category-1-5-1', 'Category-1»Category-1-5»Category-1-5-1', '000100050001', 1, '2019-06-10 10:39:41', '2019-06-10 10:39:41');
INSERT INTO `category` VALUES (37, 10, 'Category-1-5-2', 'Category-1»Category-1-5»Category-1-5-2', '000100050002', 1, '2019-06-10 10:39:42', '2019-06-10 10:39:42');
INSERT INTO `category` VALUES (38, 10, 'Category-1-5-3', 'Category-1»Category-1-5»Category-1-5-3', '000100050003', 1, '2019-06-10 10:39:44', '2019-06-10 10:39:44');
INSERT INTO `category` VALUES (39, 10, 'Category-1-5-4', 'Category-1»Category-1-5»Category-1-5-4', '000100050004', 1, '2019-06-10 10:39:45', '2019-06-10 10:39:45');
INSERT INTO `category` VALUES (40, 11, 'Category-2-1-1', 'Category-2»Category-2-1»Category-2-1-1', '000200010001', 1, '2019-06-10 10:41:25', '2019-06-10 10:41:25');
INSERT INTO `category` VALUES (41, 11, 'Category-2-1-2', 'Category-2»Category-2-1»Category-2-1-2', '000200010002', 1, '2019-06-10 10:41:27', '2019-06-10 10:41:27');
INSERT INTO `category` VALUES (42, 11, 'Category-2-1-3', 'Category-2»Category-2-1»Category-2-1-3', '000200010003', 1, '2019-06-10 10:41:28', '2019-06-10 10:41:28');
INSERT INTO `category` VALUES (43, 11, 'Category-2-1-4', 'Category-2»Category-2-1»Category-2-1-4', '000200010004', 1, '2019-06-10 10:41:31', '2019-06-10 10:41:31');
INSERT INTO `category` VALUES (44, 12, 'Category-2-2-1', 'Category-2»Category-2-2»Category-2-2-1', '000200020001', 1, '2019-06-10 10:41:36', '2019-06-10 10:41:36');
INSERT INTO `category` VALUES (45, 12, 'Category-2-2-2', 'Category-2»Category-2-2»Category-2-2-2', '000200020002', 1, '2019-06-10 10:41:37', '2019-06-10 10:41:37');
INSERT INTO `category` VALUES (46, 12, 'Category-2-2-3', 'Category-2»Category-2-2»Category-2-2-3', '000200020003', 1, '2019-06-10 10:41:38', '2019-06-10 10:41:38');
INSERT INTO `category` VALUES (47, 12, 'Category-2-2-4', 'Category-2»Category-2-2»Category-2-2-4', '000200020004', 1, '2019-06-10 10:41:40', '2019-06-10 10:41:40');
INSERT INTO `category` VALUES (48, 12, 'Category-2-2-5', 'Category-2»Category-2-2»Category-2-2-5', '000200020005', 1, '2019-06-10 10:41:42', '2019-06-10 10:41:42');
INSERT INTO `category` VALUES (49, 13, 'Category-2-3-1', 'Category-2»Category-2-3»Category-2-3-1', '000200030001', 1, '2019-06-10 10:41:47', '2019-06-10 10:41:47');
INSERT INTO `category` VALUES (50, 13, 'Category-2-3-2', 'Category-2»Category-2-3»Category-2-3-2', '000200030002', 1, '2019-06-10 10:41:49', '2019-06-10 10:41:49');
INSERT INTO `category` VALUES (51, 13, 'Category-2-3-3', 'Category-2»Category-2-3»Category-2-3-3', '000200030003', 1, '2019-06-10 10:41:51', '2019-06-10 10:41:51');
INSERT INTO `category` VALUES (52, 13, 'Category-2-3-4', 'Category-2»Category-2-3»Category-2-3-4', '000200030004', 1, '2019-06-10 10:41:52', '2019-06-10 10:41:52');
INSERT INTO `category` VALUES (53, 13, 'Category-2-3-5', 'Category-2»Category-2-3»Category-2-3-5', '000200030005', 1, '2019-06-10 10:41:54', '2019-06-10 10:41:54');
INSERT INTO `category` VALUES (54, 14, 'Category-2-4-1', 'Category-2»Category-2-4»Category-2-4-1', '000200040001', 1, '2019-06-10 10:42:07', '2019-06-10 10:42:07');
INSERT INTO `category` VALUES (55, 14, 'Category-2-4-2', 'Category-2»Category-2-4»Category-2-4-2', '000200040002', 1, '2019-06-10 10:42:09', '2019-06-10 10:42:09');
INSERT INTO `category` VALUES (56, 14, 'Category-2-4-3', 'Category-2»Category-2-4»Category-2-4-3', '000200040003', 1, '2019-06-10 10:42:10', '2019-06-10 10:42:10');
INSERT INTO `category` VALUES (57, 14, 'Category-2-4-4', 'Category-2»Category-2-4»Category-2-4-4', '000200040004', 1, '2019-06-10 10:42:12', '2019-06-10 10:42:12');
INSERT INTO `category` VALUES (58, 14, 'Category-2-4-5', 'Category-2»Category-2-4»Category-2-4-5', '000200040005', 1, '2019-06-10 10:42:14', '2019-06-10 10:42:14');
INSERT INTO `category` VALUES (59, 15, 'Category-2-5-1', 'Category-2»Category-2-5»Category-2-5-1', '000200050001', 1, '2019-06-10 10:42:20', '2019-06-10 10:42:20');
INSERT INTO `category` VALUES (60, 15, 'Category-2-5-2', 'Category-2»Category-2-5»Category-2-5-2', '000200050002', 1, '2019-06-10 10:42:22', '2019-06-10 10:42:22');
INSERT INTO `category` VALUES (61, 15, 'Category-2-5-3', 'Category-2»Category-2-5»Category-2-5-3', '000200050003', 1, '2019-06-10 10:42:23', '2019-06-10 10:42:23');
INSERT INTO `category` VALUES (62, 3, 'Category-3-1', 'Category-3»Category-3-1', '00030001', 0, '2019-06-10 10:42:49', '2019-06-10 10:42:49');
INSERT INTO `category` VALUES (63, 3, 'Category-3-2', 'Category-3»Category-3-2', '00030002', 0, '2019-06-10 10:42:51', '2019-06-10 10:42:51');
INSERT INTO `category` VALUES (64, 3, 'Category-3-3', 'Category-3»Category-3-3', '00030003', 0, '2019-06-10 10:42:53', '2019-06-10 10:42:53');
INSERT INTO `category` VALUES (65, 3, 'Category-3-4', 'Category-3»Category-3-4', '00030004', 0, '2019-06-10 10:42:54', '2019-06-10 10:42:54');
INSERT INTO `category` VALUES (66, 62, 'Category-3-1-1', 'Category-3»Category-3-1»Category-3-1-1', '000300010001', 1, '2019-06-10 10:44:08', '2019-06-10 10:44:08');
INSERT INTO `category` VALUES (67, 62, 'Category-3-1-2', 'Category-3»Category-3-1»Category-3-1-2', '000300010002', 1, '2019-06-10 10:44:09', '2019-06-10 10:44:09');
INSERT INTO `category` VALUES (68, 62, 'Category-3-1-3', 'Category-3»Category-3-1»Category-3-1-3', '000300010003', 1, '2019-06-10 10:44:10', '2019-06-10 10:44:10');
INSERT INTO `category` VALUES (69, 62, 'Category-3-1-4', 'Category-3»Category-3-1»Category-3-1-4', '000300010004', 1, '2019-06-10 10:44:12', '2019-06-10 10:44:12');
INSERT INTO `category` VALUES (70, 63, 'Category-3-2-1', 'Category-3»Category-3-2»Category-3-2-1', '000300020001', 1, '2019-06-10 10:44:17', '2019-06-10 10:44:17');
INSERT INTO `category` VALUES (71, 63, 'Category-3-2-2', 'Category-3»Category-3-2»Category-3-2-2', '000300020002', 1, '2019-06-10 10:44:18', '2019-06-10 10:44:18');
INSERT INTO `category` VALUES (72, 63, 'Category-3-2-3', 'Category-3»Category-3-2»Category-3-2-3', '000300020003', 1, '2019-06-10 10:44:20', '2019-06-10 10:44:20');
INSERT INTO `category` VALUES (73, 63, 'Category-3-2-4', 'Category-3»Category-3-2»Category-3-2-4', '000300020004', 1, '2019-06-10 10:44:21', '2019-06-10 10:44:21');
INSERT INTO `category` VALUES (74, 64, 'Category-3-3-1', 'Category-3»Category-3-3»Category-3-3-1', '000300030001', 1, '2019-06-10 10:44:28', '2019-06-10 10:44:28');
INSERT INTO `category` VALUES (75, 64, 'Category-3-3-2', 'Category-3»Category-3-3»Category-3-3-2', '000300030002', 1, '2019-06-10 10:44:29', '2019-06-10 10:44:29');
INSERT INTO `category` VALUES (76, 64, 'Category-3-3-3', 'Category-3»Category-3-3»Category-3-3-3', '000300030003', 1, '2019-06-10 10:44:31', '2019-06-10 10:44:31');
INSERT INTO `category` VALUES (77, 65, 'Category-3-4-1', 'Category-3»Category-3-4»Category-3-4-1', '000300040001', 1, '2019-06-10 10:44:36', '2019-06-10 10:44:36');
INSERT INTO `category` VALUES (78, 65, 'Category-3-4-2', 'Category-3»Category-3-4»Category-3-4-2', '000300040002', 1, '2019-06-10 10:44:37', '2019-06-10 10:44:37');
INSERT INTO `category` VALUES (79, 65, 'Category-3-4-3', 'Category-3»Category-3-4»Category-3-4-3', '000300040003', 1, '2019-06-10 10:44:39', '2019-06-10 10:44:39');
INSERT INTO `category` VALUES (80, 65, 'Category-3-4-4', 'Category-3»Category-3-4»Category-3-4-4', '000300040004', 1, '2019-06-10 10:44:41', '2019-06-10 10:44:41');
INSERT INTO `category` VALUES (81, 65, 'Category-3-4-5', 'Category-3»Category-3-4»Category-3-4-5', '000300040005', 1, '2019-06-10 10:44:44', '2019-06-10 10:44:56');
INSERT INTO `category` VALUES (82, 65, 'Category-3-4-6', 'Category-3»Category-3-4»Category-3-4-6', '000300040006', 1, '2019-06-10 10:44:49', '2019-06-10 10:45:00');
INSERT INTO `category` VALUES (83, 4, 'Category-4-1', 'Category-4»Category-4-1', '00040001', 0, '2019-06-10 10:48:31', '2019-06-10 10:48:31');
INSERT INTO `category` VALUES (84, 4, 'Category-4-2', 'Category-4»Category-4-2', '00040002', 1, '2019-06-10 10:48:35', '2019-06-10 10:48:35');
INSERT INTO `category` VALUES (85, 83, 'Category-4-1-1', 'Category-4»Category-4-1»Category-4-1-1', '000400010001', 1, '2019-06-12 03:04:21', '2019-06-12 03:04:21');
INSERT INTO `category` VALUES (86, 83, 'Category-4-1-2', 'Category-4»Category-4-1»Category-4-1-2', '000400010002', 1, '2019-06-12 03:04:23', '2019-06-12 03:04:23');
INSERT INTO `category` VALUES (87, 5, 'Category-5-1', 'Category-5»Category-5-1', '00050001', 1, '2019-06-12 03:04:39', '2019-06-12 03:04:39');
INSERT INTO `category` VALUES (88, 5, 'Category-5-2', 'Category-5»Category-5-2', '00050002', 1, '2019-06-12 03:04:41', '2019-06-12 03:04:41');
INSERT INTO `category` VALUES (89, 5, 'Category-5-3', 'Category-5»Category-5-3', '00050003', 1, '2019-06-12 03:04:42', '2019-06-12 03:04:42');
INSERT INTO `category` VALUES (90, 5, 'Category-5-4', 'Category-5»Category-5-4', '00050004', 1, '2019-06-12 03:04:44', '2019-06-12 03:04:44');
INSERT INTO `category` VALUES (91, 5, 'Category-5-5', 'Category-5»Category-5-5', '00050005', 1, '2019-06-12 03:04:46', '2019-06-12 03:04:46');

-- ----------------------------
-- Table structure for category2
-- ----------------------------
DROP TABLE IF EXISTS `category2`;
CREATE TABLE `category2`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of category2
-- ----------------------------
INSERT INTO `category2` VALUES (1, 2, 'custom-category-1', '2019-06-18 03:34:51', '2019-06-30 12:48:23');
INSERT INTO `category2` VALUES (2, 2, 'custom-category-2', '2019-06-18 03:35:10', '2019-06-18 03:35:10');
INSERT INTO `category2` VALUES (3, 2, 'custom-category-3', '2019-06-18 03:35:15', '2019-06-18 03:35:15');
INSERT INTO `category2` VALUES (4, 2, 'custom-category-4', '2019-06-18 03:35:19', '2019-06-18 03:35:19');
INSERT INTO `category2` VALUES (5, 2, 'custom-category-5', '2019-06-18 03:35:24', '2019-06-18 03:35:24');
INSERT INTO `category2` VALUES (6, 2, 'custom-category-6', '2019-06-26 10:32:55', '2019-06-26 10:32:55');
INSERT INTO `category2` VALUES (7, 2, 'custom-category-7', '2019-06-18 03:35:46', '2019-06-18 03:35:46');
INSERT INTO `category2` VALUES (8, 3, 'notebook', '2019-06-18 04:27:28', '2019-06-18 04:27:28');
INSERT INTO `category2` VALUES (9, 3, 'desktop', '2019-06-18 04:27:31', '2019-06-18 04:27:31');
INSERT INTO `category2` VALUES (10, 3, 'mouse', '2019-06-18 04:27:33', '2019-06-18 04:27:33');
INSERT INTO `category2` VALUES (11, 3, 'keyboard', '2019-06-18 04:27:37', '2019-06-18 04:27:37');
INSERT INTO `category2` VALUES (12, 3, 'speaker', '2019-06-18 04:27:40', '2019-06-18 04:27:40');
INSERT INTO `category2` VALUES (13, 2, 'custom-category-8', '2019-06-30 12:46:36', '2019-06-30 12:46:36');
INSERT INTO `category2` VALUES (14, 2, 'custom-category-9', '2019-06-30 12:47:22', '2019-06-30 12:47:22');
INSERT INTO `category2` VALUES (15, 3, 'phone', '2019-07-01 05:38:20', '2019-07-01 05:38:20');
INSERT INTO `category2` VALUES (16, 2, 'subcategory-1', '2019-07-12 09:23:28', '2019-07-12 09:23:28');
INSERT INTO `category2` VALUES (17, 2, 'subcategory-2', '2019-07-12 09:24:09', '2019-07-12 09:24:09');
INSERT INTO `category2` VALUES (18, 2, 'subcategory-3', '2019-07-12 09:25:16', '2019-07-12 09:25:16');
INSERT INTO `category2` VALUES (19, 2, 'subcategory-4', '2019-07-12 09:27:05', '2019-07-12 09:27:05');
INSERT INTO `category2` VALUES (20, 2, 'subcategory-5', '2019-07-12 09:30:36', '2019-07-12 09:30:36');
INSERT INTO `category2` VALUES (21, 2, 'subcategory-6', '2019-07-12 09:31:50', '2019-07-12 09:31:50');
INSERT INTO `category2` VALUES (22, 2, 'subcategory-7', '2019-07-12 09:39:49', '2019-07-12 09:39:49');
INSERT INTO `category2` VALUES (23, 2, 'subcategory-8', '2019-07-12 09:45:39', '2019-07-12 09:45:39');
INSERT INTO `category2` VALUES (24, 2, 'subcategory-9', '2019-07-12 09:48:40', '2019-07-12 09:48:40');
INSERT INTO `category2` VALUES (25, 2, 'new-category-abc', '2019-07-15 03:25:27', '2019-07-15 03:25:27');

-- ----------------------------
-- Table structure for category_option
-- ----------------------------
DROP TABLE IF EXISTS `category_option`;
CREATE TABLE `category_option`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `category_id`(`category_id`, `name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of category_option
-- ----------------------------
INSERT INTO `category_option` VALUES (1, 1, 'Color', '2019-06-18 04:13:15', '2019-06-18 04:13:15');
INSERT INTO `category_option` VALUES (2, 1, 'Material', '2019-06-18 04:13:23', '2019-06-18 04:13:23');
INSERT INTO `category_option` VALUES (3, 6, 'Fashion', '2019-06-18 04:14:35', '2019-06-30 09:31:54');
INSERT INTO `category_option` VALUES (4, 16, 'Shape', '2019-06-18 04:15:17', '2019-06-18 04:15:17');

-- ----------------------------
-- Table structure for category_option_detail
-- ----------------------------
DROP TABLE IF EXISTS `category_option_detail`;
CREATE TABLE `category_option_detail`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NULL DEFAULT NULL,
  `option_id` int(11) NULL DEFAULT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `category_id`(`category_id`, `option_id`, `name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of category_option_detail
-- ----------------------------
INSERT INTO `category_option_detail` VALUES (1, 1, 1, 'Color-Red', '2019-06-18 04:13:38', '2019-06-18 04:13:38');
INSERT INTO `category_option_detail` VALUES (2, 1, 1, 'Color-Blue', '2019-06-18 04:13:43', '2019-06-18 04:13:43');
INSERT INTO `category_option_detail` VALUES (3, 1, 1, 'Color-Green', '2019-06-18 04:13:50', '2019-06-18 04:13:50');
INSERT INTO `category_option_detail` VALUES (4, 1, 2, 'Geniune Cotton', '2019-06-18 04:17:41', '2019-06-18 04:17:41');
INSERT INTO `category_option_detail` VALUES (5, 1, 2, 'Geniun Plastic', '2019-06-18 04:14:21', '2019-06-18 04:14:21');
INSERT INTO `category_option_detail` VALUES (6, 6, 3, 'Fashion-1', '2019-06-18 04:14:55', '2019-06-18 04:14:55');
INSERT INTO `category_option_detail` VALUES (7, 6, 3, 'Fashion-2', '2019-06-18 04:15:01', '2019-06-18 04:15:01');
INSERT INTO `category_option_detail` VALUES (8, 6, 3, 'Fashion-3', '2019-06-18 04:15:07', '2019-06-18 04:15:07');
INSERT INTO `category_option_detail` VALUES (9, 16, 4, 'Rounded', '2019-06-18 04:15:26', '2019-06-18 04:15:26');
INSERT INTO `category_option_detail` VALUES (10, 16, 4, 'Rectangle', '2019-06-18 04:15:32', '2019-06-18 04:15:32');
INSERT INTO `category_option_detail` VALUES (11, 16, 4, 'Circle', '2019-06-18 04:15:40', '2019-06-18 04:15:40');

-- ----------------------------
-- Table structure for company
-- ----------------------------
DROP TABLE IF EXISTS `company`;
CREATE TABLE `company`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `type` tinyint(4) NOT NULL DEFAULT 1,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of company
-- ----------------------------
INSERT INTO `company` VALUES (1, 2, 1, '<p>\n\nA wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart. I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine. I am so happy, my dear friend, so absorbed in the exquisite sense of mere tranquil existence, that I neglect my talents. I should be incapable of drawing a single stroke at the present moment; and yet I feel that I never was a greater artist than now.<br></p><p>From CloudStar Administrator</p>', '2019-06-30 11:50:55', '2019-07-01 05:42:07');
INSERT INTO `company` VALUES (2, 2, 2, '<p>\n\nAnim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven\'t heard of them accusamus labore sustainable VHS.<br></p>', '2019-06-30 11:51:22', '2019-06-30 11:51:22');
INSERT INTO `company` VALUES (3, 2, 3, '<p>\n\n<ul><li>Lorem ipsum dolor sit amet</li><li>Consectetur adipiscing elit</li><li>Integer molestie lorem at massa</li><li>Facilisis in pretium nisl aliquet</li><li>Nulla volutpat aliquam velit</li></ul>\n\n<br></p>', '2019-06-30 11:51:46', '2019-06-30 11:51:46');
INSERT INTO `company` VALUES (4, 3, 1, '<p>\n\n消費者への直送×ご購入前の販売×画像転載○ネット販売×代金引換×\n\n<br></p>', '2019-07-01 05:35:01', '2019-07-01 05:35:01');
INSERT INTO `company` VALUES (5, 3, 2, '<p>\n\n＊運賃改定致しました。<br><br>■運送業者：福山通運／西濃運輸／ヤマト運輸　、その他<br>※配送業者のご指定はお受けいたし兼ねます。ご了承ください。<br><br>■沖縄・離島のお客様へ<br>他エリアと異なり、ご注文に関しましては配送費が発生致します。<br>配送費のご確認につきましては、ご発注時にお問合わせ下さい。<br><br>■日曜・祝日の納品は不可とさせていただきます。\n\n<br></p>', '2019-07-01 05:35:13', '2019-07-01 05:35:13');
INSERT INTO `company` VALUES (6, 3, 3, '<p>\n\n<p>出荷状況により、送料が変更になる場合がございます。</p><p>※配送先が離島のお客様へ</p><p>送料（見込み）金額は 都道府県名のみで判別しております。<br>離島への発送時は画面上の送料から大きく変わる可能性がございます。<br>ご不明な場合は、ご注文の前に必ずご連絡いただくようお願いいたします。</p>\n\n<br></p>', '2019-07-01 05:35:31', '2019-07-01 05:35:31');

-- ----------------------------
-- Table structure for deliver
-- ----------------------------
DROP TABLE IF EXISTS `deliver`;
CREATE TABLE `deliver`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `product_detail_id` int(11) NULL DEFAULT NULL,
  `quantity` double NULL DEFAULT NULL,
  `address` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(4) NULL DEFAULT 0,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of deliver
-- ----------------------------
INSERT INTO `deliver` VALUES (1, '2019-07-01', 2, 'buyer1', 1, 2, 'buyer1 address', 1, '2019-07-01 06:37:40', '2019-07-01 06:37:40');
INSERT INTO `deliver` VALUES (2, '2019-07-01', 2, 'buyer1', 2, 1, 'buyer1 address', 1, '2019-07-01 06:37:40', '2019-07-01 06:37:40');
INSERT INTO `deliver` VALUES (3, '2019-07-01', 2, 'buyer1', 3, 2, 'buyer1 address', 1, '2019-07-01 06:38:40', '2019-07-01 06:38:40');
INSERT INTO `deliver` VALUES (4, '2019-07-01', 2, 'buyer1', 4, 2, 'buyer1 address', 1, '2019-07-01 06:38:40', '2019-07-01 06:38:40');
INSERT INTO `deliver` VALUES (10, '2019-07-12', 2, 'buyer1', 2, 3, 'buyer1 address', 0, '2019-07-12 08:22:02', '2019-07-12 08:22:02');
INSERT INTO `deliver` VALUES (11, '2019-07-12', 2, 'buyer1', 3, 2, 'buyer1 address', 0, '2019-07-12 08:22:03', '2019-07-12 08:22:03');
INSERT INTO `deliver` VALUES (12, '2019-07-12', 2, 'buyer1', 4, 2, 'buyer1 address', 0, '2019-07-12 08:23:30', '2019-07-12 08:23:30');

-- ----------------------------
-- Table structure for memo
-- ----------------------------
DROP TABLE IF EXISTS `memo`;
CREATE TABLE `memo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NULL DEFAULT NULL,
  `product_id` int(11) NULL DEFAULT NULL,
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of memo
-- ----------------------------
INSERT INTO `memo` VALUES (1, 4, 1, 'abcd', '2019-07-01 04:28:34', '2019-07-01 04:49:33');
INSERT INTO `memo` VALUES (2, 4, 2, 'ads', '2019-07-01 04:28:39', '2019-07-01 04:28:39');
INSERT INTO `memo` VALUES (3, 4, 3, 'zzz', '2019-07-01 04:28:41', '2019-07-01 04:28:41');
INSERT INTO `memo` VALUES (5, 13, 8, 'nice', '2019-07-01 05:53:35', '2019-07-01 05:53:35');
INSERT INTO `memo` VALUES (6, 4, 9, 'zzxzzz', '2019-07-16 01:57:06', '2019-07-16 01:57:06');
INSERT INTO `memo` VALUES (7, 2, 10, 'Test', '2019-10-13 10:15:30', '2019-10-13 10:15:30');

-- ----------------------------
-- Table structure for price
-- ----------------------------
DROP TABLE IF EXISTS `price`;
CREATE TABLE `price`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `customer_username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `customer_id` int(11) NULL DEFAULT NULL,
  `status` tinyint(4) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of price
-- ----------------------------
INSERT INTO `price` VALUES (1, 2, 'buyer3', 12, 1, '2019-07-22 04:25:50', '2019-07-22 04:25:50');
INSERT INTO `price` VALUES (2, 2, 'buyer1', 4, 1, '2019-07-22 04:55:56', '2019-07-22 04:55:56');
INSERT INTO `price` VALUES (3, 3, 'buyer1', 4, 1, '2019-07-22 05:24:23', '2019-07-22 05:24:23');
INSERT INTO `price` VALUES (4, 2, 'buyer2', 5, 1, '2019-07-24 01:40:07', '2019-07-24 01:40:07');
INSERT INTO `price` VALUES (5, 2, 'buyer4', 13, 0, '2019-07-24 04:12:09', '2019-07-24 04:12:09');
INSERT INTO `price` VALUES (6, 2, 'wholesaler1', 2, 0, '2019-10-13 10:45:13', '2019-10-13 10:45:13');
INSERT INTO `price` VALUES (7, 2, 'test', 33, 0, '2019-10-13 13:12:42', '2019-10-13 13:12:42');
INSERT INTO `price` VALUES (8, 3, 'test', 33, 0, '2019-10-13 13:19:15', '2019-10-13 13:19:15');

-- ----------------------------
-- Table structure for price_discuss
-- ----------------------------
DROP TABLE IF EXISTS `price_discuss`;
CREATE TABLE `price_discuss`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price_id` int(11) NULL DEFAULT NULL,
  `product_id` int(11) NULL DEFAULT NULL,
  `product_detail_id` int(11) NULL DEFAULT NULL,
  `wholesale_price` double NOT NULL DEFAULT 0,
  `retail_price` double NOT NULL DEFAULT 0,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of price_discuss
-- ----------------------------
INSERT INTO `price_discuss` VALUES (1, 4, 1, 1, 95, 205, '2019-07-24 03:16:07', '2019-07-24 03:16:41');
INSERT INTO `price_discuss` VALUES (2, 4, 1, 2, 195, 410, '2019-07-24 03:19:44', '2019-07-24 03:19:44');
INSERT INTO `price_discuss` VALUES (3, 4, 1, 3, 280, 590, '2019-07-24 03:20:48', '2019-07-24 03:20:48');
INSERT INTO `price_discuss` VALUES (4, 4, 2, 4, 1000, 1950, '2019-07-24 03:20:56', '2019-07-24 03:20:56');
INSERT INTO `price_discuss` VALUES (5, 4, 6, 14, 400, 550, '2019-07-24 03:21:13', '2019-07-24 03:21:13');
INSERT INTO `price_discuss` VALUES (6, 4, 2, 5, 2000, 3900, '2019-07-24 04:10:36', '2019-07-24 04:10:36');
INSERT INTO `price_discuss` VALUES (7, 5, 1, 1, 90, 210, '2019-07-24 04:12:27', '2019-07-24 04:12:27');

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `brand_id` int(11) NOT NULL DEFAULT 0,
  `category_id` int(11) NOT NULL DEFAULT 0,
  `options` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `category_id2` int(11) NOT NULL DEFAULT 0,
  `main_image` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sku` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `keywords` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `standard` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `shipping` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sizeandcapacity` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `notes` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `is_show_image` tinyint(4) NOT NULL DEFAULT 1,
  `is_new` tinyint(4) NOT NULL DEFAULT 1,
  `original_price` double NOT NULL DEFAULT 0,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES (1, '2019-07-24', 2, 1, 1, '1,3,4,5', 1, 'product1.jpg', 'sku1', 'product1', 'keyword-1,keyword-2,keyword-3,new-keyword-1', '<p>product1\'s standard</p>', '<p>product1\'s description</p>', '<p>product1\'s shipping methods</p>', '<p>product1\'s size and capacity</p>', '<p>product1\'s notes</p>', 1, 0, 101, '2019-07-01 03:01:22', '2019-07-24 03:28:54');
INSERT INTO `product` VALUES (2, '2019-07-24', 2, 2, 1, '1,2,3,5', 2, 'product2.jpg', 'sku2', 'product2', 'keyword-1,keyword-4,keyword-5', '<p>product2\'s standard</p>', '<p>product2\'s description</p>', '<p>product2\'s shipping methods</p>', '<p>product2\'s size and capacity</p>', '<p>product2\'s notes</p>', 0, 0, 1200, '2019-07-01 03:30:40', '2019-07-24 03:29:14');
INSERT INTO `product` VALUES (3, '2019-07-01', 2, 1, 6, '1,3,4,5,7,8', 3, 'product3.jpg', 'sku3', 'product3', 'keyword-1,keyword-3,keyword-5', '<p>product3\'s standard</p>', '<p>product3\'s description</p>', '<p>product3\'s shipping methods</p>', '<p>product3\'s size and capacity</p>', '<p>product3\'s notes</p>', 1, 1, 100, '2019-07-01 03:34:43', '2019-07-01 03:34:43');
INSERT INTO `product` VALUES (4, '2019-07-01', 2, 9, 7, '2,3,5', 4, 'product4.jpg', 'sku4', 'product4', 'keyword-2,keyword-5', '<p>product4\'s standard</p>', '<p>product4\'s description</p>', '<p>product4\'s shipping methods</p>', '<p>product4\'s size and capacity</p>', '<p>product4\'s notes</p>', 1, 0, 200, '2019-07-01 03:39:19', '2019-07-01 03:39:19');
INSERT INTO `product` VALUES (5, '2019-07-15', 2, 3, 6, '1,3,4,7', 2, 'prodcut5.jpg', 'sku5', 'product5', 'apple-keyword,keyword-2,keyword-4,new-keyword-2', '<p>product5\'s standard</p>', '<p>product5\'s description</p>', '<p>product5\'s shipping methods</p>', '<p>product5\'s size and capacity</p>', '<p>product5\'s notes</p>', 1, 1, 300, '2019-07-01 05:25:53', '2019-07-15 03:51:40');
INSERT INTO `product` VALUES (6, '2019-07-01', 2, 3, 7, '2,4,5', 1, 'product6.jpg', 'sku6', 'product6', 'keyword-2,keyword-5', '<p>product6\'s standard</p>', '<p>product6\'s description</p>', '<p>product6\'s shipping methods</p>', '<p>product6\'s size and capacity</p>', '<p>product6\'s notes</p>', 1, 1, 400, '2019-07-01 05:35:30', '2019-07-01 05:35:30');
INSERT INTO `product` VALUES (7, '2019-07-01', 2, 2, 18, '2,4,5,7,8', 2, 'product7.jpg', 'sku7', 'product7', 'keyword-2,keyword-3', '<p>product7\'s standard</p>', '<p>product7\'s description</p>', '<p>product7\'s shipping methods.</p>', '<p>product7\'s size and capacity</p>', '<p>product7\'s notes</p>', 1, 1, 500, '2019-07-01 05:40:37', '2019-07-01 05:40:37');
INSERT INTO `product` VALUES (8, '2019-07-01', 3, 20, 8, '1,2,3', 15, 'tech_phone.jpg', 'tp23434', 'tech_phone', 'keyword-1,keyword-3,keyword-5', '<p>tech_phone standard</p>', '<p>Tech_Phone Description  </p>', '<p>Tech_Phone shipping methods</p>', '<p>techo_phone size and capacity</p>', '<p>tech_phone notes</p>', 0, 1, 600, '2019-07-01 05:40:01', '2019-07-01 05:45:12');
INSERT INTO `product` VALUES (9, '2019-07-01', 2, 4, 9, '2,4,5', 1, 'product8.jpg', 'sku8', 'product8', 'keyword-3,keyword-5', '<p>product8\'s standard</p>', '<p>product8\'s description</p>', '<p>product8\'s shipping methods</p>', '<p>product8\'s size and capacity</p>', '<p>product8\'s notes</p>', 1, 1, 700, '2019-07-01 05:45:00', '2019-07-01 05:45:39');
INSERT INTO `product` VALUES (10, '2019-07-01', 2, 9, 2, '', 5, 'product9.jpg', 'sku9', 'product9', 'keyword-2,keyword-4,keyword-5', '<p>product9\'s standard</p>', '<p>product9\'s description</p>', '<p>product9\'s shipping methods</p>', '<p>product9\'s size and capacity</p>', '<p>product9\'s notes</p>', 1, 1, 800, '2019-07-01 05:50:16', '2019-07-01 05:50:16');
INSERT INTO `product` VALUES (11, '2019-07-01', 3, 20, 38, '1,2,3,5', 8, 'mac.jpg', 'm2343', 'iMac', 'apple-keyword,keyword-1,keyword-3', '<p>\n\nABCDEFGHIJKLMNOPQRSTUVWXYZ\n\n<br></p>', '<p>ABCDEFGHIJKLMNOPQRSTUVWXYZ</p>', '<p>\n\nABCDEFGHIJKLMNOPQRSTUVWXYZ\n\n<br></p>', '<p>\n\nABCDEFGHIJKLMNOPQRSTUVWXYZ\n\n<br></p>', '<p>\n\nABCDEFGHIJKLMNOPQRSTUVWXYZ\n\n<br></p>', 1, 1, 900, '2019-07-01 05:48:30', '2019-07-01 05:51:19');
INSERT INTO `product` VALUES (12, '2019-07-01', 2, 17, 12, '', 7, 'product10.jpg', 'sku10', 'product10', 'apple-keyword,keyword-3,keyword-5', '<p>product10\'s standard</p>', '<p>product10\'s description</p>', '<p>product10\'s shipping methods</p>', '<p>product10\'s size and capacity</p>', '<p>product10\'s notes</p>', 1, 1, 1100, '2019-07-01 05:55:18', '2019-07-01 05:55:18');
INSERT INTO `product` VALUES (14, '2019-10-13', 2, 1, 22, '2,4,5', 4, 'test.jpg', '151234', 'Prrr', 'keyword-2', '', '', '', '', '', 1, 0, 112, '2019-10-13 09:31:18', '2019-10-13 09:31:18');

-- ----------------------------
-- Table structure for product_detail
-- ----------------------------
DROP TABLE IF EXISTS `product_detail`;
CREATE TABLE `product_detail`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `product_id` int(11) NULL DEFAULT NULL,
  `product_number` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `breakdown` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `barcode` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `jan` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `quantity` double NULL DEFAULT NULL,
  `retail_type` tinyint(4) NOT NULL DEFAULT 1,
  `retail_price` double NOT NULL DEFAULT 0,
  `wholesale_type` tinyint(4) NOT NULL DEFAULT 0,
  `wholesale_price` double NOT NULL DEFAULT 0,
  `is_stock` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of product_detail
-- ----------------------------
INSERT INTO `product_detail` VALUES (1, 2, 1, 'number-1-1', 'breakdown-1-1', 'barcode-1-1', 'jan-1-1', 100, 1, 200, 0, 100, 1, '2019-07-01 03:13:35', '2019-10-13 00:42:43');
INSERT INTO `product_detail` VALUES (2, 2, 1, 'number-1-2', 'breakdown-1-2', 'barcode-1-2', 'jan-1-2', 200, 2, 400, 1, 200, 1, '2019-07-01 03:16:02', '2019-07-01 03:16:02');
INSERT INTO `product_detail` VALUES (3, 2, 1, 'number1-3', 'breakdown1-3', 'barcode1-3', 'jan1-3', 300, 2, 600, 1, 300, 1, '2019-07-01 03:28:17', '2019-07-01 03:28:17');
INSERT INTO `product_detail` VALUES (4, 2, 2, 'number-2-1', 'breakdown2-1', 'barcode2-1', 'jan2-1', 1000, 1, 2000, 1, 1000, 1, '2019-07-01 03:32:39', '2019-07-01 03:32:39');
INSERT INTO `product_detail` VALUES (5, 2, 2, 'number2-2', 'breakdown2-2', 'barcode2-2', 'jan2-2', 2000, 2, 4000, 1, 2000, 1, '2019-07-01 03:33:21', '2019-07-01 03:33:21');
INSERT INTO `product_detail` VALUES (6, 2, 3, 'number3-1', 'breakdown3-1', 'barcode3-1', 'jan3-1', 150, 2, 230, 1, 200, 1, '2019-07-01 03:36:02', '2019-07-01 03:36:02');
INSERT INTO `product_detail` VALUES (7, 2, 3, 'number3-2', 'breakdown3-2', 'barcode3-2', 'jan3-2', 400, 5, 1000, 1, 800, 1, '2019-07-01 03:36:43', '2019-07-01 03:36:43');
INSERT INTO `product_detail` VALUES (8, 2, 4, 'number4-1', 'breakdown4-1', 'barcode4-1', 'jan4-1', 500, 2, 800, 1, 550, 1, '2019-07-01 03:40:29', '2019-07-01 03:40:29');
INSERT INTO `product_detail` VALUES (9, 2, 4, 'number4-2', 'breakdown4-2', 'barcode4-2', 'jan4-2', 400, 2, 700, 1, 500, 1, '2019-07-01 03:42:10', '2019-07-01 03:42:10');
INSERT INTO `product_detail` VALUES (10, 2, 4, 'number4-3', 'breakdown4-3', 'barcode4-3', 'jan4-3', 420, 2, 600, 1, 450, 1, '2019-07-01 03:43:02', '2019-07-01 03:43:02');
INSERT INTO `product_detail` VALUES (11, 2, 5, 'S1', 'breakdown5-1', 'barcode5-1', 'jan-5-1', 400, 2, 800, 1, 400, 1, '2019-07-01 05:32:56', '2019-07-01 05:32:56');
INSERT INTO `product_detail` VALUES (12, 2, 5, 'S2', 'breakdown5-2', 'barcode5-2', 'jan5-2', 600, 1, 800, 1, 540, 1, '2019-07-01 05:33:26', '2019-07-01 05:33:26');
INSERT INTO `product_detail` VALUES (13, 2, 5, 'S3', 'breakdown5-3', 'barcode5-3', 'jan5-3', 550, 3, 340, 1, 200, 1, '2019-07-01 05:33:58', '2019-07-01 05:33:58');
INSERT INTO `product_detail` VALUES (14, 2, 6, 'S1', 'breakdown6-1', 'barcode6-1', 'jan6-1', 123, 2, 500, 1, 400, 1, '2019-07-01 05:36:45', '2019-07-01 05:36:45');
INSERT INTO `product_detail` VALUES (15, 2, 6, 'S2', 'breakdown6-2', 'barcode6-2', 'jan6-2', 50, 2, 500, 1, 450, 1, '2019-07-01 05:37:17', '2019-07-01 05:37:17');
INSERT INTO `product_detail` VALUES (16, 2, 6, 'S3', 'breakdown6-3', 'barcode6-3', 'jan6-3', 100, 2, 400, 1, 350, 1, '2019-07-01 05:37:40', '2019-07-01 05:37:40');
INSERT INTO `product_detail` VALUES (17, 2, 7, 'S1', 'breakdown7-1', 'barcode7-1', 'jan7-1', 430, 2, 300, 1, 200, 1, '2019-07-01 05:41:18', '2019-07-01 05:41:18');
INSERT INTO `product_detail` VALUES (18, 2, 7, 'S2', 'breakdown7-2', 'barcode7-2', 'jan7-2', 400, 2, 120, 1, 100, 1, '2019-07-01 05:41:41', '2019-07-01 05:41:41');
INSERT INTO `product_detail` VALUES (19, 3, 8, 'S1', 'breakdown1', 'jp123213', '432434', 5, 1, 2000, 1, 1500, 1, '2019-07-01 05:41:08', '2019-07-01 05:41:08');
INSERT INTO `product_detail` VALUES (20, 2, 7, 'S3', 'breakdown7-3', 'barcode7-3', 'jan7-3', 300, 2, 500, 1, 350, 1, '2019-07-01 05:43:05', '2019-07-01 05:43:05');
INSERT INTO `product_detail` VALUES (21, 3, 8, 'S2', 'breakdown2', 'jp24324', '555', 10, 1, 3000, 1, 2500, 1, '2019-07-01 05:42:34', '2019-07-01 05:42:34');
INSERT INTO `product_detail` VALUES (22, 2, 9, 'S1', 'breakdown8-1', 'barcode8-1', 'jan8-1', 500, 3, 330, 1, 300, 1, '2019-07-01 05:46:04', '2019-07-01 05:46:04');
INSERT INTO `product_detail` VALUES (23, 2, 9, 'S2', 'breakdown8-2', 'barcode8-2', 'jan8-2', 200, 2, 350, 1, 250, 1, '2019-07-01 05:46:27', '2019-07-01 05:46:27');
INSERT INTO `product_detail` VALUES (24, 2, 9, 'S3', 'breakdown8-3', 'barcode8-3', 'jan8-3', 400, 2, 500, 1, 400, 1, '2019-07-01 05:47:53', '2019-07-01 05:47:53');
INSERT INTO `product_detail` VALUES (25, 2, 10, 'S1', 'breakdown9-1', 'barcode9-1', 'jan9-1', 200, 5, 400, 1, 380, 1, '2019-07-01 05:51:11', '2019-07-01 05:51:11');
INSERT INTO `product_detail` VALUES (26, 2, 10, 'S2', 'breakdown9-2', 'barcode9-2', 'jan9-2', 400, 4, 0, 1, 300, 1, '2019-07-01 05:51:49', '2019-07-01 05:51:49');
INSERT INTO `product_detail` VALUES (27, 3, 11, 'S1', 'breakdown1', 'ca123231', '5000', 5, 1, 3000, 1, 2500, 1, '2019-07-01 05:50:25', '2019-07-01 05:50:25');
INSERT INTO `product_detail` VALUES (28, 2, 10, 'S3', 'breakdown9-3', 'barcode9-3', 'jan9-3', 220, 1, 280, 1, 250, 1, '2019-07-01 05:52:28', '2019-07-01 05:52:28');
INSERT INTO `product_detail` VALUES (29, 3, 11, 'S2', 'breakdown2', 'barcode423', '234', 20, 1, 2500, 1, 2000, 1, '2019-07-01 05:51:01', '2019-07-01 05:51:01');
INSERT INTO `product_detail` VALUES (30, 2, 12, 'S1', 'breakdown10-1', 'barcode10-1', 'jan10-1', 500, 2, 350, 1, 300, 1, '2019-07-01 05:55:58', '2019-07-01 05:55:58');
INSERT INTO `product_detail` VALUES (31, 2, 12, 'S2', 'breakdown10-2', 'barcode10-2', 'jan10-2', 200, 2, 200, 1, 180, 1, '2019-07-01 05:56:25', '2019-07-01 05:56:25');
INSERT INTO `product_detail` VALUES (32, 2, 12, 'S3', 'breakdown10-3', 'barcode10-3', 'jan10-3', 190, 2, 310, 1, 280, 1, '2019-07-01 05:56:48', '2019-07-01 05:56:48');

-- ----------------------------
-- Table structure for product_image
-- ----------------------------
DROP TABLE IF EXISTS `product_image`;
CREATE TABLE `product_image`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NULL DEFAULT NULL,
  `image` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of product_image
-- ----------------------------
INSERT INTO `product_image` VALUES (1, 1, 'product1-1.jpg', '2019-07-01 03:11:39', '2019-07-01 03:11:39');
INSERT INTO `product_image` VALUES (2, 1, 'product1-2.jpg', '2019-07-01 03:11:42', '2019-07-01 03:11:42');
INSERT INTO `product_image` VALUES (3, 1, 'product1-3.jpg', '2019-07-01 03:11:46', '2019-07-01 03:11:46');
INSERT INTO `product_image` VALUES (4, 2, 'product2-1.jpg', '2019-07-01 03:31:44', '2019-07-01 03:31:44');
INSERT INTO `product_image` VALUES (5, 2, 'product2-2.jpg', '2019-07-01 03:31:47', '2019-07-01 03:31:47');
INSERT INTO `product_image` VALUES (6, 2, 'product2-3.jpg', '2019-07-01 03:31:49', '2019-07-01 03:31:49');
INSERT INTO `product_image` VALUES (7, 3, 'product3-1.jpg', '2019-07-01 03:34:52', '2019-07-01 03:34:52');
INSERT INTO `product_image` VALUES (8, 3, 'product3-2.jpg', '2019-07-01 03:34:54', '2019-07-01 03:34:54');
INSERT INTO `product_image` VALUES (9, 3, 'product3-3.jpg', '2019-07-01 03:34:57', '2019-07-01 03:34:57');
INSERT INTO `product_image` VALUES (10, 4, 'product4-1.jpg', '2019-07-01 03:39:58', '2019-07-01 03:39:58');
INSERT INTO `product_image` VALUES (11, 4, 'product4-2.jpg', '2019-07-01 03:40:00', '2019-07-01 03:40:00');
INSERT INTO `product_image` VALUES (12, 4, 'product4-3.jpg', '2019-07-01 03:40:02', '2019-07-01 03:40:02');
INSERT INTO `product_image` VALUES (13, 5, 'product5-1.jpg', '2019-07-01 05:32:03', '2019-07-01 05:32:03');
INSERT INTO `product_image` VALUES (14, 5, 'product5-2.jpg', '2019-07-01 05:32:06', '2019-07-01 05:32:06');
INSERT INTO `product_image` VALUES (15, 5, 'product5-3.jpg', '2019-07-01 05:32:09', '2019-07-01 05:32:09');
INSERT INTO `product_image` VALUES (16, 6, 'product6-1.jpg', '2019-07-01 05:37:54', '2019-07-01 05:37:54');
INSERT INTO `product_image` VALUES (17, 6, 'product6-2.jpg', '2019-07-01 05:37:57', '2019-07-01 05:37:57');
INSERT INTO `product_image` VALUES (18, 6, 'product6-3.jpg', '2019-07-01 05:38:00', '2019-07-01 05:38:00');
INSERT INTO `product_image` VALUES (19, 7, 'product7-1.jpg', '2019-07-01 05:40:48', '2019-07-01 05:40:48');
INSERT INTO `product_image` VALUES (20, 7, 'product7-2.jpg', '2019-07-01 05:40:50', '2019-07-01 05:40:50');
INSERT INTO `product_image` VALUES (21, 7, 'product7-3.jpg', '2019-07-01 05:40:52', '2019-07-01 05:40:52');
INSERT INTO `product_image` VALUES (22, 8, 'tech_tablet.jpg', '2019-07-01 05:44:10', '2019-07-01 05:44:10');
INSERT INTO `product_image` VALUES (23, 8, 'phone_buildings.jpg', '2019-07-01 05:44:15', '2019-07-01 05:44:15');
INSERT INTO `product_image` VALUES (24, 9, 'product8-1.jpg', '2019-07-01 05:48:09', '2019-07-01 05:48:09');
INSERT INTO `product_image` VALUES (25, 9, 'product8-2.jpg', '2019-07-01 05:48:14', '2019-07-01 05:48:14');
INSERT INTO `product_image` VALUES (26, 9, 'product8-3.jpg', '2019-07-01 05:48:19', '2019-07-01 05:48:19');
INSERT INTO `product_image` VALUES (27, 10, 'product9-1.jpg', '2019-07-01 05:50:32', '2019-07-01 05:50:32');
INSERT INTO `product_image` VALUES (28, 10, 'product9-2.jpg', '2019-07-01 05:50:35', '2019-07-01 05:50:35');
INSERT INTO `product_image` VALUES (29, 10, 'product9-3.jpg', '2019-07-01 05:50:38', '2019-07-01 05:50:38');
INSERT INTO `product_image` VALUES (30, 10, 'product9-4.jpg', '2019-07-01 05:50:42', '2019-07-01 05:50:42');
INSERT INTO `product_image` VALUES (31, 11, 'tech_mic.jpg', '2019-07-01 05:49:54', '2019-07-01 05:49:54');
INSERT INTO `product_image` VALUES (32, 12, 'product10-1.jpg', '2019-07-01 05:55:30', '2019-07-01 05:55:30');
INSERT INTO `product_image` VALUES (33, 12, 'product10-2.jpg', '2019-07-01 05:55:33', '2019-07-01 05:55:33');
INSERT INTO `product_image` VALUES (34, 12, 'product10-3.jpg', '2019-07-01 05:55:36', '2019-07-01 05:55:36');

-- ----------------------------
-- Table structure for product_keywords
-- ----------------------------
DROP TABLE IF EXISTS `product_keywords`;
CREATE TABLE `product_keywords`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 82 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of product_keywords
-- ----------------------------
INSERT INTO `product_keywords` VALUES (12, 3, 'keyword-1', '2019-07-01 03:34:43', '2019-07-01 03:34:43');
INSERT INTO `product_keywords` VALUES (13, 3, 'keyword-3', '2019-07-01 03:34:43', '2019-07-01 03:34:43');
INSERT INTO `product_keywords` VALUES (14, 3, 'keyword-5', '2019-07-01 03:34:43', '2019-07-01 03:34:43');
INSERT INTO `product_keywords` VALUES (15, 4, 'keyword-2', '2019-07-01 03:39:19', '2019-07-01 03:39:19');
INSERT INTO `product_keywords` VALUES (16, 4, 'keyword-5', '2019-07-01 03:39:19', '2019-07-01 03:39:19');
INSERT INTO `product_keywords` VALUES (31, 6, 'keyword-2', '2019-07-01 05:35:30', '2019-07-01 05:35:30');
INSERT INTO `product_keywords` VALUES (32, 6, 'keyword-5', '2019-07-01 05:35:30', '2019-07-01 05:35:30');
INSERT INTO `product_keywords` VALUES (33, 7, 'keyword-2', '2019-07-01 05:40:37', '2019-07-01 05:40:37');
INSERT INTO `product_keywords` VALUES (34, 7, 'keyword-3', '2019-07-01 05:40:37', '2019-07-01 05:40:37');
INSERT INTO `product_keywords` VALUES (40, 9, 'keyword-3', '2019-07-01 05:45:39', '2019-07-01 05:45:39');
INSERT INTO `product_keywords` VALUES (41, 9, 'keyword-5', '2019-07-01 05:45:39', '2019-07-01 05:45:39');
INSERT INTO `product_keywords` VALUES (42, 8, 'keyword-1', '2019-07-01 05:45:12', '2019-07-01 05:45:12');
INSERT INTO `product_keywords` VALUES (43, 8, 'keyword-3', '2019-07-01 05:45:12', '2019-07-01 05:45:12');
INSERT INTO `product_keywords` VALUES (44, 8, 'keyword-5', '2019-07-01 05:45:12', '2019-07-01 05:45:12');
INSERT INTO `product_keywords` VALUES (45, 10, 'keyword-2', '2019-07-01 05:50:16', '2019-07-01 05:50:16');
INSERT INTO `product_keywords` VALUES (46, 10, 'keyword-4', '2019-07-01 05:50:16', '2019-07-01 05:50:16');
INSERT INTO `product_keywords` VALUES (47, 10, 'keyword-5', '2019-07-01 05:50:16', '2019-07-01 05:50:16');
INSERT INTO `product_keywords` VALUES (51, 11, 'apple-keyword', '2019-07-01 05:51:19', '2019-07-01 05:51:19');
INSERT INTO `product_keywords` VALUES (52, 11, 'keyword-1', '2019-07-01 05:51:19', '2019-07-01 05:51:19');
INSERT INTO `product_keywords` VALUES (53, 11, 'keyword-3', '2019-07-01 05:51:20', '2019-07-01 05:51:20');
INSERT INTO `product_keywords` VALUES (54, 12, 'apple-keyword', '2019-07-01 05:55:19', '2019-07-01 05:55:19');
INSERT INTO `product_keywords` VALUES (55, 12, 'keyword-3', '2019-07-01 05:55:19', '2019-07-01 05:55:19');
INSERT INTO `product_keywords` VALUES (56, 12, 'keyword-5', '2019-07-01 05:55:19', '2019-07-01 05:55:19');
INSERT INTO `product_keywords` VALUES (64, 5, 'apple-keyword', '2019-07-15 03:51:40', '2019-07-15 03:51:40');
INSERT INTO `product_keywords` VALUES (65, 5, 'keyword-2', '2019-07-15 03:51:40', '2019-07-15 03:51:40');
INSERT INTO `product_keywords` VALUES (66, 5, 'keyword-4', '2019-07-15 03:51:41', '2019-07-15 03:51:41');
INSERT INTO `product_keywords` VALUES (67, 5, 'new-keyword-2', '2019-07-15 03:51:41', '2019-07-15 03:51:41');
INSERT INTO `product_keywords` VALUES (72, 1, 'keyword-1', '2019-07-24 03:28:54', '2019-07-24 03:28:54');
INSERT INTO `product_keywords` VALUES (73, 1, 'keyword-2', '2019-07-24 03:28:54', '2019-07-24 03:28:54');
INSERT INTO `product_keywords` VALUES (74, 1, 'keyword-3', '2019-07-24 03:28:54', '2019-07-24 03:28:54');
INSERT INTO `product_keywords` VALUES (75, 1, 'new-keyword-1', '2019-07-24 03:28:54', '2019-07-24 03:28:54');
INSERT INTO `product_keywords` VALUES (76, 2, 'keyword-1', '2019-07-24 03:29:14', '2019-07-24 03:29:14');
INSERT INTO `product_keywords` VALUES (77, 2, 'keyword-4', '2019-07-24 03:29:14', '2019-07-24 03:29:14');
INSERT INTO `product_keywords` VALUES (78, 2, 'keyword-5', '2019-07-24 03:29:14', '2019-07-24 03:29:14');
INSERT INTO `product_keywords` VALUES (79, 13, 'keyword-1', '2019-10-13 08:10:29', '2019-10-13 08:10:29');
INSERT INTO `product_keywords` VALUES (80, 13, 'FEw', '2019-10-13 08:10:29', '2019-10-13 08:10:29');
INSERT INTO `product_keywords` VALUES (81, 14, 'keyword-2', '2019-10-13 09:31:18', '2019-10-13 09:31:18');

-- ----------------------------
-- Table structure for product_review
-- ----------------------------
DROP TABLE IF EXISTS `product_review`;
CREATE TABLE `product_review`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `type` tinyint(4) NOT NULL DEFAULT 1,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `product_id` int(11) NOT NULL DEFAULT 0,
  `rating` double NOT NULL DEFAULT 0,
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `telephone` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `address` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `permission` tinyint(4) NOT NULL DEFAULT 0,
  `role` tinyint(4) NOT NULL DEFAULT 3,
  `is_new` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@sd.com', 'admin\'s telephone', 'admin\'s address', 1, 1, 0, '2019-06-29 16:02:03', '2019-07-02 02:30:35');
INSERT INTO `user` VALUES (2, 'wholesaler1', 'f4345c8980a262c8df7b1b8a7b7fdc8d', 'wholesaler1@sd.com', 'wholesaler1\'s telephone', 'wholesaler1\'s address', 1, 2, 0, '2019-06-30 00:08:40', '2019-10-13 10:12:23');
INSERT INTO `user` VALUES (3, 'wholesaler2', 'f2dc6e81b5db7ea1a3fb1f9668a71bf5', 'wholesaler2@sd.com', 'wholesaler2\'s telephone', 'wholesaler2\'s address', 1, 2, 0, '2019-06-30 00:09:07', '2019-06-30 05:11:35');
INSERT INTO `user` VALUES (4, 'buyer1', '5cbd9d629096842872fdc665d2d03ba3', 'buyer1@sd.com', 'buyer1 telephone', 'buyer1 address', 1, 3, 0, '2019-06-30 00:12:13', '2019-07-25 04:18:38');
INSERT INTO `user` VALUES (5, 'buyer2', 'ba71d29d4efdd8753c516db594fab6d8', 'buyer2@sd.com', 'buyer2\'s telephone', 'buyer2\'s address', 1, 3, 0, '2019-06-30 00:12:40', '2019-07-25 04:09:23');
INSERT INTO `user` VALUES (6, 'wholesaler3', 'ae34d972f420d931f9445634a886a2f7', 'wholesaler3@sd.com', 'wholesaler3\'s telephone', 'wholesaler3\'s address', 0, 2, 0, '2019-06-30 00:15:00', '2019-06-30 05:10:07');
INSERT INTO `user` VALUES (7, 'wholesaler4', '77aaa1a8926a9561dc6401ce4217da3c', 'wholesaler4@sd.com', 'wholesaler4\'s telephone', 'wholesaler4\'s address', 0, 2, 0, '2019-06-30 00:15:27', '2019-06-30 04:56:21');
INSERT INTO `user` VALUES (8, 'wholesaler5', '68809823ad78516556695dd3b2455287', 'wholesaler5@sd.com', 'wholesaler5\'s telephone', 'wholesaler5\'s address', 0, 2, 0, '2019-06-30 00:16:49', '2019-06-30 00:16:49');
INSERT INTO `user` VALUES (9, 'wholesaler6', 'f52e735e437a89549307e374108bce5a', 'wholesaler6@sd.com', 'wholesaler6\'s telephone', 'wholesaler6\'s address', 0, 2, 0, '2019-06-30 00:17:15', '2019-10-13 00:02:20');
INSERT INTO `user` VALUES (10, 'wholesaler7', 'cc6acb965b35f104818c06a6a652198d', 'wholesaler7@sd.com', 'wholesaler7\'s telephone', 'wholesaler7\'s address', 0, 2, 0, '2019-06-30 00:17:41', '2019-06-30 05:00:40');
INSERT INTO `user` VALUES (11, 'wholesaler8', '407676826a62d1ecf98496d370fe25f6', 'wholesaler8@sd.com', 'wholesaler8\'s telephone', 'wholesaler8\'s address', 0, 2, 0, '2019-06-30 00:18:02', '2019-06-30 00:18:02');
INSERT INTO `user` VALUES (12, 'buyer3', '3cb52c98f366dad959eb21181107c7a7', 'buyer3@sd.com', 'buyer3\'s telephone', 'buyer3\'s address', 1, 3, 0, '2019-06-29 08:22:59', '2019-07-22 07:47:14');
INSERT INTO `user` VALUES (13, 'buyer4', '1757397eb4f922f8c89b65f08333a96f', 'buyer4@sd.com', 'buyer4\'s telephone', 'buyer4\'s address', 1, 3, 0, '2019-06-29 08:23:01', '2019-07-24 04:12:02');
INSERT INTO `user` VALUES (14, 'buyer5', 'adeeaf80af67860d2a1e966d61e61841', 'buyer5@sd.com', 'buyer5\'s telephone', 'buyer5\'s address', 1, 3, 0, '2019-06-29 08:23:03', '2019-07-25 03:33:21');
INSERT INTO `user` VALUES (15, 'buyer6', 'ea8391c61d816d90fa9743172b7ce7f1', 'buyer6@sd.com', 'buyer6\'s telephone', 'buyer6\'s address', 0, 3, 0, '2019-06-29 08:23:05', '2019-06-29 08:23:05');
INSERT INTO `user` VALUES (16, 'buyer7', '10d7735c45d2e43108f06e820cce9b6a', 'buyer7@sd.com', 'buyer7\'s telephone', 'buyer7\'s address', 1, 3, 0, '2019-06-29 08:23:08', '2019-07-22 07:49:33');
INSERT INTO `user` VALUES (17, 'buyer8', 'a54308e7d203092637a173dec6bd9165', 'buyer8@sd.com', 'buyer8\'s telephone', 'buyer8\'s address', 0, 3, 0, '2019-06-29 08:23:09', '2019-06-29 08:23:09');
INSERT INTO `user` VALUES (18, 'buyer9', '00006fe3aab674ccb0a6be7b41bc1616', 'buyer9@sd.com', 'buyer9\'s telephone', 'buyer9\'s address', 0, 3, 0, '2019-06-29 08:23:12', '2019-06-29 08:23:12');
INSERT INTO `user` VALUES (19, 'buyer10', 'f0213dd656b6ecd5b235c700f9feebae', 'buyer10@sd.com', 'buyer10\'s telephone', 'buyer10\'s address', 0, 3, 0, '2019-06-29 08:23:14', '2019-06-29 08:23:14');
INSERT INTO `user` VALUES (21, 'wholesaler8', '407676826a62d1ecf98496d370fe25f6', 'wholesaler8@sd.com', 'wholesaler8\'s telephone', 'wholesaler8\'s address', 1, 2, 0, '2019-06-30 05:17:05', '2019-06-30 05:17:05');
INSERT INTO `user` VALUES (22, 'wholesaler9', 'a1a6c6742de9d08d176bc601c3e0a569', 'wholesaler9@sd.com', 'wholesaler9\'s telephone', 'wholesaler9\'s address', 1, 2, 0, '2019-06-30 05:17:46', '2019-06-30 05:17:46');
INSERT INTO `user` VALUES (23, 'wholesaler10', '3165ff7f042108286782996f1085544e', 'wholesaler10@sd.com', 'wholesaler10\'s telephone', 'wholesaler10\'s address', 1, 2, 0, '2019-06-30 05:18:28', '2019-06-30 05:18:28');
INSERT INTO `user` VALUES (24, 'buyer11', '853992521f291a8acad6ad6c7e4d9f17', 'buyer11@sd.com', 'buyer11\'s telephone', 'buyer11\'s address', 1, 3, 0, '2019-06-30 05:18:58', '2019-06-30 05:18:58');
INSERT INTO `user` VALUES (25, 'buyer12', '60c86ecba2e4c9258df071a380d4f33d', 'buyer12@sd.com', 'buyer12\'s telephone', 'buyer12\'s address', 0, 3, 1, '2019-06-30 05:19:19', '2019-06-30 05:19:19');
INSERT INTO `user` VALUES (26, 'buyer13', 'c81d2ef5ef9ff0fd83456093274ff73a', 'buyer13@sd.com', 'buyer13\'s telephone', 'buyer13\'s address', 0, 3, 1, '2019-06-30 05:22:24', '2019-06-30 05:22:24');
INSERT INTO `user` VALUES (27, 'buyer14', '8efb6969faf220d62a6beb5413818d92', 'buyer14@sd.com', 'buyer14\'s telephone', 'buyer14\'s address', 0, 3, 1, '2019-06-30 07:35:08', '2019-06-30 07:35:08');
INSERT INTO `user` VALUES (29, 'buyer16', '5c464136e4ae38c0cd1bed82e09b8d1f', 'buyer16@sd.com', 'buyer16\'s telephone', 'buyer16\'s address', 0, 3, 1, '2019-06-30 07:40:43', '2019-06-30 07:40:43');
INSERT INTO `user` VALUES (30, 'buyer17', 'b970fee49430465bf7a16b8199515ed5', 'buyer17@sd.com', 'buyer17\'s telephone', 'buyer17\'s address', 0, 3, 1, '2019-06-30 07:46:03', '2019-06-30 07:46:03');
INSERT INTO `user` VALUES (31, 'wholesaler11', '9cf1b6b275578c8a6d8bfd8fe955e14a', 'wholesaler11@sd.com', 'wholesaler11\'s telephone', 'wholesaler11\'s address', 0, 2, 1, '2019-06-30 07:47:48', '2019-06-30 07:47:48');
INSERT INTO `user` VALUES (32, 'wholesaler12', '0e1d9e349c147d900362e2a9a4d5f7d1', 'wholesaler12@sd.com', 'wholesaler12\'s telephone', 'wholesaler12\'s address', 0, 2, 1, '2019-06-30 07:48:22', '2019-06-30 07:48:22');
INSERT INTO `user` VALUES (33, 'test', '098f6bcd4621d373cade4e832627b4f6', 'test1@test.com', 'test_telephone', 'test_address', 1, 3, 0, '2019-10-13 09:52:24', '2019-10-13 13:10:59');

-- ----------------------------
-- Table structure for wishlist
-- ----------------------------
DROP TABLE IF EXISTS `wishlist`;
CREATE TABLE `wishlist`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `product_id` int(11) NULL DEFAULT NULL,
  `owner_id` int(11) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wishlist
-- ----------------------------
INSERT INTO `wishlist` VALUES (5, 16, 4, 2, '2019-07-01 03:11:41', '2019-07-01 03:11:41');
INSERT INTO `wishlist` VALUES (10, 13, 8, 3, '2019-07-01 05:52:53', '2019-07-01 05:52:53');
INSERT INTO `wishlist` VALUES (12, 13, 1, 2, '2019-07-01 05:55:17', '2019-07-01 05:55:17');
INSERT INTO `wishlist` VALUES (13, 13, 3, 2, '2019-07-01 05:55:18', '2019-07-01 05:55:18');
INSERT INTO `wishlist` VALUES (15, 13, 11, 3, '2019-07-01 05:57:03', '2019-07-01 05:57:03');
INSERT INTO `wishlist` VALUES (16, 13, 9, 2, '2019-07-01 05:59:50', '2019-07-01 05:59:50');
INSERT INTO `wishlist` VALUES (17, 5, 1, 2, '2019-07-12 04:29:20', '2019-07-12 04:29:20');
INSERT INTO `wishlist` VALUES (18, 5, 2, 2, '2019-07-12 04:29:21', '2019-07-12 04:29:21');
INSERT INTO `wishlist` VALUES (20, 4, 11, 3, '2019-07-12 06:05:01', '2019-07-12 06:05:01');
INSERT INTO `wishlist` VALUES (21, 4, 4, 2, '2019-07-12 06:05:22', '2019-07-12 06:05:22');
INSERT INTO `wishlist` VALUES (23, 4, 1, 2, '2019-07-12 08:18:45', '2019-07-12 08:18:45');
INSERT INTO `wishlist` VALUES (25, 4, 9, 2, '2019-07-12 09:05:36', '2019-07-12 09:05:36');
INSERT INTO `wishlist` VALUES (26, 4, 8, 3, '2019-07-15 03:28:00', '2019-07-15 03:28:00');
INSERT INTO `wishlist` VALUES (32, 4, 2, 2, '2019-07-22 03:43:50', '2019-07-22 03:43:50');
INSERT INTO `wishlist` VALUES (33, 12, 2, 2, '2019-07-22 04:26:00', '2019-07-22 04:26:00');
INSERT INTO `wishlist` VALUES (34, 12, 1, 2, '2019-07-22 04:26:03', '2019-07-22 04:26:03');
INSERT INTO `wishlist` VALUES (35, 2, 10, 2, '2019-10-13 10:14:51', '2019-10-13 10:14:51');

SET FOREIGN_KEY_CHECKS = 1;
