zTreeSelector = {auto_increment: 0};
zTreeSelector.init = function(valueObj, textObj, setting, zNodes, selector_setting) {
	
	textObj.attr("autocomplete", "off");
	var ztree_id = 'ztree-selector-'+zTreeSelector.auto_increment;
	if(selector_setting == null) {
		selector_setting = {width: 280, height: 350, callback: null, idSplit: ",", textSplit: ","};
	}
	
	var width = selector_setting.width;
	var height = selector_setting.height;
	var callback = selector_setting.callback;
	var idField = selector_setting.id;
	var textField = selector_setting.text;
	var idSplit = selector_setting.idSplit != undefined ? selector_setting.idSplit : ",";
	var textSplit = selector_setting.idSplit != undefined ? selector_setting.textSplit : ",";
	
	if(width == null)
		width=280;
	if(height == null)
		height=350;
	
	if(textField == null) {
		if(setting.data.key != null) {
			textField = setting.data.key.name;
		} 
		if(textField == null) {
			textField = 'name';
		}
	}
	
	if(idField == null) {
		if(setting.data.simpleData != null) {
			idField = setting.data.simpleData.idKey;
		}
		if(idField == null) {
			idField = 'id';
		}
	}
	
	var onSelectNode = function() {
		var zTree = $.fn.zTree.getZTreeObj(ztree_id),
		nodes = zTree.getSelectedNodes(true),
		valText = '', valId = '', node;
		if(nodes.length>0) {
			node = nodes[0];
			valId = node[idField];
			valText = node[textField];
			valueObj.val(valId);
			textObj.val(valText);
			if(callback != null) {
				callback(valId, valText, node);
			}
			containerObj.fadeOut('fast');
			textObj[0].focus();
		}
	};
	
	var beforeClickNode = function (treeId, treeNode) {
		var zTree = $.fn.zTree.getZTreeObj(ztree_id);
		zTree.checkNode(treeNode, !treeNode.checked, true, true);
		return false;
	};
	
	var onCheckNode = function(e, treeId, treeNode) {
		var zTree = $.fn.zTree.getZTreeObj(ztree_id),
		nodes = zTree.getCheckedNodes(true),
		valText = '', valId = '';
		for (var i=0, l=nodes.length; i<l; i++) {
			valId += nodes[i][idField] + idSplit;
			valText += nodes[i][textField] + textSplit;
		}
		if(callback != null)
		if (valText.length> 0 ) valText = valText.substring(0, valText.length-idSplit.length);
		if (valId.length> 0 ) valId = valId.substring(0, valId.length-idSplit.length);
		valueObj.val(valId);
		textObj.val(valText);
		if(callback != null) {
			callback(valId, valText, nodes);
		}
		textObj[0].focus();
	};
	
	var blankmenu_id = 'ztree-selector-blankmenu-'+zTreeSelector.auto_increment;
	var container_id = "ztree-selector-container-"+zTreeSelector.auto_increment;
	$('body').append('<div id="'+container_id+'" style="z-index: 10000; padding: 5px; border: #cfcfcf 1px solid; background: #fff; display:none; position: absolute;"><div style="padding-bottom: 5px;"><button type="button" id="'+blankmenu_id+'" class="btn" style="vertical-align:top;padding:2px;line-height:1;float:right;margin-bottom:5px"><span style="font-size:12px">×</span></div><ul id="'+ztree_id+'" class="ztree" style="margin-top:0; width:'+width+'px; height: '+height+'px;"></ul></div>');
	
	var containerObj = $('#'+container_id);
	var blankmenuObj = $('#'+blankmenu_id);
	
	var onBodyDown = function(event) {
		if (!(event.target.id == container_id || event.target.id == textObj.attr('id') || $(event.target).parents("#"+container_id).length>0)) {
			containerObj.fadeOut("fast");
			$("body").unbind("mousedown", onBodyDown);
			textObj[0].focus();
		}
	};
	
	if(setting.check != null || setting.check != undefined) {
		setting.callback = {
				onCheck: onCheckNode,
				beforeClick: beforeClickNode
		};
		blankmenuObj.parent().remove();
		var onFocus = function() {
			var textOffset = textObj.offset();
			var bodyHeight = $('body').outerHeight(true);
			var scrollTop = textObj.scrollTop();
			if(textOffset.top-scrollTop<=bodyHeight-textOffset.top-scrollTop) {
				containerObj.css({left:textOffset.left + "px", top:textOffset.top + textObj.outerHeight(true) + "px"}).slideDown("fast");
			} else {
				containerObj.css({left:textOffset.left + "px", top:textOffset.top - containerObj.outerHeight(true) + "px"}).fadeIn("fast");
			}
			var value = valueObj.val();
			if(value != '') {
				values = value.split(idSplit);
				var zTree = $.fn.zTree.getZTreeObj(ztree_id);
				zTree.checkAllNodes(false);
				for(var i=0, ni=values.length; i<ni; i++) {
					nodes = zTree.getNodesByParam(idField, values[i], null);
					if(nodes.length>0) {
						var node = nodes[0];
						zTree.checkNode(node, true, false, false);
					}
				}
			}
			textObj[0].select();
			$("body").bind("mousedown", onBodyDown);
		};
		
		$.fn.zTree.init($('#'+ztree_id), setting, zNodes);
		
		textObj.bind('click', onFocus);
		valueObj[0].realValue = function(value) {
			if(value == undefined) {
				return valueObj.val();
			} else {
				var value = valueObj.val();
				if(value != '') {
					var values = value.split(idSplit);
					var text ='';
					var zTree = $.fn.zTree.getZTreeObj(ztree_id);
					zTree.checkAllNodes(false);
					for(var i=0, ni=values.length; i<ni; i++) {
						nodes = zTree.getNodesByParam(idField, values[i], null);
						if(nodes.length>0) {
							var node = nodes[0];
							text += node[textField] + textSplit;
							zTree.checkNode(node, true, false, false);
						}
					}
					if (text.length> 0 ) text = text.substring(0, text.length-idSplit.length);
					if(text != '') {
						textObj.val(text);
					}
				}
			}
		};
	} else {
		setting.callback = {
				onClick: onSelectNode
		};
		
		textObj.bind('keydown', function(event) {
			if(event.keyCode == 13) {
				if(containerObj.css('display') != 'none') {
					onSelectNode();
				}
				return false;
			} else if(event.keyCode == 9) {
				
			} else if(event.keyCode == 27) {
				blankmenuObj.trigger('click');
				return false;
			} else if(event.keyCode == 40 || event.keyCode == 38) {
				if(containerObj.css('display') == 'none') {
					onFocus();
				}
				var zTree = $.fn.zTree.getZTreeObj(ztree_id);
				var selectedNodes = zTree.getSelectedNodes(), selectedNode = null, index = 0;
				var textSearch = textObj.val();
				var nodes = zTree.getNodesByFilter(function(node) {return node[textField].indexOf(textSearch) >=0;}, false, null);
				if(selectedNodes.length>0) {
					selectedNode = selectedNodes[0];
				}
				if(selectedNode != null) {
					for(var i=0, ni=nodes.length; i<ni; i++) {
						if(nodes[i] == selectedNode) {
							if(event.keyCode == 40) {
								index = i+1;
							} else if(event.keyCode == 38) {
								index = i-1;
							} else {
								index = 0;
							}
							break;
						}
					}
					if(index<0) {
						index = nodes.length-1;
					}
					if(index>=nodes.length) {
						index = 0;
					}
				}
				
				if(nodes.length>0) {
					node = nodes[index];
					zTree.selectNode(node);
					zTree.expandNode(node, true, false);
				}
				textObj[0].focus();
			}
		});
		
		blankmenuObj.bind('click', function() {
			valueObj[0].realValue('');
			containerObj.fadeOut("fast");
			$("body").unbind("mousedown", onBodyDown);
			textObj.val("");
			textObj.focus();
			if(callback != null) {
				callback();
			}
		});
		
		var onFocus = function() {
			var textOffset = textObj.offset();
			var bodyHeight = $('body').outerHeight(true);
			var scrollTop = textObj.scrollTop();
			if(textOffset.top-scrollTop<=bodyHeight-textOffset.top-scrollTop) {
				containerObj.css({'z-index': 10000, left:textOffset.left + "px", top:textOffset.top + textObj.outerHeight(true) + "px"}).slideDown("fast");
			} else {
				containerObj.css({left:textOffset.left + "px", top:textOffset.top - containerObj.outerHeight(true) + "px"}).fadeIn("fast");
			}
			
			var value = valueObj.val();
			if(value != '') {
				var zTree = $.fn.zTree.getZTreeObj(ztree_id),
				nodes = zTree.getNodesByParam(idField, value, null);
				if(nodes.length>0) {
					var node = nodes[0];
					zTree.selectNode(node);
				}
			}
			textObj[0].select();
			
			$("body").bind("mousedown", onBodyDown);
		};
		
		$.fn.zTree.init($('#'+ztree_id), setting, zNodes);
		
		textObj.bind('click', onFocus);
		
		valueObj[0].realValue = function(value) {
			if(value == undefined) {
				return valueObj.val();
			} else {
				valueObj.val(value);
				var zTree = $.fn.zTree.getZTreeObj(ztree_id),
				nodes = zTree.getNodesByParam(idField, value, null);
				if(nodes.length>0) {
					var node = nodes[0];
					textObj.val(node[textField]);
				} else {
					if(valueObj == '')
						textObj.val('');
				}
			}
		};
	}
	valueObj[0].realValue(valueObj.val());
	zTreeSelector.auto_increment++;
};
