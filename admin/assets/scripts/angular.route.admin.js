var app = angular.module("SuperDeliveryApp", ["ngRoute"]);

app.config(["$routeProvider", "$locationProvider", function($routeProvider, $locationProvider) {
	$locationProvider.hashPrefix("");

	$routeProvider
	.when("/", {
		templateUrl: "admin/product/index"
	})
	.when("/category", {
		templateUrl: "admin/category/index"
	})
	.when("/keyword", {
		templateUrl: "admin/keyword/index"
	})
	.when("/brand", {
		templateUrl: "admin/brand/index"
	})
	.when("/user", {
		templateUrl: "admin/user/index"
	})
	.otherwise({redirectTo: "/"});
}]);

/* menu active controller. */
app.controller("SidebarController", function($scope, $location) {
	$scope.activeStatus = function(path) {
		if (path == $location.path())
			return true;
		else
			return false;
	}
});