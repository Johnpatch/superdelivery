var app = angular.module("SuperDeliveryApp", ["ngRoute"]);

app.config(["$routeProvider", "$locationProvider", function($routeProvider, $locationProvider) {
	$locationProvider.hashPrefix("");

	$routeProvider
	.when("/", {
		templateUrl: "app/product/index"
	})
	.when("/category", {
		templateUrl: "app/category/index"
	})
	.when("/purchase", {
		templateUrl: "app/purchase/index"
	})
	.when("/price", {
		templateUrl: "app/price/index"
	})
	.when("/counterpart", {
		templateUrl: "app/counterpart/index"
	})
	.when("/cart", {
		templateUrl: "app/cart/index"
	})
	.when("/cartlog", {
		templateUrl: "app/cartlog/index"
	})
	.otherwise({redirectTo: "/"});
}]);

/* menu active controller */
app.controller("SidebarController", function($scope, $location) {
	$scope.activeStatus = function(path) {
		if (path == $location.path())
			return true;
		else
			return false;
	}
});