var app = angular.module("SuperDeliveryApp", ["ngRoute"]);

app.config(["$routeProvider", "$locationProvider", function($routeProvider, $locationProvider) {
	$locationProvider.hashPrefix("");

	$routeProvider
	.when("/", {
		templateUrl: "auth/login",
		controller: "LoginController"
	})
	.when("/register", {
		templateUrl: "auth/register",
		controller: "RegisterController"
	})
	.otherwise({redirectTo: "/"});
}]);

app.controller("LoginController", function($scope, $http) {
	angular.element(document).find("#userid").focus();

	$scope.login = function() {
		if (angular.isUndefined($scope.userid)) {
			toastr["warning"]("Please input your userid.", "Warning");
			angular.element(document).find("#userid").focus();
			return;
		}
		if (angular.isUndefined($scope.password)) {
			toastr["warning"]("Please input your password.", "Warning");
			angular.element(document).find("#password").focus();
			return;
		}

		var params = "User[userid]=" + $scope.userid + "&User[password]=" + $scope.password;
		$http({
			method: "post",
			url: "auth/login",
			data: params,
			headers: {"Content-type": "application/x-www-form-urlencoded"}
		}).then(function(res) {
			if (res.data.success)
				window.location.reload();
			else
				toastr["warning"](res.data.message, "Warning");
		});
	}
});

app.controller("RegisterController", function($scope, $http, $location) {
	angular.element(document).find("#userid").focus();

	$scope.register = function() {
		if (angular.isUndefined($scope.userid)) {
			toastr["warning"]("Please input your userid.", "Warning");
			angular.element(document).find("#userid").focus();
			return;
		}
		if (angular.isUndefined($scope.username)) {
			toastr["warning"]("Please input your username.", "Warning");
			angular.element(document).find("#username").focus();
			return;
		}
		if (angular.isUndefined($scope.password)) {
			toastr["warning"]("Please input your password.", "Warning");
			angular.element(document).find("#password").focus();
			return;
		}
		if (angular.isUndefined($scope.confirm)) {
			toastr["warning"]("Please input your confirm password.", "Warning");
			angular.element(document).find("#confirm").focus();
			return;
		}
		if (!angular.equals($scope.password, $scope.confirm)) {
			toastr["warning"]("Your password and confirm password do not match.", "Warning");
			angular.element(document).find("#confirm").focus();
			return;
		}
		if (angular.isUndefined($scope.email)) {
			toastr["warning"]("Please input your email address.", "Warning");
			angular.element(document).find("#email").focus();
			return;
		}
		if (angular.isUndefined($scope.role)) {
			toastr["warning"]("Please select your role.", "Warning");
			angular.element(document).find("#role").focus();
			return;
		}
		if (angular.isUndefined($scope.telephone)) {
			toastr["warning"]("Please input your telephone number.", "Warning");
			angular.element(document).find("#telephone").focus();
			return;
		}
		if (angular.isUndefined($scope.address)) {
			toastr["warning"]("Please input your address.", "Warning");
			angular.element(document).find("#address").focus();
			return;
		}

		if (!confirm("Are you sure to create this account ?"))
			return;

		var params = "User[userid]="+$scope.userid+"&User[username]=" + $scope.username + "&User[password]=" + $scope.password + "&User[confirm]=" + $scope.confirm
			+ "&User[email]=" + $scope.email + "&User[address]=" + $scope.address + "&User[telephone]=" + $scope.telephone + "&User[role]=" + $scope.role;
		$http({
			method: "post",
			url: "auth/register",
			data: params,
			headers: {"Content-type": "application/x-www-form-urlencoded"}
		}).then(function(res) {
			if (res.data.success) {
				toastr["success"]("You are registered successfully.", "Create an account");
				$location.path("/");
			} else {
				toastr["error"]("Your operation is failed.", "Create an account");
			}
		});
	}
});