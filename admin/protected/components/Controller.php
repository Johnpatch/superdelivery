<?php
/* base controller for this app. */
class Controller extends CController {

	public $success = "Your operation is completed successfully.";
	public $error = "Your operation is failed.";

	/* get new users count logged in this app. */
	public function actionGet_count_new_user() {
		echo User::model()->count(array(
				"condition"=>"is_new = :is_new",
				"params"=>array(":is_new"=>User::NEW_YES)
		));
	}

	/* get the new requested price to wholesaler. */
	public function actionGet_count_new_price() {
		echo Price::model()->count(array(
				"condition"=>"status = 0 AND user_id = :user_id",
				"params"=>array(":user_id"=>Yii::app()->user->info->id),
				"group"=>"customer_id"
		));
	}
}