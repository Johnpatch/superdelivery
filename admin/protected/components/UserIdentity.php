<?php
class UserIdentity extends CUserIdentity {

	const ERROR_NOT_ALLOWED = 3;
	const ERROR_NOT_AUTHORIZE = 4;

	public function authenticate() {
		$user = User::model()->findByAttributes(array("userid"=>$this->username));
	
		if (!isset($user))
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		else if ($user->password !== $this->password)
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		else if ($user->permission == User::PERMISSION_DISABLED)
			$this->errorCode = self::ERROR_NOT_ALLOWED;
		else if ($user->role == User::ROLE_USER)
			$this->errorCode = self::ERROR_NOT_AUTHORIZE;
		else {
			$this->setState("info", $user);
			$this->errorCode = self::ERROR_NONE;
		}
	
		return !$this->errorCode;
	}
}