<?php
class Utility extends CApplicationComponent {

	/* convert array to list. */
	public function arrayToList($list, $k, $v = null, $list2 = array()) {
		foreach ($list as $t) {
			if (is_array($k)) {
				$key = "";
				foreach ($k as $kk) {
					if ($key != "")
						$key .= "-";
					
					$key .= $t[$kk];
				}
			} else
				$key = $t[$k];
			
			if (isset($v)) {
				if (is_array($v)) {
					$list2[$key] = "";
					foreach ($v as $t2) {
						if ($list2[$key] != "")
							$list2[$key] .= " ";
						
						$list2[$key] .= $t[$t2];
					}
				} else
					$list2[$key] = $t[$v];
			} else
				$list2[$key] = $t;
		}
		
		return $list2;
	}
	
	/* get today. */
	public function getCurrentDate() {
		return date("Y-m-d");	
	}
	
	/* get current datetime. */
	public function getCurrentDateTime() {
		return date("Y-m-d H:i:s");
	}
}