<?php
/* class for ztree plugin. */
class ZTreeRecord extends CActiveRecord {

	const PATH_SEPARATOR = " - ";
	
	public $id;
	public $parent_id;
	public $path;
	public $arrange;
	public $leaf;
	
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}
	
	public function getNodeData() {
		return $this->attributes;
	}
	
	public function getAsyncNodeData() {
		$nodeData = $this->getNodeData();
		if (!$this->leaf)
			$nodeData["isParent"] = true;
		
		return $nodeData;
	}
	
	public function setNodeData($nodeData, $parent, $index) {
		if(isset($nodeData["children"]))
			$nodeData["leaf"] = 0;
		else
			$nodeData["leaf"] = 1;
		
		$this->setAttributes($nodeData, false);
		if ($parent == null) {
			$this->parent_id = 0;
			$this->arrange = sprintf("%04d", $index);
		} else {
			$this->parent_id = $parent->id;
			$this->arrange = sprintf("%s%04d", $parent->arrange, $index);
		}
	}
	
	public static function getListNodeData($list) {
		$arr = array();
		foreach($list as $item)
			array_push($arr, $item->getNodeData());
		
		return $arr;
	}
	
	public static function getListNodeDataEx($list, $attributes) {
		$arr =  array();
		foreach ($list as $item) {
			$itemData = $item->getNodeData();
			
			$arr_item = array();
			foreach($attributes as $key)
				$arr_item[$key] = $itemData[$key];
			
			array_push($arr, $arr_item);
		}
		
		return $arr;
	}
	
	public static function getListData($list) {
		$arr = array();
		foreach ($list as $item) {
			array_push($arr, $item->getData());
		}
		return $arr;
	}
	
	public static function getListDataEx($list, $attributes) {
		$arr = array();
		foreach ($list as $item) {
			$itemData = $item->getData();
			$arr_item = array();
			foreach ($attributes as $key)
				$arr_item[$key] = isset($itemData[$key]) ? $itemData[$key] : "";
			
			array_push($arr, $arr_item);
		}
		
		return $arr;
	}
	
	public static function getListAsyncNodeData($list) {
		$arr = array();
		foreach ($list as $item)
			array_push($arr, $item->getAsyncNodeData());
		
		return $arr;
	}
	
	public static function saveAllNodeDataArray($className, $arrNodeData) {
		$index = 0;
		foreach ($arrNodeData as $nodeData) {
			$index++;
			self::saveAllNodeDataArrayInner($className, $nodeData, null, $index);
		}
	}
	
	private static function saveAllNodeDataArrayInner($className, $nodeData, $parent, $index) {
		$model = null;
		if (isset($nodeData["id"]))
			$model = CActiveRecord::model($className)->findByPk($nodeData["id"]);
		
		if ($model == null)
			$model = new $className;
		
		$model->setNodeData($nodeData, $parent, $index);
		
		$model->save();
		if (isset($nodeData["children"])) {
			$arrChildNodeData = $nodeData["children"];
			$index = 0;
			foreach ($arrChildNodeData as $childNodeData) {
				$index++;
				self::saveAllNodeDataArrayInner($className, $childNodeData, $model, $index);
			}
		}
	}
	
	public static function saveAllRecordArray($className, $arr) {
		foreach($arr as $item) {
			$model = null;
			if (isset($item["id"]))
				$model = CActiveRecord::model($className)->findByPk($item["id"]);
			
			if ($model == null)
				$model = new $className;
			
			$model->setAttributes($item, false);
			$model->save();
		}
	}
	
	public static function deleteAllByPkArray($className, $arrKey) {
		foreach ($arrKey as $key)
			CActiveRecord::model($className)->deleteByPk($key);
	}
}