<?php
	// uncomment the following to define a path alias
	// Yii::setPathOfAlias("local","path/to/local-folder");
	
	// This is the main Web application configuration. Any writable
	// CWebApplication properties can be configured here.
	return array(
		"basePath"=>dirname(__FILE__) . DIRECTORY_SEPARATOR . "..",
		"name"=>"Admin Management",
		"defaultController"=>"auth",
	
		// preloading "log" component
		"preload"=>array("log"),
	
		// autoloading model and component classes
		"import"=>array(
				"application.models.*",
				"application.components.*",
		),
	
		"modules"=>array(
				"user"=>array(
						// enable cookie-based authentication
						"stateKeyPrefix"=>"user",
						"allowAutoLogin"=>false,
				),
		),
	
		// application components
		"components"=>array(
			"util"=>array("class"=>"Utility"),
			"user"=>array(
			// enable cookie-based authentication
				"allowAutoLogin"=>true,
			),
				
		// uncomment the following to enable URLs in path-format
			"urlManager"=>array(
				"urlFormat"=>"path",
				"rules"=>array(
					"<controller:\w+>/<id:\d+>"=>"<controller>/view",
					"<controller:\w+>/<action:\w+>/<id:\d+>"=>"<controller>/<action>",
					"<controller:\w+>/<action:\w+>"=>"<controller>/<action>",
				),
				"showScriptName"=>false
			),
			
			// uncomment the following to use a MySQL database
			"db"=>array(
				"connectionString" => "mysql:host=localhost;dbname=superdelivery;port=3306",
				"emulatePrepare" => true,
				"username" => "root",
				"password" => "",
				"persistent"=> true,
				"charset" => "utf8",
			),
			
			"errorHandler"=>array(
				// use "site/error" action to display errors
				"errorAction"=>"site/error",
			),
		),
			
		// application-level parameters that can be accessed
		// using Yii::app()->params["paramName"]
		"params"=>require_once("params.php")
	);