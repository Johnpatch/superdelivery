<?php
/* controller for user login and register. */
class AuthController extends Controller {

	public function actionIndex() {
		if (Yii::app()->user->isGuest)
			$this->render("//layouts/layout-home");
		else if (Yii::app()->user->info->role == User::ROLE_MANAGER)
			$this->render("//layouts/layout-app");
		else if (Yii::app()->user->info->role == User::ROLE_ADMIN)
			$this->render("//layouts/layout-admin");
	}

	/* login action. */
	public function actionLogin() {
		if (isset($_REQUEST["User"])) {
			$user = new User();
			$user->setScenario("login");
			$user->setAttributes($_REQUEST["User"], false);
			if ($user->login())
				echo json_encode(array("success"=>true));
			else
				echo json_encode(array("success"=>false, "message"=>$user->getError("userid")));
			
			return;
		}

		$this->render("login");
	}
	
	/* register action */
	public function actionRegister() {
		if (isset($_REQUEST["User"])) {
			$user = new User();
			$user->setScenario("register");
			$user->setAttributes($_REQUEST["User"], false);

			$user->created_at = Yii::app()->util->getCurrentDateTime();
			$user->updated_at = Yii::app()->util->getCurrentDateTime();
			$userid = $user->userid;
			$is_exist = User::model()->countByAttributes(array("userid"=>$userid));
			if($is_exist > 0){
				echo json_encode(array("success"=>false, "message"=>"顧客IDはすでに存在しています"));
				return;
			} else{
				if (!User::model()->count()) {
					$user->permission = User::PERMISSION_ENABLED;
					$user->role = User::ROLE_ADMIN;
					$user->is_new = User::NEW_NO;
				}
				if ($user->save(false)) {
					echo json_encode(array("success"=>true));
					return;
				}	
			}
			
		}

		$this->render("register");
	}

	/* logout action */
	public function actionLogout() {
		Yii::app()->user->logout(false);
		$this->redirect($this->createUrl("/"));
	}

	/* change user's password action. */
	public function actionChange_password() {
		$old_password = Yii::app()->request->getParam("old_password");
		$new_password = Yii::app()->request->getParam("new_password");

		if (md5($old_password) !== Yii::app()->user->info->password)
			echo json_encode(array("success"=>false, "message"=>"Your old password is incorrect."));
		else {
			$user = User::model()->findByPk(Yii::app()->user->info->id);
			$user->password = md5($new_password);
			if ($user->save(false))
				echo json_encode(array("success"=>true, "message"=>$this->success));
			else
				echo json_encode(array("success"=>false, "message"=>$this->error));
		}
	}
}