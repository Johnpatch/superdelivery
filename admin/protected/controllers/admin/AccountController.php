<?php
/* class for new users logged in this app. */
class AccountController extends Controller {

	public function actionIndex() {
		$this->render("index");
	}

	/* display user list logged in this app. */
	public function actionList_page() {
		$role = Yii::app()->request->getParam("role");

		$criteria = new CDbCriteria(array(
				"condition"=>"is_new = :is_new",
				"params"=>array(":is_new"=>User::NEW_YES)
		));
		if ($role) {
			$criteria->condition .= " AND role = :role";
			$criteria->params[":role"] = $role;
		}
		$list = User::model()->findAll($criteria);

		$this->renderPartial("list", compact("list"));
	}

	/* display user's detail information. */
	public function actionDetail_page() {
		$id = Yii::app()->request->getParam("id");
		$model = User::model()->findByPk($id);
		$this->renderPartial("detail", compact("model"));
	}

	/* accpet requested user. */
	public function actionAccpet_user() {
		$id = Yii::app()->request->getParam("id");

		$model = User::model()->findByPk($id);
		$model->is_new = User::NEW_NO;
		$model->permission = User::PERMISSION_ENABLED;

		if ($model->save(false))
			echo json_encode(array("success"=>true, "message"=>$this->success));
		else
			echo json_encode(array("success"=>false, "message"=>$this->error));
	}

	/* decline requested user */
	public function actionDecline_user(){
		$id = Yii::app()->request->getParam("id");

		$model = User::model()->findByPk($id);
		$model->is_new = User::NEW_NO;
		$model->permission = User::PERMISSION_DISABLED;

		if ($model->save(false))
			echo json_encode(array("success"=>true, "message"=>$this->success));
		else
			echo json_encode(array("success"=>false, "message"=>$this->error));
	}
}