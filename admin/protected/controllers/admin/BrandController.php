<?php
class BrandController extends Controller {

	/* render the featured brand page. */
	public function actionIndex() {
		$this->render("index");
	}

	/* render the page to show the brands. */
	public function actionList_page() {
		$list = Brand::model()->findAll();
		$this->renderPartial("list", compact("list"));
	}

	/* render the page to add the brand. */
	public function actionAdd_page() {
		$id = Yii::app()->request->getParam("id");
		$model = Brand::model()->findByPk($id);
		$this->renderPartial("add", compact("model"));
	}

	/* save the brand information. */
	public function actionBrand_save() {
		$id = Yii::app()->request->getParam("id");
		$name = Yii::app()->request->getParam("name");
		$description = Yii::app()->request->getParam("description");
		$image = CUploadedFile::getInstanceByName("image");

		/*if ($image == null) {
			echo json_encode(array("success"=>false, "message"=>"Please select image file."));
			return;
		}*/

		$model = Brand::model()->findByPk($id);
		if ($model == null) {
			$model = new Brand();
			$model->created_at = Yii::app()->util->getCurrentDateTime();
		}
		$model->user_id = Yii::app()->user->info->role == User::ROLE_MANAGER ? Yii::app()->user->info->id : 0;
		$model->name = $name;
		$model->description = $description;
		if($image != null)
			$model->image = $image->getName();
		$model->updated_at = Yii::app()->util->getCurrentDateTime();

		if ($model->save(false)) {
			if (!file_exists($model->getImagePath()))
				mkdir($model->getImagePath(), 0777);
			if($image != null)
				$image->saveAs($model->getImageFile());
			echo json_encode(array("success"=>true, "message"=>$this->success));
			return;
		}

		$model->delete();
		echo json_encode(array("success"=>false, "message"=>$this->error));
	}

	/* edit the brand information. */
	public function actionBrand_edit() {
		$this->actionAdd_page();
	}

	/* delete the brand information. */
	public function actionBrand_delete() {
		$id = Yii::app()->request->getParam("id");
		$model = Brand::model()->findByPk($id);
		if ($model->delete() && $model->removeImageFile())
			echo json_encode(array("success"=>true, "message"=>$this->success));
		else
			echo json_encode(array("success"=>false, "message"=>$this->error));
	}
}