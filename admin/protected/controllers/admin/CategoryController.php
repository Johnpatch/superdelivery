<?php
class CategoryController extends Controller {

	/* display the categories managed by administrator. */
	public function actionIndex() {
		$request = Yii::app()->request;
		$cmd = $request->getParam("cmd", "");
			
		$criteria = new CDbCriteria(array("order"=>"arrange ASC"));
		$list = Category::model()->findAll($criteria);
		$categories = ZTreeRecord::getListNodeData($list);
			
		$this->render("index", compact("categories"));
	}
	
	/* save the category by administrator. */
	public function actionCategory_save() {
		$attributes = $_REQUEST["Category"];
		$model = Category::model()->findByPk($attributes["id"]);
		$parent = Category::model()->findByPk($attributes["parent_id"]);
		unset($attributes["id"]);
		
		if ($model == null) {
			$model = new Category();
			$index = Category::model()->countByAttributes(array("parent_id"=>$attributes["parent_id"])) + 1;
			$model->setNodeData($attributes, $parent, $index);
			$model->created_at = Yii::app()->util->getCurrentDateTime();
			$model->updated_at = Yii::app()->util->getCurrentDateTime();
		} else {
			$model->setAttributes($attributes, false);
			$model->updated_at = Yii::app()->util->getCurrentDateTime();
		}
			
		if ($model->save(true))
			$result = array("result"=>"success", "data"=>$model->getNodeData());
		else
			$result = array("result"=>"fail", "message"=>"Your operation is failed.");
		
		echo json_encode($result);
	}

	/* save the big category by administrator. */
	public function actionCategory_save_big() {
		$attributes = $_REQUEST["Category"];
		unset($attributes["id"]);
		$model = new Category();
		$index = Category::model()->countByAttributes(array("parent_id"=>0)) + 1;
		$model->setNodeData($attributes, null, $index);
		$model->created_at = Yii::app()->util->getCurrentDateTime();
		$model->updated_at = Yii::app()->util->getCurrentDateTime();
		if ($model->save(true))
			$result = array("result"=>"success", "data"=>$model->getNodeData());
		else
			$result = array("result"=>"fail", "message"=>"Your operation is failed.");
		
		echo json_encode($result);
		
	}
	
	/* delete the category by administrator. */
	public function actionCategory_delete() {
		$attributes = $_REQUEST["Category"];
		$model = isset($attributes["id"]) ? Category::model()->findByPk($attributes["id"]) : null;
	
		if($model->delete())
			$result = array("result"=>"success");
		else
			$result = array("result"=>"fail", "message"=>"Your operation is failed.");
	
		echo json_encode($result);
	}

	/* display the category options by administrator. */
	public function actionCategory_option() {
		$category_id = Yii::app()->request->getParam("category_id");
		$model = Category::model()->findByPk($category_id);
		$option_list = Category_option::model()->findAll(array(
				"alias"=>"T",
				"select"=>"T.*, T2.arrange",
				"join"=>"INNER JOIN category AS T2 ON (T.category_id = T2.id)",
				"condition"=>":arrange LIKE CONCAT(T2.arrange, '%')",
				"params"=>array(":arrange"=>$model->arrange)
		));
		
		$this->renderPartial("option", compact("category_id", "option_list"));
	}

	/* save the category option. */
	public function actionCategory_option_save() {
		$id = Yii::app()->request->getParam("id");
		$category_id = Yii::app()->request->getParam("category_id");
		$name = Yii::app()->request->getParam("name");

		$model = Category_option::model()->findByPk($id);
		if ($model == null) {
			$model = new Category_option();
			$model->created_at = Yii::app()->util->getCurrentDateTime();
		}

		$model->category_id = $category_id;
		$model->name = $name;
		$model->updated_at = Yii::app()->util->getCurrentDateTime();

		if ($model->save(false))
			echo json_encode(array("success"=>true, "message"=>$this->success));
		else
			echo json_encode(array("success"=>false, "message"=>$this->error));
	}

	/* edit the category option. */
	public function actionCategory_option_edit() {
		$id = Yii::app()->request->getParam("id");
		$model = Category_option::model()->findByPk($id);
		echo json_encode($model);
	}

	/* delete the category option. */
	public function actionCategory_option_delete() {
		$id = Yii::app()->request->getParam("id");
		if (Category_option::model()->deleteByPk($id)) {
			Category_option_detail::model()->deleteAllByAttributes(array("option_id"=>$id));
			echo json_encode(array("success"=>true, "message"=>$this->success));
		} else {
			echo json_encode(array("success"=>false, "message"=>$this->error));
		}
	}

	/* display the category option detail by administrator. */
	public function actionCategory_option_detail() {
		$category_id = Yii::app()->request->getParam("category_id");
		$model = Category::model()->findByPk($category_id);
		$option_list = Category_option::model()->findAll(array(
				"alias"=>"T",
				"select"=>"T.*, T2.arrange",
				"join"=>"INNER JOIN category AS T2 ON (T.category_id = T2.id)",
				"condition"=>":arrange LIKE CONCAT(T2.arrange, '%')",
				"params"=>array(":arrange"=>$model->arrange)
		));
		$option_detail_list = Category_option_detail::model()->findAll(array(
				"alias"=>"T",
				"select"=>"T.*, T2.arrange",
				"join"=>"INNER JOIN category AS T2 ON (T.category_id = T2.id)",
				"condition"=>":arrange LIKE CONCAT(T2.arrange, '%')",
				"params"=>array(":arrange"=>$model->arrange)
		));
		
		$this->renderPartial("option_detail", compact("category_id", "option_list", "option_detail_list"));
	}

	/* save the category detail option. */
	public function actionCategory_option_detail_save() {
		$id = Yii::app()->request->getParam("id");
		$category_id = Yii::app()->request->getParam("category_id");
		$option_id = Yii::app()->request->getParam("option_id");
		$name = Yii::app()->request->getParam("name");

		$model = Category_option_detail::model()->findByPk($id);
		if ($model == null) {
			$model = new Category_option_detail();
			$model->created_at = Yii::app()->util->getCurrentDateTime();
		}

		$model->category_id = $category_id;
		$model->option_id = $option_id;
		$model->name = $name;
		$model->updated_at = Yii::app()->util->getCurrentDateTime();

		if ($model->save(false))
			echo json_encode(array("success"=>true, "message"=>$this->success));
		else
			echo json_encode(array("success"=>false, "message"=>$this->error));
	}

	/* edit the category detail option. */
	public function actionCategory_option_detail_edit() {
		$id = Yii::app()->request->getParam("id");
		$model = Category_option_detail::model()->findByPk($id);
		echo json_encode($model);
	}

	/* delete the category detail option. */
	public function actionCategory_option_detail_delete() {
		$id = Yii::app()->request->getParam("id");
		if (Category_option_detail::model()->deleteByPk($id))
			echo json_encode(array("success"=>true, "message"=>$this->success));
		else
			echo json_encode(array("success"=>false, "message"=>$this->error));
	}
}