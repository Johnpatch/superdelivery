<?php
/* class for registered user management. */
class KeywordController extends Controller {

	public function actionIndex() {
		$this->render("index");
	}

	/* render the keyword page. */
	public function actionList_page() {
		$list = Product_keywords::model()->findAll(array("group"=>"name"));
		$this->renderPartial("list", compact("list"));
	}

	/* display the page to update the keyword's information. */
	public function actionDetail_page() {
		$id = Yii::app()->request->getParam("id");
		$model = Product_keywords::model()->findByPk($id);
		$this->renderPartial("detail", compact("model"));
	}

	public function actionAdd_page() {
		$this->renderPartial("add", compact("model"));
	}

	/* update the keyword's information. */
	public function actionSave_changes() {
		$name = Yii::app()->request->getParam("name");
		$org_name = Yii::app()->request->getParam("org_name");
		
		$model = Product_keywords::model()->updateAll(array("name"=>$name),"name='".$org_name."'");
		
		echo json_encode(array("success"=>true, "message"=>$this->success));
	}

	public function actionAdd_keyword(){
		$model = new Product_keywords();
		$model->name = Yii::app()->request->getParam("name");
		$model->created_at = Yii::app()->util->getCurrentDateTime();
		$model->updated_at = Yii::app()->util->getCurrentDateTime();
		if ($model->save(false))
			echo json_encode(array("success"=>true, "model"=>$model, "message"=>$this->success));
		else
			echo json_encode(array("success"=>false, "message"=>$this->error));
	}

	/* delete a keyword. */
	public function actionDelete() {
        $name = Yii::app()->request->getParam("name");
        
		if (Product_keywords::model()->deleteAll("name = '".$name."'"))
			echo json_encode(array("success"=>true, "message"=>$this->success));
		else
			echo json_encode(array("success"=>false, "message"=>$this->error));
	}
}