<?php
/* class for registered user management. */
class UserController extends Controller {

	public function actionIndex() {
		$this->render("index");
	}

	/* render the users page. */
	public function actionList_page() {
		$role = Yii::app()->request->getParam("role");

		$criteria = new CDbCriteria(array(
				"condition"=>"is_new = :is_new",
				"params"=>array(":is_new"=>User::NEW_NO)
		));
		if ($role) {
			$criteria->condition .= " AND role = :role";
			$criteria->params[":role"] = $role;
		}
		$list = User::model()->findAll($criteria);

		$this->renderPartial("list", compact("list"));
	}

	/* display the page to update the user's information. */
	public function actionDetail_page() {
		$id = Yii::app()->request->getParam("id");
		$model = User::model()->findByPk($id);
		$this->renderPartial("detail", compact("model"));
	}

	public function actionAdd_page() {
		$this->renderPartial("add", compact("model"));
	}

	/* update the user's information. */
	public function actionSave_changes() {
		$id = Yii::app()->request->getParam("id");
		$permission = Yii::app()->request->getParam("permission");
		$role = Yii::app()->request->getParam("role");
		$username = Yii::app()->request->getParam("username");
		$email = Yii::app()->request->getParam("email");
		$telephone = Yii::app()->request->getParam("telephone");
		$address = Yii::app()->request->getParam("address");
		$userid = Yii::app()->request->getParam("userid");
		$user = User::model()->countByAttributes(array("userid"=>$userid),"id != :id",array('id'=>$id));
		if($user > 0)
			echo json_encode(array("success"=>false, "message"=>"顧客IDはすでに存在しています"));
		else{
			$model = User::model()->findByPk($id);
			$model->permission = $permission;
			$model->role = $role;
			$model->username = $username;
			$model->email = $email;
			$model->telephone = $telephone;
			$model->address = $address;
			$model->userid = $userid;
			if(!empty(Yii::app()->request->getParam("password")))
				$model->password = Yii::app()->request->getParam("password");
			$model->updated_at = Yii::app()->util->getCurrentDateTime();
	
			if ($model->save(false))
				echo json_encode(array("success"=>true, "message"=>$this->success));
			else
				echo json_encode(array("success"=>false, "message"=>$this->error));
		}
		
	}

	public function actionAdd_user(){
		$model = new User();
		$model->permission = Yii::app()->request->getParam("permission");
		$model->role = Yii::app()->request->getParam("role");
		$model->username = Yii::app()->request->getParam("username");
		$model->email = Yii::app()->request->getParam("email");
		$model->telephone = Yii::app()->request->getParam("telephone");
		$model->address = Yii::app()->request->getParam("address");
		$model->is_new = User::NEW_NO;
		$model->password = Yii::app()->request->getParam("password");
		$model->created_at = Yii::app()->util->getCurrentDateTime();
		$model->updated_at = Yii::app()->util->getCurrentDateTime();
		$model->userid = Yii::app()->request->getParam("userid");
		$userid = Yii::app()->request->getParam("userid");
		$user = User::model()->countByAttributes(array("userid"=>$userid));
		if($user > 0)
			echo json_encode(array("success"=>false, "message"=>"顧客ID はすでに存在しています"));
		else if ($model->save(false))
			echo json_encode(array("success"=>true, "model"=>$model, "message"=>$this->success));
		else
			echo json_encode(array("success"=>false, "message"=>$this->error));
	}

	/* reset user's password to username. */
	public function actionReset_password() {
		$id = Yii::app()->request->getParam("id");

		$model = User::model()->findByPk($id);
		$model->password = $model->username;
		$model->updated_at = Yii::app()->util->getCurrentDateTime();
		
		if ($model->save(false))
			echo json_encode(array("success"=>true, "message"=>$this->success));
		else
			echo json_encode(array("success"=>false, "message"=>$this->error));
	}

	/* delete a user. */
	public function actionDelete() {
		$id = Yii::app()->request->getParam("id");
		if (User::model()->deleteByPk($id))
			echo json_encode(array("success"=>true, "message"=>$this->success));
		else
			echo json_encode(array("success"=>false, "message"=>$this->error));
	}
}