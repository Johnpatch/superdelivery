<?php
/* class for cart orders. */
class CartController extends Controller {

	public function actionIndex() {
		$this->render("index");
	}

	/* render a requested page for cart. */
	public function actionRequested_page() {
		$requested_list = Deliver::model()->findAll(array(
				"condition"=>"user_id = :user_id AND status = :status",
				"params"=>array(":status"=>Deliver::STATUS_REQUESTED, ":user_id"=>Yii::app()->user->info->id),
				"group"=>"username"
		));
		$this->renderPartial("requested", compact("requested_list"));
	}

	/* display the page to show the list in shipping state. */
	public function actionShipping_page() {
		$shipping_list = Deliver::model()->findAll(array(
				"condition"=>"user_id = :user_id AND status = :status",
				"params"=>array(":status"=>Deliver::STATUS_SHIPPING, ":user_id"=>Yii::app()->user->info->id),
				"group"=>"username"
		));
		$this->renderPartial("shipping", compact("shipping_list"));
	}

	/* display the requested products. */
	public function actionProduct_page() {
		$username = Yii::app()->request->getParam("username");

		$sql = "
				SELECT T.username, T3.name AS product_name, T2.product_number, T2.barcode,
					T2.breakdown, T.quantity, T.address
				FROM deliver AS T
				INNER JOIN product_detail AS T2 ON (T.product_detail_id = T2.id)
				INNER JOIN product AS T3 ON (T2.product_id = T3.id)
				WHERE T.status = :status AND T.username = :username AND T.user_id = :user_id
		";
		$params = array(":status"=>Deliver::STATUS_REQUESTED, ":username"=>$username, ":user_id"=>Yii::app()->user->info->id);
		$product_list = Yii::app()->db->createCommand($sql)->queryAll(true, $params);

		$this->renderPartial("product", compact("product_list", "username"));
	}

	/* accept the requested cart and make it shipping state. */
	public function actionAccept_cart() {
		$username = Yii::app()->request->getParam("username");

		$sql = "UPDATE deliver SET status = :status WHERE username = :username AND status = :status2";
		$params = array(":status"=>Deliver::STATUS_SHIPPING, ":username"=>$username, ":status2"=>Deliver::STATUS_REQUESTED);
		if (Yii::app()->db->createCommand($sql)->execute($params))
			echo json_encode(array("success"=>true, "message"=>$this->success));
		else
			echo json_encode(array("success"=>false, "message"=>$this->error));
	}

	/* complete the requested cart and make it completed state. */
	public function actionComplete_cart() {
		$username = Yii::app()->request->getParam("username");

		$sql = "UPDATE deliver SET status = :status WHERE username = :username AND status = :status2";
		$params = array(":status"=>Deliver::STATUS_COMPLETED, ":username"=>$username, ":status2"=>Deliver::STATUS_SHIPPING);
		if (Yii::app()->db->createCommand($sql)->execute($params))
			echo json_encode(array("success"=>true, "message"=>$this->success));
		else
			echo json_encode(array("success"=>false, "message"=>$this->error));
	}
}