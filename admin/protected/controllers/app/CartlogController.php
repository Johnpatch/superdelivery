<?php
/* class for display the cart log. */
class CartlogController extends Controller {

	public function actionIndex() {
		$this->render("index");
	}

	public function actionList_page() {
		$sql = "
				SELECT T.username, T3.name AS product_name, T2.product_number, T.quantity, T.address
				FROM deliver AS T
				INNER JOIN product_detail AS T2 ON (T.product_detail_id = T2.id)
				INNER JOIN product AS T3 ON (T2.product_id = T3.id)
				WHERE T.status = :status AND T.user_id = :user_id
		";
		$params = array(":status"=>Deliver::STATUS_COMPLETED, ":user_id"=>Yii::app()->user->info->id);
		$list = Yii::app()->db->createCommand($sql)->queryAll(true, $params);

		$this->render("list", compact("list"));
	}
}