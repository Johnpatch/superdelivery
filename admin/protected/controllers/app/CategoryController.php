<?php
/* controller for managing categories registered by wholesaler. */
class CategoryController extends Controller {

	public function actionIndex() {
		$this->render("index");
	}

	/* display the categories. */
	public function actionCategory_page() {
		$category_list = Category2::model()->findAllByAttributes(array("user_id"=>Yii::app()->user->info->id));
		$this->renderPartial("category", compact("category_list"));
	}

	/* save a category. */
	public function actionCategory_save() {
		$id = Yii::app()->request->getParam("id");
		$name = Yii::app()->request->getParam("name");

		$model = Category2::model()->findByPk($id);
		if ($model == null) {
			$model = new Category2();
			$model->created_at = Yii::app()->util->getCurrentDateTime();
		}

		$model->user_id = Yii::app()->user->info->id;
		$model->name = $name;
		$model->updated_at = Yii::app()->util->getCurrentDateTime();
		if ($model->save(false))
			echo json_encode(array("success"=>true, "message"=>$this->success));
		else
			echo json_encode(array("success"=>false, "message"=>$this->error));
	}

	/* edit a category. */
	public function actionCategory_edit() {
		$id = Yii::app()->request->getParam("id");
		$model = Category2::model()->findByPk($id);
		echo json_encode($model);
	}

	/* delete a category. */
	public function actionCategory_delete() {
		$id = Yii::app()->request->getParam("id");
		if (Category2::model()->deleteByPk($id))
			echo json_encode(array("success"=>true, "message"=>$this->success));
		else
			echo json_encode(array("success"=>false, "message"=>$this->error));
	}

	/* display brands. */
	public function actionBrand_page() {
		$brand_list = Brand::model()->findAll(array(
				"condition"=>"user_id = :user_id",
				"params"=>array(":user_id"=>Yii::app()->user->info->id)
		));
		$this->renderPartial("brand", compact("brand_list"));
	}

	/* save a brand. */
	public function actionBrand_save() {
		$id = Yii::app()->request->getParam("id");
		$name = Yii::app()->request->getParam("name");

		$model = Brand::model()->findByPk($id);
		if ($model == null) {
			$model = new Brand();
			$model->created_at = Yii::app()->util->getCurrentDateTime();
		}

		$model->user_id = Yii::app()->user->info->id;
		$model->name = $name;
		$model->updated_at = Yii::app()->util->getCurrentDateTime();
		if ($model->save(false))
			echo json_encode(array("success"=>true, "message"=>$this->success));
		else
			echo json_encode(array("success"=>false, "message"=>$this->error));
	}

	/* edit a brand. */
	public function actionBrand_edit() {
		$id = Yii::app()->request->getParam("id");
		$model = Brand::model()->findByPk($id);
		echo json_encode($model);
	}

	/* delete a brand. */
	public function actionBrand_delete() {
		$id = Yii::app()->request->getParam("id");
		if (Brand::model()->deleteByPk($id))
			echo json_encode(array("success"=>true, "message"=>$this->success));
		else
			echo json_encode(array("success"=>false, "message"=>$this->error));
	}
}