<?php
/* controller to display the counterpart. */
class CounterpartController extends Controller {

	public function actionIndex() {
		$this->render("index");
	}

	public function actionList_page() {
		$list = Price::model()->findAll(array(
				"condition"=>"user_id = :user_id AND status != :status",
				"params"=>array(":user_id"=>Yii::app()->user->info->id, ":status"=>0)
		));
		$this->renderPartial("list", compact("list"));
	}

	public function actionChange_status() {
		$id = Yii::app()->request->getParam("id");
		$status = Yii::app()->request->getParam("status");

		$model = Price::model()->findByPk($id);
		$model->status = $status;

		if ($model->save(false))
			echo json_encode(array("success"=>true, "message"=>$this->success));
		else
			echo json_encode(array("success"=>false, "message"=>$this->error));
	}
}