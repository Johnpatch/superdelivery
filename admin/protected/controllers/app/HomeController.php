<?php
/* controller for managing company information. */
class HomeController extends Controller {

	public function actionIndex() {
		$this->render("index");
	}

	/* display the company information. */
	public function actionDetail_page() {
		$type = Yii::app()->request->getParam("type");
		$model = Company::model()->findByAttributes(array("type"=>$type, "user_id"=>Yii::app()->user->info->id));
		$this->renderPartial("detail", compact("model"));
	}

	/* edit the company information. */
	public function actionModal_page() {
		$type = Yii::app()->request->getParam("type");
		$model = Company::model()->findByAttributes(array("type"=>$type, "user_id"=>Yii::app()->user->info->id));
		$this->renderPartial("modal", compact("type", "model"));
	}

	/* save the company information. */
	public function actionInformation_save() {
		$type = Yii::app()->request->getParam("type");
		$description = Yii::app()->request->getParam("description");

		$model = Company::model()->findByAttributes(array("type"=>$type, "user_id"=>Yii::app()->user->info->id));
		if ($model == null) {
			$model = new Company();
			$model->created_at = Yii::app()->util->getCurrentDateTime();
		}

		$model->user_id = Yii::app()->user->info->id;
		$model->type = $type;
		$model->description = $description;
		$model->updated_at = Yii::app()->util->getCurrentDateTime();

		if ($model->save(false))
			echo json_encode(array("success"=>true, "message"=>$this->success));
		else
			echo json_encode(array("success"=>false, "message"=>$this->error));
	}
}