<?php
/* controller for discussing price with buyer. */
class PriceController extends Controller {

	public function actionIndex() {
		$this->render("index");
	}

	/* display requested list. */
	public function actionList_page() {
		$customer_list = Price::model()->findAll(array(
				"condition"=>"status = 0 AND user_id = :user_id",
				"params"=>array(":user_id"=>Yii::app()->user->info->id),
				"group"=>"customer_id"
		));

		$this->renderPartial("list", compact("customer_list"));
	}

	/* display the detail info of buyer. */
	public function actionView_detail() {
		$username = Yii::app()->request->getParam("username");
		$user = User::model()->findByAttributes(array("username"=>$username));
		$this->renderPartial("detail", compact("user"));
	}

	/* display the order log of buyer. */
	public function actionView_orderlog() {
		$username = Yii::app()->request->getParam("username");
		$sql = "
				SELECT T.date, T4.name AS category, T5.name AS category2,
					T3.sku, T3.name, T2.product_number, T2.breakdown, T2.barcode,
					T2.jan, T.quantity
				FROM deliver AS T
				INNER JOIN product_detail AS T2 ON (T.product_detail_id = T2.id)
				INNER JOIN product AS T3 ON (T2.product_id = T3.id)
				INNER JOIN category AS T4 ON (T3.category_id = T4.id)
				INNER JOIN category2 AS T5 ON (T3.category_id2 = T5.id)
				WHERE T.user_id = :user_id AND T.username = :username
		";
		$params = array(":user_id"=>Yii::app()->user->info->id, ":username"=>$username);
		$list = Yii::app()->db->createCommand($sql)->queryAll(true, $params);

		$this->renderPartial("orderlog", compact("username", "orderlog", "list"));
	}

	/* display requested detail list. */
	public function actionOpen_price() {
		$username = Yii::app()->request->getParam("username");
		
		$sql = "
				SELECT T.id, T3.name AS category, T4.name AS category2, T2.name, T.product_number,
					T.retail_price, T.wholesale_price
				FROM product_detail AS T
				INNER JOIN product AS T2 ON (T.product_id = T2.id)
				INNER JOIN category AS T3 ON (T2.category_id = T3.id)
				INNER JOIN category2 AS T4 ON (T2.category_id2 = T4.id)
				WHERE T.user_id = :user_id
		";
		$params = array(":user_id"=>Yii::app()->user->info->id);
		$product_list = Yii::app()->db->createCommand($sql)->queryAll(true, $params);

		$sql = "
				SELECT T.product_id, T.product_detail_id, T.wholesale_price, T.retail_price
				FROM price_discuss AS T
				INNER JOIN price AS T2 ON (T.price_id = T2.id)
				WHERE T2.customer_username = :username AND T2.user_id = :user_id
		";
		$params = array(":username"=>$username, ":user_id"=>Yii::app()->user->info->id);
		$discuss_list = Yii::app()->db->createCommand($sql)->queryAll(true, $params);

		foreach ($product_list as $k=>$v) {
			foreach ($discuss_list as $k2=>$v2) {
				if ($v["id"] == $v2["product_detail_id"]) {
					$v["retail_price"] .= " / " . $v2["retail_price"];
					$v["wholesale_price"] .= " / " . $v2["wholesale_price"];
					break;
				}
			}
			$product_list[$k] = $v;
		}

		$this->render("openprice", compact("product_list", "username"));
	}

	/* display the all products. */
	public function actionProduct_detail() {
		$id = Yii::app()->request->getParam("id");
		$username = Yii::app()->request->getParam("username");

		$detail = Product_detail::model()->findByPk($id);
		$product = Product::model()->findByPk($detail->product_id);
		$category = Category::model()->findByPk($product->category_id);
		$category2 = Category2::model()->findByPk($product->category_id2);
		$brand = Brand::model()->findByPk($product->brand_id);

		$this->renderPartial("productdetail", compact("category", "category2", "brand", "product", "detail", "username"));
	}

	/* open prices. */
	public function actionAccept_price() {
		$username = Yii::app()->request->getParam("username");

		$sql = "UPDATE price SET status = :status WHERE customer_username = :username AND user_id = :user_id";
		$params = array(":status"=>1, ":username"=>$username, ":user_id"=>Yii::app()->user->info->id);

		if (Yii::app()->db->createCommand($sql)->execute($params))
			echo json_encode(array("success"=>true, "message"=>$this->success));
		else
			echo json_encode(array("success"=>false, "message"=>$this->error));
	}

	/* modify products' prices. */
	public function actionModify_price() {
		$username = Yii::app()->request->getParam("username");
		$product_id = Yii::app()->request->getParam("product_id");
		$product_detail_id = Yii::app()->request->getParam("product_detail_id");
		$retail_price = Yii::app()->request->getParam("retail_price");
		$wholesale_price = Yii::app()->request->getParam("wholesale_price");

		$price = Price::model()->findByAttributes(array("customer_username"=>$username, "user_id"=>Yii::app()->user->info->id));

		$model = Price_discuss::model()->findByAttributes(array("product_id"=>$product_id, "product_detail_id"=>$product_detail_id, "price_id"=>$price->id));
		if ($model == null) {
			$model = new Price_discuss();
			$model->created_at = Yii::app()->util->getCurrentDateTime();
		}
		
		$model->price_id = $price->id;
		$model->product_id = $product_id;
		$model->product_detail_id = $product_detail_id;
		$model->retail_price = $retail_price;
		$model->wholesale_price = $wholesale_price;
		$model->updated_at = Yii::app()->util->getCurrentDateTime();

		if ($model->save(false))
			echo json_encode(array("success"=>true, "message"=>$this->success));
		else
			echo json_encode(array("success"=>false, "message"=>$this->error));
	}
}