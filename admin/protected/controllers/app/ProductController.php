<?php
/* controller for managing products. */
class ProductController extends Controller {

	public function actionIndex() {
		$category_id = Yii::app()->request->getParam("category_id", "");
		$category_list = Category::model()->findAll(array("order"=>"arrange ASC"));
		$categories = ZTreeRecord::getListNodeData($category_list);
		$this->render("index", compact("category_id", "categories"));
	}

	/* display the products. */
	public function actionList_page() {
		$category_id = Yii::app()->request->getParam("id");

		$criteria = new CDbCriteria(array(
				"alias"=>"T",
				"select"=>"T.*, T2.path AS path, T3.name as brand",
				"join"=>"
						INNER JOIN category AS T2 ON (T.category_id = T2.id)
						LEFT JOIN brand AS T3 ON (T.brand_id = T3.id)
				",
				"condition"=>"T.user_id = :user_id",
				"params"=>array(":user_id"=>Yii::app()->user->info->id),
				"order"=>"T2.arrange, T.name, T.standard"
		));

		if ($category_id) {
			$category = Category::model()->findByPk($category_id);
			$criteria->condition .= " AND T2.arrange LIKE :arrange";
			$criteria->params[":arrange"] = $category->arrange . "%";
		}

		$list = Product::model()->findAll($criteria);

		$this->renderPartial("list", compact("list", "category_id"));
	}

	/* add or edit a product. */
	public function actionProduct_add() {
		$product_id = Yii::app()->request->getParam("product_id");

		$category_list = Category::model()->findAll(array("order"=>"arrange ASC"));
		$categories = ZTreeRecord::getListNodeData($category_list);

		$product = Product::model()->findByPk($product_id);
		$category_list2 = Category2::model()->findAllByAttributes(array("user_id"=>Yii::app()->user->info->id));
		$brand_list = Brand::model()->findAll(array(
				"order"=>"name"
		));
		$keywords_list = Product_keywords::model()->findAll(array(
			"condition"=>"type = 0",
			"group"=>"name",));

		$this->renderPartial("add", compact("categories", "product_id", "product", "category_list2", "brand_list", "keywords_list"));
	}

	/* if you have not sub category, save a new sub category. */
	public function actionNew_category_save() {
		$name = Yii::app()->request->getParam("name");
		$model = new Category2();
		$model->user_id = Yii::app()->user->info->id;
		$model->name = $name;
		$model->created_at = Yii::app()->util->getCurrentDateTime();
		$model->updated_at = Yii::app()->util->getCurrentDateTime();
		if ($model->save(false))
			echo json_encode(array("success"=>true, "model"=>$model, "message"=>$this->success));
		else
			echo json_encode(array("success"=>false, "message"=>$this->error));
	}

	/* if you have not brand, save a new brand. */
	public function actionNew_brand_save() {
		$name = Yii::app()->request->getParam("name");
		$model = new Brand();
		$model->user_id = Yii::app()->user->info->id;
		$model->name = $name;
		$model->created_at = Yii::app()->util->getCurrentDateTime();
		$model->updated_at = Yii::app()->util->getCurrentDateTime();
		if ($model->save(false))
			echo json_encode(array("success"=>true, "model"=>$model, "message"=>$this->success));
		else
			echo json_encode(array("success"=>false, "message"=>$this->error));
	}

	/* display the category options. */
	public function actionOption_page() {
		$category_id = Yii::app()->request->getParam("category_id");
		$product_id = Yii::app()->request->getParam("product_id");

		$category = Category::model()->findByPk($category_id);
		$product = Product::model()->findByPk($product_id);

		if ($product == null)
			$product = new Product();

		if ($category != null) {
			$option_list = Category_option::model()->findAll(array(
					"alias"=>"T",
					"select"=>"T.*, T2.arrange",
					"join"=>"INNER JOIN category AS T2 ON (T.category_id = T2.id)",
					"condition"=>":arrange LIKE CONCAT(T2.arrange, '%')",
					"params"=>array(":arrange"=>$category->arrange)
			));
			$option_detail_list = Category_option_detail::model()->findAll(array(
					"alias"=>"T",
					"select"=>"T.*, T2.arrange",
					"join"=>"INNER JOIN category AS T2 ON (T.category_id = T2.id)",
					"condition"=>":arrange LIKE CONCAT(T2.arrange, '%')",
					"params"=>array(":arrange"=>$category->arrange)
			));
		} else {
			$option_list = array();
			$option_detail_list = array();
		}

		$this->renderPartial("option", compact("category_id", "product", "option_list", "option_detail_list"));
	}

	/* delete the product. */
	public function actionProduct_delete() {
		$id = Yii::app()->request->getParam("id");
		if (Product::model()->deleteByPk($id)) {
			Product_detail::model()->deleteAllByAttributes(array("product_id"=>$id));
			echo json_encode(array("success"=>true, "message"=>$this->success));
		} else {
			echo json_encode(array("success"=>false, "message"=>$this->error));
		}
	}

	/* check sku number of a product. */
	public function actionCheck_sku() {
		$sku = Yii::app()->request->getParam("sku");
		$model = Product::model()->find(array(
				"condition"=>"sku = :sku",
				"params"=>array(":sku"=>$sku)
		));
		if ($model != null)
			echo json_encode(array("success"=>false));
		else
			echo json_encode(array("success"=>true));
	}

	/* save a product. */
	public function actionProduct_save() {
		$product_id = Yii::app()->request->getParam("product_id");
		$category_id = Yii::app()->request->getParam("category_id");
		$category_name = Yii::app()->request->getParam("category_name");
		$name = Yii::app()->request->getParam("product_name");
		$sku = Yii::app()->request->getParam("sku");
		$brand_id = Yii::app()->request->getParam("brand_id");
		//$options = Yii::app()->request->getParam("options");
		$keywords = Yii::app()->request->getParam("keywords");
		/*if(!empty($keywords) && $keywords != null && $keywords != 'null')
			$keywords = $keywords.",".$name.",".$category_name.",".$sku;
		else
			$keywords = $name.",".$category_name.",".$sku;*/
		//$category_id2 = Yii::app()->request->getParam("category_id2");
		//$is_show_image = Yii::app()->request->getParam("is_show_image");
		$description = Yii::app()->request->getParam("description");
		//$shipping = Yii::app()->request->getParam("shipping");
		//$sizeandcapacity = Yii::app()->request->getParam("sizeandcapacity");
		//$standard = Yii::app()->request->getParam("standard");
		//$notes = Yii::app()->request->getParam("notes");
		$original_price = Yii::app()->request->getParam("original_price");
		$sale_price = $original_price + $original_price/5;
		$is_new = Yii::app()->request->getParam("is_new");

		$model = Product::model()->findByPk($product_id);
		if ($model == null) {
			$model = new Product();
			$model->created_at = Yii::app()->util->getCurrentDateTime();
		}

		$model->user_id = Yii::app()->user->info->id;
		$model->date = Yii::app()->util->getCurrentDate();
		$model->category_id = $category_id;
		$model->name = $name;
		$model->sku = $sku;
		$model->brand_id = $brand_id;
		//$model->options = $options;
		$model->keywords = $keywords;
		//$model->category_id2 = $category_id2;
		//$model->is_show_image = $is_show_image;
		$model->description = $description;
		//$model->shipping = $shipping;
		//$model->sizeandcapacity = $sizeandcapacity;
		//$model->standard = $standard;
		//$model->notes = $notes;
		$model->original_price = $original_price;
		$model->sale_price = $sale_price;
		$model->is_new = $is_new;
		$model->updated_at = Yii::app()->util->getCurrentDateTime();

		$model->setIsNewRecord($model->id == null);
		if ($model->save(false)) {
			$keywords_list = explode(",", $keywords);
			Product_keywords::model()->deleteAllByAttributes(array("product_id"=>$model->id));
			foreach ($keywords_list as $no=>$item) {
				$m = new Product_keywords();
				$m->product_id = $model->id;
				$m->name = $item;
				$m->type = 0;
				$m->created_at = Yii::app()->util->getCurrentDateTime();
				$m->updated_at = Yii::app()->util->getCurrentDateTime();
				$m->save(false);
			}
			if(!empty($name)){
				$m = new Product_keywords();
				$m->product_id = $model->id;
				$m->name = $name;
				$m->type = 1;
				$m->created_at = Yii::app()->util->getCurrentDateTime();
				$m->updated_at = Yii::app()->util->getCurrentDateTime();
				$m->save(false);
			}
			if(!empty($sku)){
				$m = new Product_keywords();
				$m->product_id = $model->id;
				$m->name = $sku;
				$m->type = 1;
				$m->created_at = Yii::app()->util->getCurrentDateTime();
				$m->updated_at = Yii::app()->util->getCurrentDateTime();
				$m->save(false);
			}
			if(!empty($category_name)){
				$m = new Product_keywords();
				$m->product_id = $model->id;
				$m->name = $category_name;
				$m->type = 1;
				$m->created_at = Yii::app()->util->getCurrentDateTime();
				$m->updated_at = Yii::app()->util->getCurrentDateTime();
				$m->save(false);
			}
			echo json_encode(array("success"=>true, "message"=>$this->success, "product_id"=>$model->id));
		}
		else
			echo json_encode(array("success"=>false, "message"=>$this->error));
	}

	/* display the image page. */
	public function actionImage_page() {
		$product_id = Yii::app()->request->getParam("product_id");
		
		$product = Product::model()->findByPk($product_id);
		
		$image_list = Product_image::model()->findAll(array(
				"condition"=>"product_id = :product_id",
				"params"=>array(":product_id"=>$product_id)
		));
		
		$this->renderPartial("image", compact("product", "image_list"));
	}

	/* save a main image of a product. */
	public function actionProduct_main_image_save() {
		$product_id = Yii::app()->request->getParam("product_id");
		$main_image = CUploadedFile::getInstanceByName("main_image");

		if ($main_image != null) {
			$product = Product::model()->findByPk($product_id);
			$product->main_image = $main_image->getName();
			
			if (!file_exists($product->getMainImagePath()))
				mkdir($product->getMainImagePath(), 0777);
			
			if ($product->save(false)) {
				$main_image->saveAs($product->getMainImageFile());
				echo json_encode(array("success"=>true));
			} else
				echo json_encode(array("success"=>false));
		} else
			echo json_encode(array("success"=>false));
	}
	
	/* delete a main image of a product. */
	public function actionProduct_main_image_delete() {
		$product_id = Yii::app()->request->getParam("product_id");
		
		$product = Product::model()->findByPk($product_id);
		$product->removeMainImageFile();
		$product->main_image = "";
		
		if ($product->save(false))
			echo json_encode(array("success"=>true));
		else
			echo json_encode(array("success"=>false));
	}

	/* save a gallery image of a product. */
	public function actionProduct_gallery_image_save() {
		$product_id = Yii::app()->request->getParam("product_id");
		$gallery_image = CUploadedFile::getInstanceByName("gallery_image");
		
		if ($gallery_image != null) {
			$model = new Product_image();
			$model->product_id = $product_id;
			$model->image = $gallery_image->getName();
			$model->created_at = Yii::app()->util->getCurrentDateTime();
			$model->updated_at = Yii::app()->util->getCurrentDateTime();
			
			if (!file_exists($model->getGalleryImagePath()))
				mkdir($model->getGalleryImagePath(), 0777);
		
			if ($model->save(false)) {
				$gallery_image->saveAs($model->getGalleryImageFile());
				echo json_encode(array("success"=>true));
			} else
				echo json_encode(array("success"=>false));
		} else
			echo json_encode(array("success"=>false));
	}
	
	/* delete a gallery image of a product. */
	public function actionProduct_gallery_image_delete() {
		$id = Yii::app()->request->getParam("id");
		
		$model = Product_image::model()->findByPk($id);
		if ($model->image != "") {
			$model->removeGalleryImageFile();
			$model->delete();
			echo json_encode(array("success"=>true));
		} else
			echo json_encode(array("success"=>false));	
	}

	/* display a detail page. */
	public function actionDetail_page() {
		$product_id = Yii::app()->request->getParam("product_id");

		$product = Product::model()->findByPk($product_id);
		$product_detail_list = Product_detail::model()->findAll(array(
				"condition"=>"product_id = :product_id AND user_id = :user_id",
				"params"=>array(":product_id"=>$product_id, ":user_id"=>Yii::app()->user->info->id)
		));

		$this->renderPartial("detail", compact("product_detail_list", "product"));
	}

	/* save the detail product. */
	public function actionProduct_detail_save() {
		$product_detail_id = Yii::app()->request->getParam("product_detail_id");
		$product_id = Yii::app()->request->getParam("product_id");
		$product_number = Yii::app()->request->getParam("product_number");
		$breakdown = Yii::app()->request->getParam("breakdown");
		$barcode = Yii::app()->request->getParam("barcode");
		$jan = Yii::app()->request->getParam("jan");
		$quantity = Yii::app()->request->getParam("quantity");
		$retail_type = Yii::app()->request->getParam("retail_type");
		$wholesale_type = Yii::app()->request->getParam("wholesale_type");
		$retail_price = Yii::app()->request->getParam("retail_price");
		$wholesale_price = Yii::app()->request->getParam("wholesale_price");

		$model = Product_detail::model()->findByPk($product_detail_id);
		if ($model == null) {
			$model = new Product_detail;
			$model->created_at = Yii::app()->util->getCurrentDateTime();
		}

		$model->user_id = Yii::app()->user->info->id;
		$model->product_id = $product_id;
		$model->product_number = $product_number;
		$model->breakdown = $breakdown;
		$model->barcode = $barcode;
		$model->jan = $jan;
		$model->quantity = $quantity;
		$model->retail_type = $retail_type;
		$model->wholesale_type = $wholesale_type;
		$model->retail_price = $retail_price;
		$model->wholesale_price = $wholesale_price;
		$model->updated_at = Yii::app()->util->getCurrentDateTime();

		if ($model->save(false))
			echo json_encode(array("success"=>true, "message"=>$this->success));
		else
			echo json_enode(array("success"=>false, "message"=>$this->error));
	}

	/* edit a detail product. */
	public function actionProduct_detail_edit() {
		$id = Yii::app()->request->getParam("id");
		$model = Product_detail::model()->findByPk($id);
		echo json_encode($model);
	}

	/* delete a detail product. */
	public function actionProduct_detail_delete() {
		$id = Yii::app()->request->getParam("id");
		if (Product_detail::model()->deleteByPk($id))
			echo json_encode(array("success"=>true, "message"=>$this->success));
		else
			echo json_encode(array("success"=>false, "message"=>$this->error));
	}
}