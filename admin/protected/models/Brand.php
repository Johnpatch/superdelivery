<?php
class Brand extends CActiveRecord {

	public $id = null;
	public $user_id;
	public $name;
	public $image;
	public $description;
	public $created_at;
	public $updated_at;

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return "brand";
	}

	public function getImage() {
		return Yii::app()->baseUrl . "/images/brand_" . $this->id . "_" . $this->image;
	}

	public function getImagePath() {
		return dirname(__FILE__) . "/../../images/";
	}

	public function getImageFile() {
		return $this->getImagePath() . "brand_" . $this->id . "_" . $this->image;
	}
	
	public function removeImageFile() {
		if(!empty($this->id) && !empty($this->image))
			return unlink($this->getImageFile());
		return true;
	}
}