<?php
class Category extends ZTreeRecord {

	public $name;

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return "category";
	}

	public function relations() {
		return array (
				"children" => array(
						self::HAS_MANY,
						"category",
						"parent_id"
				),
				"parent" => array(
						self::BELONGS_TO,
						"Category",
						"parent_id"
				)
		);
	}

	public function getPathin() {
		if ($this->path != null) {
			if (($index = strpos($this->path, self::PATH_SEPARATOR)) !== false)
				return substr($this->path, $index + 2);
		}
		return "";
	}

	public function getLeveledname() {
		$ret = str_repeat("&nbsp;", strlen ($this->arrange) - 4) . $this->name;
		return $ret;
	}

	public function getNodeData() {
		$nodeData = $this->attributes;
		$nodeData["pathin"] = $this->getPathin();
		return $nodeData;
	}

	public function setNodeData($nodeData, $parent, $index = null) {
		if (isset ($nodeData["children"] ))
			$nodeData["leaf"] = 0;
		else
			$nodeData["leaf"] = 1;
		
		$this->setAttributes($nodeData, false);
		if ($parent == null) {
			$this->parent_id = 0;
			$this->path = $this->name;
			if ($index != null)
				$this->arrange = sprintf("%04d", $index);
		} else {
			$this->parent_id = $parent->id;
			$this->path = $parent->path != "" && strlen($parent->arrange) >= 4 ? ($parent->path . self::PATH_SEPARATOR . $this->name) : $this->name;
			if ($index != null)
				$this->arrange = sprintf("%s%04d", $parent->arrange, $index);
		}
	}
}