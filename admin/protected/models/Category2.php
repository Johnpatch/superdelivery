<?php
class Category2 extends CActiveRecord {

	public $id = null;
	public $user_id;
	public $name;
	public $created_at;
	public $updated_at;

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return "category2";
	}
}