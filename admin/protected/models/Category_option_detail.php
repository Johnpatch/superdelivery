<?php
class Category_option_detail extends CActiveRecord {

	public $id = null;
	public $category_id;
	public $option_id;
	public $name;
	public $created_at;
	public $updated_at;
	
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}
	
	public function tableName() {
		return "category_option_detail";
	}
}