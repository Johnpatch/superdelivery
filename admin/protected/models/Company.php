<?php
class Company extends CActiveRecord {

	const TYPE_TERMS = 1;
	const TYPE_SHIPPING = 2;
	const TYPE_ENTERPRISE = 3;
	public static $TYPE_LIST = array(
			self::TYPE_TERMS=>"取引条件",
			self::TYPE_SHIPPING=>"送料・決済方法",
			self::TYPE_ENTERPRISE=>"企業情報"
	);

	public $id = 0;
	public $type;
	public $description;
	public $created_at;
	public $updated_at;

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return "company";
	}
}