<?php
class Deliver extends CActiveRecord {

	const STATUS_REQUESTED = 0;
	const STATUS_SHIPPING = 1;
	const STATUS_COMPLETED = 2;
	public static $STATUS_LIST = array(
			self::STATUS_REQUESTED=>"Requested",
			self::STATUS_SHIPPING=>"Shipping",
			self::STATUS_COMPLETED=>"Completed"
	);

	public $id = null;
	public $user_id;
	public $date;
	public $username;
	public $product_detail_id;
	public $quantity;
	public $address;
	public $status;
	public $created_at;
	public $updated_at;

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return "deliver";
	}
}