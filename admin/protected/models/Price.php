<?php
class Price extends CActiveRecord {

	const STATUS_SETUP = 1;
	const STATUS_CANCEL = 2;
	public static $STATUS_LIST = array(
			self::STATUS_SETUP=>"セットアップ",
			self::STATUS_CANCEL=>"キャンセル"
	);

	public $id = null;
	public $user_id;
	public $customer_username;
	public $customer_id;
	public $status;
	public $created_at;
	public $updated_at;

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return "price";
	}
}