<?php
class Price_discuss extends CActiveRecord {

	public $id = null;
	public $price_id;
	public $product_id;
	public $product_detail_id;
	public $real_price;
	public $created_at;
	public $updated_at;

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return "price_discuss";
	}
}