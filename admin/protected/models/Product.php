<?php
class Product extends CActiveRecord {

	const NOT_NEW_PRODUCT = 0;
	const NEW_PRODUCT = 1;

	public $id = null;
	public $date;
	public $user_id;
	public $brand_id;
	public $main_image;
	public $category_id;
	public $category_id2;
	public $sku;
	public $name;
	public $standard;
	public $is_new;
	public $keywords;
	public $options;
	public $is_show_image;
	public $notes;
	public $sizeandcapacity;
	public $shipping;
	public $description;
	public $original_price;
	public $sale_price;
	public $created_at;
	public $updated_at;

	/* additional attributes */
	public $path;
	public $category2;
	public $brand;
	
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return "product";
	}

	public function getBasePath() {
		return dirname(__FILE__) . "/../../";
	}

	public function getMainImagePath() {
		return $this->getBasePath() . "images/";
	}

	public function getMainImageFile() {
		return $this->getMainImagePath() . "main_" . $this->id . "_" . $this->main_image;
	}
	
	public function removeMainImageFile() {
		return unlink($this->getMainImageFile());
	}
}