<?php
class Product_detail extends CActiveRecord {

	const OUT_STOCK = 0;
	const IN_STOCK = 1;
	public static $STOCK_TYPE_LIST = array(
			self::IN_STOCK=>"In Stock",
			self::OUT_STOCK=>"Out Of Stock"
	);

	public static $TYPE_LIST = array(
		1=>"メーカー希望小売価格",
		2=>"カタログ価格",
		3=>"参考上代",
		4=>"オープンプライス",
		5=>"上代",
		6=>"下代"
	);

	public $id = null;
	public $user_id;
	public $product_id;
	public $product_number;
	public $breakdown;
	public $barcode;
	public $jan;
	public $quantity;
	public $retail_type;
	public $retail_price;
	public $wholesale_type;
	public $wholesale_price;
	public $is_stock;
	public $created_at;
	public $updated_at;

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return "product_detail";
	}
}