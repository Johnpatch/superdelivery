<?php
class Product_image extends CActiveRecord
{
	public $id = null;
	public $product_id;
	public $image;
	public $created_at;
	public $updated_at;

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return "product_image";
	}
	
	public function getBasePath()
	{
		return dirname(__FILE__) . "/../../";
	}
	
	public function getGalleryImagePath()
	{
		return $this->getBasePath() . "images/";
	}
	
	public function getGalleryImageFile()
	{
		return $this->getGalleryImagePath() . "gallery_" . $this->product_id . "_" . $this->image;
	}
	
	public function removeGalleryImageFile()
	{
		return unlink($this->getGalleryImageFile());
	}
}