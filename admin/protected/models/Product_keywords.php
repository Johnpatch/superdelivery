<?php
class Product_keywords extends CActiveRecord {

	public $id = null;
	public $name;
	public $type;
	public $created_at;
	public $updated_at;

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return "product_keywords";
	}
}