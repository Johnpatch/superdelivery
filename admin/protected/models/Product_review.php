<?php
class Product_review extends CActiveRecord
{
	public $id = null;
	public $user_id;
	public $type;
	public $email;
	public $product_id;
	public $rating;
	public $content;
	public $created_at;
	public $updated_at;

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return "product_review";
	}
}