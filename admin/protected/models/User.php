<?php
class User extends CActiveRecord {

	const PERMISSION_DISABLED = 0;
	const PERMISSION_ENABLED = 1;
	public static $PERMISSION_LIST = array(
			self::PERMISSION_DISABLED=>"使用禁止中",
			self::PERMISSION_ENABLED=>"使用中"
	);
	
	const ROLE_ADMIN = 1;
	const ROLE_MANAGER = 2;
	const ROLE_USER = 3;
	public static $ROLE_LIST = array(
		self::ROLE_MANAGER=>"セラー",
		self::ROLE_USER=>"バイヤー",
		self::ROLE_ADMIN=>"アドミン"
	);

	const NEW_NO = 0;
	const NEW_YES = 1;
	public static $NEW_LIST = array(
			self::NEW_NO=>"",
			self::NEW_YES=>"New User"
	);

	public $id = null;
	public $userid;
	public $username;
	public $password;
	public $email;
	public $telephone;
	public $address;
	public $permission;
	public $role;
	public $is_new;
	public $created_at;
	public $updated_at;
	
	private $_identity;

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return "user";
	}
	
	public function authenticate() {
		if (!$this->hasErrors()) {
			$this->_identity = new UserIdentity($this->userid, $this->password);
			$this->_identity->authenticate();
			
			if ($this->_identity->errorCode == UserIdentity::ERROR_USERNAME_INVALID)
				$this->addError("userid", "You are an unregistered user.");
			else if ($this->_identity->errorCode == UserIdentity::ERROR_PASSWORD_INVALID)
				$this->addError("userid", "Your password is incorrect.");
			else if ($this->_identity->errorCode == UserIdentity::ERROR_NOT_ALLOWED)
				$this->addError("userid", "Your permission is denied.");
			else if ($this->_identity->errorCode == UserIdentity::ERROR_NOT_AUTHORIZE)
				$this->addError("userid", "You can not login this site, because you are an normal user.");
		}
	}
	
	public function login() {
		$this->authenticate();
		if ($this->_identity->errorCode == UserIdentity::ERROR_NONE) {
			Yii::app()->user->login($this->_identity);
			return true;
		}
		return false;
	}
}