<div class="modal fade" id="user-detail-modal">
	<div class="modal-dialog" style="min-width:1000px">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><?=$model->email?>'s information</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal">
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
				        		<label class="control-label col-md-3">ユーザー名</label>
				        		<div class="col-md-9">
				                	<input type="text" class="form-control" value="<?=$model->username?>" disabled>
				              	</div>
				            </div>
				            <div class="form-group">
				            	<label class="control-label col-md-3">E-mail</label>
				              	<div class="col-md-9">
				                	<input type="text" class="form-control" value="<?=$model->email?>" disabled>
				              	</div>
				            </div>
				            <div class="form-group">
				            	<label class="control-label col-md-3">電話番号</label>
				              	<div class="col-md-9">
				                	<input type="text" class="form-control" value="<?=$model->telephone?>" disabled>
				              	</div>
				            </div>
				            <div class="form-group">
				            	<label class="control-label col-md-3">アドレス</label>
				              	<div class="col-md-9">
				                	<input type="text" class="form-control" value="<?=$model->address?>" disabled>
				              	</div>
				            </div>
				            <div class="form-group">
				            	<label class="control-label col-md-3">創造時間</label>
				              	<div class="col-md-9">
				                	<input type="text" class="form-control" value="<?=$model->created_at?>" disabled>
				              	</div>
				            </div>
				            <div class="form-group">
				            	<label class="control-label col-md-3">ユーザロール</label>
				              	<div class="col-md-9">
				                	<?=CHtml::dropDownList("role2", $model->role, User::$ROLE_LIST, array("class"=>"form-control", "disabled"=>true))?>
				              	</div>
				            </div>
						</div>
						<div class="col-xs-6">
							<textarea id="description" class="form-control"></textarea>
						</div>
					</div>
		        </form>
			</div>
			<div class="modal-footer">
				<button type="button" id="close-btn" class="btn btn-default" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 閉じる
				</button>
				<button type="button" class="btn btn-warning" onclick="declineUser(<?=$model->id?>)">
					<span class="glyphicon glyphicon-arrow-left"></span> このユーザー断り
				</button>
				<button type="button" class="btn btn-primary" onclick="accpetUser(<?=$model->id?>)">
					<span class="glyphicon glyphicon-save"></span> このユーザー受け入り
				</button>
			</div>
		</div>
	</div>
</div>
<script>
	$(function() {
		$("#description").wysihtml5({
			toolbar: {
		        "font-styles": false,
		        "color": false,
		        "emphasis": {"small": true},
		        "blockquote": false,
		        "lists": false,
		        "html": false,
		        "link": false,
		        "image": false,
		        "smallmodals": false
		    }
		});

		$("#user-detail-modal").modal();
	});

	function declineUser(id) {
		if (!confirm("このユーザーを本当に断りますか ?"))
			return;

		/*alert("Declining...");

		var description = $("#description").val();*/
		$.ajax({
			type: "post",
			url: "<?=$this->createUrl("decline_user")?>",
			data: {id: id},
			dataType: "json",
			success: function(res) {
				if (res.success) {
					toastr["success"](res.message, "Success");
					$("#close-btn").trigger("click");
					getNewUserCount();
					setTimeout(userListPage, 700);
				} else {
					toastr["error"](res.message, "Error");
				}
			}
		});
	}

	function accpetUser(id) {
		if (!confirm("このユーザーを本当に受け入りますか ?"))
			return;

		$.ajax({
			type: "post",
			url: "<?=$this->createUrl("accpet_user")?>",
			data: {id: id},
			dataType: "json",
			success: function(res) {
				if (res.success) {
					toastr["success"](res.message, "Success");
					$("#close-btn").trigger("click");
					getNewUserCount();
					setTimeout(userListPage, 700);
				} else {
					toastr["error"](res.message, "Error");
				}
			}
		});
	}
</script>