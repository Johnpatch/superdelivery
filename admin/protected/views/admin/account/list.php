<table class="table table-bordered table-strpied table-hover" id="user-table">
	<thead>
		<tr>
			<th>No</th>
			<th>ユーザー名</th>
			<th>E-mail</th>
			<th>要求権限</th>
			<th>創造時間</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($list as $no=>$item) {?>
		<tr>
			<td><?=$no + 1?></td>
			<td><?=$item->username?></td>
			<td><a href="javascript:;" onclick="userDetailPage(<?=$item->id?>)"><?=$item->email?></a></td>
			<td><?=User::$ROLE_LIST[$item->role]?></td>
			<td><?=$item->created_at?></td>
		</tr>
		<?php }?>
	</tbody>
</table>
<div id="user-detail-page"></div>
<script>
	$(function() {
		$("#user-table").DataTable();
	});

	function userDetailPage(id) {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("detail_page")?>",
			data: "id=" + id,
			success: function(page) {
				$("#user-detail-page").html(page);
			}
		});
	}
</script>