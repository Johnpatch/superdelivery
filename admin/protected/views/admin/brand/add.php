<form role="form">
	<div class="form-group">
		<label for="name">メーカー名</label>
		<input type="text" id="name" class="form-control" value="<?=$model != null ? $model->name : ""?>">
	</div>
	<div class="form-group">
		<label for="description">説明</label>
		<textarea id="description" class="form-control">
			<?=$model != null ? $model->description : ""?>
		</textarea>
	</div>
	<div class="form-group">
		<div class="btn btn-default btn-file">
        	<span class="glyphicon glyphicon-paperclip"></span> メーカーイメージ選択
        	<input type="file" id="image-file" onchange="selectBrandImage()">
        </div>
        <button type="button" class="btn btn-info" onclick="brandSave()">
			<span class="glyphicon glyphicon-save"></span> このメーカーを保存
        </button>
	</div>
	<div class="form-group">
		<img id="image" src="<?=$model != null ? $model->getImage() : ""?>">
	</div>
</form>
<input type="hidden" id="brand_id" value="<?=$model != null ? $model->id : 0?>">
<script>
	$(function() {
		$("#description").wysihtml5({
			toolbar: {
		        "font-styles": false,
		        "color": false,
		        "emphasis": {"small": true},
		        "blockquote": false,
		        "lists": false,
		        "html": false,
		        "link": false,
		        "image": false,
		        "smallmodals": false
		    }
		});
	});

	function brandSave() {
		var id = $("#brand_id").val();
		var name = $("#name").val();
		var description = $("#description").val();
		var image = $("#image-file").prop("files")[0];

		if (name.trim() == "") {
			toastr["warning"]("ブランド名を入力してください", "Warning");
			$("#name").focus();
			return;
		}
		if ($("#image").attr("src") == "") {
			toastr["warning"]("イメージファイルを選択してください", "Warning");
			$("#image-file").trigger("click");
			return;
		}

		if (!confirm("このブランドを本当に保存しますか ?"))
			return;

		var params = new FormData();
		params.append("id", id);
		params.append("name", name);
		params.append("description", description);
		params.append("image", image);

		$.ajax({
			type: "post",
			url: "<?=$this->createUrl("brand_save")?>",
			contentType: false,
			processData: false,
			data: params,
			dataType: "json",
			success: function(res) {
				if (res.success) {
					toastr["success"](res.message, "Success");
					brandListPage();
					brandAddPage();
				} else {
					toastr["error"](res.message, "Error");
				}
			}
		});
	}

	function selectBrandImage() {
		var image = $("#image-file").prop("files")[0];
		var reader = new FileReader();

		if (image)
			reader.readAsDataURL(image);
		else
			toastr["error"]("開けられません.", "ブランドイメージ選択");

		reader.onloadend = function() {
			$("#image").attr("src", reader.result);
		}
	}
</script>