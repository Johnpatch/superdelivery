<div class="box box-solid">
	<div class="box-header with-border">
		<h3 class="box-title">メーカー</h3>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-xs-8">
				<div id="brand-list-page"></div>
			</div>
			<div class="col-xs-4">
				<div id="brand-add-page"></div>
			</div>
		</div>
	</div>
</div>
<script>
	$(function() {
		brandListPage();
		brandAddPage();
	});

	function brandListPage() {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("list_page")?>",
			success: function(page) {
				$("#brand-list-page").html(page);
			}
		});
	}

	function brandAddPage(id = 0) {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("add_page")?>",
			data: "id=" + id,
			success: function(page) {
				$("#brand-add-page").html(page);
			}
		});
	}
</script>