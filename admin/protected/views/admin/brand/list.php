<table class="table table-bordered table-strpied table-hover" id="brand-table">
	<thead>
		<tr>
			<th>No</th>
			<th>メーカー名</th>
			<th>イメージ</th>
			<th>編集</th>
			<th>削除</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($list as $no=>$item) {?>
		<tr>
			<td><?=$no + 1?></td>
			<td><?=$item->name?></td>
			<td><?=$item->image?></td>
			<td><a href="javascript:;" onclick="brandEdit(<?=$item->id?>)">編集</a></td>
			<td><a href="javascript:;" onclick="brandDelete(<?=$item->id?>)">削除</a></td>
		</tr>
		<?php }?>
	</tbody>
</table>
<script>
	$(function() {
		$("#brand-table").DataTable();
	});

	function brandEdit(id) {
		brandAddPage(id);
	}

	function brandDelete(id) {
		if (!confirm("このアイテムを本当に削除しますか ?"))
			return;
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("brand_delete")?>",
			data: "id=" + id,
			dataType: "json",
			success: function(res) {
				if (res.success) {
					toastr["success"](res.message, "Success");
					brandListPage();
				} else {
					toastr["error"](res.message, "Error");
				}
			}
		});
	}
</script>