<div class="box box-solid">
	<div class="box-header with-border">
		<h3 class="box-title">商品カテゴリー</h3>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-xs-4">
				<label>カテゴリーアイテム</label>
				<ul id="category-tree" class="ztree" style="min-height:800px"></ul>
			</div>
			<div class="col-xs-8">
				<form action="javascript:;" class="form-horizontal" id="category-edit-form" autocomplete="off">
					<div class="form-group">
						<div class="col-md-8">
							<label>カテゴリ一名</label>
							<input type="text" name="Category[name]" id="Category_name" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12">
							<input type="hidden" name="cmd" value="insert">
							<input type="hidden" name="Category[id]">
							<input type="hidden" name="Category[parent_id]">
							<button type="button" class="btn btn-primary" id="category-save-big">大カテゴリー追加</button>
							<!--button type="button" class="btn btn-primary" id="category-save">中,小カテゴリ一追加</button-->
							<button type="button" class="btn btn-success" id="category-addnew">中,小カテゴリ一追加</button>
							<button type="button" class="btn btn-danger" id="category-delete">カテゴリ一削除</button>
							
						</div>
					</div>
				</form>
				<!--div class="row">
					<div class="col-xs-12">
						<div id="category-option"></div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-xs-12">
						<div id="category-option-detail"></div>
					</div>
				</div-->
			</div>
		</div>
	</div>
</div>
<script>
	var zTreeObj;
	
	function setFontCss(treeId, treeNode) {
		return {"font-weight" : "normal"};
	};
	
	var setting = {
		data: {
			key: {
				name: "name",
				iconSkin: "treeIconSkin",
			},
			simpleData: {
				enable: true,
				idKey: "id",
				pIdKey: "parent_id",
			},
		},
		view: {
			fontCss: setFontCss,
			showLine: true,
			showIcon: false
		},
		edit: {
			drag: {
				autoExpandTrigger: true,
			}
		},
		callback: {
			onClick: onNodeClick
		},
	};
	
	var zNodes = <?php echo json_encode($categories)?>;
	var Keys = ["id", "parent_id", "name"];
	var Model = "Category";
	
	var category_selected_node = null;
	
	function onNodeClick(event, treeId, treeNode, clickFlag) {
		var form = $("#category-edit-form")[0];
		category_selected_node = treeNode;
		if (category_selected_node == null) {
			$("#category-addnew").trigger("click");
		} else {
			for (var i = 0, ni = Keys.length; i < ni; i++) {
				if (form.elements[Model + "[" + Keys[i] + "]"] != null) {
					form.elements[Model + "[" + Keys[i] + "]"].value =
						category_selected_node[Keys[i]] == undefined ? "" : category_selected_node[Keys[i]];
				}
			}
			form.elements["cmd"].value = "update";
			$("#category-save").html("更新カテゴリ");
			$("#category-delete")[0].disabled = false;
			$("#category-message").html("");
			form.elements["Category[name]"].select();
		}

		//getCategoryOptionList(treeNode.id);
		//getCategoryOptionDetailList(treeNode.id);
	}

	function getCategoryOptionList(category_id) {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("category_option")?>",
			data: "category_id=" + category_id,
			success: function(page) {
				$("#category-option").html(page);
			}
		});
	}

	function getCategoryOptionDetailList(category_id) {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("category_option_detail")?>",
			data: "category_id=" + category_id,
			success: function(page) {
				$("#category-option-detail").html(page);
			}
		});
	}
	
	function getTreeNodeJSON(nodes) {
		var node, item;
		var arr = new Array();
		for (var i = 0, ni = nodes.length; i < ni; i++) {
	        node = nodes[i];
	        item = new Object();
	        for (var j = 0, nj = Keys.length; j < nj; j++) {
				item[Keys[j]] = node[Keys[j]];
			}
	        if (node.children != null && node.children.length > 0) {
	            item.children = getTreeNodeJSON(node.children);
	        }
	        arr.push(item);
	    }
	    return arr;
	}
	
	$(document).ready(function() {
		$("#Category_name").keydown(function(e) {
			if (e.keyCode == 13) {
				$("#category-save").trigger("click");
			}
		});

		zTreeObj = $.fn.zTree.init($("#category-tree"), setting, zNodes);
	
		$("#category-save").click(function() {
			var form = $("#category-edit-form")[0];
			if (form.elements["Category[name]"].value == "" || form.elements["Category[name]"].value == undefined) {
				toastr["error"]("カテゴリ名を入力してください", "Validation error");
				$("#Category_name").focus();
				return;
			}
			/*if(category_selected_node == null){
				toastr["error"]("カテゴリーを選択してください", "Validation error");
				return;
			}*/
			if (category_selected_node != null && category_selected_node.user_id == 0) {
        		<?php if (Yii::app()->user->info->role != User::ROLE_ADMIN) {?>
            		toastr["warning"]("This category can be modified by administrator.", "Update Operation Error");
            		return;
        		<?php }?>
        	}

			var cmd = form.elements["cmd"].value;
			$.ajax({
				url: "<?=$this->createUrl("category_save")?>",
				data: $(form).serialize(),
				dataType: "json",
				success: function(data) {
					if (data["result"] == "success") {
						toastr["success"]("Your operation is completed successfully.", "Success");
						if (cmd == "update") {
							for(var i = 0, ni = Keys.length; i<ni; i++) {
								category_selected_node[Keys[i]] = data["data"][Keys[i]];
							}
							zTreeObj.updateNode(category_selected_node);
						} else {
							zTreeObj.addNodes(category_selected_node, data["data"]);
						}
					} else {
						toastr["error"](data["message"], "Category Save Error");
					}
				}
			});
			form.elements["Category[name]"].select();
		});

		$("#category-save-big").click(function() {
			var form = $("#category-edit-form")[0];
			if (form.elements["Category[name]"].value == "" || form.elements["Category[name]"].value == undefined) {
				toastr["error"]("カテゴリ名を入力してください", "Validation error");
				$("#Category_name").focus();
				return;
			}

			var cmd = form.elements["cmd"].value;
			$.ajax({
				url: "<?=$this->createUrl("category_save_big")?>",
				data: $(form).serialize(),
				dataType: "json",
				success: function(data) {
					if (data["result"] == "success") {
						toastr["success"]("Your operation is completed successfully.", "Success");
						console.log(data["data"]);
						zTreeObj.addNodes(null, data["data"]);
						//location.reload();
					} else {
						toastr["error"](data["message"], "Category Save Error");
					}
				}
			});
			form.elements["Category[name]"].select();
		});

		$("#category-addnew").click(function() {
			var form = $("#category-edit-form")[0];
			form.elements["cmd"].value = "insert";
			
			if (category_selected_node != null) {
				form.elements["Category[parent_id]"].value = category_selected_node.id;
	        } else {
	        	form.elements["Category[parent_id]"].value = 0;
	        }
	        form.elements["Category[id]"].value = "";
			form.elements["Category[name]"].select();

			form = $("#category-edit-form")[0];
			if (form.elements["Category[name]"].value == "" || form.elements["Category[name]"].value == undefined) {
				toastr["error"]("カテゴリ名を入力してください", "Validation error");
				$("#Category_name").focus();
				return;
			}
			if(category_selected_node == null){
				toastr["error"]("カテゴリーを選択してください", "Validation error");
				return;
			}
			if (category_selected_node != null && category_selected_node.user_id == 0) {
        		<?php if (Yii::app()->user->info->role != User::ROLE_ADMIN) {?>
            		toastr["warning"]("This category can be modified by administrator.", "Update Operation Error");
            		return;
        		<?php }?>
        	}

			var cmd = form.elements["cmd"].value;
			$.ajax({
				url: "<?=$this->createUrl("category_save")?>",
				data: $(form).serialize(),
				dataType: "json",
				success: function(data) {
					if (data["result"] == "success") {
						toastr["success"]("Your operation is completed successfully.", "Success");
						if (cmd == "update") {
							for(var i = 0, ni = Keys.length; i<ni; i++) {
								category_selected_node[Keys[i]] = data["data"][Keys[i]];
							}
							zTreeObj.updateNode(category_selected_node);
						} else {
							zTreeObj.addNodes(category_selected_node, data["data"]);
						}
					} else {
						toastr["error"](data["message"], "Category Save Error");
					}
				}
			});
			form.elements["Category[name]"].select();
		});
	
		$("#category-delete").click(function() {
			if (category_selected_node == null) {
				toastr["error"]("Please select your category to delete.", "Delete Operation Error");
	        } else {
	            if (category_selected_node.children == null || category_selected_node.children.length <= 0) {
	            	if (category_selected_node.user_id == 0) {
	            		<?php if (Yii::app()->user->info->role != User::ROLE_ADMIN) {?>
		            		toastr["warning"]("This category can be deleted by administrator.", "Delete Operation Error");
		            		return;
	            		<?php }?>
	            	}
	                if (!confirm("Are you sure to delete this item ?")) {
	                    return;
	                }
	            	$.ajax({
	                   	async: false,
	                   	url: "<?=$this->createUrl("category_delete")?>",
	                   	data: {"Category[id]": category_selected_node.id},
	                   	dataType: "json",
	                   	success: function(data) {
	                       	if (data["result"] == "success") {
	                       		zTreeObj.removeNode(category_selected_node);
	                       		category_selected_node = null;
	                       	}
	                   	}
	               	});
	            } else {
	            	toastr["warning"]("このカテゴリには子があるため、削除できません", "Delete Operation Error");
	            }
	        }
	    });
	
	    <?php if (isset($message)) { ?>
	    	toastr["error"]("<?=$message?>", "Notification");
	    <?php } ?>
	
	    //$("#category-addnew").trigger("click");
	});
</script>