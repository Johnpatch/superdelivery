<hr>
<form class="form-horizontal">
	<div class="form-group">
		<label class="control-label col-md-1">オプション</label>
		<div class="col-md-4">
			<input type="text" class="form-control" id="option_name">
		</div>
		<div class="col-md-4">
			<button type="button" class="btn btn-warning" onclick="optionSave()">
				<span class="glyphicon glyphicon-edit"></span> 保存
			</button>
		</div>
	</div>
</form>
<table class="table table-bordered table-hover table-striped" id="option-table">
	<thead>
		<tr>
			<th>No</th>
			<th>Option</th>
			<th>Edit</th>
			<th>削除</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($option_list as $no=>$item) {?>
		<tr>
			<td><?=$no + 1?></td>
			<td><?=$item->name?></td>
			<td><a href="javascript:;" onclick="optionEdit(<?=$item->id?>)">編集</a></td>
			<td><a href="javascript:;" onclick="optionDelete(<?=$item->id?>)">削除</a></td>
		</tr>
		<?php }?>
	</tbody>
</table>
<input type="hidden" id="option_id" value="0">
<script>
	$(function() {
		$("#option-table").DataTable({"lengthChange": false, "searching": false, "pageLength": 5});
	});

	function optionSave() {
		var option_id = $("#option_id").val();
		var option_name = $("#option_name").val();

		if (option_name.trim() == "") {
			toastr["warning"]("Please input the option name.", "Warning");
			$("#option_name").focus();
			return;
		}

		if (!confirm("Are you sure to save this option ?"))
			return;

		$.ajax({
			type: "post",
			url: "<?=$this->createUrl("category_option_save")?>",
			data: {id: option_id, category_id: "<?=$category_id?>", name: option_name},
			dataType: "json",
			success: function(res) {
				if (res.success) {
					toastr["success"](res.message, "Success");
					getCategoryOptionList(<?=$category_id?>);
					getCategoryOptionDetailList(<?=$category_id?>);
				} else {
					toastr["error"](res.message, "Error");
				}
			}
		});
	}

	function optionEdit(id) {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("category_option_edit")?>",
			data: "id=" + id,
			dataType: "json",
			success: function(model) {
				$("#option_id").val(model.id);
				$("#option_name").val(model.name);
			}
		});
	}

	function optionDelete(id) {
		if (!confirm("Are you sure to delete this item ?"))
			return;

		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("category_option_delete")?>",
			data: "id=" + id,
			dataType: "json",
			success: function(res) {
				if (res.success) {
					toastr["success"](res.message, "Success");
					getCategoryOptionList(<?=$category_id?>);
					getCategoryOptionDetailList(<?=$category_id?>);
				} else {
					toastr["error"](res.message, "Error");
				}
			}
		});
	}
</script>