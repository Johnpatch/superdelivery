<?php
	$arr_option = Yii::app()->util->arrayToList($option_list, "id", "name");
?>
<hr>
<form class="form-horizontal">
	<div class="form-group">
		<label class="control-label col-md-1">Option</label>
		<div class="col-md-4">
			<?=CHtml::dropDownList("option_id2", null, $arr_option, array("class"=>"form-control", "empty"=>""))?>
		</div>
		<label class="control-label col-md-1">Name</label>
		<div class="col-md-4">
			<input type="text" class="form-control" id="option_detail_name">
		</div>
		<div class="col-md-2">
			<button type="button" class="btn btn-warning" onclick="optionDetailSave()">
				<span class="glyphicon glyphicon-edit"></span> 保存
			</button>
		</div>
	</div>
</form>
<table class="table table-bordered table-hover table-striped" id="option-detail-table">
	<thead>
		<tr>
			<th>No</th>
			<th>Option</th>
			<th>Option Detail</th>
			<th>編集</th>
			<th>削除</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($option_detail_list as $no=>$item) {?>
		<tr>
			<td><?=$no + 1?></td>
			<td><?=$arr_option[$item->option_id]?></td>
			<td><?=$item->name?></td>
			<td><a href="javascript:;" onclick="optionDetailEdit(<?=$item->id?>)">編集</a></td>
			<td><a href="javascript:;" onclick="optionDetailDelete(<?=$item->id?>)">削除</a></td>
		</tr>
		<?php }?>
	</tbody>
</table>
<input type="hidden" id="option_detail_id" value="0">
<script>
	$(function() {
		$("#option-detail-table").DataTable({"lengthChange": false, "searching": false, "pageLength": 5});
	});

	function optionDetailSave() {
		var id = $("#option_detail_id").val();
		var option_id = $("#option_id2").val();
		var option_detail_name = $("#option_detail_name").val();

		if (option_id == undefined || option_id == 0) {
			toastr["warning"]("Please select the option name.", "Warning");
			$("#option_id2").focus();
			return;
		}
		if (option_detail_name.trim() == "") {
			toastr["warning"]("Please input the detail option name.", "Warning");
			$("#option_detail_name").focus();
			return;
		}

		if (!confirm("Are you sure to save this detail option ?"))
			return;

		$.ajax({
			type: "post",
			url: "<?=$this->createUrl("category_option_detail_save")?>",
			data: {id: id, option_id: option_id, category_id: "<?=$category_id?>", name: option_detail_name},
			dataType: "json",
			success: function(res) {
				if (res.success) {
					toastr["success"](res.message, "Success");
					getCategoryOptionDetailList(<?=$category_id?>);
				} else {
					toastr["error"](res.message, "Error");
				}
			}
		});
	}

	function optionDetailEdit(id) {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("category_option_detail_edit")?>",
			data: "id=" + id,
			dataType: "json",
			success: function(model) {
				$("#option_detail_id").val(model.id);
				$("#option_id2").val(model.option_id);
				$("#option_detail_name").val(model.name);
			}
		});
	}

	function optionDetailDelete(id) {
		if (!confirm("Are you sure to delete this item ?"))
			return;

		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("category_option_detail_delete")?>",
			data: "id=" + id,
			dataType: "json",
			success: function(res) {
				if (res.success) {
					toastr["success"](res.message, "Success");
					getCategoryOptionDetailList(<?=$category_id?>);
				} else {
					toastr["error"](res.message, "Error");
				}
			}
		});
	}
</script>