<div class="modal fade" id="keyword-detail-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">商品キーワード</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal">
					<div class="form-group">
		        		<label class="control-label col-md-3">顧客ID</label>
		        		<div class="col-md-9">
		                	<input type="text" id="name" class="form-control" value="<?=$model->name?>" >
		              	</div>
		            </div>
		        </form>
			</div>
			<div class="modal-footer">
				<button type="button" id="close-btn" class="btn btn-default" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 閉じる
				</button>
				<button type="button" class="btn btn-primary" onclick="saveChanges('<?=$model->name?>')">
					<span class="glyphicon glyphicon-save"></span> 変更保存
				</button>
			</div>
		</div>
	</div>
</div>
<script>
	$(function() {
		$("#keyword-detail-modal").modal();
	});

	function saveChanges(org_name) {
		var name = $('#name').val();
		if(name == ''){
			alert('商品キーワードを入力してください');
		}else{
			if (!confirm("この変更を保存しますか?"))
				return;
			$.ajax({
				type: "post",
				url: "<?=$this->createUrl("save_changes")?>",
				data: {name:name,org_name:org_name},
				dataType: "json",
				success: function(res) {
					if (res.success) {
						toastr["success"](res.message, "Success");
						$("#close-btn").trigger("click");
						setTimeout(keywordListPage, 700);
					} else {
						toastr["error"](res.message, "Error");
					}
				}
			});
		}
	}
</script>