<div class="box box-solid">
	<div class="box-header with-border">
		<h3 class="box-title">商品キーワード</h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal">
			<div class="form-group">
				<div class="col-md-2" style="float:right;">
					<button class="btn btn-primary" onclick="addKeyword()">キーワード追加</button>
				</div>
			</div>
		</form>
		<div id="keyword-list-page"></div>
		<div id="keyword-add-page"></div>
	</div>
</div>
<script>
	$(function() {
		keywordListPage();
	});

	function keywordListPage() {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("list_page")?>",
			data: "",
			success: function(page) {
				$("#keyword-list-page").html(page);
			}
		});
	}

	function addKeyword(){
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("add_page")?>",
			success: function(page) {
				$("#keyword-add-page").html(page);
			}
		});
	}
</script>