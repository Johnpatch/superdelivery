<table class="table table-bordered table-strpied table-hover" id="keyword-table">
	<thead>
		<tr>
			<th>No</th>
			<th>キーワード</th>
			<th>編集</th>
			<th>削除</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($list as $no=>$item) {?>
		<tr>
			<td><?=$no + 1?></td>
			<td><?=$item->name?></td>
			<td><a href="javascript:;" onclick="keywordDetailPage(<?=$item->id?>)">編集</a></td>
			<td><a href="javascript:;" onclick="keywordDelete(<?=$item->id?>,'<?=$item->name?>')">削除</a></td>
		</tr>
		<?php }?>
	</tbody>
</table>
<div id="keyword-detail-page"></div>
<script>
	$(function() {
		$("#keyword-table").DataTable();
	});

	function keywordDetailPage(id) {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("detail_page")?>",
			data: "id=" + id,
			success: function(page) {
				$("#keyword-detail-page").html(page);
			}
		});
	}

	function keywordDelete(id,name) {
		if (!confirm("このキーワードを本当に削除しますか?"))
			return;

		$.ajax({
			type: "post",
			url: "<?=$this->createUrl("delete")?>",
			data: {id: id, name: name},
			dataType: "json",
			success: function(data) {
				if (data.success) {
					toastr["success"](data.message, "Success");
					keywordListPage();
				} else {
					toastr["error"](data.message, "Error");
				}
			}
		});
	}
</script>