<div class="form-group">
<div class="form-horizontal">
	<div class="form-group">
		<div class="col-xs-3">
			<?php if ($product->main_image != "") {?>
            <button class="btn btn-warning" onclick="removeMainImage()">
            	<i class="fa fa-paperclip"></i> メインイメージを削除
            </button>
            <?php } else { ?>
			<div class="btn btn-default btn-file">
			メインイメージを選択
            	<input type="file" id="main_image" onchange="selectMainImage()">
            </div>
            <?php }?>
			<span class="mailbox-attachment-icon has-img" style="margin-top:10px">
				<?php if ($product->main_image != "") {?>
				<img src="<?=Yii::app()->baseUrl?>/images/main_<?=$product->id . "_" . $product->main_image?>" alt="Attachment">
				<?php }?>
			</span>
		</div>
		<div class="col-xs-9">
			<div class="btn btn-default btn-file">
			ギャラリーイメージを選択
            	<input type="file" id="gallery_image" onchange="selectGalleryImage()">
            </div>
			<div class="row">
				<?php foreach ($image_list as $no=>$item) {?>
				<div class="col-md-2">
					<span class="mailbox-attachment-icon has-img" style="margin-top:10px">
						<img src="<?=Yii::app()->baseUrl?>/images/gallery_<?=$item->product_id?>_<?=$item->image?>" alt="Attachment">
					</span>
					<br>
	                <button class="btn btn-danger" onclick="removeGalleryImage(<?=$item->id?>)">イメージ削除</button>
				</div>
				<?php }?>
			</div>
		</div>
	</div>
</div>
<script>
	function selectMainImage() {
		var formData = new FormData();
		var main_image = $("#main_image").prop("files")[0];

		formData.append("product_id", <?=$product->id?>);
		formData.append("main_image", main_image);

		$.ajax({
			type: "post",
			url: "<?=$this->createUrl("product_main_image_save")?>",
			processData: false,
			contentType: false,
			data: formData,
			dataType: "json",
			success: function(data) {
				if (data.success) {
					toastr["success"]("操作が正確に完成されました", "Select main image");
					getImagePage(<?=$product->id?>);
					getListPage();
					$("html, body").animate({ scrollTop: $(document).height() }, 1000);
				} else {
					toastr["error"]("操作が失敗しました", "Select main image");
				}
			}
		});
	}

	function removeMainImage() {
		if (!confirm("このイメージを削除しますか?"))
			return;

		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("product_main_image_delete")?>",
			data: "product_id=" + <?=$product->id?>,
			dataType: "json",
			success: function(data) {
				if (data.success) {
					toastr["success"]("操作が正確に完成されました", "Remove main image");
					getImagePage(<?=$product->id?>);
				} else {
					toastr["error"]("操作が失敗しました", "Remove main image");
				}
			}
		});
	}

	function selectGalleryImage() {
		var formData = new FormData();
		var gallery_image = $("#gallery_image").prop("files")[0];

		formData.append("product_id", <?=$product->id?>);
		formData.append("gallery_image", gallery_image);

		$.ajax({
			type: "post",
			url: "<?=$this->createUrl("product_gallery_image_save")?>",
			processData: false,
			contentType: false,
			data: formData,
			dataType: "json",
			success: function(data) {
				if (data.success) {
					toastr["success"]("操作が正確に完成されました", "Select gallery image");
					getImagePage(<?=$product->id?>);
					$("html, body").animate({ scrollTop: $(document).height() }, 1000);
				} else {
					toastr["error"]("操作が失敗しました.", "Select gallery image");
				}
			}
		});
	}

	function removeGalleryImage(id) {
		if (!confirm("このイメージを削除しますか?"))
			return;

		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("product_gallery_image_delete")?>",
			data: "id=" + id,
			dataType: "json",
			success: function(data) {
				if (data.success) {
					toastr["success"]("操作が正確に完成されました.", "Remove gallery image");
					getImagePage(<?=$product->id?>);
				} else {
					toastr["error"]("操作が失敗しました.", "Remove gallery image");
				}
			}
		});
	}
</script>