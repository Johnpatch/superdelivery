<style>
	td {
		vertical-align: middle !important;
	}
</style>
<!-- <ul class="products-list product-list-in-box">
	<?php //foreach ($list as $no=>$item) {?>
	<li class="item">
		<div class="product-img">
			<img src="<?//=Yii::app()->baseUrl?>/images/main_<?//=$item->id . "_" . $item->main_image?>" alt="Product Image">
		</div>
		<div class="product-info">
			<a href="javascript::;" class="product-title">
				<?//=$item->name?>
				<?//=$item->is_new == Product::NEW_PRODUCT ? "<span class='label label-danger pull-right'>New</span>" : ""?>
			</a>
			<span class="product-description">
				[Category]: <?//=$item->path?>,
				[SKU]: <?//=$item->sku?>,
				[SubCategory]: <?//=$item->category2?>,
				[Standard]: <?//=strip_tags($item->standard)?>,
				[SizeAndCapacity]: <?//=strip_tags($item->sizeandcapacity)?>
			</span>
		</div>
	</li>
	<?php //}?>
</ul> -->
<table class="table table-hover" id="product-table">
	<thead>
		<tr>
			<th>No</th>
			<th>イメージ</th>
			<th>カテゴリー</th>
			<th>SKU</th>
			<th>商品名</th>
			<th>値段</th>
			<th>販売価格</th>
			<th>メーカー</th>
			<th>新</th>
			<th>編集</th>
			<th>削除</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($list as $no=>$item) {?>
		<tr>
			<td><?=$no + 1?></td>
			<td>
				<?php if(!empty($item->main_image)){ ?>
					<img class="product-img" src="<?=Yii::app()->baseUrl?>/images/main_<?=$item->id . "_" . $item->main_image?>">
				<?php } ?>
			</td>
			<td><?=$item->path?></td>
			<td><?=$item->sku?></td>
			<td><?=$item->name?></td>
			<td>¥ <?=number_format($item->original_price,2)?></td>
			<td>¥ <?=number_format($item->sale_price,2)?></td>
			<td><?=$item->brand?></td>
			<td><?=$item->is_new == Product::NEW_PRODUCT ? "<span class='badge bg-red'>New</span>" : ""?></td>
			<td><a href="javascript:;" onclick="productAdd(<?=$item->id?>)">編集</a></td>
			<td><a href="javascript:;" onclick="productDelete(<?=$item->id?>)">削除</a></td>
		</tr>
		<?php }?>
	</tbody>
</table>
<script>
	$(function() {
		$("#product-table").DataTable();
	});

	function productDelete(id) {
		if (!confirm("このアイテムを本当に削除しますか?"))
			return;

		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("product_delete")?>",
			data: "id=" + id,
			dataType: "json",
			success: function(res) {
				if (res.success) {
					toastr["success"](res.message, "Success");
					getListPage(<?=$category_id?>);
				} else {
					toastr["error"](res.message, "Error");
				}
			}
		});
	}
</script>