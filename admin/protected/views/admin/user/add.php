<div class="modal fade" id="user-detail-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">New User information</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal">
					<div class="form-group">
		        		<label class="control-label col-md-3">顧客ID</label>
		        		<div class="col-md-9">
		                	<input type="text" id="userid" class="form-control">
		              	</div>
		            </div>
					<div class="form-group">
		        		<label class="control-label col-md-3">ユーザー名</label>
		        		<div class="col-md-9">
		                	<input type="text" id="username" class="form-control">
		              	</div>
		            </div>
		            <div class="form-group">
		            	<label class="control-label col-md-3">E-mail</label>
		              	<div class="col-md-9">
		                	<input type="email" id="email" class="form-control">
		              	</div>
		            </div>
					<div class="form-group">
		            	<label class="control-label col-md-3">パスワード</label>
		              	<div class="col-md-9">
		                	<input type="password" id="password" class="form-control">
		              	</div>
		            </div>
		            <div class="form-group">
		            	<label class="control-label col-md-3">電話番号</label>
		              	<div class="col-md-9">
		                	<input type="text" id="telephone" class="form-control">
		              	</div>
		            </div>
		            <div class="form-group">
		            	<label class="control-label col-md-3">アドレス</label>
		              	<div class="col-md-9">
		                	<input type="text" id="address" class="form-control">
		              	</div>
		            </div>
					<div class="form-group">
		        		<label class="control-label col-md-3">許可</label>
		        		<div class="col-md-9">
		                	<?=CHtml::dropDownList("permission", 0, User::$PERMISSION_LIST, array("class"=>"form-control"))?>
		              	</div>
		            </div>
		            <div class="form-group">
		            	<label class="control-label col-md-3">ユーザロール</label>
		              	<div class="col-md-9">
		                	<?=CHtml::dropDownList("role2", 0, User::$ROLE_LIST, array("class"=>"form-control"))?>
		              	</div>
		            </div>
		        </form>
			</div>
			<div class="modal-footer">
				<button type="button" id="close-btn" class="btn btn-default" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 閉じる
				</button>
				<button type="button" class="btn btn-primary" onclick="add_user()">
					<span class="glyphicon glyphicon-save"></span> 変更保存
				</button>
			</div>
		</div>
	</div>
</div>
<script>
	$(function() {
		$("#user-detail-modal").modal();
	});

	function add_user() {
		var permission = $("#permission").val();
		var role = $("#role2").val();
		var user_id = $('#userid').val();
		if(user_id == ''){
			alert('顧客IDを入力してください');
		} else{
			if (!confirm("ユーザーを追加しますか？"))
				return;
			$.ajax({
				type: "post",
				url: "<?=$this->createUrl("add_user")?>",
				data: {userid: $('#userid').val() ,permission: permission, role: role, username: $('#username').val(), email:$('#email').val(), telephone: $('#telephone').val(), address: $('#address').val(), password: $('#password').val()},
				dataType: "json",
				success: function(res) {
					if (res.success) {
						toastr["success"](res.message, "Success");
						$("#close-btn").trigger("click");
						setTimeout(userListPage, 700);
					} else {
						toastr["error"](res.message, "Error");
					}
				}
			});
		}
	}
</script>