<div class="box box-solid">
	<div class="box-header with-border">
		<h3 class="box-title">許可されたユーザアカウント</h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal">
			<div class="form-group">
				<label class="control-label col-md-1">ロール</label>
				<div class="col-md-2">
					<?=CHtml::dropDownList("role", null, User::$ROLE_LIST, array("class"=>"form-control", "empty"=>"", "onchange"=>"userListPage()"))?>
				</div>
				<div class="col-md-2" style="float:right;">
					<button class="btn btn-primary" onclick="addUser()">Add User</button>
				</div>
			</div>
		</form>
		<div id="user-list-page"></div>
		<div id="user-add-page"></div>
	</div>
</div>
<script>
	$(function() {
		userListPage();
	});

	function userListPage() {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("list_page")?>",
			data: "role=" + $("#role").val(),
			success: function(page) {
				$("#user-list-page").html(page);
			}
		});
	}

	function addUser(){
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("add_page")?>",
			success: function(page) {
				$("#user-add-page").html(page);
			}
		});
	}
</script>