<table class="table table-bordered table-strpied table-hover" id="user-table">
	<thead>
		<tr>
			<th>No</th>
			<th>顧客ID</th>
			<th>ユーザー名</th>
			<th>E-mail</th>
			<th>パスワード</th>
			<th>許可</th>
			<th>ロール</th>
			<th>編集</th>
			<th>削除</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($list as $no=>$item) {?>
		<tr>
			<td><?=$no + 1?></td>
			<td><?=$item->userid?></td>
			<td><?=$item->username?></td>
			<td><?=$item->email?></td>
			<td><?=$item->password?></td>
			<td><?=User::$PERMISSION_LIST[$item->permission]?></td>
			<td><?=User::$ROLE_LIST[$item->role]?></td>
			<td><a href="javascript:;" onclick="userDetailPage(<?=$item->id?>)">編集</a></td>
			<td><a href="javascript:;" onclick="userDelete(<?=$item->id?>)">削除</a></td>
		</tr>
		<?php }?>
	</tbody>
</table>
<div id="user-detail-page"></div>
<script>
	$(function() {
		$("#user-table").DataTable();
	});

	function userDetailPage(id) {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("detail_page")?>",
			data: "id=" + id,
			success: function(page) {
				$("#user-detail-page").html(page);
			}
		});
	}

	function resetPassword(id) {
		if (!confirm("このユーザーのパスワードを本当にリセットしますか ?"))
			return;

		$.ajax({
			type: "post",
			url: "<?=$this->createUrl("reset_password")?>",
			data: {id: id},
			dataType: "json",
			success: function(data) {
				if (data.success)
					toastr["success"](data.message, "Success");
				else
					toastr["error"](data.message, "Error");
			}
		});
	}

	function userDelete(id) {
		if (!confirm("このユーザーを本当に削除しますか?"))
			return;

		$.ajax({
			type: "post",
			url: "<?=$this->createUrl("delete")?>",
			data: {id: id},
			dataType: "json",
			success: function(data) {
				if (data.success) {
					toastr["success"](data.message, "Success");
					userListPage();
				} else {
					toastr["error"](data.message, "Error");
				}
			}
		});
	}
</script>