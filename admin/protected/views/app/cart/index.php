<div class="box box-solid">
	<div class="box-header with-border">
		<h3 class="box-title">あなたのカート</h3>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-xs-6">
				<div id="requested-page"></div>
			</div>
			<div class="col-xs-6">
				<div id="shipping-page"></div>
			</div>
		</div>
	</div>
</div>
<div id="product-page"></div>
<script>
	$(function() {
		getRequestedPage();
		getShippingPage();
	});

	function getRequestedPage() {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("requested_page")?>",
			success: function(page) {
				$("#requested-page").html(page);
			}
		});
	}

	function getShippingPage() {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("shipping_page")?>",
			success: function(page) {
				$("#shipping-page").html(page);
			}
		});
	}
</script>