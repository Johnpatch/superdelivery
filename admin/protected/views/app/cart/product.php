<div class="box box-solid">
	<div class="box-header with-border">
		<h3 class="box-title">全てのカート商品</h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal">
			<div class="form-group">
				<label class="control-label col-md-6" style="text-align: left"><?=$username?>の商品</label>
				<div class="col-md-6">
					<button type="button" class="btn btn-success pull-right" onclick="acceptCart()">
						<span class="glyphicon glyphicon-ok"></span> このカートを受け入れる
					</button>
				</div>
			</div>
		</form>
		<table class="table table-bordered table-hover table-striped" id="product-table">
			<thead>
				<tr>
					<th>No</th>
					<th>商品名</th>
					<th>番号</th>
					<th>ブレークダウン</th>
					<th>バーコード</th>
					<th>数量</th>
					<th>アドレス</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($product_list as $no=>$item) {?>
				<tr>
					<td><?=$no + 1?></td>
					<td><?=$item["product_name"]?></td>
					<td><?=$item["product_number"]?></td>
					<td><?=$item["breakdown"]?></td>
					<td><?=$item["barcode"]?></td>
					<td><?=$item["quantity"]?></td>
					<td><?=$item["address"]?></td>
				</tr>
				<?php }?>
			</tbody>
		</table>
	</div>
</div>
<script>
	$(function() {
		$("#product-table").DataTable();
	});

	function acceptCart() {
		if (!confirm("本当にこのカートを受け入れますか?"))
			return;

		$.ajax({
			type: "post",
			url: "<?=$this->createUrl("accept_cart")?>",
			data: {username: "<?=$username?>"},
			dataType: "json",
			success: function(res) {
				if (res.success) {
					toastr["success"](res.message, "Success");
					getRequestedPage();
					getShippingPage();
					$("#product-page").html("");
				} else {
					toastr["error"](res.message, "Error");
				}
			}
		});
	}
</script>