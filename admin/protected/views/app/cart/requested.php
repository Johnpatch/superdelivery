<table class="table table-bordered table-hover table-striped" id="requested-table">
	<thead>
		<tr>
			<th>No</th>
			<th>ユーザー名</th>
			<th>ステータス</th>
			<th>アックション</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($requested_list as $no=>$item) {?>
		<tr>
			<td><?=$no + 1?></td>
			<td><?=$item->username?></td>
			<td><span class='badge bg-red'><?=Deliver::$STATUS_LIST[$item->status]?></span></td>
			<td><a href="javascript:;" onclick="viewProducts('<?=$item->username?>')">全ての商品を見せる</a></td>
		</tr>
		<?php }?>
	</tbody>
</table>
<script>
	$(function() {
		$("#requested-table").DataTable();
	});

	function viewProducts(username) {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("product_page")?>",
			data: "username=" + username,
			success: function(page) {
				$("#product-page").html(page);
			}
		});
	}
</script>