<table class="table table-bordered table-hover table-striped" id="shipping-table">
	<thead>
		<tr>
			<th>No</th>
			<th>ユーザー名</th>
			<th>ステータス</th>
			<th>アックション</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($shipping_list as $no=>$item) {?>
		<tr>
			<td><?=$no + 1?></td>
			<td><?=$item->username?></td>
			<td><span class='badge bg-green'><?=Deliver::$STATUS_LIST[$item->status]?></span></td>
			<td><a href="javascript:;" onclick="completeCart('<?=$item->username?>')">完成</a></td>
		</tr>
		<?php }?>
	</tbody>
</table>
<script>
	$(function() {
		$("#shipping-table").DataTable();
	});

	function completeCart(username) {
		if (!confirm("本当にカートを完了しますか？"))
			return;

		$.ajax({
			type: "post",
			url: "<?=$this->createUrl("complete_cart")?>",
			data: {username: username},
			dataType: "json",
			success: function(res) {
				if (res.success) {
					toastr["success"](res.message, "Success");
					getShippingPage();
				} else {
					toastr["error"](res.message, "Error");
				}
			}
		});
	}
</script>