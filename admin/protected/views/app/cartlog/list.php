<table class="table table-bordered table-hover table-striped" id="cartlog-table">
	<thead>
		<tr>
			<th>No</th>
			<th>ユーザー名</th>
			<th>商品名</th>
			<th>商品番号</th>
			<th>数量</th>
			<th>アドレス</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($list as $no=>$item) {?>
		<tr>
			<td><?=$no + 1?></td>
			<td><?=$item["username"]?></td>
			<td><?=$item["product_name"]?></td>
			<td><?=$item["product_number"]?></td>
			<td><?=$item["quantity"]?></td>
			<td><?=$item["address"]?></td>
		</tr>
		<?php }?>
	</tbody>
</table>
<script>
	$(function() {
		$("#cartlog-table").DataTable();
	});
</script>