<form class="form-horizontal" autocomplete="off">
	<div class="form-group">
		<label class="control-label col-md-2">ブランド</label>
		<div class="col-md-7">
			<input type="text" class="form-control" id="brand_name">
		</div>
		<div class="col-md-3">
			<button type="button" class="btn btn-primary" onclick="brandSave()">
				<span class="glyphicon glyphicon-save"></span> 保存
			</button>
		</div>
	</div>
</form>
<table class="table table-bordered table-hover table-striped" id="brand-table">
	<thead>
		<tr>
			<th>No</th>
			<th>ブランド名</th>
			<th>編集</th>
			<th>削除</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($brand_list as $no=>$item) {?>
		<tr>
			<td><?=$no + 1?></td>
			<td><?=$item->name?></td>
			<td><a href="javascript:;" onclick="brandEdit(<?=$item->id?>)">編集</a></td>
			<td><a href="javascript:;" onclick="brandDelete(<?=$item->id?>)">削除</a></td>
		</tr>
		<?php }?>
	</tbody>
</table>
<input type="hidden" id="brand_id" value="0">
<script>
	$(function() {
		$("#brand-table").DataTable();
	});

	function brandSave() {
		var brand_id = $("#brand_id").val();
		var brand_name = $("#brand_name").val();

		if (brand_name.trim() == "") {
			toastr["warning"]("ブランド名を入力してください", "Warning");
			$("#brand_name").focus();
			return;
		}

		if (!confirm("このブランドを本当に保存しますか?"))
			return;

		$.ajax({
			type: "post",
			url: "<?=$this->createUrl("brand_save")?>",
			data: {id: brand_id, name: brand_name},
			dataType: "json",
			success: function(res) {
				if (res.success) {
					toastr["success"](res.message, "Success");
					getBrandPage();
				} else {
					toastr["error"](res.message, "Error");
				}
			}
		});
	}

	function brandEdit(id) {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("brand_edit")?>",
			data: "id=" + id,
			dataType: "json",
			success: function(model) {
				$("#brand_id").val(model.id);
				$("#brand_name").val(model.name);
			}
		});
	}

	function brandDelete(id) {
		if (!confirm("このアイテムを本当に削除しますか?"))
			return;

		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("brand_delete")?>",
			data: "id=" + id,
			dataType: "json",
			success: function(res) {
				if (res.success) {
					toastr["success"](res.message, "Success");
					getBrandPage();
				} else {
					toastr["error"](res.message, "Error");
				}
			}
		});
	}
</script>