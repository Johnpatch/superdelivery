<form class="form-horizontal" autocomplete="off">
	<div class="form-group">
		<label class="control-label col-md-2">カテゴリ一</label>
		<div class="col-md-7">
			<input type="text" class="form-control" id="category_name">
		</div>
		<div class="col-md-3">
			<button type="button" class="btn btn-primary" onclick="categorySave()">
				<span class="glyphicon glyphicon-save"></span> 保存
			</button>
		</div>
	</div>
</form>
<table class="table table-bordered table-hover table-striped" id="category-table">
	<thead>
		<tr>
			<th>No</th>
			<th>カテゴリ一名</th>
			<th>編集</th>
			<th>削除</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($category_list as $no=>$item) {?>
		<tr>
			<td><?=$no + 1?></td>
			<td><?=$item->name?></td>
			<td><a href="javascript:;" onclick="categoryEdit(<?=$item->id?>)">編集</a></td>
			<td><a href="javascript:;" onclick="categoryDelete(<?=$item->id?>)">削除</a></td>
		</tr>
		<?php }?>
	</tbody>
</table>
<input type="hidden" id="category_id" value="0">
<script>
	$(function() {
		$("#category-table").DataTable();
	});

	function categorySave() {
		var category_id = $("#category_id").val();
		var category_name = $("#category_name").val();

		if (category_name.trim() == "") {
			toastr["warning"]("Please input the category name.", "Warning");
			$("#category_name").focus();
			return;
		}

		if (!confirm("このカテゴリを本当に保存しますか?"))
			return;

		$.ajax({
			type: "post",
			url: "<?=$this->createUrl("category_save")?>",
			data: {id: category_id, name: category_name},
			dataType: "json",
			success: function(res) {
				if (res.success) {
					toastr["success"](res.message, "Success");
					getCategoryPage();
				} else {
					toastr["error"](res.message, "Error");
				}
			}
		});
	}

	function categoryEdit(id) {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("category_edit")?>",
			data: "id=" + id,
			dataType: "json",
			success: function(model) {
				$("#category_id").val(model.id);
				$("#category_name").val(model.name);
			}
		});
	}

	function categoryDelete(id) {
		if (!confirm("このアイテムを本当に削除しますか?"))
			return;

		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("category_delete")?>",
			data: "id=" + id,
			dataType: "json",
			success: function(res) {
				if (res.success) {
					toastr["success"](res.message, "Success");
					getCategoryPage();
				} else {
					toastr["error"](res.message, "Error");
				}
			}
		});
	}
</script>