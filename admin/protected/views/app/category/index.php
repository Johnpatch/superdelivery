<div class="box box-solid">
	<div class="box-header with-border">
		<h3 class="box-title">カストムのカテゴリ＆ブランド</h3>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-xs-6">
				<div id="category-page"></div>
			</div>
			<div class="col-xs-6">
				<div id="brand-page"></div>
			</div>
		</div>
	</div>
</div>
<script>
	$(function() {
		getCategoryPage();
		getBrandPage();
	});

	function getCategoryPage() {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("category_page")?>",
			success: function(page) {
				$("#category-page").html(page);
			}
		});
	}

	function getBrandPage() {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("brand_page")?>",
			success: function(page) {
				$("#brand-page").html(page);
			}
		});
	}
</script>