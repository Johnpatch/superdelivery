<table class="table table-bordered table-hover table-striped" id="counterpart-table">
	<thead>
		<tr>
			<th>No</th>
			<th>ユーザー名</th>
			<th>創造時間</th>
			<th>商売相手状況</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($list as $no=>$item) {?>
		<tr>
			<td><?=$no + 1?></td>
			<td><?=$item->customer_username?></td>
			<td><?=$item->created_at?></td>
			<td><?=CHtml::radioButtonList("status_" . $item->id, $item->status, Price::$STATUS_LIST, array("separator"=>"&nbsp;", "onchange"=>"changeStatus($item->id)"))?></td>
		</tr>
		<?php }?>
	</tbody>
</table>
<script>
	$(function() {
		$("#counterpart-table").DataTable();
	});

	function changeStatus(id) {
		if (!confirm("本当にこの変更を保存しますか?"))
			return;

		var status = 1;
		if ($("#status_" + id + "_1").prop("checked"))
			status = 2;

		$.ajax({
			type: "post",
			url: "<?=$this->createUrl("change_status")?>",
			data: {id: id, status: status},
			dataType: "json",
			success: function(res) {
				if (res.success)
					toastr["success"](res.message, "Success");
				else
					toastr["error"](res.message, "Error");
			}
		});
	}
</script>