<div class="box box-solid">
	<div class="box-header with-border">
		<h3 class="box-title">取引条件</h3>
		<button type="button" class="btn btn-default btn-xs pull-right" onclick="showModal(<?=Company::TYPE_TERMS?>)">
			<span class="glyphicon glyphicon-pencil"></span>
		</button>
	</div>
	<div class="box-body">
		<div id="terms-page"></div>
	</div>
</div>
<div class="box box-solid">
	<div class="box-header with-border">
		<h3 class="box-title">送料・決済方法</h3>
		<button type="button" class="btn btn-default btn-xs pull-right" onclick="showModal(<?=Company::TYPE_SHIPPING?>)">
			<span class="glyphicon glyphicon-pencil"></span>
		</button>
	</div>
	<div class="box-body">
		<div id="shipping-page"></div>
	</div>
</div>
<div class="box box-solid">
	<div class="box-header with-border">
		<h3 class="box-title">企業情報</h3>
		<button type="button" class="btn btn-default btn-xs pull-right" onclick="showModal(<?=Company::TYPE_ENTERPRISE?>)">
			<span class="glyphicon glyphicon-pencil"></span>
		</button>
	</div>
	<div class="box-body">
		<div id="enterprise-page"></div>
	</div>
</div>
<div id="modal-page"></div>
<script>
	$(function() {
		getContentPage(<?=Company::TYPE_TERMS?>);
		getContentPage(<?=Company::TYPE_SHIPPING?>);
		getContentPage(<?=Company::TYPE_ENTERPRISE?>);
	});

	function getContentPage(type) {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("detail_page")?>",
			data: "type=" + type,
			success: function(page) {
				if (type == <?=Company::TYPE_TERMS?>)
					$("#terms-page").html(page);
				else if (type == <?=Company::TYPE_SHIPPING?>)
					$("#shipping-page").html(page);
				else if (type == <?=Company::TYPE_ENTERPRISE?>)
					$("#enterprise-page").html(page);
			}
		});
	}

	function showModal(type) {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("modal_page")?>",
			data: "type=" + type,
			success: function(page) {
				$("#modal-page").html(page);
			}
		});
	}
</script>