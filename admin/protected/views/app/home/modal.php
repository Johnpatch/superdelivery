<div class="modal fade" id="company-modal">
	<div class="modal-dialog" style="width:1000px">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title"><?=Company::$TYPE_LIST[$type]?></h3>
			</div>
			<div class="modal-body">
				<textarea class="form-control input-sm" id="description" style="height:350px">
					<?=$model != null ? $model->description : ""?>
				</textarea>
			</div>
			<div class="modal-footer">
				<button type="button" id="close-btn" class="btn btn-default" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 閉じる
				</button>
				<button type="button" class="btn btn-primary pull-right" onclick="informationSave()">
					<span class="glyphicon glyphicon-save"></span> 情報保存
				</button>
			</div>
		</div>
	</div>
</div>
<input type="hidden" id="type" value="<?=$type?>">
<script>
	$(function() {
		$("#description").wysihtml5({
			toolbar: {
		        "font-styles": true,
		        "color": false,
		        "emphasis": {
		        	"small": true
		        },
		        "blockquote": false,
		        "lists": false,
		        "html": false,
		        "link": false,
		        "image": false,
		        "smallmodals": false
		      }
		});
		
		$("#company-modal").modal();
	});

	function informationSave() {
		var type = $("#type").val();
		var description = $("#description").val();

		if (description.trim() == "") {
			toastr["warning"]("あなたの会社情報を入力してください", "Warning");
			return;
		}

		if (!confirm("この情報を本当に保存しますか?"))
			return;

		$.ajax({
			type: "post",
			url: "<?=$this->createUrl("information_save")?>",
			data: {type: type, description: description},
			dataType: "json",
			success: function(data) {
				if (data.success) {
					toastr["success"](data.message, "Success");
					$("#close-btn").trigger("click");

					if (type == <?=Company::TYPE_TERMS?>)
						setTimeout(getContentPage(<?=Company::TYPE_TERMS?>), 700);
					else if (type == <?=Company::TYPE_SHIPPING?>)
						setTimeout(getContentPage(<?=Company::TYPE_SHIPPING?>), 700);
					else if (type == <?=Company::TYPE_ENTERPRISE?>)
						setTimeout(getContentPage(<?=Company::TYPE_ENTERPRISE?>), 700);
				} else {
					toastr["error"](data.message, "Error");
				}
			}
		});
	}
</script>