<div class="modal fade" id="user-detail-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><?=$user->username?>の情報</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal">
					<div class="form-group">
		        		<label class="control-label col-md-3">ユーザー名</label>
		        		<div class="col-md-9">
		                	<input type="text" class="form-control" value="<?=$user->username?>" disabled>
		              	</div>
		            </div>
		            <div class="form-group">
		            	<label class="control-label col-md-3">電話番号</label>
		              	<div class="col-md-9">
		                	<input type="text" class="form-control" value="<?=$user->telephone?>" disabled>
		              	</div>
		            </div>
		            <div class="form-group">
		            	<label class="control-label col-md-3">アドレス</label>
		              	<div class="col-md-9">
		                	<input type="text" class="form-control" value="<?=$user->address?>" disabled>
		              	</div>
		            </div>
		            <div class="form-group">
		            	<label class="control-label col-md-3">創造時間</label>
		              	<div class="col-md-9">
		                	<input type="text" class="form-control" value="<?=$user->created_at?>" disabled>
		              	</div>
		            </div>
					<div class="form-group">
		        		<label class="control-label col-md-3">許可</label>
		        		<div class="col-md-9">
		                	<?=CHtml::dropDownList("permission", $user->permission, User::$PERMISSION_LIST, array("class"=>"form-control", "disabled"=>true))?>
		              	</div>
		            </div>
		            <div class="form-group">
		            	<label class="control-label col-md-3">ユーザロール</label>
		              	<div class="col-md-9">
		                	<?=CHtml::dropDownList("role2", $user->role, User::$ROLE_LIST, array("class"=>"form-control", "disabled"=>true))?>
		              	</div>
		            </div>
		        </form>
			</div>
			<div class="modal-footer">
				<button type="button" id="close-btn" class="btn btn-default" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 閉じる
				</button>
			</div>
		</div>
	</div>
</div>
<script>
	$(function() {
		$("#user-detail-modal").modal();
	});
</script>