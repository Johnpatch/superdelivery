<div class="box box-solid">
	<div class="box-header with-border">
		<h3 class="box-title">最終要求値段</h3>
	</div>
	<div class="box-body">
		<div id="list-page"></div>
	</div>
</div>
<div id="product-page"></div>
<script>
	$(function() {
		getListPage();
	});

	function getListPage() {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("list_page")?>",
			success: function(page) {
				$("#list-page").html(page);
			}
		});
	}
</script>