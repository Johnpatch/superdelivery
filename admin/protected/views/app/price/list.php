<table class="table table-bordered table-striped table-hover" id="price-table">
	<thead>
		<tr>
			<th>No</th>
			<th>お客様</th>
			<th>創造時間</th>
			<th>オープン単価</th>
			<th>詳細</th>
			<th>注文ログ</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($customer_list as $no=>$item) {?>
		<tr>
			<td><?=$no + 1?></td>
			<td><?=$item->customer_username?></td>
			<td><?=$item->created_at?></td>
			<td><a href="javascript:;" onclick="openPrice('<?=$item->customer_username?>')">オープン単価</a></td>
			<td><a href="javascript:;" onclick="viewDetail('<?=$item->customer_username?>')">詳しく見る</a></td>
			<td><a href="javascript:;" onclick="viewOrderLog('<?=$item->customer_username?>')">注文ログ閲覧</a></td>
		</tr>
		<?php }?>
	</tbody>
</table>
<div id="view-detail-panel"></div>
<div id="view-orderlog-panel"></div>
<script>
	$(function() {
		$("#price-table").DataTable({"pageLength": 5});
	});

	function openPrice(username) {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("open_price")?>",
			data: "username=" + username,
			success: function(page) {
				$("#product-page").html(page);
			}
		});
	}

	function viewDetail(username) {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("view_detail")?>",
			data: "username=" + username,
			success: function(page) {
				$("#view-detail-panel").html(page);
			}
		});
	}

	function viewOrderLog(username) {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("view_orderlog")?>",
			data: "username=" + username,
			success: function(page) {
				$("#view-orderlog-panel").html(page);
			}
		});
	}
</script>