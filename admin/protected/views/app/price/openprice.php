<div class="box box-solid">
	<div class="box-header with-border">
		<h3 class="box-title">私の商品</h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal">
			<div class="form-group">
				<div class="col-xs-12">
					<button type="button" class="btn btn-warning pull-right" onclick="acceptPrice()">
						<span class="glyphicon glyphicon-pencil"></span> この商品の値段を公開
					</button>
				</div>
			</div>
		</form>
		<table class="table table-bordered table-striped table-hover" id="product-table">
			<thead>
				<tr>
					<th>No</th>
					<th>カテゴリ一</th>
					<th>サブカテゴリー</th>
					<th>商品名</th>
					<th>番号</th>
					<th>小売</th>
					<th>卸売</th>
					<th>アックション</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($product_list as $no=>$item) {?>
				<tr>
					<td><?=$no + 1?></td>
					<td><?=$item["category"]?></td>
					<td><?=$item["category2"]?></td>
					<td><?=$item["name"]?></td>
					<td><?=$item["product_number"]?></td>
					<td><?=$item["retail_price"]?></td>
					<td><?=$item["wholesale_price"]?></td>
					<td><a href="javascript:;" onclick="productDetail(<?=$item["id"]?>)">詳しく見る</a></td>
				</tr>
				<?php }?>
			</tbody>
		</table>
	</div>
</div>
<div id="discuss-page"></div>
<script>
	$(function() {
		$("#product-table").DataTable();
	});

	function productDetail(id) {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("product_detail")?>",
			data: "id=" + id + "&username=" + "<?=$username?>",
			success: function(page) {
				$("#discuss-page").html(page);
			}
		});
	}

	function acceptPrice() {
		if (!confirm("この商品の値段を公開しますか？"))
			return;

		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("accept_price")?>",
			data: "username=" + "<?=$username?>",
			dataType: "json",
			success: function(data) {
				if (data.success) {
					toastr["success"](data.message, "Success");
					$("#product-page").html("");
					getListPage();
				}
				else
					toastr["error"](data.message, "Error");
			}
		});
	}
</script>