<div class="modal fade" id="order-log-modal">
	<div class="modal-dialog" style="min-width:1300px">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><?=$username?>の注文ログ</h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered table-striped table-hover" id="order-log-table">
					<thead>
						<tr>
							<th>No</th>
							<th>日付</th>
							<th>カテゴリ一</th>
							<th>サブカテゴリー</th>
							<th>SKU</th>
							<th>名前</th>
							<th>商品番号</th>
							<th>ブレークダウン</th>
							<th>バーコード</th>
							<th>Jan</th>
							<th>数量</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($list as $no=>$item) {?>
						<tr>
							<td><?=$no + 1?></td>
							<td><?=$item["date"]?></td>
							<td><?=$item["category"]?></td>
							<td><?=$item["category2"]?></td>
							<td><?=$item["sku"]?></td>
							<td><?=$item["name"]?></td>
							<td><?=$item["product_number"]?></td>
							<td><?=$item["breakdown"]?></td>
							<td><?=$item["barcode"]?></td>
							<td><?=$item["jan"]?></td>
							<td><?=$item["quantity"]?></td>
						</tr>
						<?php }?>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" id="close-btn" class="btn btn-default" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 閉じる
				</button>
			</div>
		</div>
	</div>
</div>
<script>
	$(function() {
		$("#order-log-modal").modal();
		$("#order-log-table").DataTable();
	});
</script>