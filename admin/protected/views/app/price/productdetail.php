<div class="modal fade" id="discuss-modal">
	<div class="modal-dialog" style="min-width:1000px">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">価格討論</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal">
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<label class="control-label col-md-4">カテゴリ一</label>
								<div class="col-md-8">
									<input type="text" class="form-control" disabled value="<?=$category->name?>">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">サブカテゴリー</label>
								<div class="col-md-8">
									<input type="text" class="form-control" disabled value="<?=$category2->name?>">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">ブランド</label>
								<div class="col-md-8">
									<input type="text" class="form-control" disabled value="<?=$brand->name?>">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">SKU番号</label>
								<div class="col-md-8">
									<input type="text" class="form-control" disabled value="<?=$product->sku?>">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">名前</label>
								<div class="col-md-8">
									<input type="text" class="form-control" disabled value="<?=$product->name?>">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">番号</label>
								<div class="col-md-8">
									<input type="text" class="form-control" disabled value="<?=$detail->product_number?>">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">ブレークダウン</label>
								<div class="col-md-8">
									<input type="text" class="form-control" disabled value="<?=$detail->breakdown?>">
								</div>
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
								<label class="control-label col-md-4">バーコード</label>
								<div class="col-md-8">
									<input type="text" class="form-control" disabled value="<?=$detail->barcode?>">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">JAN</label>
								<div class="col-md-8">
									<input type="text" class="form-control" disabled value="<?=$detail->jan?>">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">小売タイプ</label>
								<div class="col-md-8">
									<input type="text" class="form-control" disabled value="<?=Product_detail::$TYPE_LIST[$detail->retail_type]?>">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">小売値段</label>
								<div class="col-md-8">
									<input type="number" class="form-control" id="retail_price" value="<?=$detail->retail_price?>">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">卸売値段</label>
								<div class="col-md-8">
									<input type="number" class="form-control" id="wholesale_price" value="<?=$detail->wholesale_price?>">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">卸売タイプ</label>
								<div class="col-md-8">
									<div class="checkbox">
										<label>
											<input type="checkbox" <?=$detail->wholesale_type ? "checked" : ""?> disabled> メンバーだけに見せる
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" onclick="modifyPrice()">
					<span class="glyphicon glyphicon-refresh"></span> この価格を改修
				</button>
				<button type="button" id="discuss-close-btn" class="btn btn-default" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 閉じる
				</button>
			</div>
		</div>
	</div>
</div>
<script>
	$(function() {
		$("#discuss-modal").modal();
	});

	function modifyPrice() {
		var retail_price = $("#retail_price").val();
		var wholesale_price = $("#wholesale_price").val();

		if (retail_price == 0) {
			toastr["warning"]("子売単価を入力してください", "Warning");
			return;
		}
		if (wholesale_price == 0) {
			toastr["warning"]("卸値を入力してください", "Warning");
			return;
		}

		if (!confirm("本当にこの商品価格を改修しますか?"))
			return;

		$.ajax({
			type: "post",
			url: "<?=$this->createUrl("modify_price")?>",
			data: {username: "<?=$username?>", product_id: "<?=$product->id?>", product_detail_id: "<?=$detail->id?>", retail_price: retail_price, wholesale_price: wholesale_price},
			dataType: "json",
			success: function(res) {
				if (res.success) {
					toastr["success"](res.message, "Success");
					$("#discuss-close-btn").trigger("click");
					setTimeout(openPrice, 700, "<?=$username?>");
				} else
					toastr["error"](res.message, "Error");
			}
		});
	}
</script>