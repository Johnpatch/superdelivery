<div class="box box-solid">
	<div class="box-header with-border">
		<h3 class="box-title">商品追加</h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal">
			<div class="form-group">
				<label class="control-label col-md-2">商品カテゴリー</label>
				<div class="col-md-10">
					<input type="text" class="form-control" id="add_category" >
					<input type="hidden" id="add_category_id" name="add_category_id" value="<?=$product != null ? $product->category_id : ""?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">メーカー</label>
				<div class="col-md-9">
					<select class="form-control" id="brand_id">
						<option value="0">&nbsp;</option>
						<?php foreach ($brand_list as $no=>$item) {?>
						<option <?=$product != null && $product->brand_id == $item->id ? "selected" : ""?> value="<?=$item->id?>"><?=$item->name?></option>
						<?php }?>
					</select>
				</div>
				<div class="col-md-1">
					<button type="button" class="btn btn-primary btn-block" onclick="$('#new-brand-page').css('display', 'block');$('#new_brand').focus()">追加</button>
				</div>
			</div>
			<div class="form-group" id="new-brand-page" style="display:none">
				<label class="control-label col-md-2">新メーカー</label>
				<div class="col-md-9">
					<input type="text" class="form-control" id="new_brand">
				</div>
				<div class="col-md-1">
					<button type="button" class="btn btn-warning btn-block" onclick="brandSave()">保存</button>
				</div>
			</div>
			<div id="product-option-page"></div>
			<div class="form-group">
				<label class="control-label col-md-2">商品名</label>
				<div class="col-md-10">
					<input type="text" class="form-control" id="product_name" value="<?=$product != null ? $product->name : ""?>" >
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">SKU番号</label>
				<div class="col-md-10">
					<input type="text" class="form-control" id="sku" value="<?=$product != null ? $product->sku : ""?>" >
				</div>
			</div>
			<!--div class="form-group">
				<label class="control-label col-md-2">カストムカテゴリ</label>
				<div class="col-md-9">
					<select class="form-control" id="category_id2">
						<option value="0">&nbsp;</option>
						<?php foreach ($category_list2 as $no=>$item) {?>
						<option <?=$product != null && $product->category_id2 == $item->id ? "selected" : ""?> value="<?=$item->id?>"><?=$item->name?></option>
						<?php }?>
					</select>
				</div>
				<div class="col-md-1">
					<button type="button" class="btn btn-primary btn-block" onclick="$('#new-category2-page').css('display', 'block');$('#new_category2').focus()">追加</button>
				</div>
			</div>
			<div class="form-group" id="new-category2-page" style="display:none">
				<label class="control-label col-md-2">新カテゴリー</label>
				<div class="col-md-9">
					<input type="text" class="form-control" id="new_category2">
				</div>
				<div class="col-md-1">
					<button type="button" class="btn btn-warning btn-block" onclick="category2Save()">保存</button>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">商品ブランド</label>
				<div class="col-md-9">
					<select class="form-control" id="brand_id">
						<option value="0">&nbsp;</option>
						<?php foreach ($brand_list as $no=>$item) {?>
						<option <?=$product != null && $product->brand_id == $item->id ? "selected" : ""?> value="<?=$item->id?>"><?=$item->name?></option>
						<?php }?>
					</select>
				</div>
				<div class="col-md-1">
					<button type="button" class="btn btn-primary btn-block" onclick="$('#new-brand-page').css('display', 'block');$('#new_brand').focus()">追加</button>
				</div>
			</div-->
			<div class="form-group" id="new-brand-page" style="display:none">
				<label class="control-label col-md-2">新ブランド</label>
				<div class="col-md-9">
					<input type="text" class="form-control" id="new_brand">
				</div>
				<div class="col-md-1">
					<button type="button" class="btn btn-warning btn-block" onclick="brandSave()">保存</button>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">商品キーワード</label>
				<div class="col-md-9">
					<select class="form-control" id="keywords" multiple="multiple" >
						<?php foreach ($keywords_list as $no=>$item) {?>
						<option <?=$product != null && stripos($product->keywords, $item->name) !== false ? "selected" : ""?> value="<?=$item->name?>"><?=$item->name?></option>
						<?php }?>
					</select>
				</div>
				<div class="col-md-1">
					<button type="button" class="btn btn-primary btn-block" onclick="$('#new-keyword-page').css('display', 'block');$('#new_keyword').focus()" >追加</button>
				</div>
			</div>
			<div class="form-group" id="new-keyword-page" style="display:none">
				<label class="control-label col-md-2">新キーワード</label>
				<div class="col-md-9">
					<input type="text" class="form-control" id="new_keyword" >
				</div>
				<div class="col-md-1">
					<button type="button" class="btn btn-warning btn-block" onclick="keywordSave()" >Save</button>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">単価</label>
				<div class="col-md-10">
					<input type="number" class="form-control" id="original_price" value="<?=$product != null ? $product->original_price : ""?>" >
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">商品説明</label>
				<div class="col-md-10">
					<textarea class="form-control" id="description" ><?=$product != null ? $product->description : ""?></textarea>
				</div>
			</div>
			<!--div class="form-group">
				<label class="control-label col-md-2">運送方法</label>
				<div class="col-md-10">
					<textarea class="form-control" id="shipping"><?=$product != null ? $product->shipping : ""?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">サイズ＆容量</label>
				<div class="col-md-10">
					<textarea class="form-control" id="sizeandcapacity"><?=$product != null ? $product->sizeandcapacity : ""?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">商品スタンダード</label>
				<div class="col-md-10">
					<textarea class="form-control" id="standard"><?=$product != null ? $product->standard : ""?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">商品メモ</label>
				<div class="col-md-10">
					<textarea class="form-control" id="notes"><?=$product != null ? $product->notes : ""?></textarea>
				</div>
			</div-->
			<div class="form-group">
				<div class="row">
					<!--div class="col-xs-6">
						<label class="control-label col-md-4">イメージ表示</label>
						<div class="col-md-8">
							<div class="checkbox">
								<label>
									<input type="checkbox" id="is_show_image" <?=$product != null && $product->is_show_image ? "checked" : ""?>>
									アンログインユーザーに表示
								</label>
							</div>
						</div>
					</div-->
					<div class="col-xs-6">
						<label class="control-label col-md-4">商品タイプ</label>
						<div class="col-md-6">
							<div class="checkbox">
								<label>
									<input type="checkbox" id="is_new" <?=$product != null && $product->is_new ? "checked" : ""?> >
									これは新たな商品です.
								</label>
							</div>
						</div>
						<div class="col-md-2">
							<button type="button" class="btn btn-success pull-right" onclick="productSave()" >
								<span class="glyphicon glyphicon-save"></span> 商品追加
							</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<?php if ($product != null) {?>
<div class="box box-solid">
	<div class="box-header with-border">
		<h3 class="box-title">商品イメージ</h3>
	</div>
	<div class="box-body">
		<div id="product-image-page"></div>
	</div>
</div>
<!--div class="box box-solid">
	<div class="box-header with-border">
		<h3 class="box-title">商品説明</h3>
	</div>
	<div class="box-body">
		<div id="product-detail-page"></div>
	</div>
</div-->
<div class="row add_product_section" style="display: none;">
	<div class="col-md-3">
		<button type="button" class="btn btn-primary" onclick="productAdd()">
			<span class="glyphicon glyphicon-pencil"></span> 商品追加
		</button>
	</div>
</div>
<?php }?>

<input type="hidden" id="product_id" value="<?=$product == null ? 0 : $product->id?>">

<script>
	var setting = {
		data: {
			key: {
				name: "name", 
			},
			simpleData: {
				enable: true,
				idKey: "id",
				pIdKey: "parent_id",
			},
		},
		view: {
			showIcon: false
		}
	};

	var zAddNodes = <?php echo CJSON::encode($categories)?>;
	var arrOptions = new Array();

	$(function() {
		zTreeSelector.init($("#add_category_id"), $("#add_category"), setting, zAddNodes, {
			callback: function(id, value, node) {
				getOptionPage(id);
			}
		});

		$("#description").wysihtml5();
		$("#shipping").wysihtml5();
		$("#sizeandcapacity").wysihtml5();
		$("#standard").wysihtml5();
		$("#notes").wysihtml5();

		$("#category_id2").select2();
		$("#brand_id").select2();
		$("#keywords").select2();

		<?php if ($product != null) {?>
			getOptionPage(<?=$product->category_id?>);
			getImagePage();
			//getDetailPage();
		<?php }?>
	});

	function getOptionPage(category_id) {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("option_page")?>",
			data: "category_id=" + category_id + "&product_id=" + $("#product_id").val(),
			success: function(page) {
				$("#product-option-page").html(page);
			}
		});
	}

	function getImagePage() {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("image_page")?>",
			data: "product_id=" + $("#product_id").val(),
			success: function(page) {
				$("#product-image-page").html(page);
				$('.add_product_section').css('display','block');
			}
		});
	}

	function getDetailPage() {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("detail_page")?>",
			data: "product_id=" + $("#product_id").val(),
			success: function(page) {
				$("#product-detail-page").html(page);
			}
		});
	}

	function category2Save() {
		var name = $("#new_category2").val();
		if (name.trim() == "") {
			toastr["warning"]("サブカテゴリ名を入力してください", "Warning");
			$("#new_category2").focus();
			return;
		}
		if (!confirm("このカテゴリを本当に保存しますか?"))
			return;

		$.ajax({
			type: "post",
			url: "<?=$this->createUrl("new_category_save")?>",
			data: {name: name},
			dataType: "json",
			success: function(res) {
				if (res.success) {
					toastr["success"](res.message, "Success");
					$("#new_category2").val("");
					$("#new_category2").focus();
					$("#category_id2").append("<option selected value='" + res.model.id + "'>" + res.model.name + "</option>");

					$("#category_id2").select2().val(res.model.id);
				} else {
					toastr["error"](res.message, "Error");
				}
			}
		});
	}

	function brandSave() {
		var name = $("#new_brand").val();
		if (name.trim() == "") {
			toastr["warning"]("ブランド名を入力してください.", "Warning");
			$("#new_brand").focus();
			return;
		}
		if (!confirm("このブランドを本当に保存しますか?"))
			return;

		$.ajax({
			type: "post",
			url: "<?=$this->createUrl("new_brand_save")?>",
			data: {name: name},
			dataType: "json",
			success: function(res) {
				if (res.success) {
					toastr["success"](res.message, "Success");
					$("#new_brand").val("");
					$("#new_brand").focus();
					$("#brand_id").append("<option selected value='" + res.model.id + "'>" + res.model.name + "</option>");

					$("#brand_id").select2().val(res.model.id);
				} else {
					toastr["error"](res.message, "Error");
				}
			}
		});
	}

	function keywordSave() {
		var name = $("#new_keyword").val();
		if (name.trim() == "") {
			toastr["warning"]("キーワードを入力してください", "Warning");
			$("#new_keyword").focus();
			return;
		}

		$("#new_keyword").val("");
		$("#new_keyword").focus();

		$("#keywords").append("<option selected value='" + name + "'>" + name + "</option>");
		$("#keywords").select2();
	}

	function productSave() {
		var category_id = $("#add_category_id").val();
		var category_name = $('#add_category').val();
		var product_id = $("#product_id").val();
		var product_name = $("#product_name").val();
		var sku = $("#sku").val();
		var brand_id = $("#brand_id").val();
		//var options = arrOptions.toString();
		var keywords = $("#keywords").val();
		//var category_id2 = $("#category_id2").val();
		//var is_show_image = $("#is_show_image").prop("checked") ? 1 : 0;
		var description = $("#description").val();
		//var shipping = $("#shipping").val();
		//var sizeandcapacity = $("#sizeandcapacity").val();
		//var standard = $("#standard").val();
		//var notes = $("#notes").val();
		var original_price = $("#original_price").val();
		var is_new = $("#is_new").prop("checked") ? 1 : 0;

		if (category_id == "" || category_id == 0) {
			toastr["warning"]("あなたのカテゴリを選択してください", "Warning");
			$("#add_category").trigger("click");
			return;
		}
		if (product_name.trim() == "" || product_name == undefined) {
			toastr["warning"]("商品名を入力してください.", "Warning");
			$("#product_name").focus();
			return;
		}
		if (sku.trim() == "" || sku == undefined) {
			toastr["warning"]("SKU番号を入力してください", "Warning");
			$("#sku").focus();
			return;
		}
		/*if (category_id2 == 0 || category_id2 == "" || category_id2 == undefined) {
			toastr["warning"]("あなたのカテゴリを選択してください", "Warning");
			$("#category_id2").focus();
			return;
		}*/
		if (brand_id == 0 || brand_id == "") {
			toastr["warning"]("あなたのブランドを入力してください", "Warning");
			$("#brand_id").focus();
			return;
		}
		if (original_price == 0) {
			toastr["warning"]("あなたのオリジナル価格を入力してください", "Warning");
			$("#original_price").focus();
			return;
		}

		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("check_sku")?>",
			data: "sku=" + $("#sku").val(),
			dataType: "json",
			success: function(data) {
				if (data.success == false && !product_id) {
					toastr["warning"]("SKUがすでに存在しています", "Warning");
					$("#sku").select();
					return;
				} else {
					if (!confirm("この商品を本当に追加しますか？"))
						return;

					var formData = new FormData();
					formData.append("product_id", product_id);
					formData.append("category_id", category_id);
					formData.append("category_name", category_name);
					formData.append("product_name", product_name);
					formData.append("sku", sku);
					formData.append("brand_id", brand_id);
					//formData.append("options", options);
					formData.append("keywords", keywords);
					//formData.append("category_id2", category_id2);
					//formData.append("is_show_image", is_show_image);
					formData.append("description", description);
					//formData.append("shipping", shipping);
					//formData.append("sizeandcapacity", sizeandcapacity);
					//formData.append("standard", standard);
					//formData.append("notes", notes);
					formData.append("original_price", original_price);
					formData.append("is_new", is_new);

					$.ajax({
						type: "post",
						url: "<?=$this->createUrl("product_save")?>",
						contentType: false,
						processData: false,
						data: formData,
						dataType: "json",
						success: function(data) {
							if (data.success) {
								toastr["success"](data.message, "Success");
								getListPage();
								productAdd(data.product_id);
								$("html, body").animate({ scrollTop: $(document).height() }, 1000);
							} else {
								toastr["error"](data.message, "Error");
							}
						}
					});
				}
			}
		});
	}
</script>