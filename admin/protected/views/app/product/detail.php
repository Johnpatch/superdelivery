<form class="form-horizontal">
	<div class="form-group">
		<label class="control-label col-md-2">番号</label>
		<div class="col-md-3">
			<input type="text" class="form-control" id="product_number">
		</div>
		<label class="control-label col-md-2">ブレークダウン</label>
		<div class="col-md-5">
			<input type="text" class="form-control" id="breakdown">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2">バーコード</label>
		<div class="col-md-3">
			<input type="text" class="form-control" id="barcode">
		</div>
		<label class="control-label col-md-2">JAN</label>
		<div class="col-md-2">
			<input type="text" class="form-control" id="jan">
		</div>
		<label class="control-label col-md-1">数量</label>
		<div class="col-md-2">
			<input type="number" class="form-control" id="quantity">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2">小売タイプ</label>
		<div class="col-md-3">
			<?=CHtml::dropDownList("retail_type", null, Product_detail::$TYPE_LIST, array("class"=>"form-control", "empty"=>"", "onchange"=>"changeRetailType()"))?>
		</div>
		<label class="control-label col-md-2">小売値段</label>
		<div class="col-md-5">
			<input type="number" class="form-control" id="retail_price">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2">卸売タイプ</label>
		<div class="col-md-3">
			<div class="checkbox">
				<label>
					<input type="checkbox" checked id="wholesale_type" onclick="changeWholesaleType()"> メンバーだけに見せる
				</label>
			</div>
		</div>
		<label class="control-label col-md-2">卸売値段</label>
		<div class="col-md-3">
			<input type="number" class="form-control" id="wholesale_price">
		</div>
		<div class="col-md-2">
			<button type="button" class="btn btn-warning" onclick="productDetailSave()">具体的な商品を保存</button>
		</div>
	</div>
</form>
<table class="table table-bordered table-striped table-hover">
	<thead>
		<tr>
			<th>No</th>
			<th>商品番号</th>
			<th>ブレークダウン</th>
			<th>数量</th>
			<th>小売値段</th>
			<th>卸売値段</th>
			<th>編集</th>
			<th>削除</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($product_detail_list as $no=>$item) {?>
		<tr>
			<td><?=$no + 1?></td>
			<td><?=$item->product_number?></td>
			<td><?=$item->breakdown . "(" . $item->barcode . ")" . $item->jan?></td>
			<td><?=$item->quantity?></td>
			<td><?=$item->retail_price?></td>
			<td><?=$item->wholesale_price?></td>
			<td><a href="javascript:;" onclick="productDetailEdit(<?=$item->id?>)">編集</a></td>
			<td><a href="javascript:;" onclick="productDetailDelete(<?=$item->id?>)">削除</a></td>
		</tr>
		<?php }?>
	</tbody>
</table>
<input type="hidden" id="product_detail_id" value="0">
<script>
	$(function() {
		$("#product_number").focus();
	});

	function changeRetailType() {
		var type = $("#retail_type").val();
		if (type == 4) {
			$("#retail_price").attr("disabled", true);
		} else {
			$("#retail_price").removeAttr("disabled");
		}
	}

	function changeWholesaleType() {
		if ($("#wholesale_type").prop("checked"))
			$("#wholesale_price").removeAttr("disabled");
		else
			$("#wholesale_price").attr("disabled", true);
	}

	function productDetailSave() {
		var product_detail_id = $("#product_detail_id").val();
		var product_id = <?=$product != null ? $product->id : 0?>;
		var product_number = $("#product_number").val();
		var breakdown = $("#breakdown").val();
		var barcode = $("#barcode").val();
		var jan = $("#jan").val();
		var quantity = $("#quantity").val();
		var retail_type = $("#retail_type").val();
		var retail_price = $("#retail_price").val();
		var wholesale_type = $("#wholesale_type").prop("checked") ? 1 : 0;
		var wholesale_price = $("#wholesale_price").val();

		if (product_number.trim() == "") {
			toastr["warning"]("商品番号を入力してください", "Warning");
			$("#product_number").focus();
			return;
		}
		if (breakdown.trim() == "") {
			toastr["warning"]("ブレークダウンを入力してください", "Warning");
			$("#breakdown").focus();
			return;
		}
		if (barcode.trim() == "") {
			toastr["warning"]("バーコードを入力してください", "Warning");
			$("#barcode").focus();
			return;
		}
		if (jan.trim() == "") {
			toastr["warning"]("JANを入力してください", "Warning");
			$("#jan").focus();
			return;
		}
		if (quantity == 0) {
			toastr["warning"]("数量を入力してください", "Warning");
			$("#quantity").focus();
			return;
		}
		if (retail_type == 0 || retail_type == undefined || retail_type == "") {
			toastr["warning"]("小売りタイプを選択してください", "Warning");
			$("#retail_type").focus();
			return;
		}

		if (!confirm("この商品の詳細情報を本当に保存しますか?"))
			return;

		$.ajax({
			type: "post",
			url: "<?=$this->createUrl("product_detail_save")?>",
			data: {product_id: product_id, product_detail_id: product_detail_id, product_number: product_number,
				breakdown: breakdown, barcode: barcode, jan: jan, quantity: quantity, retail_type: retail_type,
				retail_price: retail_price, wholesale_type: wholesale_type, wholesale_price: wholesale_price},
			dataType: "json",
			success: function(res) {
				if (res.success) {
					toastr["success"](res.message, "Success");
					getDetailPage();
				} else
					toastr["error"](res.message, "Error");
			}
		});
	}

	function productDetailEdit(id) {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("product_detail_edit")?>",
			data: "id=" + id,
			dataType: "json",
			success: function(data) {
				$("#product_detail_id").val(data.id);
				$("#product_number").val(data.product_number);
				$("#breakdown").val(data.breakdown);
				$("#barcode").val(data.barcode);
				$("#jan").val(data.jan);
				$("#quantity").val(data.quantity);
				$("#retail_type").val(data.retail_type);
				$("#retail_price").val(data.retail_price);
				$("#wholesale_price").val(data.wholesale_price);
				
				if (data.retail_type == 4) {
					$("#retail_price").attr("disabled", true);
				} else {
					$("#retail_price").removeAttr("disabled");
				}

				if (data.wholesale_type == 0) {
					$("#wholesale_type").prop("checked", false);
					$("#wholesale_price").attr("disabled", true);
				} else {
					$("#wholesale_type").prop("checked", true);
					$("#wholesale_price").removeAttr("disabled");
				}
			}
		});
	}

	function productDetailDelete(id) {
		if (!confirm("このアイテムを本当に削除しますか?"))
			return;

		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("product_detail_delete")?>",
			data: "id=" + id,
			dataType: "json",
			success: function(res) {
				if (res.success) {
					toastr["success"](res.message, "Success");
					getDetailPage();
				} else
					toastr["error"](res.message, "Error");
			}
		});
	}
</script>