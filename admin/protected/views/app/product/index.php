<div class="box box-solid">
	<div class="box-header with-border">
		<h3 class="box-title">私の商品</h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal">
			<div class="form-group">
				<label class="control-label col-md-1">カテゴリ一</label>
				<div class="col-md-4">
					<input type="text" class="form-control" id="category">
					<input type="hidden" id="category_id" name="category_id" value="<?=$category_id?>">
				</div>
				<div class="col-md-4"></div>
				<div class="col-md-3">
					<button type="button" class="btn btn-primary pull-right" onclick="productAdd()">
						<span class="glyphicon glyphicon-pencil"></span> 商品追加
					</button>
				</div>
			</div>
		</form>
		<div id="list-page"></div>
	</div>
</div>
<div id="add-page"></div>
<script>
	var setting = {
		data: {
			key: {
				name: "name", 
			},
			simpleData: {
				enable: true,
				idKey: "id",
				pIdKey: "parent_id",
			},
		},
		view: {
			showIcon: false
		}
	};

	var zNodes = <?php echo CJSON::encode($categories)?>;

	$(function() {
		zTreeSelector.init($("#category_id"), $("#category"), setting, zNodes, {
			callback: function(id, value, node) {
				getListPage(id);
			}
		});

		getListPage();
	});

	function getListPage(category_id = 0) {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("list_page")?>",
			data: "id=" + category_id,
			success: function(page) {
				$("#list-page").html(page);
			}
		});
	}

	function productAdd(product_id = 0) {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("product_add")?>",
			data: "product_id=" + product_id,
			success: function(page) {
				$("#add-page").html(page);
				$('html, body').animate({
					scrollTop: $("div#add-page").offset().top - 70
				}, 1000);	
			}
		});
	}
</script>