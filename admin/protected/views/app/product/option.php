<?php if (count($option_list) && count($option_detail_list)) {?>
	<div class="form-group">
		<label class="control-label col-md-2">カテゴリーオプション</label>
		<div class="col-md-10">
			<?php
				foreach ($option_list as $k=>$v) {
					echo "<div class='col-md-3'>";
					echo "<label class='control-label'>" . $v->name . "</label>";
					foreach ($option_detail_list as $k2=>$v2) {
						if ($v->id == $v2->option_id) {
							echo "<div class='checkbox'>";
							echo "<label><input type='checkbox' id='option_$v2->id' onclick='applyOption($v2->id)'" . (stripos($product->options, $v2->id) !== false ? "checked" : "") . "> " . $v2->name . "</label>";
							echo "</div>";
						}
					}
					echo "</div>";
				}
			?>
		</div>
	</div>
<?php }?>
<script>
	$(function() {
		<?php foreach ($option_detail_list as $k2=>$v2) {?>
			if ($("#option_" + <?=$v2->id?>).prop("checked")) {
				arrOptions.push(<?=$v2->id?>);
			}
		<?php }?>
	});

	function applyOption(option_detail_id) {
		if ($("#option_" + option_detail_id).prop("checked")) {
			arrOptions.push(option_detail_id);
		} else {
			var newOptions = new Array();
			for (var i = 0; i < arrOptions.length; i++) {
				if (arrOptions[i] != option_detail_id) {
					newOptions.push(arrOptions[i]);
				}
			}
			arrOptions = newOptions;
		}

		arrOptions = arrOptions.sort(function(a, b) {return a - b});
	}
</script>