<form ng-submit="login()" autocomplete="off">
	<label>顧客ID</label>
	<div class="form-group has-feedback">
		<input type="text" ng-model="userid" id="userid" class="form-control" placeholder="名前を入力してください">
		<span class="glyphicon glyphicon-user form-control-feedback"></span>
	</div>
	<label>パスワード</label>
	<div class="form-group has-feedback">
		<input type="password" ng-model="password" id="password" class="form-control" placeholder="パスワードを入力してください">
		<span class="glyphicon glyphicon-lock form-control-feedback"></span>
	</div>
	<div class="row">
		<div class="col-xs-6">
            <button type="submit" class="btn btn-primary btn-block">
                <span class="glyphicon glyphicon-log-in"></span> ログイン</button>
        </div>
        <div class="col-xs-6">
            <a href="#/register" class="btn btn-warning btn-block">
                <span class="glyphicon glyphicon-registration-mark"></span> 登録</a>
        </div>
	</div>
</form>