<form ng-submit="register()" autocomplete="off">
	<label>顧客ID</label>
	<div class="form-group has-feedback">
		<input type="text" ng-model="userid" id="userid" class="form-control" placeholder="顧客ID入力してください">
		<span class="glyphicon glyphicon-user form-control-feedback"></span>
	</div>
	<label>ユーザー名</label>
	<div class="form-group has-feedback">
		<input type="text" ng-model="username" id="username" class="form-control" placeholder="名前を入力してください">
		<span class="glyphicon glyphicon-user form-control-feedback"></span>
	</div>
	<label>パスワード</label>
	<div class="form-group has-feedback">
		<input type="password" ng-model="password" id="password" class="form-control" placeholder="パスワードを入力してください">
		<span class="glyphicon glyphicon-lock form-control-feedback"></span>
	</div>
	<label>パスワード確認</label>
	<div class="form-group has-feedback">
		<input type="password" ng-model="confirm" id="confirm" class="form-control" placeholder="再度パスワードを入力してください">
		<span class="glyphicon glyphicon-lock form-control-feedback"></span>
	</div>
	<label>E-mail</label>
	<div class="form-group has-feedback">
		<input type="text" ng-model="email" id="email" class="form-control" placeholder="あなたのメールアドレスを入力してください">
		<span class="glyphicon glyphicon-globe form-control-feedback"></span>
	</div>
	<label>要求権限</label>
	<div class="form-group has-feedback">
		<select ng-model="role" id="role" class="form-control">
			<?php foreach (User::$ROLE_LIST as $no=>$item) {?>
				<option value="<?=$no?>"><?=$item?></option>
			<?php }?>
		</select>
	</div>
	<label>電話番号</label>
	<div class="form-group has-feedback">
		<input type="text" ng-model="telephone" id="telephone" class="form-control" placeholder="あなたの電話番号を入力してください">
		<span class="glyphicon glyphicon-phone form-control-feedback"></span>
	</div>
	<label>アドレス</label>
	<div class="form-group has-feedback">
		<input type="text" ng-model="address" id="address" class="form-control" placeholder="アドレスを入力してください">
		<span class="glyphicon glyphicon-home form-control-feedback"></span>
	</div>
	<div class="row">
		<div class="col-xs-6">
            <a href="#/" class="btn btn-warning btn-block">
                <span class="glyphicon glyphicon-registration-mark"></span> ログインに戻り</a>
        </div>
		<div class="col-xs-6">
            <button type="submit" class="btn btn-primary btn-block">
                <span class="glyphicon glyphicon-log-in"></span> 登録</button>
        </div>
	</div>
</form>