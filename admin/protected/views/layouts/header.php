<header class="main-header">
	<a href="#/" class="logo">
		<span class="logo-mini"><b>LOGO</b></span>
		<span class="logo-lg"><b>LOGO</b></span>
	</a>
	<nav class="navbar navbar-static-top" role="navigation">
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="glyphicon glyphicon-option-horizontal"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
            	<li>
					<a href="javascript:;" onclick="showPasswordModal()">
						<span class="glyphicon glyphicon-lock"></span> パスワード変更
					</a>
				</li>
              	<li>
					<a href="<?=$this->createUrl("auth/logout")?>">
						<span class="glyphicon glyphicon-log-out"></span> ログアウト
					</a>
				</li>
            </ul>
        </div>
	</nav>
</header>