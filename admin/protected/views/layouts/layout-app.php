<!doctype html>
<html ng-app="SuperDeliveryApp">
	<head>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">

	    <title><?=Yii::app()->name?></title>
		
	    <link rel="stylesheet" href="<?=Yii::app()->baseUrl?>/assets/bootstrap/css/bootstrap.min.css">
	    <link rel="stylesheet" href="<?=Yii::app()->baseUrl?>/assets/plugins/select2/select2.min.css">
	    <link rel="stylesheet" href="<?=Yii::app()->baseUrl?>/assets/css/AdminLTE.min.css">
	    <link rel="stylesheet" href="<?=Yii::app()->baseUrl?>/assets/css/_all-skins.min.css">
	    <link rel="stylesheet" href="<?=Yii::app()->baseUrl?>/assets/css/animation.css">
	    <link rel="stylesheet" href="<?=Yii::app()->baseUrl?>/assets/css/custom.css">
	    <link rel="stylesheet" href="<?=Yii::app()->baseUrl?>/assets/plugins/datatables/dataTables.bootstrap.css">
	    <link rel="stylesheet" href="<?=Yii::app()->baseUrl?>/assets/plugins/bootstrap-toastr/toastr.min.css">
	    <link rel="stylesheet" href="<?=Yii::app()->baseUrl?>/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
	    <link rel="stylesheet" href="<?=Yii::app()->baseUrl?>/assets/plugins/ztree/ztree.css">
		
	    <script src="<?=Yii::app()->baseUrl?>/assets/plugins/jquery/jquery.min.js"></script>
	    <script src="<?=Yii::app()->baseUrl?>/assets/bootstrap/js/bootstrap.min.js"></script>
	    <script src="<?=Yii::app()->baseUrl?>/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	    <script src="<?=Yii::app()->baseUrl?>/assets/plugins/app/app.js"></script>
	    <script src="<?=Yii::app()->baseUrl?>/assets/plugins/datatables/jquery.dataTables.min.js"></script>
	    <script src="<?=Yii::app()->baseUrl?>/assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
	    <script src="<?=Yii::app()->baseUrl?>/assets/plugins/bootstrap-toastr/toastr.min.js"></script>
	    <script src="<?=Yii::app()->baseUrl?>/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js"></script>
	    <script src="<?=Yii::app()->baseUrl?>/assets/plugins/ztree/jquery.ztree.all-3.5.min.js"></script>
	    <script src="<?=Yii::app()->baseUrl?>/assets/plugins/ztree/ztree.selector.js"></script>
	    <script src="<?=Yii::app()->baseUrl?>/assets/plugins/select2/select2.min.js"></script>

	    <script src="<?=Yii::app()->baseUrl?>/assets/scripts/angular.min.js"></script>
	    <script src="<?=Yii::app()->baseUrl?>/assets/scripts/angular.route.min.js"></script>
	    <script src="<?=Yii::app()->baseUrl?>/assets/scripts/angular.route.app.js"></script>
	</head>
	<body class="hold-transition skin-blue fixed sidebar-mini">
		<div class="wrapper fade-in">
			<?php
				require_once "header.php";
				require_once "sidebar-app.php";
			?>
			<div class="content-wrapper">
				<section class="content fade-in" ng-view></section>
		    </div>
		    <?php require_once "password_modal.php";?>
		</div>
	</body>
</html>