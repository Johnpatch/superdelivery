<!doctype html>
<html ng-app="SuperDeliveryApp">
	<head>
	    <meta charset="utf-8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">

	    <title><?=Yii::app()->name?></title>
		
	    <link rel="stylesheet" href="<?=Yii::app()->baseUrl?>/assets/bootstrap/css/bootstrap.min.css">
	    <link rel="stylesheet" href="<?=Yii::app()->baseUrl?>/assets/css/AdminLTE.min.css">
	    <link rel="stylesheet" href="<?=Yii::app()->baseUrl?>/assets/css/animation.css">
	    <link rel="stylesheet" href="<?=Yii::app()->baseUrl?>/assets/css/custom.css">
	    <link rel="stylesheet" href="<?=Yii::app()->baseUrl?>/assets/plugins/bootstrap-toastr/toastr.min.css">
		
	    <script src="<?=Yii::app()->baseUrl?>/assets/plugins/jquery/jquery.min.js"></script>
	    <script src="<?=Yii::app()->baseUrl?>/assets/plugins/bootstrap-toastr/toastr.min.js"></script>
	    
	    <script src="<?=Yii::app()->baseUrl?>/assets/scripts/angular.min.js"></script>
	    <script src="<?=Yii::app()->baseUrl?>/assets/scripts/angular.route.min.js"></script>
	    <script src="<?=Yii::app()->baseUrl?>/assets/scripts/angular.route.home.js"></script>
	</head>
	<body class="login-page">
	    <div class="login-box">
			<div class="box box-widget widget-user">
                <div class="widget-user-header bg-aqua-active text-center">
                	<h3 class="widget-user-username">Admin Management</h3>
                </div>
                <div class="widget-user-image">
                	<!--img class="img-circle" src="<?=Yii::app()->baseUrl?>/assets/images/avatar.png"-->
                </div>
                <div class="box-footer">
			        <div class="login-box-body fade-in" ng-view></div>
			    </div>
			</div>
		</div>
	</body>
</html>