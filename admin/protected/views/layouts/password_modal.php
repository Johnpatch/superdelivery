<div class="modal fade" id="change_password_modal">
	<div class="modal-dialog" style="width:500px">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">パスワードを変える</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal">
		        	<div class="form-group">
		        		<label class="control-label col-md-4">現在のパスワード</label>
		        		<div class="col-md-8">
		                	<input type="password" class="form-control" id="old_password" placeholder="元のパスワードを入力してください">
		              	</div>
		            </div>
		            <div class="form-group">
		            	<label class="control-label col-md-4">新しいパスワード</label>
		              	<div class="col-md-8">
		                	<input type="password" class="form-control" id="new_password" placeholder="新パスワードを入力してください">
		              	</div>
		            </div>
		            <div class="form-group">
		            	<label class="control-label col-md-4">パスワード確認</label>
		              	<div class="col-md-8">
		                	<input type="password" class="form-control" id="confirm_password" placeholder="パスワードを再度入力してください">
		              	</div>
		            </div>
		        </form>
			</div>
			<div class="modal-footer">
				<button type="button" id="close-modal-btn" class="btn btn-default" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 閉じる
				</button>
				<button type="button" class="btn btn-primary pull-right" onclick="changePassword()">
					<span class="glyphicon glyphicon-lock"></span> パスワード変更
				</button>
			</div>
		</div>
	</div>
</div>
<script>
	function changePassword() {
		var old_password = $("#old_password").val();
		var new_password = $("#new_password").val();
		var confirm_password = $("#confirm_password").val();

		if (old_password.trim() == "") {
			toastr["warning"]("Please input your old password.", "Warning");
			$("#old_password").focus();
			return;
		}
		if (new_password == "") {
			toastr["warning"]("Please input your new password.", "Warning");
			$("#new_password").focus();
			return;
		}
		if (confirm_password == "") {
			toastr["warning"]("Please input your confirm password.", "Warning");
			$("#confirm_password").focus();
			return;
		}
		if (new_password != confirm_password) {
			toastr["warning"]("Your password and confirm password do not match.", "Warning");
			$("#confirm_password").focus();
			return;
		}

		if (!confirm("Are you sure to change your password ?"))
			return;

		$.ajax({
			type: "post",
			url: "<?=$this->createUrl("auth/change_password")?>",
			data: {old_password: old_password, new_password: new_password},
			dataType: "json",
			success: function(data) {
				if (!data.success) {
					toastr["error"](data.message, "Error");
					$("#old_password").focus();
				} else {
					toastr["success"](data.message, "Success");
					$("#close-modal-btn").trigger("click");
				}
			}
		});
	}
</script>