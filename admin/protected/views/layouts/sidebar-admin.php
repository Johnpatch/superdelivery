<aside class="main-sidebar" ng-controller="SidebarController">
	<section class="sidebar">
		<ul class="sidebar-menu">
			<li class="header">Admin Management</li>
			<li ng-class="{active: activeStatus('/')}">
				<a href="#/">
					<span class="glyphicon glyphicon-phone"></span>
					<span>商品</span>
				</a>
			</li>
			<li ng-class="{active: activeStatus('/category')}">
				<a href="#/category">
					<span class="glyphicon glyphicon-align-left"></span>
					<span>カテゴリー</span>
				</a>
			</li>
			<li ng-class="{active: activeStatus('/keyword')}">
				<a href="#/keyword">
					<span class="glyphicon glyphicon-search"></span>
					<span>商品キーワード</span>
				</a>
			</li>
			<li ng-class="{active: activeStatus('/brand')}">
				<a href="#/brand">
					<span class="glyphicon glyphicon-picture"></span>
					<span>メーカー</span>
				</a>
			</li>
			<!--li ng-class="{active: activeStatus('/account')}">
				<a href="#/account">
					<span class="glyphicon glyphicon-sunglasses"></span>
					<span>新ユーザアカウント</span>
					<small class="badge pull-right bg-green" id="remain-user-count">
						<?=$this->actionGet_count_new_user()?>
					</small>
				</a>
			</li-->
			<li ng-class="{active: activeStatus('/user')}">
				<a href="#/user">
					<span class="glyphicon glyphicon-user"></span>
					<span>ユーザアカウント</span>
				</a>
			</li>
		</ul>
	</section>
</aside>
<script>
	$(function() {
		setInterval(getNewUserCount, 1000 * 60 * 1);
	});

	function getNewUserCount() {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("get_count_new_user")?>",
			success: function(count) {
				$("#remain-user-count").text(count);
			}
		});
	}

	function showPasswordModal() {
		$("#change_password_modal").modal();
	}
</script>