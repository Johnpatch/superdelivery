<aside class="main-sidebar" ng-controller="SidebarController">
	<section class="sidebar">
		<ul class="sidebar-menu">
			<li class="header">Admin Management</li>
			<!--li ng-class="{active: activeStatus('/')}">
				<a href="#/">
					<span class="glyphicon glyphicon-flag"></span>
					<span>会社情報</span>
				</a>
			</li>
			<li ng-class="{active: activeStatus('/category')}">
				<a href="#/category">
					<span class="glyphicon glyphicon-random"></span>
					<span>カテゴリー＆ブランド</span>
				</a>
			</li-->
			<li ng-class="{active: activeStatus('/')}">
				<a href="#/">
					<span class="glyphicon glyphicon-phone"></span>
					<span>私の商品</span>
				</a>
			</li>
			<!--li ng-class="{active: activeStatus('/price')}">
				<a href="#/price">
					<span class="glyphicon glyphicon-share-alt"></span>
					<span>要求された値段</span>
					<small class="badge pull-right bg-red" id="requested-price-count">
						<?=$this->actionGet_count_new_price()?>
					</small>
				</a>
			</li>
			<li ng-class="{active: activeStatus('/counterpart')}">
				<a href="#/counterpart">
					<span class="glyphicon glyphicon-user"></span>
					<span>商売相手</span>
				</a>
			</li>
			<li ng-class="{active: activeStatus('/cart')}">
				<a href="#/cart">
					<span class="glyphicon glyphicon-shopping-cart"></span>
					<span>買い物かご</span>
				</a>
			</li>
			<li ng-class="{active: activeStatus('/cartlog')}">
				<a href="#/cartlog">
					<span class="glyphicon glyphicon-stats"></span>
					<span>カートログ</span>
				</a>
			</li-->
		</ul>
	</section>
</aside>
<script>
	$(function() {
		setInterval(getNewPriceCount, 1000 * 60 * 1);
	});

	function getNewPriceCount() {
		$.ajax({
			type: "get",
			url: "<?=$this->createUrl("get_count_new_price")?>",
			success: function(count) {
				$("#requested-price-count").text(count);
			}
		});
	}

	function showPasswordModal() {
		$("#change_password_modal").modal();
	}
</script>