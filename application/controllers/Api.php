<?php

class Api extends Base_Controller {
	
	public function __construct() {
		parent::__construct();
	}

	public function post_registration() {
        $posted_data = json_decode($this->input->post('posted_data'));
        $email = $posted_data->email;
        $username = $posted_data->username;
        $password = $posted_data->password;

		$user_new = new User_model();
		$user_new->username = $username;
		$user_new->password = md5($password);
		$user_new->email = $email;
		$user_new->telephone = $username. "_telephone";
		$user_new->address =  $username. "_address";
		$user_new->permission = User_model::PERMISSION_DISABLED;
		$user_new->role = User_model::ROLE_USER;
		$user_new->is_new = 1;
		$user_new->created_at = $this->utils->get_current_datetime();
		$user_new->updated_at = $this->utils->get_current_datetime();
		$this->user->set_new_record(true);
		$this->user->save($user_new);
	}

	public function post_login() {
		$posted_data = json_decode($this->input->post('posted_data'));
        $username = $posted_data->username;
        $password = $posted_data->password;
		$error = $this->user->authenticate($username, $password);

		if ($error == User_model::ERROR_NONE) {
			$user = $this->user->find_by_attributes(array('username'=>$username));
			$user->updated_at = $this->utils->get_current_datetime();
			$this->user->set_new_record(false);
			$this->user->save($user);

			$current_wishlist = json_decode($this->session->userdata('current_wishlist'));
			foreach ($current_wishlist as $key => $value) {
				$t_wish = $this->wishlist->find_by_attributes(array('user_id'=>$user->id, 'product_id'=>$value->product_id));
				if ($t_wish == null) {
					$new_wishlist = new Wishlist_model();
					$new_wishlist->user_id = $user->id;
					$new_wishlist->product_id = $value->product_id;
					$new_wishlist->owner_id = $value->owner_id;
					$new_wishlist->created_at = $this->utils->get_current_datetime();
					$new_wishlist->updated_at = $this->utils->get_current_datetime();
					$this->wishlist->set_new_record(true);
					$this->wishlist->save($new_wishlist);
				}
				$t_memo = $this->memo->find_by_attributes(array('customer_id'=>$user->id, 'product_id'=>$value->product_id));
				if ($t_memo == null) {
					$new_memo = new Memo_model();
					$new_memo->customer_id = $user->id;
					$new_memo->product_id = $value->product_id;
					$new_memo->content = $value->memo;
					$new_memo->created_at = $this->utils->get_current_datetime();
					$new_memo->updated_at = $this->utils->get_current_datetime();
					$this->memo->set_new_record(true);
					$this->memo->save($new_memo);
				} else {
					$t_memo->content = $value->memo;
					$t_memo->updated_at = $this->utils->get_current_datetime();
					$this->memo->set_new_record(false);
					$this->memo->save($t_memo);
				}
			}	
			$this->session->set_userdata("current_wishlist", json_encode(array()));
			// $this->session->set_userdata("current_cart", json_encode(array()));
			$this->identity->login($user);
		}
		echo $error;
	}

	public function post_comment() {
		if ($this->identity->is_guest()) {
			echo -2;
			return;
		}
		$posted_data = json_decode($this->input->post('posted_data'));
		$product_id = $posted_data->product_id;
        $rating = $posted_data->rating;
        $content = $posted_data->content;

		$new_review = new Product_reviews_model();
		$new_review->product_id = $product_id;
		$new_review->email =$this->identity->user()->email;
		$new_review->rating = $rating;
		$new_review->content = $content;
		$new_review->created_at = $this->utils->get_current_datetime();
		$new_review->updated_at = $this->utils->get_current_datetime();

		$this->product_reviews->set_new_record(true);
		$this->product_reviews->save($new_review);
		echo "1";
	}

	public function post_wishlist() {
		$posted_data = json_decode($this->input->post('posted_data'));
		$product_id = $posted_data->product_id;
		$owner_id = $posted_data->owner_id;
		if ($this->identity->is_guest()) {
			$current_wishlist = json_decode($this->session->userdata('current_wishlist'));
			if (!is_array($current_wishlist))
				$current_wishlist = array();
			$my_wishlist = array();
			$is_removed = false;
			foreach ($current_wishlist as $key => $value) {
				if ($value->product_id != $product_id) {
					array_push($my_wishlist, $value);
				} else{
					$is_removed = true;
				}
			}
			if (!$is_removed)
				array_push($my_wishlist, array('product_id'=>$product_id, 'owner_id'=>$owner_id, 'memo'=>''));	
			$this->session->set_userdata("current_wishlist", json_encode($my_wishlist));
			echo 1;
			return;
		}
		$user_id = $this->identity->user()->id;
		$wishlist_var = $this->wishlist->find_by_attributes(array('user_id'=>$user_id, 'product_id'=>$product_id));
		if ($wishlist_var == null) {
			$new_wishlist = new Wishlist_model();
			$new_wishlist->user_id = $user_id;
			$new_wishlist->product_id = $product_id;
			$new_wishlist->owner_id = $owner_id;
			$new_wishlist->created_at = $this->utils->get_current_datetime();
			$new_wishlist->updated_at = $this->utils->get_current_datetime();
			$this->wishlist->set_new_record(true);
			$this->wishlist->save($new_wishlist);
			if ($this->wishlist->get_primary_key() > 0)
				echo 1;	// success;
			else
				echo -1;	// failed;
		} else {
			if ($this->wishlist->delete_by_pk($wishlist_var->id) == true)
				echo 1;
			else
				echo -1;
		}
	}

	public function post_del_wishlists() {
		$posted_data = json_decode($this->input->post('posted_data'));
		$del_list = $posted_data->del_list;
		if ($this->identity->is_guest()) {
			$current_wishlist = json_decode($this->session->userdata('current_wishlist'));
			if (!is_array($current_wishlist))
				$current_wishlist = array();
			$my_wishlist = array();
			foreach ($current_wishlist as $value) {
				$is_removed = false;
				foreach ($del_list as $del_product_id) {
					if ($value->product_id == $del_product_id) {
						$is_removed = true;
					}
				}
				if (!$is_removed) 
					array_push($my_wishlist, $value);
			}
			$this->session->set_userdata("current_wishlist", json_encode($my_wishlist));
			return;
		}
		$user_id = $this->identity->user()->id;
		foreach ($del_list as $d_l_item) {
			$wishlist_var = $this->wishlist->find_by_attributes(array('user_id'=>$user_id, 'product_id'=>$d_l_item));

			$this->wishlist->delete_by_pk($wishlist_var->id);
		}
	}

	public function get_cart() {
		$current_cart = json_decode($this->session->userdata('current_cart'));
		if (!is_array($current_cart))
			$current_cart = array();
		echo json_encode($current_cart);
	}

	public function post_add2cart() {
		$posted_data = json_decode($this->input->post('posted_data'));
		$user_id = $posted_data->user_id;
		$wholesaler = $this->user->find_by_pk($user_id);
		if ($wholesaler == null)
			return;
		$carts = $posted_data->carts;
		$total_price = 0;
		foreach ($carts as $cart) {
			$total_price += $cart[2] * $cart[3];
		}
// $carts[0] is for product_id, $carts[1] is for product_detail_id, $carts[2] is for qty, $carts[3] is for current_price
        $current_cart = json_decode($this->session->userdata('current_cart'));
		if (!is_array($current_cart))
			$current_cart = array();
		foreach ($current_cart as $key => $value) {
			if ($value->user_id == $user_id) {
				$prev_carts = $value->carts;
				$my_carts = array();
				for ($i=0; $i < count($carts); $i++) { 
					for ($j=0; $j < count($prev_carts); $j++) { 
						if ($carts[$i][1] == $prev_carts[$j][1]) {
							$carts[$i][2] += $prev_carts[$j][2];
							$prev_carts[$j][2] = -1;
							break;
						}
					}
					array_push($my_carts, $carts[$i]);
				}
				for ($i=0; $i < count($prev_carts); $i++) { 
					if ($prev_carts[$i][2] > 0) {
						array_push($my_carts, $prev_carts[$i]);
					}
				}
				$current_cart[$key]->total_price += $total_price;
				$current_cart[$key]->carts = $my_carts;
				// print_r($current_cart);
				$this->session->set_userdata("current_cart", json_encode($current_cart));
				return;
			}
		}

		array_push($current_cart, array('user_id'=>$user_id, 'username'=>$wholesaler->username, 'total_price'=>$total_price, 'carts'=>$carts));	
		// print_r($current_cart);
		$this->session->set_userdata("current_cart", json_encode($current_cart));
	}

	public function post_removeProduct() {
		if ($this->identity->is_guest()) {
			redirect("auth/login");
		}
		$posted_data = json_decode($this->input->post('posted_data'));
		$product_detail_id = $posted_data->product_detail_id;
		$user_id = $posted_data->user_id;

        $current_cart = json_decode($this->session->userdata('current_cart'));
        $my_cart = array();
        foreach ($current_cart as $key => $value) {
        	if ($value->user_id == $user_id) {
        		$updated_cart = array();
        		foreach ($value->carts as $cart) {
        			if ($cart[1] != $product_detail_id) {
        				array_push($updated_cart, $cart);
        			}
        		}
        		$current_cart[$key]->carts = $updated_cart;
        	}
        	if (count($current_cart[$key]->carts) != 0)
        		array_push($my_cart, $current_cart[$key]);
    	}
		$this->session->set_userdata("current_cart", json_encode($my_cart));
	}

	public function post_removeSubAllProduct() {
		if ($this->identity->is_guest()) {
			redirect("auth/login");
		}
		$posted_data = json_decode($this->input->post('posted_data'));
		$user_id = $posted_data->user_id;
        $current_cart = json_decode($this->session->userdata('current_cart'));
        $my_cart = array();
		foreach ($current_cart as $key => $value) {
        	if ($value->user_id != $user_id) {
        		array_push($my_cart, $current_cart[$key]);
        	}
    	}
		$this->session->set_userdata("current_cart", json_encode($my_cart));
	}

	public function post_change_cartQty() {
		if ($this->identity->is_guest()) {
			redirect("auth/login");
		}
		$posted_data = json_decode($this->input->post('posted_data'));
		$user_id = $posted_data->user_id;
		$product_detail_id = $posted_data->product_detail_id;
		$changed_qty = $posted_data->changed_qty;
        $current_cart = json_decode($this->session->userdata('current_cart'));
		foreach ($current_cart as $key => $value) {
        	if ($value->user_id == $user_id) {
        		foreach ($value->carts as $key2 => $cart) {
        			if ($cart[1] == $product_detail_id) {
        				$current_cart[$key]->carts[$key2][2] = $changed_qty;
						$this->session->set_userdata("current_cart", json_encode($current_cart));
						return;
        			}
        		}
        	}
    	}
	}
	
	public function post_save_memo() {
		$posted_data = json_decode($this->input->post('posted_data'));
		$product_id = $posted_data->product_id;
		$memo = $posted_data->memo;
		if ($this->identity->is_guest()) {
			$current_wishlist = json_decode($this->session->userdata('current_wishlist'));
			if (!is_array($current_wishlist))
				$current_wishlist = array();
			foreach ($current_wishlist as $key => $value) {
				if ($value->product_id == $product_id) {
					$current_wishlist[$key]->memo = $memo;
					break;
				}
			}
			$this->session->set_userdata("current_wishlist", json_encode($current_wishlist));
			return;
		}

		$user_id = $this->identity->user()->id;
		$my_memo = $this->memo->find_by_attributes(array('customer_id'=>$user_id, 'product_id'=>$product_id));
		if ($my_memo == null) {
			$new_memo = new Memo_model();
			$new_memo->customer_id = $user_id;
			$new_memo->product_id = $product_id;
			$new_memo->content = $memo;
			$new_memo->created_at = $this->utils->get_current_datetime();
			$new_memo->updated_at = $this->utils->get_current_datetime();
			$this->memo->set_new_record(true);
			$this->memo->save($new_memo);
		} else {
			$my_memo->content = $memo;
			$my_memo->updated_at = $this->utils->get_current_datetime();
			$this->memo->set_new_record(false);
			$this->memo->save($my_memo);
		}
	}
}