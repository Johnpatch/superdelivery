<?php
class Auth extends Base_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function login() {
		$title_bar = _lang('login'). '｜'. _lang('title');
		$this->render_home('login', compact('title_bar'));
	}

	public function logout() {
		$this->identity->logout();
	}

	public function signup() {
		$this->render('signup');
	}

	public function process() {
		// $this->load->view(strtolower(get_class($this)) . "/" ."process");
		$this->render('process');
	}
}