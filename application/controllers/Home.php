<?php

class Home extends Base_Controller {
	
	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$title_bar = _lang('title');
		$product_count = $this->product->count();
		$brand_count = $this->brand->count();
		$ranking_products = $this->product->find_all(array(), array(), 12);
		foreach ($ranking_products as $key => $r_product) {
			if ($r_product->is_show_image || $this->identity->is_logined()) 
				$ranking_products[$key]->main_image = $this->utils->main_image_url($r_product->id, $r_product->main_image);
			else 
				$ranking_products[$key]->main_image = base_url().'assets/img/pages/detail/required2view.png';
		}
		$top_brands = $this->brand->find_all(array(), array(), 8);
		$new_products = $this->product->find_all(array("is_new"=>1), 'date DESC');
		foreach ($new_products as $key => $n_product) {
			if ($n_product->is_show_image || $this->identity->is_logined()) 
				$new_products[$key]->main_image = $this->utils->main_image_url($n_product->id, $n_product->main_image);
			else 
				$new_products[$key]->main_image = base_url().'assets/img/pages/detail/required2view.png';
		}
		$this->render_home('index', compact('title_bar', 'product_count', 'brand_count', 'ranking_products', 'new_products', 'top_brands'));
	}

	public function get_options_string($options_string) {
		if ($options_string == '')
			return '';
		$options_array = explode(",", $options_string);
		$options = array();
		foreach ($options_array as $key => $option_value) {
			$this->db->select("T.name, T2.name AS option_name");
			$this->db->from("category_option_detail AS T");
			$this->db->join("category_option AS T2", "T.option_id = T2.id");
			$temp_option = $this->db->where(array("T.id"=>$option_value))->get()->result()[0];
			array_push($options, $temp_option);
		}
		foreach ($options as $key => $option) {
			$option_names[$key] = $option->option_name;
		}
		array_multisort($option_names, SORT_DESC, $options);
		$result_str = "";
		foreach ($options as $key => $option) {
			if ($key > 0 && $options[$key-1]->option_name == $option->option_name)
				$result_str.= ','. $option->name;
			else 
				$result_str.= ' '. $option->option_name . ":" . $option->name;
		}
		return $result_str;
	}

	public function get_company_info($user_id = 0) {
		$user = $this->user->find_by_pk($user_id);
		if ($user == null) {
			return array();
		}
		$company_informations = $this->company->find_all(array('user_id'=>$user_id));
		$company_informations = $this->utils->array_to_list($company_informations, 'type', 'description');
		if (count($company_informations) == 0) {
			$company_informations[1] = _lang('no_data');
			$company_informations[2] = _lang('no_data');
			$company_informations[3] = _lang('no_data');
		}
		$c_info = array();
		$c_info['user_id'] = $user_id;
		$c_info['username'] = $user->username;
		$c_info['company_informations'] = $company_informations;
		if ($this->identity->is_guest()) {
			$c_info['company_status'] = -1;
		} else {
			$my_price = $this->price->find_by_attributes(array('user_id'=>$user->id, 'customer_id'=>$this->identity->user()->id));
			if ($my_price == null) {
				$c_info['company_status'] = -1;
			} else{
				$c_info['company_status'] = $my_price->status;
				$c_info['price_id'] = $my_price->id;
			}
		}
		return $c_info;
	}

	public function good_detail($id = 0) {
		$product = $this->product->find_by_pk($id);
		if ($product == null) {
			redirect('home');
			return;
		}
		$company_info = $this->get_company_info($product->user_id);
		if ($product->is_show_image || $this->identity->is_logined()) 
			$product->main_image = $this->utils->main_image_url($product->id, $product->main_image);
		else 
			$product->main_image = base_url().'assets/img/pages/detail/required2view.png';
		$product->options = $this->get_options_string($product->options);
		$product_details = $this->product_detail->find_all(array('product_id'=>$product->id));
		if ($this->identity->is_guest()) {
			$current_wishlist = json_decode($this->session->userdata('current_wishlist'));
			if (count($current_wishlist) == 0)
				$current_wishlist = array();
			$product->is_wished = 0;
			foreach ($current_wishlist as $cw_product) {
				if ($product->id == $cw_product->product_id) {
					$product->is_wished = 1;
					break;
				}
			}
		} else {
			$my_wish = $this->wishlist->find_by_attributes(array('user_id'=>$this->identity->user()->id, 'product_id'=>$product->id));
			$product->is_wished = isset($my_wish);
			if ($company_info['company_status'] == 1) {
				foreach ($product_details as $key=>$p_detail) {
					$my_price_discuss = $this->price_discuss->find_by_attributes(array('price_id'=>$company_info['price_id'], 'product_id'=>$id, 'product_detail_id'=>$p_detail->id));
					if ($my_price_discuss == null) {
						$product_details[$key]->current_price = $p_detail->wholesale_price;
					} else {
						$product_details[$key]->current_price = $my_price_discuss->wholesale_price;
					}
				}
			}
		}
		$title_bar = $product->name. '｜'. _lang('title');

		$galleries = $this->product_image->find_all(array('product_id'=>$id));
		$gallery_images = array();
		foreach ($galleries as $gallery) {
			if ($product->is_show_image || $this->identity->is_logined()) {
				array_push($gallery_images, $this->utils->gallery_image_url($gallery->product_id, $gallery->image));
			} else {
				array_push($gallery_images, base_url().'assets/img/pages/detail/required2view.png');
			}
		}

		$brand = $this->brand->find_by_pk($product->brand_id);

		$cate_results = array();
		$temp_category = null;
		do {
			if ($temp_category == null) 
				$temp_category = $this->category->find_by_pk($product->category_id);
			else
				$temp_category = $this->category->find_by_attributes(array('id'=>$temp_category->parent_id));
			array_push($cate_results, $temp_category);
		} while ($temp_category->parent_id != 0);

		$cate2 = $this->category2->find_by_pk($product->category_id2);

		$keywords = explode(',', $product->keywords);

		if ($this->identity->user()) {
			$my_wishlist = $this->get_my_wishlist();
		}
		
		$related_products = $this->get_related_products($product, 3);
		$recommend_products = $this->get_recommend_products($product);

		// $reviews = $this->product_reviews->find_all(array('product_id'=>$product->id));
		$reviews = [];

/******		These are just for recent product list using cookie.  **********/
		$recents = json_decode($this->session->userdata('recents'));
		if (!is_array($recents)) {
        	$recents = array();
		}
        $recent_index = -1;
        foreach ($recents as $key => $recent) {
        	if ($recent->product_id == $product->id) {
        		$recent_index = $key;
        	}
    	}
    	if ($recent_index == -1) {
    		array_push($recents, array('product_id'=>$product->id, 'visited_time'=>$this->utils->get_current_datetime()));
    	} else {
    		$recents[$recent_index]->visited_time = $this->utils->get_current_datetime();
    	}
		$this->session->set_userdata('recents', json_encode($recents));
		$recent_products = $this->get_recent_products();
/******************************************************************************************/
		$this->render_home('detail', compact('title_bar', 'product', 'company_info', 'gallery_images', 'brand', 'company_informations', 'cate_results', 'cate2', 'product_details', 'related_products', 'recommend_products', 'recent_products', 'reviews', 'keywords'));
	}

	public function get_my_wishlist() {
		$my_wishes = $this->wishlist->find_all(array('user_id'=>$this->identity->user()->id));
		$my_wishlist = $this->utils->array_to_list($my_wishes, 'product_id', 'id');
		return $my_wishlist;
	}

	public function get_related_products($product, $count) {
		if ($this->identity->user()) {
			$my_wishlist = $this->get_my_wishlist();
		}
		$this->db->select('T.*, T2.username');
		$this->db->from('product AS T');
		$this->db->join('user AS T2', 'T2.id = T.user_id');
		$this->db->limit($count);
		$related_products = $this->db->where(array('T.id!='=>$product->id, 'T.category_id'=>$product->category_id, 'T.user_id'=>$product->user_id))->get()->result();
		foreach ($related_products as $key => $related_product) {
			if ($this->identity->is_guest()) {
				$current_wishlist = json_decode($this->session->userdata('current_wishlist'));
				if (count($current_wishlist) == 0)
					$current_wishlist = array();
				$related_products[$key]->is_wished = 0;
				foreach ($current_wishlist as $cw_product) {
					if ($related_product->id == $cw_product->product_id) {
						$related_products[$key]->is_wished = 1;
						break;
					}
				}
			}
			else if (isset($my_wishlist[$related_product->id]) && $my_wishlist[$related_product->id] > 0)
				$related_products[$key]->is_wished = 1;
			else
				$related_products[$key]->is_wished = 0;
			if ($related_product->is_show_image || $this->identity->is_logined())  
				$related_products[$key]->main_image = $this->utils->main_image_url($related_product->id, $related_product->main_image);
			else 
				$related_products[$key]->main_image = base_url().'assets/img/pages/detail/required2view.png';
		}
		return $related_products;
	}
	public function get_recommend_products($product) {
		if ($this->identity->user()) {
			$my_wishlist = $this->get_my_wishlist();
		}

		$this->db->select('T.*, T2.username');
		$this->db->from('product AS T');
		$this->db->join('user AS T2', 'T2.id = T.user_id');
		$this->db->limit(5);
		$recommend_products = $this->db->where(array('T.category_id'=>$product->category_id, 'T.user_id!='=>$product->user_id))->get()->result();
		foreach ($recommend_products as $key => $recommend_product) {
			if ($this->identity->is_guest()) {
				$current_wishlist = json_decode($this->session->userdata('current_wishlist'));
				if (count($current_wishlist) == 0)
					$current_wishlist = array();
				$recommend_products[$key]->is_wished = 0;
				foreach ($current_wishlist as $cw_product) {
					if ($recommend_product->id == $cw_product->product_id) {
						$recommend_products[$key]->is_wished = 1;
						break;
					}
				}
			}
			else if (isset($my_wishlist[$recommend_product->id]) && $my_wishlist[$recommend_product->id] > 0)
				$recommend_products[$key]->is_wished = 1;
			else
				$recommend_products[$key]->is_wished = 0;
			if ($recommend_product->is_show_image || $this->identity->is_logined())  
				$recommend_products[$key]->main_image = $this->utils->main_image_url($recommend_product->id, $recommend_product->main_image);
			else 
				$recommend_products[$key]->main_image = base_url().'assets/img/pages/detail/required2view.png';
		}
		return $recommend_products;
	}

	public function get_recent_products() {
		if ($this->identity->user()) {
			$my_wishlist = $this->get_my_wishlist();
		}

		$recents = json_decode($this->session->userdata('recents'));
		$recent_list = $this->utils->array_to_list($recents, 'product_id', 'visited_time');

		$this->db->select('T.*, T2.username');
		$this->db->from('product AS T');
		$this->db->join('user AS T2', 'T2.id = T.user_id');
		$this->db->limit(5);

		foreach ($recents as $key => $recent) {
			$this->db->or_where(array('T.id'=>$recent->product_id));
		}
		$recent_products = $this->db->get()->result();
		$visited_times = array();
		foreach ($recent_products as $key => $recent_product) {
			if ($this->identity->is_guest()) {
				$current_wishlist = json_decode($this->session->userdata('current_wishlist'));
				if (count($current_wishlist) == 0)
					$current_wishlist = array();
				$recent_products[$key]->is_wished = 0;
				foreach ($current_wishlist as $cw_product) {
					if ($recent_product->id == $cw_product->product_id) {
						$recent_products[$key]->is_wished = 1;
						break;
					}
				}
			}
			else if (isset($my_wishlist[$recent_product->id]) && $my_wishlist[$recent_product->id] > 0)
				$recent_products[$key]->is_wished = 1;
			else
				$recent_products[$key]->is_wished = 0;
			if ($recent_product->is_show_image || $this->identity->is_logined())  
				$recent_products[$key]->main_image = $this->utils->main_image_url($recent_product->id, $recent_product->main_image);
			else 
				$recent_products[$key]->main_image = base_url().'assets/img/pages/detail/required2view.png';
			$visited_times[$key] = $recent_list[$recent_product->id];
			$recent_products[$key]->visited_time = $recent_list[$recent_product->id];
		}
		array_multisort($visited_times, SORT_DESC, $recent_products);
		return $recent_products;
	}

	public function apply() {
		if ($this->identity->is_guest()) {
			return redirect('auth/login');
		}
		$user_id = $this->route->get_param('user_id', 0);
		$user = $this->user->find_by_pk($user_id);
		if ($user == null) {
			redirect('home');
			return;
		}
		
		$company_info = $this->get_company_info($user_id);
		$company_info['company_status'] = -2;
		$title_bar = _lang('title');
		$this->render_home('apply', compact('title_bar', 'company_info', 'company_informations'));
	}

	public function done() {
		if ($this->identity->is_guest()) {
			return redirect('auth/login');
		}
		$user_id = $this->route->get_param('user_id', 0);
		$user = $this->user->find_by_pk($user_id);
		if ($user == null) {
			redirect('home');
			return;
		}
		$my_user = $this->identity->user();
		$my_price = $this->price->find_by_attributes(array('customer_id'=>$my_user->id, 'user_id'=>$user_id));
		if ($my_price == null) {
			$my_new_price = new Price_model();
			$my_new_price->user_id = $user_id;
			$my_new_price->customer_username = $my_user->username;
			$my_new_price->customer_id = $my_user->id;
			$my_new_price->status = 0;
			$my_new_price->created_at = $this->utils->get_current_datetime();
			$my_new_price->updated_at = $this->utils->get_current_datetime();
			$this->price->set_new_record(true);
			$this->price->save($my_new_price);

		}
		$company_info = $this->get_company_info($user_id);
		$title_bar = _lang('title');
		
		$one_product = $this->product->find_by_attributes(array('user_id'=>$user_id));
		$related_products = $this->get_related_products($one_product, 5);
		$this->render_home('done', compact('title_bar', 'company_info', 'company_informations', 'related_products'));
	}

	public function search() {
		$keyword = $this->route->get_param('keyword', '');
		$custom_category_id = $this->route->get_param('custom_category', 0);
		$detail_search_data = $this->route->get_param('detail_search_data', -1);
		$group_by = $this->route->get_param('group_by_type', 1);
		if ($detail_search_data == -1) {
			if ($custom_category_id == 0)
				$this->show_product_list(0, -1, 0, 0, 0, '', $keyword, -1, -1, -1, $group_by);
			else
				$this->show_product_list(-1, $custom_category_id, 0, 0, 0, '', $keyword, -1, -1, -1, $group_by);
		}
		else {
			$detail_search_data = json_decode($detail_search_data);
			if ($detail_search_data->category == 0) {
				$this->show_product_list(0, -1, 0, 0, 0, '', $detail_search_data->keyword, $detail_search_data->exclude_keyword, $detail_search_data->from_cost, $detail_search_data->to_cost, $group_by);

			} else {
				$this->show_product_list(-1, $detail_search_data->category, 0, 0, 0, '', $detail_search_data->keyword, $detail_search_data->exclude_keyword, $detail_search_data->from_cost, $detail_search_data->to_cost, $group_by);
			}
		}
	}

	public function product_list() {
		$category_id = $this->route->get_param('category_id', 0);
		$custom_category_id = $this->route->get_param('custom_category', -1);

		$user_id = $this->route->get_param('user_id', 0);
		$brand_id = $this->route->get_param('brand_id', 0);
		$date = $this->route->get_param('date', 0);
		$options = $this->route->get_param('options', '');
		$keyword = $this->route->get_param('keyword', '');
		$group_by = $this->route->get_param('group_by_type', 1);
		if ($custom_category_id == -1)
			$this->show_product_list($category_id, -1, $user_id, $brand_id, $date, $options, $keyword, -1, -1, -1, $group_by);
		else
			$this->show_product_list(-1, $custom_category_id, $user_id, $brand_id, $date, $options, $keyword, -1, -1, -1, $group_by);
	}

	public function show_product_list($category_id, $custom_category_id, $user_id, $brand_id, $date, $options, $keyword, $exclude_keyword, $from_cost, $to_cost, $group_by) {
		if ($category_id <= 0) {
			$category_id = 0;
			$product_count = $this->product->count();
			$my_category = array('product_count'=>$product_count, 'name'=>_lang('all'), 'leaf'=>0);
		} else {
			$va_my_category = $this->category->find_by_pk($category_id);
			$products = $this->get_all_products($category_id, -1, 0, 0, 0, '', '', -1, -1, -1);
			$my_category = array('product_count'=>count($products), 'name'=>$va_my_category->name, 'leaf'=>$va_my_category->leaf);
		}

		$sub_categories = $this->category->find_all(array('parent_id'=>$category_id));
		$sub_category_list = array();
		foreach ($sub_categories as $sub_category) {
			$sub_products = $this->get_all_products($sub_category->id, -1, 0, 0, 0, '', '', -1, -1, -1);
			array_push($sub_category_list, array('id'=>$sub_category->id, 'name'=>$sub_category->name, 'leaf'=>$sub_category->leaf, 'product_count'=>count($sub_products))); 
		}

		// $all_brands = $this->brand->find_all();
		// foreach ($all_brands as $key => $a_brand) {
		// 	$b_product_count = $this->product->count(array('brand_id'=>$a_brand->id));
		// 	$all_brands[$key]->product_count = $b_product_count;
		// }

		$products = $this->get_all_products($category_id, $custom_category_id, $user_id, $brand_id, $date, $options, $keyword, $exclude_keyword, $from_cost, $to_cost);

		$options_str = '0';

		if ($this->identity->user()) {
			$my_wishlist = $this->get_my_wishlist();
		}
		
		foreach ($products as $key => $product) {
			$options_str.= ','.$product->options;
			if ($this->identity->is_guest()) {
				$current_wishlist = json_decode($this->session->userdata('current_wishlist'));
				if (count($current_wishlist) == 0)
					$current_wishlist = array();
				$products[$key]->is_wished = 0;
				foreach ($current_wishlist as $cw_product) {
					if ($product->id == $cw_product->product_id) {
						$products[$key]->is_wished = 1;
						break;
					}
				}
			}
			else if (isset($my_wishlist[$product->id]) && $my_wishlist[$product->id] > 0)
				$products[$key]->is_wished = 1;
			else
				$products[$key]->is_wished = 0;
			if ($product->is_show_image || $this->identity->is_logined())  
				$products[$key]->main_image = $this->utils->main_image_url($product->id, $product->main_image);
			else 
				$products[$key]->main_image = base_url().'assets/img/pages/detail/required2view.png';
			if ($this->identity->is_guest()) {
				$products[$key]->p_status = -1;
			} else  {
				$my_price = $this->price->find_by_attributes(array('customer_id'=>$this->identity->user()->id, 'user_id'=>$product->user_id));
				if ($my_price == null)
					$products[$key]->p_status = -1;
				else
					$products[$key]->p_status = $my_price->status;
			}
		}
		$options = explode(',', $options_str);
		$options = array_unique($options);

		$this->db->select('T.*, T2.name AS group_name');
		$this->db->from('category_option_detail AS T');
		$this->db->join('category_option AS T2', 'T.option_id = T2.id');

		foreach ($options as $option) {
			if ($option != 0 && $option != '') {
				$this->db->or_where(array('T.id'=>$option));
			} 
		}
		$this->db->order_by('T2.name');
		$options = $this->db->get()->result();

		$user_ids = array();
		$custom_category_ids = array();
		$brand_ids = array();
		foreach ($products as $key => $product) { 
			array_push($custom_category_ids, $product->category_id2);
			array_push($brand_ids, $product->brand_id);
			if ($key == 0 ||(($key>0) && ($products[$key-1]->user_id != $product->user_id))) { 
				array_push($user_ids, $product->user_id);
			}
		}
		$custom_category_ids = array_unique($custom_category_ids);
		$brand_ids = array_unique($brand_ids);
		
		$this->db->select('*');
		$this->db->from('category2');
		foreach ($custom_category_ids as $cc_id) {
			$this->db->or_where(array('id'=>$cc_id));
		}
		$my_custom_categories = $this->db->get()->result();

		$this->db->select('*');
		$this->db->from('brand');
		foreach ($brand_ids as $b_id) {
			$this->db->or_where(array('id'=>$b_id));
		}
		$my_brands = $this->db->get()->result();

		$user_ids = implode(',', $user_ids);
		$title_bar = _lang('product_list');

		$company_info = $this->get_company_info($user_id);
		if ($group_by == 1 && $user_id == 0) {
			$filtered_products = array();
			$f_index = -1;
			$buy_allowed_products_count = 0;
			foreach ($products as $key => $product) {
				if ($key == 0 ||(($key>0) && ($products[$key-1]->user_id != $product->user_id))) { 
					$f_index++;
					array_push($filtered_products, array('user_info' => array('user_id'=>$product->user_id, 'username'=>$product->username, 'bap_cnt'=>0, 'company_status'=>$product->p_status), 'products' => array()));
				}
				if ($this->identity->is_logined() && $product->p_status == 1) {
					$filtered_products[$f_index]['user_info']['bap_cnt'] += 1;
				}
				array_push($filtered_products[$f_index]['products'], $product);
			}
			$this->render_home('product_list', compact('title_bar', 'category_id', 'user_id', 'keyword', 'custom_category_id', 'options', 'group_by' ,'user_ids', 'filtered_products', 'my_category', 'sub_category_list', 'all_brands', 'company_info', 'my_custom_categories', 'my_brands'));
		} else {
			$this->render_home('product_list', compact('title_bar', 'category_id', 'user_id', 'keyword', 'custom_category_id', 'options', 'group_by', 'user_ids', 'products', 'my_category', 'sub_category_list', 'all_brands', 'company_info', 'my_custom_categories', 'my_brands'));
		}
	}

	public function wishlist() {
		$title_bar = _lang('wishlist');

		if ($this->identity->is_guest()) {
			$current_wishlist = json_decode($this->session->userdata('current_wishlist'));
			$products = array();
			$memoes = array();

			if (count($current_wishlist) == 0) {
				$current_wishlist = array();
			} else {
				$this->db->select('T.*, T2.username');
				$this->db->from('product AS T');
				$this->db->join('user AS T2', 'T2.id = T.user_id');
				foreach ($current_wishlist as $key => $value) {
					$this->db->or_where(array('T.id'=>$value->product_id));
					$memoes[$value->product_id] = $value->memo;
				}
				$this->db->order_by('T.user_id');
				$products = $this->db->get()->result();
			}
		} else {
			$this->db->select('T.user_id, T.owner_id, T2.*, T3.username');
			$this->db->from('wishlist AS T');
			$this->db->join('product AS T2', 'T.product_id = T2.id');
			$this->db->join('user AS T3', 'T.owner_id = T3.id');
			$this->db->where(array('T.user_id'=>$this->identity->user()->id));
			$this->db->order_by('T.owner_id');
			$products = $this->db->get()->result();

			$memoes = $this->memo->find_all(array('customer_id'=>$this->identity->user()->id));
			$memoes = $this->utils->array_to_list($memoes, 'product_id', 'content');
		}
		foreach ($products as $key => $product) {
			if ($product->is_show_image || $this->identity->is_logined()) 
				$products[$key]->main_image = $this->utils->main_image_url($product->id, $product->main_image);
			else 
				$products[$key]->main_image = base_url().'assets/img/pages/detail/required2view.png';
		}

		$product_count = $this->product->count();
		$my_category = array('product_count'=>$product_count, 'name'=>_lang('all'), 'leaf'=>0);

		$sub_categories = $this->category->find_all(array('parent_id'=>0));
		$sub_category_list = array();
		foreach ($sub_categories as $sub_category) {
			$sub_products = $this->get_all_products($sub_category->id, -1, 0, 0, 0, '', '', -1, -1, -1);
			array_push($sub_category_list, array('id'=>$sub_category->id, 'name'=>$sub_category->name, 'leaf'=>$sub_category->leaf, 'product_count'=>count($sub_products))); 
		}

		

		$this->render_home('wishlist', compact('title_bar', 'products', 'my_category', 'sub_category_list', 'memoes'));
	}

	public function shoppingcart() {
		if ($this->identity->is_guest()) {
			redirect("auth/login");
		}
		$title_bar = _lang('shoppingcart');
		$current_cart = json_decode($this->session->userdata('current_cart'));
		if (!is_array($current_cart) || count($current_cart) == 0) {
        	$current_cart = array();
        	$cart_products = array();
			$this->render_home('shoppingcart', compact('title_bar', 'cart_products'));
			return;
		}
		if ($this->identity->user()) {
			$my_wishlist = $this->get_my_wishlist();
		}
		
		$cart_products = array();
		foreach ($current_cart as $c_cart) {
			$wholesaler = $this->user->find_by_pk($c_cart->user_id);
			$w_carts = $c_cart->carts;
			
			$w_carts_list = array();
			$products = array();
			foreach ($w_carts as $w_cart) {
				$this->db->select("T.*, T2.name, T2.main_image, T2.sku");
				$this->db->from("product_detail AS T");
				$this->db->join("product AS T2", "T2.id = T.product_id");
				$this->db->where("T.id", $w_cart[1]);
				$results = $this->db->get()->result();
				if (count($results) == 0) {
					$this->render_home('shoppingcart', compact('title_bar', 'cart_products'));
					return;
				}
				$results = $results[0];

				array_push($products, $results);
				$w_carts_list[$w_cart[1]] = $w_cart[2];
			}
			$totalPrice = 0;
			foreach ($products as $key => $product) {
				$my_price = $this->price->find_by_attributes(array('user_id'=>$product->user_id, 'customer_id'=>$this->identity->user()->id));
				if ($my_price != null) {
					$my_price_discuss = $this->price_discuss->find_by_attributes(array('price_id'=>$my_price->id, 'product_id'=>$product->product_id, 'product_detail_id'=>$product->id));
					if ($my_price_discuss != null) {
						$products[$key]->r_price = $my_price_discuss->wholesale_price;
					} else {
						$products[$key]->r_price = $product->wholesale_price;
					}
				} else {
					$products[$key]->r_price = $product->wholesale_price;
				}
				$products[$key]->cart_qty = $w_carts_list[$product->id];
				if (isset($my_wishlist[$product->product_id]) && $my_wishlist[$product->product_id] > 0)
					$products[$key]->is_wished = 1;
				else
					$products[$key]->is_wished = 0;
				$totalPrice += $product->cart_qty * $products[$key]->r_price * $product->quantity;
			}
			$wholesaler->totalPrice = $totalPrice;
			array_push($cart_products, array('user'=>$wholesaler, 'products'=>$products));
		}
		$this->render_home('shoppingcart', compact('title_bar', 'cart_products'));
	}

	public function order() {
		$title_bar = _lang('order');
		if ($this->identity->is_guest()) {
			redirect("auth/login");
		}
		$user_id = $this->route->get_param('user_id');
        $current_cart = json_decode($this->session->userdata('current_cart'));
        $new_cart = array();
		foreach ($current_cart as $key => $c_cart) {
			if ($c_cart->user_id == $user_id) {
				foreach ($c_cart->carts as $p_detail) {
					$user = $this->identity->user();
					$wholesaler = $this->user->find_by_pk($user_id);
					$pdt = $this->product->find_by_pk($p_detail[0]);
					$pdt_detail = $this->product_detail->find_by_pk($p_detail[1]);
					$price_dis = $this->price_discuss->find_by_attributes(array('user_id'=>$user_id, 'customer_id'=>$user->id, 'product_detail_id'=>$p_detail[1]));

					$new_deliver = new Deliver_model();
					$new_deliver->date = $this->utils->get_current_date();
					$new_deliver->user_id = $user_id;
					$new_deliver->username = $user->username;
					$new_deliver->product_detail_id = $pdt_detail->id;
					// $new_deliver->name = $pdt->name;
					// $new_deliver->product_number = $pdt_detail->product_number;
					// $new_deliver->price = $price_dis->price * $pdt_detail->quantity;
					$new_deliver->quantity = $p_detail[2];
					$new_deliver->address = $user->address;
					// $new_deliver->telephone = $user->telephone;
					$new_deliver->status = 0;
					$new_deliver->created_at = $this->utils->get_current_datetime();
					$new_deliver->updated_at = $this->utils->get_current_datetime();

					$this->deliver->set_new_record(true);
					$this->deliver->save($new_deliver);

				}
			} else {
				array_push($new_cart, $c_cart);
			}
		}
		$this->session->set_userdata("current_cart", json_encode($new_cart));
		redirect('home/shoppingcart');
	}

	public function tradinglist() {
		if ($this->identity->is_guest()) {
			redirect("auth/login");
		}
		$title_bar = _lang('tradinglist');
		$this->db->select("T.*");
		$this->db->from("user AS T");
		$this->db->join("price AS T2", "T.id = T2.user_id");
		$trading_companies = $this->db->where(array("T2.customer_id"=>$this->identity->user()->id))->get()->result();
		foreach ($trading_companies as $key => $t_company) {
			$trading_companies[$key]->product_count =  $this->product->count(array('user_id'=>$t_company->id));
			$trading_companies[$key]->first_product = $this->product->find_by_attributes(array('user_id'=>$t_company->id));
		}	

		$my_category = array('product_count'=>0, 'name'=>_lang('all'), 'leaf'=>0);
		$sub_categories = $this->category->find_all(array('parent_id'=>0));
		$sub_category_list = array();
		foreach ($sub_categories as $sub_category) {
			$sub_products = $this->get_all_products($sub_category->id, -1, 0, 0, 0, '', '', -1, -1, -1);
			array_push($sub_category_list, array('id'=>$sub_category->id, 'name'=>$sub_category->name, 'leaf'=>$sub_category->leaf, 'product_count'=>count($sub_products))); 
		}

		$this->render_home('tradinglist', compact('title_bar', 'trading_companies', 'my_category', 'sub_category_list'));

	}

	public function buyhistory() {
		if ($this->identity->is_guest()) {
			redirect("auth/login");
		}
		$title_bar = _lang('buyhistoryt');

	}
}