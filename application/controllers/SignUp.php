<?php
class SignUp extends Base_Controller {

	public function __construct() {
		parent::__construct();

	}

	public function index() {
		$this->render('index');
	}

	public function process() {
		// $this->load->view(strtolower(get_class($this)) . "/" ."process");
		$this->render('process');
	}
}