<?php

class Base_Controller extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library("identity");
		$this->load->library("route");
		$this->load->library("utils");

		$this->load->model('Brand_model', 'brand');
		$this->load->model('Category_model', 'category');
		$this->load->model('Category_option_model', 'category_option');
		$this->load->model('Category_option_detail_model', 'category_option_detail');
		$this->load->model('Category2_model', 'category2');
		$this->load->model('Company_model', 'company');
		$this->load->model('Deliver_model', 'deliver');
		$this->load->model('Memo_model', 'memo');
		$this->load->model('Product_model', 'product');
		$this->load->model('Price_model', 'price');
		$this->load->model('Price_discuss_model', 'price_discuss');
		$this->load->model('Product_detail_model', 'product_detail');
		$this->load->model('Product_image_model', 'product_image');
		$this->load->model('Product_keywords_model', 'product_keywords');
		$this->load->model('Product_reviews_model', 'product_reviews');
		$this->load->model('Wishlist_model', 'wishlist');
        
        $this->load->helper('my_lang_helper');
        $this->lang->load('japanese_lang', 'japanese');
	}

	public function render($view, $data = array()) {
		$class_name = strtolower(get_class($this));
		
		$view_url = $class_name. '/'. $view;

		$content = $this->load->view($view_url, $data, true);
		$css_content = $this->load->view($view_url. "_css", $data, true);
		$js_content = $this->load->view($view_url. "_js", $data, true);

		$this->load->view('layouts/layout_' . $class_name , compact("content","css_content" ,"js_content"));
	}

	public function get_all_sub_categories($selected_category_id) {
		if ($selected_category_id <= 0) {
			return $this->category->find_all(array(), 'arrange');
		}
		$selected_category = $this->category->find_by_pk($selected_category_id);
		if ($selected_category == null) {
			return redirect('home');
		}
		$this->db->select('*');
		$this->db->from('category');
		$this->db->like('arrange', $selected_category->arrange, 'after');
		$this->db->order_by('arrange');
		$all_categories = $this->db->where(array())->get()->result();
		return $all_categories;
	}

	public function get_all_products($category_id, $custom_category_id, $user_id, $brand_id, $date, $options, $keyword, $exclude_keyword, $from_cost, $to_cost) {

		if ($custom_category_id == -1) {
			$categories = $this->get_all_sub_categories($category_id);
		}

		$this->db->select('T.*, T2.username, T3.name AS brand_name');
		$this->db->from('product AS T');
		$this->db->join('user AS T2', 'T2.id = T.user_id');
		$this->db->join('brand AS T3', 'T3.id = T.brand_id');

		if ($custom_category_id != -1) {
			$this->db->where(array('T.category_id2'=>$custom_category_id));
		} else {
			$this->db->group_start();
			foreach ($categories as $category) {
				$this->db->or_where(array('T.category_id'=>$category->id));
			}
			$this->db->group_end();
		}
		if ($user_id != 0) 
			$this->db->where(array('T.user_id'=>$user_id));
		if ($brand_id != 0) 
			$this->db->where(array('T.brand_id'=>$brand_id));
		if ($date != 0) 
			$this->db->where(array('T.date'=>$date));
		$this->db->like('T.options', $options);
		if ($keyword != '') {
			$this->db->group_start();
			$this->db->like('T.name', $keyword);
			$this->db->or_like('T3.name', $keyword);
			$this->db->or_like('T.keywords', $keyword);
			$this->db->group_end();
			
		}
		if ($exclude_keyword != -1 & $exclude_keyword != '') {
			$this->db->not_like('T.name', $exclude_keyword);
			$this->db->not_like('T3.name', $exclude_keyword);
			$this->db->not_like('T.keywords', $exclude_keyword);
		}
		
		// if ($from_cost > 0 && $to_cost > 0)
			// $this->db->where(array('T.price>=' => $from_cost, 'T.price<=' => $to_cost));
		$this->db->order_by('user_id');
		$products = $this->db->get()->result();
		return $products;
	}

	public function render_home($view, $data = array()) {
		$categories = $this->category->find_all(array('length(arrange)<' => 13), 'arrange ASC');
		$custom_categories = $this->category2->find_all(array());
		foreach ($custom_categories as $key => $custom_category) {
			$custom_categories[$key]->product_count = $this->product->count(array('category_id2'=>$custom_category->id));
		}

		$data['categories'] = json_encode($categories);
		$data['custom_categories'] = $custom_categories;

		$view_url = 'home/'. $view;
		$content = $this->load->view($view_url, $data, true);
		$css_content = $this->load->view($view_url. "_css", $data, true);
		$js_content = $this->load->view($view_url. "_js", $data, true);
		$this->load->view('layouts/layout_home', compact("content", "css_content", "js_content"));
	}

	public function render_partial($view, $data = []) {
		$this->load->view(strtolower(get_class($this)) . "/" . $view, $data);
	}
}