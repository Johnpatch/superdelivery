<?php

class Base_Model extends CI_Model {
	
	private $pk = "id";
	private $field_names = null;
	private $new_record = true;
	private $table_name;

	public function __construct() {
		parent::__construct();
	}

	public function get_new_record() {
		return $this->new_record;
	}

	public function set_new_record($new_record) {
		$this->new_record = $new_record;
	}

	public function get_primary_key() {
		$pk_name = $this->pk;
		return $this->$pk_name;
	}

	public function set_primary_key($pk) {
		$pk_name = $this->pk;
		$this->$pk_name = $pk;
	}

	public function get_table_name() {
		return $this->table_name;
	}

	public function set_table_name($table_name) {
		$this->table_name = $table_name;
	}

	public function save($model) {
		if ($this->field_names === null)
			$this->field_names = $this->db->list_fields($this->table_name);
		
		foreach ($this->field_names as $field) {
			if (isset($model->$field))
				$this->db->set($field, $model->$field, true);
		}
		
		if ($this->get_new_record() === true) {
			$this->db->set($this->pk, null);
			$this->db->insert($this->table_name);

			if ($this->db->affected_rows()) {
				$this->set_primary_key($this->db->insert_id());
				$this->set_new_record(false);
				return true;
			}
		} else {
			$pk_name = $this->pk;
			$this->db->where($pk_name,$model->id);
			$this->db->update($this->table_name);

			if ($this->db->affected_rows()) {
				$this->set_primary_key($this->pk);
				return true;
			}
		}
		
		return false;
	}

	public function count($where = null) {
		if ($where)
			$this->db->where($where);
		
		return $this->db->count_all_results($this->table_name);
	}

	public function find_by_pk($pk) {
		$this->db->where($this->pk, $pk);
		return $this->db->get($this->table_name)->row();
	}

	public function find_by_attributes($where = null) {
		if ($where)
			$this->db->where($where);
		return $this->db->get($this->table_name)->row();
	}

	public function find_all($where = null, $order = null, $limit_or_offset = 0, $length = 0) {
		if (isset($where))
			$this->db->where($where);
		if (isset($order))
			$this->db->order_by($order);
		
		if ($limit_or_offset > 0)
		{
			if ($length > 0)
				$this->db->limit($length, $limit_or_offset);
			else
				$this->db->limit($limit_or_offset);
		}
		
		return $this->db->get($this->table_name)->result_object();
	}

	public function delete_by_pk($pk)
	{
		$this->db->where($this->pk, $pk);
		$this->db->delete($this->table_name);

		return $this->db->affected_rows() ? true : false;
	}

	public function delete_all($where = null)
	{
		if (isset($where))
			$this->db->where($where);

		$this->db->delete($this->table_name);

		return $this->db->affected_rows() ? true : false;
	}
}