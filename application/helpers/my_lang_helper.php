<?php

function _lang($key) {
	$ci = & get_instance();
	return $ci->lang->line($key);
}