<?php

#title
$lang['title'] = '事業者専用の卸・仕入れサイト【スーパーデリバリー】';
$lang['login'] = 'ログイン';
$lang['logout'] = 'ログアウト';
$lang['guide'] = 'ヘルプ・使い方';
$lang['tradinglist'] = '取引企業一覧';
$lang['wishlist'] = '検討中リスト';
$lang['buyhistory'] = '購入履歴';
$lang['shoppingcart'] = 'ショッピングカート';
$lang['to_top'] = 'スーパーデリバリーTOP';
$lang['all'] = 'すべて';
$lang['menu'] = 'メニュー';
$lang['all_category'] = '全てのジャンル';
$lang['category'] = 'ジャンル';
$lang['custom_category'] = 'クローズアップ';
$lang['brand'] = 'ブランド名';
$lang['order'] = 'order';
$lang['product_list'] = 'Product List';


#button
$lang['btn_register'] = 'まずはトライアル登録';
$lang['btn_add2cart'] = 'カートに入れる';
$lang['btn_add2wishlist'] = '検討中に追加';
$lang['terms_and_conditions'] = '取引条件';
$lang['select_all'] = 'すべて選択';
$lang['btn_go2apply'] = '卸価格を見る（申請画面へ）';
$lang['btn_go2apply_short'] = '卸価格を見る（申請）';

#alert
$lang['no_data'] = 'No data';