<?php
class Identity
{
	const SD_LOGIN_USER = "SD_LOGIN_USER";

	private $CI = null;

	public function __construct()
	{
		$this->CI = &get_instance();
	}

	public function login($user)
	{
		$this->CI->session->set_userdata(self::SD_LOGIN_USER, $user);
	}

	public function logout()
	{
		$this->CI->session->unset_userdata(self::SD_LOGIN_USER);

		redirect("/");
	}

	public function is_guest()
	{
		return !$this->CI->session->has_userdata(self::SD_LOGIN_USER);
	}

	public function is_logined() {
		return $this->CI->session->has_userdata(self::SD_LOGIN_USER);
	}

	public function user()
	{
		return $this->CI->session->userdata(self::SD_LOGIN_USER);
	}
}