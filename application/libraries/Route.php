<?php
class Route
{
	private $CI = null;

	public function __construct()
	{
		$this->CI = &get_instance();
	}

	public function get_controller()
	{
		$url = explode("/", uri_string());

		if (is_array($url))
			return $url[0];
		else
			return "auth";
	}

	public function get_action()
	{
		$url = explode("/", uri_string());

		if (is_array($url) && isset($url[1]))
			return $url[1];
		else
			return "index";
	}

	public function get_param($param, $default = null)
	{
		$param = $this->CI->input->get_post($param);

		return $param == null ? $default : $param;
	}
}