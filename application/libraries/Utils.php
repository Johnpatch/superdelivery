<?php
class Utils {
	
	public function array_to_list($list, $k, $v = null, $list2 = []) {
		foreach ($list as $t) {
			if (is_array($k)) {
				$key = "";
				foreach ($k as $kk) {
					if ($key != "")
						$key .= "-";
					$key .= $t->$kk;
				}
			}
			else
				$key = $t->$k;
				
			if (isset($v)) {
				if (is_array($v)) {
					$list2[$key] = "";
					foreach ($v as $t2) {
						if ($list2[$key] != "")
							$list2[$key] .= " ";
						$list2[$key] .= $t->$t2;
					}
				}
				else
					$list2[$key] = $t->$v;
			}
			else
				$list2[$key] = $t;
		}
		return $list2;
	}

	public function get_year_list()	{
		$now = getdate();
		$start_year = $now["year"] - 3;
		$end_year = $now["year"] + 3;

		$list = [];
		for ($i = $start_year; $i <= $end_year; $i++)
			$list[$i] = $i;
		
		return $list;
	}

	public function get_month_list() {
		$list = [];
		for ($i = 1; $i <= 12; $i++)
			$list[$i] = $i;
		
		return $list;
	}

	public function get_current_year() {
		$now = getdate();
		return $now["year"];
	}
	
	public function get_current_month() {
		$now = getdate();
		return $now["mon"];
	}
	
	public function get_current_day() {
		$now = getdate();
		return $now["day"];
	}

	public function get_current_date() {
		return date("Y/m/d");
	}

	public function get_date_by_adding_days($days) {
		$date = date_create();
		date_add($date,date_interval_create_from_date_string($days. " days"));
		return date_format($date,"n/j");
	}
	
	public function get_current_datetime() {
		return date("Y/m/d H:i:s");
	}

	public function get_current_md() {
		return date("n/j");
	}

	public function print_md($str_date) {
		$date = date_create($str_date);
		return date_format($date, "n") . "/" . date_format($date, "j");
	}

	public function print_ymd($str_date) {
		$date = date_create($str_date);
		return date_format($date, "Y") . "/" . date_format($date, "n") . "/" . date_format($date, "j");
	}

	public function main_image_url($product_id, $main_image) {
		return site_url(). "admin/images/main_" . $product_id . "_" . $main_image;
	}

	public function gallery_image_url($product_id, $gallery_image) {
		return site_url(). "admin/images/gallery_" . $product_id . "_" . $gallery_image;
	}

	public function brand_image_url($brand_id, $brand_image) {
		return site_url(). "admin/images/brand_" . $brand_id . "_" . $brand_image;
	}

	public function MakeBigNumStyle($org_num) {
		$result = "";
		while ($org_num > 999) {
			$temp = $org_num % 1000;
			$tempStr = "";
			$org_num -= $temp;
			$org_num = round($org_num / 1000, 3);
			if ($temp < 100)
				$tempStr = "0";
			if ($temp < 10)
				$tempStr = "00";
			if ($org_num < 1) {
				$result = $tempStr. $temp. $result;
			} else {
				$result	= "," . $tempStr . $temp. $result;
			}
		}
		$result = $org_num. $result ;
		return	$result;
	}
}