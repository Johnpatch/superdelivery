<?php

class Brand_model extends Base_Model {

	public $id = null;
	public $user_id;
	public $name;
	public $image;
	public $description;
	public $created_at;
	public $updated_at;

	public function __construct()	{
		parent::__construct();
		$this->set_table_name('brand');
	}
}