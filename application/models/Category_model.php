<?php

class Category_model extends Base_Model {

	public $id = null;
	public $parent_id;
	public $name;
	public $path;
	public $arrange;
	public $main_image;
	public $leaf;
	public $created_at;
	public $updated_at;

	public function __construct()	{
		parent::__construct();
		$this->set_table_name('category');
	}
}