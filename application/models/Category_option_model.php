<?php

class Category_option_model extends Base_Model {

	public $id = null;
	public $category_id;
	public $name;
	public $created_at;
	public $updated_at;

	public function __construct()	{
		parent::__construct();
		$this->set_table_name('category_option');
	}
}