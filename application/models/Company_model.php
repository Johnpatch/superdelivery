<?php

class Company_model extends Base_Model {

	public $id = null;
	public $user_id;
	public $type;
	public $description;
	public $created_at;
	public $updated_at;
	
	public function __construct()	{
		parent::__construct();
		$this->set_table_name('company');
	}
}