<?php

class Deliver_model extends Base_Model {

	public $id = null;
	public $date;
	public $user_id;
	public $username;
	public $product_detail_id;
	public $quantity;
	public $address;
	public $status;
	public $created_at;
	public $updated_at;
	
	public function __construct()	{
		parent::__construct();
		$this->set_table_name('deliver');
	}
}