<?php

class Memo_model extends Base_Model {

	public $id = null;
	public $customer_id;
	public $product_id;
	public $content;
	public $created_at;
	public $updated_at;
	
	public function __construct()	{
		parent::__construct();
		$this->set_table_name('memo');
	}
}