<?php

class Price_discuss_model extends Base_Model {

	public $id = null;
	public $user_id;
	public $customer_username;
	public $customer_id;
	public $status;
	public $created_at;
	public $updated_at;
	
	public function __construct()	{
		parent::__construct();
		$this->set_table_name('price_discuss');
	}
}