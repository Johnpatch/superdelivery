<?php

class Product_detail_model extends Base_Model {

	public $id = null;
	public $user_id;
	public $product_id;
	public $product_number;
	public $breakdown;
	public $barcode;
	public $jan;
	public $quantity;
	public $retail_type;
	public $retail_price;
	public $wholesale_type;
	public $wholesale_price;
	public $is_stock;
	public $created_at;
	public $updated_at;
	
	public function __construct()	{
		parent::__construct();
		$this->set_table_name('product_detail');
	}
}