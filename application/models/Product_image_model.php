<?php

class Product_image_model extends Base_Model {

	public $id = null;
	public $product_id;
	public $image;
	public $created_at;
	public $updated_at;
	
	public function __construct()	{
		parent::__construct();
		$this->set_table_name('product_image');
	}
}