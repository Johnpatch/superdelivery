<?php

class Product_keywords_model extends Base_Model {

	public $id = null;
	public $product_id;
	public $name;
	public $created_at;
	public $updated_at;
	
	public function __construct()	{
		parent::__construct();
		$this->set_table_name('product_keywords');
	}
}