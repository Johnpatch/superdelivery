<?php

class Product_model extends Base_Model {

	public $id = null;
	public $date;
	public $user_id;
	public $brand_id;
	public $category_id;
	public $options;
	public $category_id2;
	public $main_image;
	public $sku;
	public $name;
	public $keywords;
	public $standard;
	public $description;
	public $shipping;
	public $sizeandcapacity;
	public $notes;
	public $is_show_image;
	public $is_new;
	public $created_at;
	public $updated_at;
	
	public function __construct()	{
		parent::__construct();
		$this->set_table_name('product');
	}
}