<?php

class Product_reviews_model extends Base_Model {

	public $id = null;
	public $user_id;
	public $type;
	public $email;
	public $product_id;
	public $rating;
	public $content;
	public $status;
	public $created_at;
	public $updated_at;

	public function __construct()	{
		parent::__construct();
		$this->set_table_name('product_reviews');
	}
}