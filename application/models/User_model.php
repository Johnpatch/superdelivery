<?php

class User_model extends Base_Model {

	const ERROR_NONE = 0;
	const ERROR_UNREGISTERED_USER = 1;
	const ERROR_INVALIED_PASSWORD = 2;
	const ERROR_PERMISSION_DISABLED = 3;

	const PERMISSION_DISABLED = 0;
	const PERMISSION_ENABLED = 1;
	public static $PERMISSION_LIST = [
		self::PERMISSION_DISABLED=>"Disabled",
		self::PERMISSION_ENABLED=>"Enabled"
	];

	const ROLE_ADMIN = 1;
	const ROLE_MANAGER = 2;
	const ROLE_USER = 3;
	public static $ROLE_LIST = [
		self::ROLE_USER=>"Normal User",
		self::ROLE_MANAGER=>"Manager",
		self::ROLE_ADMIN=>"Administrator",
	];

	public $id = null;
	public $username;
	public $password;
	public $email;
	public $telephone;
	public $address;
	public $permission;
	public $role;
	public $is_new;
	public $created_at;
	public $updated_at;
	
	public function __construct()	{
		parent::__construct();
		$this->set_table_name("user");
	}

	public function authenticate($username, $password) {
		$user = $this->find_by_attributes(["username"=>$username]);
		if ($user == null)
			$error = self::ERROR_UNREGISTERED_USER;
		else if ($user->password != md5($password))
			$error = self::ERROR_INVALIED_PASSWORD;
		else if ($user->permission == self::PERMISSION_DISABLED)
			$error = self::ERROR_PERMISSION_DISABLED;
		else
			$error = self::ERROR_NONE;
	
		return $error;
	}
}