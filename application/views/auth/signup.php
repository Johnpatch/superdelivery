<div id="top">
	<div id="header-fixd-content" class="wrapper fixed-header" style="padding-top: 0px;">
		<a name="top">
		</a>
		<div id="header-common" class="header-liquid co-container co-cf">
			<div class="wrap">
				<div id="header-simple" class="head-entry-form">
					<div class="co-clf">
						<div class="header-simple-left">
							<div class="logo">
								<a href="<?=site_url()?>">
									<img src="<?=base_url()?>assets/img/pages/signup/logo_rwd.png" alt="スーパーデリバリー" class="co-vabot">
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="retaile-entry" class="co-container retaile-entry-error">
			<div id="retaile-entry-wrap">
				<!-- 言語選択 -->
				<div class="language">
					<div class="now-lang-txt">
						<p>日本語</p>
						<span class="lang-arw">▼</span>
					</div>
					<ul>
						<li class="no-select">
							<a href="javascript:;">English</a>
						</li>
						<li class="no-select">
							<a href="javascript:;">繁體字</a>
						</li>
						<li class="no-select">
							<a href="javascript:;">简体字</a>
						</li>
						<li class="select">日本語</li>
					</ul>
				</div>
				<!-- メイン -->
				<div class="main w96per">
					<div class="back">
						<img src="<?=base_url()?>assets/img/pages/signup/ttl_back.jpg" alt="">
					</div>
					<div class="main-ttl">
						<img src="<?=base_url()?>assets/img/pages/signup/ttl_sub.png" alt="仕入れも、備品も、" class="sub-ttl">
						<img src="<?=base_url()?>assets/img/pages/signup/ttl.png" alt="1点から卸価格で" class="ttl">
					</div>
				</div>
				<!-- 無料トライアル登録受付中！ -->
				<div class="entry-form entry-form-location" id="entryForm1">
					<h1>無料トライアル登録受付中</h1>
					<p class="entry-coupon">
						<span>-入会特典-</span>
						<span>初めての注文で使える！</span>
						<span>送料無料クーポン発行中</span>
					</p>
					<form>
						<div class="mailAddressOverError fo-error-exclamation error-100" style="display:none;">100文字以内で入力してください。</div>
						<div class="mailAddressInvalidError fo-error-exclamation error-mail" style="display:none;">メールアドレスの形式（xxxx@xx.xx）で最後まで入力してください。</div>
						<div class="mail-input">
							<div class="mailAddressGuide realtime-err" style="display:none;">Please enter full email address in the format of the  (xxxx@xx.xx).</div>
							<input type="text" name="mailAddress" class="mailAddress" placeholder="Enter your email address" size="50" maxlength="100" ng-model="email">
							<input type="text" name="mailAddress" class="mailAddress" placeholder="Enter your username" size="50" maxlength="100"  ng-model="username">
							<input type="password" name="mailAddress" class="mailAddress" placeholder="Enter your password" size="50" maxlength="100"  ng-model="password">
							<input type="hidden" name="location" value="">
						</div>
						<div class="mail-button">
							<div class="co-btn co-btn-red co-btn-m co-btn-page">
								<span class="entryMailFormSubmit">
									<img alt="" src="<?=base_url()?>assets/img/pages/signup/icon_btn_page_03.png">
									<input type="button" value="メールを送信して登録開始！" ng-click="startRegistration()">
								</span>
							</div>
						</div>
						<div class="mailAddressRegisteredError" style="display:none;">
							<p class="fo-error-exclamation co-mt10 error-already">すでに会員登録されている<span class="co-dib">メールアドレスです。</span>
							</p>
							<p class="co-fcred co-b font-13">会員専用のIDまたはメールアドレス・<span class="co-dib">パスワードで</span>
								<span class="co-dib">
									<a href="<?=site_url()?>auth/login" class="co-writing-link">ログイン</a>をお試しください。</span>
								</p>
								<div class="entry-mailcheck-err trial-tooltip">
									<div class="co-tooltip-onclick-wrap trial-tooltip" style="z-index: 5;">
										<a href="javascript:void(0)" class="co-tooltip-button co-popup-link-02">会員登録直後の方へ</a>
										<div style="display: none;" class="co-tooltip-onclick">
											<div class="co-tooltip-cancel">×</div>
											<div class="co-m15 co-tac">
												<p class="co-mb0">ご利用開始のお手続きが完了しましたらご連絡いたしますので、<span class="co-dib">今しばらくお待ちください。</span>
												</p>
												<div class="co-pt5">
													<a href="javascript:;" class="co-001g" target="_blank">会員登録から取引までの流れ</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
						<ul>
							<li>ご登録後、事業情報の確認をさせていただきます。</li>
							<li>トライアル期間はご入会日より翌月末までです。継続利用をご希望の方はトライアル期間中にお申し込みください。</li>
						</ul>
						<p class="link">
							<a class="co-005g" href="javascript:;" target="_blank">継続利用について</a>
						</p>
						<p class="link">
							<a class="co-001g" href="javascript:;">海外に店舗をお持ちの方はこちら</a>
						</p>
					</div>
					<!-- スーパーデリバリーとは？ -->
					<div class="about w96per">
						<h4>What&nbsp;is&nbsp;SUPER&nbsp;DELIVERY?</h4>
						<h1>スーパーデリバリー<span class="co-dib">とは？</span>
						</h1>
						<div class="content-wrap">
							<div class="left">
								<p class="ttl co-b">スーパーデリバリーは、<span class="pc-br">事業者専用の卸・仕入れサイトです。</span>
								</p>
								<p>小売店を中心に飲食店・ヘアサロン・ホテル・店舗デザイン事務所など70,000以上の事業者にご利用いただいています。消費者へ販売する商品の仕入れ、店舗・事業で必要な備品・什器の調達にご利用ください。</p>
								<img src="<?=base_url()?>assets/img/pages/signup/logo_service.png" alt="日本サービス大賞" class="logo_service">
							</div>
							<div class="right">
								<img src="<?=base_url()?>assets/img/pages/signup/pc_sp.png" alt="" class="pc_sp">
							</div>
						</div>
					</div>
					<!-- 事業者向けの価格設定 -->
					<div class="config w96per">
						<h4>Pricing&nbsp;for&nbsp;businesses</h4>
						<h1>事業者向けの価格設定</h1>
						<p class="co-mb2">すべて卸価格なので<span class="sp-br">一般には公開していません</span>
						</p>
						<p class="co-mb2">会員の方だけご覧いただけます</p>
						<div class="content-wrap">
							<div class="data left-data">
								<div>
									<img src="<?=base_url()?>assets/img/pages/signup/ex_store1.jpg" alt="">
								</div>
								<div class="data-text">
									<h3>レディースのアパレルの参考データ</h3>
									<p class="price">卸価格：小売価格の<span class="big-red co-b">約47%</span>
									</p>
									<p>※2017年7月10日現在 当該ジャンルの平均</p>
								</div>
							</div>
							<div class="data right-data">
								<div>
									<img src="<?=base_url()?>assets/img/pages/signup/ex_store2.jpg" alt="">
								</div>
								<div class="data-text">
									<h3>インテリア雑貨ジャンルの参考データ</h3>
									<p class="price">卸価格：小売価格の<span class="big-red co-b">約58%</span>
									</p>
									<p>※2017年7月10日現在 当該ジャンルの平均</p>
								</div>
							</div>
							<div class="content-wrap genre-wrap">
								<div class="genre co-clf">
									<h3>その他の取り扱いジャンル</h3>
									<ul class="fashion">
										<li class="genre-ttl">ファッション</li>
										<li>レディースアパレル</li>
										<li>メンズアパレル</li>
										<li>ベビー・キッズ</li>
										<li>バック・財布・靴</li>
										<li>服飾雑貨</li>
										<li>アクセサリー</li>
									</ul>
									<ul class="interior">
										<li class="genre-ttl">家具・インテリア</li>
										<li>家具・寝具</li>
										<li>ライト・照明</li>
										<li>ファブリック・敷物</li>
										<li>インテリア雑貨</li>
									</ul>
									<ul class="goods">
										<li class="genre-ttl">生活雑貨</li>
										<li>日用品</li>
										<li>食器・キッチン</li>
										<li>ステーショナリー</li>
										<li>手芸・クラフト用品</li>
										<li>玩具・ホビー</li>
										<li>バス・トイレ・ラン<span class="co-dib">ドリー</span>
										</li>
										<li>ヘルスケア・コスメ</li>
										<li>ヒーリング・アロマ</li>
										<li>レジャー・スポーツ</li>
										<li>ガーデニング用品</li>
										<li>ペット用品</li>
										<li>介護用品</li>
									</ul>
									<ul class="electric">
										<li class="genre-ttl">電化製品</li>
										<li>PC関連機器</li>
										<li>携帯電話関連</li>
										<li>AV機器・カメラ</li>
										<li>生活家電</li>
									</ul>
									<ul class="foods">
										<li class="genre-ttl">食品・菓子・飲料</li>
										<li>健康食品</li>
										<li>調味料</li>
										<li>菓子・飲料</li>
									</ul>
									<ul class="furniture">
										<li class="genre-ttl">什器・店舗備品</li>
										<li>ラッピング用品</li>
										<li>季節用品</li>
										<li>イベント用品</li>
										<li>ディスプレイ用品</li>
										<li>店舗什器</li>
									</ul>
									<ul class="books">
										<li class="genre-ttl">本</li>
										<li>実用書</li>
										<li>絵本・児童文学</li>
										<li>雑誌</li>
									</ul>
								</div>
							</div>
							<p class="co-b p-text">掲載商品は<span class="big-red">87万点</span>以上・<span class="sp-br">出展企業<span class="big-red">1,300社</span>以上！</span>
							</p>
						</div>
					</div>
					<!-- 無料トライアル登録受付中！中間 -->
					<div class="entry-form2 entry-form-location" id="entryForm2">
						<h1>無料トライアル登録受付中！</h1>
						<form >
							<div class="mailAddressOverError fo-error-exclamation error-100" style="display:none;">100文字以内で入力してください。</div>
							<div class="mailAddressInvalidError fo-error-exclamation error-mail" style="display:none;">メールアドレスの形式（xxxx@xx.xx）で最後まで入力してください。</div>
							<div class="mail-input">
								<div class="mailAddressGuide realtime-err" style="display:none;">メールアドレスの形式（xxxx@xx.xx）で最後まで入力してください。</div>
								<input type="text" name="mailAddress" class="mailAddress" placeholder="メールアドレスを入力" size="50" maxlength="100" value="">
								<input type="hidden" name="location" value="">
							</div>
							<div class="mail-button">
								<div class="co-btn co-btn-red co-btn-m co-btn-page">
									<span class="entryMailFormSubmit">
										<img alt="" src="<?=base_url()?>assets/img/pages/signup/icon_btn_page_03.png">
										<input type="button" value="メールを送信して登録開始！">
									</span>
								</div>
							</div>
							<div class="mailAddressRegisteredError" style="display:none;">
								<div class="error-already2-text">
									<p class="fo-error-exclamation error-already2">すでに会員登録されている<span class="co-dib">メールアドレスです。</span>
									</p>
									<p class="co-fcred co-b font-13">会員専用のIDまたはメールアドレス・<span class="co-dib">パスワードで</span>
										<span class="co-dib">
											<a href="<?=site_url()?>auth/login" class="co-writing-link">ログイン</a>をお試しください。</span>
										</p>
									</div>
									<div class="entry-mailcheck-err trial-tooltip">
										<div class="co-tooltip-onclick-wrap trial-tooltip" style="z-index: 5;">
											<a href="javascript:void(0)" class="co-tooltip-button co-popup-link-02">会員登録直後の方へ</a>
											<div style="display: none;" class="co-tooltip-onclick">
												<div class="co-tooltip-cancel">×</div>
												<div class="co-m15 co-tac">
													<p class="co-mb0">ご利用開始のお手続きが完了しましたらご連絡いたしますので、<span class="co-dib">今しばらくお待ちください。</span>
													</p>
													<div class="co-pt5">
														<a href="javascript:;" class="co-001g" target="_blank">会員登録から取引までの流れ</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</form>
							<ul>
								<li>ご登録後、事業情報の確認をさせていただきます。</li>
								<li>トライアル期間はご入会日より翌月末までです。継続利用をご希望の方はトライアル期間中にお申し込みください。</li>
							</ul>
							<p class="link">
								<a class="co-005w" href="javascript:;" target="_blank">継続利用について</a>
							</p>
							<p class="link">
								<a class="co-001w" href="javascript:;">海外に店舗をお持ちの方はこちら</a>
							</p>
						</div>
						<!-- あらゆる業種の強い味方 -->
						<div class="use w96per">
							<h4>Strong&nbsp;ally&nbsp;of&nbsp;all&nbsp;business&nbsp;type</h4>
							<h1>あらゆる業種の強い味方</h1>
							<p class="co-mb2">スーパーデリバリーは<span class="sp-br">すべての事業者がご利用いただけます</span>
							</p>
							<div class="content-wrap">
								<div class="voice">
									<div class="box">
										<div class="ex-voice ex-voice1">
											<img src="<?=base_url()?>assets/img/pages/signup/ex_voice1.jpg" alt="">
										</div>
										<div class="box-text">
											<p class="co-b voice-ttl">小売業の声</p>
											<ul>
												<li>仕入れにかかる労務費コストを削減できました</li>
												<li>新しい商品を発見できます！</li>
												<li>1点買いできる便利さが良い</li>
											</ul>
										</div>
									</div>
									<div class="box">
										<div class="ex-voice ex-voice2">
											<img src="<?=base_url()?>assets/img/pages/signup/ex_voice2.jpg" alt="">
										</div>
										<div class="box-text">
											<p class="co-b voice-ttl">飲食業の声</p>
											<ul>
												<li>商品点数の多さにビックリ！</li>
												<li>重いものやまとめ買いはやっぱりネットが便利</li>
												<li>お店のイメージを変えたいときに使います</li>
											</ul>
										</div>
									</div>
									<div class="box">
										<div class="ex-voice ex-voice3">
											<img src="<?=base_url()?>assets/img/pages/signup/ex_voice3.jpg" alt="">
										</div>
										<div class="box-text">
											<p class="co-b voice-ttl">美容業の声</p>
											<ul>
												<li>卸値とクオリティに満足できる</li>
												<li>洋服や雑貨でサロンをライフスタイル提案型にできました</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<!-- 開業準備中の方もご登録ください -->
							<div class="pre-open">
								<p class="pre-open-ttl co-b">開業準備中の方も<span class="sp-br">ご登録ください！</span>
								</p>
								<div>
									<div class="image">
										<img src="<?=base_url()?>assets/img/pages/signup/pre_open.jpg" alt="">
									</div>
									<div class="right-text">
										<div class="description">　
											<p>開業前でも店舗物件が確定している方は<span>ご登録いただけます。</span>
												<span>備品を安く揃えて、<span class="sp-br">開業資金を節約しませんか？</span>
											</span>
										</p>
									</div>
									<div class="support">
										<div class="ttl">
											<div class="tri tri01">
											</div>
											<div class="text">
												<h4>開業サポート実施中！</h4>
											</div>
											<div class="tri tri02">
											</div>
										</div>
										<ul>
											<li>開業前でも初回取引から後払いOK！</li>
											<li>後払いの利用限度額を従来の30万円から70万円に増額！<p>※決済サービス「Paid」を使用。ご利用には審査があります。</p>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- よくある質問 -->
					<div class="faq w96per">
						<h4>FAQ</h4>
						<h1>よくある質問</h1>
						<p>ご不明な点がございましたら<span class="sp-br">専用のサポート窓口まで</span>
							<span class="sp-br">お気軽にご相談ください！</span>
						</p>
						<div class="content-wrap">
							<div class="box box1">
								<div class="ques">
									<img src="<?=base_url()?>assets/img/pages/signup/qa_ques.png" alt="Q">
									<p class="co-b">購入代金の<span>支払い方法は？</span>
									</p>
								</div>
								<div class="ans">
									<img src="<?=base_url()?>assets/img/pages/signup/qa_ans.png" alt="A">
									<p>クレジットカード・後払い（Paid）・代金引換がご利用いただけます。<br>※Paidとは株式会社ラクーンフィナンシャルが提供するサービスです。</p>
								</div>
							</div>
							<div class="box box2">
								<div class="ques">
									<img src="<?=base_url()?>assets/img/pages/signup/qa_ques.png" alt="Q">
									<p class="co-b">最低注文数、<span>最低注文金額は？</span>
									</p>
								</div>
								<div class="ans">
									<img src="<?=base_url()?>assets/img/pages/signup/qa_ans.png" alt="A">
									<p>掲載商品の75%以上は1点からご注文いただけます。最低注文金額の設定はございません。</p>
								</div>
							</div>
							<div class="box box3">
								<div class="ques">
									<img src="<?=base_url()?>assets/img/pages/signup/qa_ques.png" alt="Q">
									<p class="co-b">料理教室をしていますが<span class="sp-br">利用できますか？</span>
									</p>
								</div>
								<div class="ans">
									<img src="<?=base_url()?>assets/img/pages/signup/qa_ans.png" alt="A">
									<p>業種・事業規模を問わず、すべての事業者にご利用いただいております。</p>
								</div>
							</div>
							<div class="box box4">
								<div class="ques">
									<img src="<?=base_url()?>assets/img/pages/signup/qa_ques.png" alt="Q">
									<p class="co-b">消費者は<span>入会できますか？</span>
									</p>
								</div>
								<div class="ans">
									<img src="<?=base_url()?>assets/img/pages/signup/qa_ans.png" alt="A">
									<p>消費者の方はご入会いただけない事業者専用のサービスです。ご商売や事業の確認のため、ご登録情報をもとに入会の審査をさせていただきます。</p>
								</div>
							</div>
						</div>
					</div>
					<!-- 無料トライアル登録受付中！中間 -->
					<div class="entry-form2 entry-form2-bottom entry-form-location" id="entryForm3">
						<h1>無料トライアル登録受付中！</h1>
						<form>
							<div class="mailAddressOverError fo-error-exclamation error-100" style="display:none;">100文字以内で入力してください。</div>
							<div class="mailAddressInvalidError fo-error-exclamation error-mail" style="display:none;">メールアドレスの形式（xxxx@xx.xx）で最後まで入力してください。</div>
							<div class="mail-input">
								<div class="mailAddressGuide realtime-err" style="display:none;">メールアドレスの形式（xxxx@xx.xx）で最後まで入力してください。</div>
								<input type="text" name="mailAddress" class="mailAddress" placeholder="メールアドレスを入力" size="50" maxlength="100" value="">
								<input type="hidden" name="location" value="">
							</div>
							<div class="mail-button">
								<div class="co-btn co-btn-red co-btn-m co-btn-page">
									<span class="entryMailFormSubmit">
										<img alt="" src="<?=base_url()?>assets/img/pages/signup/icon_btn_page_03.png">
										<input type="button" value="メールを送信して登録開始！">
									</span>
								</div>
							</div>
							<div class="mailAddressRegisteredError" style="display:none;">
								<div class="error-already2-text">
									<p class="fo-error-exclamation error-already2">すでに会員登録されている<span class="co-dib">メールアドレスです。</span>
									</p>
									<p class="co-fcred co-b font-13">会員専用のIDまたはメールアドレス・<span class="co-dib">パスワードで</span>
										<span class="co-dib">
											<a href="<?=site_url()?>auth/login" class="co-writing-link">ログイン</a>をお試しください。</span>
										</p>
									</div>
									<div class="entry-mailcheck-err trial-tooltip">
										<div class="co-tooltip-onclick-wrap trial-tooltip" style="z-index: 5;">
											<a href="javascript:void(0)" class="co-tooltip-button co-popup-link-02">会員登録直後の方へ</a>
											<div style="display: none;" class="co-tooltip-onclick">
												<div class="co-tooltip-cancel">×</div>
												<div class="co-m15 co-tac">
													<p class="co-mb0">ご利用開始のお手続きが完了しましたらご連絡いたしますので、<span class="co-dib">今しばらくお待ちください。</span>
													</p>
													<div class="co-pt5">
														<a href="javascript:;" class="co-001g" target="_blank">会員登録から取引までの流れ</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</form>
							<ul>
								<li>ご登録後、事業情報の確認をさせていただきます。</li>
								<li>トライアル期間はご入会日より翌月末までです。継続利用をご希望の方はトライアル期間中にお申し込みください。</li>
							</ul>
							<p class="link">
								<a class="co-005w" href="javascript:;" target="_blank">継続利用について</a>
							</p>
							<p class="link">
								<a class="co-001w" href="javascript:;">海外に店舗をお持ちの方はこちら</a>
							</p>
							<div class="invitation">
								<p>スーパーデリバリーで商品を販売したい<span class="sp-br">メーカー様はこちらから</span>
								</p>
								<p>
									<a class="link" href="<?=site_url()?>admin">
										<img src="<?=base_url()?>assets/img/pages/signup/for_maker_invitation_icon00.png" alt="">
										<span>出展のご案内</span>
									</a>
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="push">
				</div>
			</div>
			<noscript>
				<div style="display:inline;">
					<img height="1" width="1" style="border-style:none;" alt="" src="//b97.yahoo.co.jp/pagead/conversion/1000127633/?guid=ON&script=0&disvt=false"/>
				</div>
			</noscript>
			<div id="footer-area-common3" class="co-container foot-simple foot-entry-form">
				<div class="up">
					<div class="wrap co-clf">
						<div class="left-column">
							<ul class="menu">
								<li class="line-h">
									<a href="javascript:;" target="_blank"><?=_lang('guide')?></a>
								</li>
<!--
-->
<li>
	<a href="javascript:;" target="_blank">プライバシー・<span class="sp-br">ステートメント</span>
	</a>
</li>
<!--
-->
<li>
	<a href="javascript:;" target="_blank">会社概要</a>
</li>
</ul>
</div>
</div>
</div>
<div class="down">
	<p class="copyright">スーパーデリバリーは個人情報を暗号化して送信するSSLに対応しています。<br>
		<span class="under">（C）2002 RACCOON HOLDINGS, Inc.</span>
	</p>
</div>
</div>
</div>