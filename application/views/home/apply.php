<div id="jsp-tiles-productdetail-c" class="add-wishlist-detail-page">
<?php require_once('partials/company_info.php'); ?>
<?php require_once('partials/company_info_dls.php'); ?>
<div id="co-fullh-mdl-mdlbg"></div>
<div class="push"></div>
<div id="trade-apply" class="co-container">
	<div class="step-info step2">
		<div class="step-txt"><p class="now-step">申請</p><p>完了</p></div>
	</div>
	<form class="co-w750">
		<input type="hidden" name="code" value="84326">
		<input type="hidden" name="digestFlag" value="0">
		<p class="co-title2"><?=_lang('terms_and_conditions')?></p>
		<p class="co-fcgray co-fs12">※下記条件をお守りいただけない場合はお取引中止となる場合がございます。</p>
		<?=$company_info['company_informations'][1]?>
		<p class="co-title2 co-mt30">申請</p>
		<div class="fo-contents fo-contents-simplline">
			<div class="wrap">
				<div class="fo-rwd-table">
					<div class="row">
						<p class="th-cap">メールマガジンの受信設定</p>
						<div class="td-txt">
							<label for="dealerMailReceiveFlg_1" id="mailmag_k" class="co-cp">
								<input type="radio" name="dealerMailReceiveFlg" value="1" checked="" id="dealerMailReceiveFlg_1">
								受け取る
							</label>
							<label for="dealerMailReceiveFlg_0" id="mailmag_d" class="co-ml20 co-cp">
								<input type="radio" name="dealerMailReceiveFlg" value="0" id="dealerMailReceiveFlg_0">
								受け取らない
							</label>
							<div class="co-mt5 co-fs12 co-fcgray">
								※メールマガジンは、登録されているメールアドレス宛に届きます。
							</div>
						</div>
					</div>
				</div>
				<div class="fo-submit">
					<div class="co-btn co-btn-red co-btn-m co-ml5">
						<a href="<?=site_url()?>home/done?user_id=<?=$company_info['user_id']?>"><span><input type="button" value="申請する（無料）"></span></a>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
<div class="push"></div>