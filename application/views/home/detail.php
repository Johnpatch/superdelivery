<div class="co-pankuzu-list">
	<a href="<?=site_url()?>"><?=_lang('to_top')?></a>
	<a href="<?=site_url()?>home/product_list"><?=_lang('all_category')?></a>
	<?php foreach (array_reverse($cate_results) as $cate_result) { ?>
		<a href="<?=site_url()?>home/product_list?category_id=<?=$cate_result->id?>"><?=$cate_result->name?></a>
	<?php } ?>
</div>
<div id="jsp-tiles-productdetail-c" class="add-wishlist-detail-page">
<?php require_once('partials/company_info.php'); ?>
<?php require_once('partials/company_info_dls.php'); ?>
<div id="co-fullh-mdl-mdlbg"></div>	
<div id="product-detail-wrap" class="product-detail-table co-clf co-container">
	<div class="product-image-wrap">
		<div class="product-image-box">	
			<div class="co-clf image-box-wrap">
				<div id="product_image_prev" class="navi-prev-box detail-navi-active"><span>前へ</span></div>
				<div id="product_image_next" class="navi-next-box detail-navi-active"><span>次へ</span></div>
				<div id="imageBoxInner" class="image-box-inner co-clf">
					<div id="largeImageParentBox" class="large-image-relative co-img-center-wrap">
						<div class="large-image-zoom-button"><img src="<?=base_url()?>assets/img/pages/detail/icon_zoom.png" alt=""></div>
						<div class="large-image-absolute">
							<div id="largeImageBox">
								<img id="product_image_preview" src="<?=$product->main_image?>" title="<?=$product->name?>">
							</div>
						</div>
					</div>
		
					<div id="thumbImageBox" class="thum-image-box">
					<?php foreach ($gallery_images as $key => $gallery_image) { ?>
						<div class="thum-image-float product_image_preview_thumbnail" data-index="<?=$key?>">
							<div id="thumbnail_<?=$key?>" class="thum-image-selected">
								<img src="<?=$gallery_image?>">
							</div>
						</div>
					<?php } ?>
					</div>
				</div>
			</div>				
		</div>
		<input type="radio" id="product-comment-show-pt" class="co-tb-only">
		<div id="product_comment" class="product-comment co-pt-only">
			<div class="product-more-txt" style="">
				<table class="product-comment-wrap">
					<tbody><tr><td>
						<?php foreach ($reviews as $key => $review) {
							echo $review->content. "<br><br>";
						} ?>
					</td></tr></tbody>
				</table>
			</div>
			<label for="product-comment-show-pt" class="co-tb-only" style="display: inline;">続きを見る</label>
		</div>
	</div>
	<div class="product-setinfo-wrap">
		<div class="product-text-box">
			<div class="product-name-wrap">
				<span class="product-name"><?=$product->name?></span>
				<span style="display: none"><img id="promark_movie" src="<?=base_url()?>assets/img/pages/detail/clear.gif" alt="" width="1" height="1" style="display: none;"></span>
				<table cellpadding="0" cellspacing="0" class="brand-genre co-pc-only">
					<tbody>
						<tr>
							<td class="co-vatop"><?=_lang('brand')?></td>
							<td class="co-vatop">：&nbsp;</td>
							<td class="co-vatop">
								<a href="<?=site_url()?>home/product_list?brand_id=<?=$brand->id?>"><?=$brand->name?></a>
							</td>
						</tr>
						<tr>
							<td class="co-vatop"><?=_lang('custom_category')?></td>
							<td class="co-vatop">：&nbsp;</td>
							<td class="co-vatop">
								<a href="<?=site_url()?>home/product_list?custom_category=<?=$cate2->id?>" style="display: inline-block;"><?=$cate2->name?></a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="co-fs12 co-pc-only">
				<span>SD品番：<?=$product->sku?></span>
			</div>
			<?php
				if ($this->identity->is_guest()) 
					require_once('partials/detail_table_logout.php'); 
				else if ($company_info['company_status'] == 1)
					require_once('partials/detail_table.php'); 
				else
					require_once('partials/detail_table_unapplied.php'); 
			?>
		</div></div>
		<div class="product-details">
			<div class="product-details-ttl co-pc-only">
				<div class="co-title2">詳細情報</div><div class="product-entry-date">
					登録日：<?=$this->utils->print_ymd($product->date)?>
				</div>
			</div>
			<div class="product-text-box">
				<div class="product-detail-information">
					<input type="radio" id="product-comment-show">
					<input type="radio" id="product-dealer-caution-show">
					<dl class="product-detail-infolist">
						<dt>出荷</dt><dd>当日</dd>
						<dt>サイズ・容量</dt><dd><?=$product->sizeandcapacity?></dd>
						<dt>規格</dt><dd><?=$product->standard?></dd>
						<dt>注意事項</dt><dd><?=$product->notes?></dd>
					</dl>
				</div>
			</div>
			<div class="co-ts-only dealer-info-list-wrap">
				<p><?=$company_info['username']?></p>
				<ul class="dealer-info-list">
					<li><a id="dl-infobtn-trade-<?=$product->user_id?>" class="dl-infobtn-mdl"><?=_lang('terms_and_conditions')?></a></li>
					<li><a id="dl-infobtn-payship-<?=$product->user_id?>" class="dl-infobtn-mdl">送料・決済方法</a></li>
					<li><a id="dl-infobtn-about-<?=$product->user_id?>" class="dl-infobtn-mdl co-001g">企業情報</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="product-word-link">
		<div id="product-word">
			<p>この商品に関連する検索ワード</p>
			<ul class="co-clf">	
				<?php foreach ($keywords as $keyword) { ?>
				<li>
					<a href="<?=site_url()?>home/search?keyword=<?=$keyword?>"><?=$keyword?></a>
				</li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div id="relatedBox" class="recommend-box-wrap">
		<div class="related-box-dl">
			<input type="checkbox" name="moreRecommend" id="more-recommend-btn01">
			<p class="recommend-ttl">関連商品</p>
			<div class="recommend-box-area">
				<?php foreach ($related_products as $related_product) { ?>
					<div class="recommend-item-box">
						<div class="recommend-item-img wish-dvs-jdg recom-wish">
							<a href="<?=site_url()?>home/good_detail/<?=$related_product->id?>">
								<img class="recommend-img" title="<?=$related_product->name?>" src="<?=$related_product->main_image?>">
							</a>
							<div class="add-to-wishlist wishlist_multi prelogin-wishlist <?=$related_product->is_wished==1?'added-wish ':''?>" id="pd_<?=$related_product->id?>_<?=$related_product->user_id?>">
								<span class="add-wish-icon" ng-click="wishlist(<?=$related_product->id?>, <?=$related_product->user_id?>)"></span>
							</div>
						</div>
						<div class="recommend-item-name">
							<a href="<?=site_url()?>home/good_detail/<?=$related_product->id?>"><?=$related_product->name?></a>
						</div>
						<div class="item-dealer"><div class="recommend-dealer-name"><a href="<?=site_url()?>home/product_list?user_id=<?=$related_product->user_id?>"><?=$related_product->username?></a></div></div>
					</div>
				<?php } ?>		
				<label for="more-recommend-btn01" class="more-recommend">もっと見る</label>
			</div>
		</div>
	</div>
	<div id="recommendBox" class="co-container recommend-box-wrap" style="display: block;">
		<div class="recommend-box-dl">
			<input type="checkbox" name="moreRecommend" id="more-recommend-btn02">
			<p class="recommend-ttl">よく一緒にチェックされている商品</p>
			<?php foreach ($recommend_products as $key => $recommend_product) { ?>
			<div id="recommendItem" class="recommend-box-area">
				<div class="recommend-item-box">
					<div class="recommend-item-img">
						<a href="<?=site_url()?>home/good_detail/<?=$recommend_product->id?>">
							<img class="recommend-img" title="<?=$recommend_product->name?>" src="<?=$recommend_product->main_image?>">
						</a>
						<div class="add-to-wishlist wishlist_multi prelogin-wishlist <?=$recommend_product->is_wished==1?'added-wish ':''?>" id="pd_<?=$recommend_product->id?>_<?=$recommend_product->user_id?>">
							<span class="add-wish-icon" ng-click="wishlist(<?=$recommend_product->id?>, <?=$recommend_product->user_id?>)"></span>
						</div>
					</div>
					<div class="recommend-item-name">
						<a href="<?=site_url()?>home/good_detail/<?=$recommend_product->id?>"><?=$recommend_product->name?></a>
					</div>
					<div class="item-dealer">
						<div class="recommend-dealer-name"><a href="<?=site_url()?>home/product_list?user_id=<?=$recommend_product->user_id?>"><?=$recommend_product->username?></a></div>
					</div>
				</div>
			</div>
			<?php } ?>
			<label for="more-recommend-btn02" class="more-recommend">もっと見る</label>
		</div>
	</div>
	<div id="btm-recent-chk-common" class="recommend-box-wrap recommend-scroll-box" style="">
		<p class="recommend-ttl">最近チェックした商品</p>
		<div class="recommend-box-area" id="recent-items-wrap">
			<div id="bottomsId-bk" class="recommend-btn-bk co-btn co-btn-wht co-btn-dsbl">
				<span><a href="javascript:void(0);"></a></span>
			</div>
			<div id="recent-items" class="recommend-scroll-area flex-box" fill-dummy="" fix-space="90" min-space="5" max-cols="8" forward-id="bottomsId-fw" back-id="bottomsId-bk" forward-disable-class="co-btn-dsbl" back-disable-class="co-btn-dsbl" is-swipe="true" swipe-border-width="940">
				<?php foreach ($recent_products as $key => $recent_product) { ?>
				<div class="recommend-item-box" style="display: inline-block; margin-left: 0px; margin-right: 126px; margin-top: 0px; height: 198px;">
					<div class="recommend-item-img wish-dvs-jdg recom-wish">
						<a href="<?=site_url()?>home/good_detail/<?=$recent_product->id?>">
							<img class="recommend-img" title="<?=$recent_product->name?>" src="<?=$recent_product->main_image?>">
						</a>
						<div class="add-to-wishlist wishlist_multi prelogin-wishlist <?=$recent_product->is_wished==1?'added-wish ':''?>" id="pd_<?=$recent_product->id?>_<?=$recent_product->user_id?>">
							<span class="add-wish-icon" ng-click="wishlist(<?=$recent_product->id?>, <?=$recent_product->user_id?>)"></span>
						</div>
					</div>
					<div class="recommend-item-name">
						<a href="<?=site_url()?>home/good_detail/<?=$recent_product->id?>"><?=$recent_product->name?></a>
					</div>
					<div class="item-dealer">
						<div class="recommend-dealer-name"><a href="<?=site_url()?>home/product_list?user_id=<?=$recent_product->user_id?>"><?=$recent_product->username?></a></div>
					</div>
				</div>
				<?php } ?>
			</div>
			<div id="bottomsId-fw" class="recommend-btn-fw co-btn co-btn-wht co-btn-dsbl">
				<span><a href="javascript:void(0);"></a></span>
			</div>
		</div>
	</div>
	<?php	require_once('partials/bottom_genre_box.php'); ?>
	<div>
		<div style="width: 100%;">
		</div>
	</div>
</div>

<div id="pdeatil-image-modal" class="co-container">
	<div class="co-tooltip-text">
		<div class="co-tooltip-cancel">×</div>
		<div id="pdeatil-large-img-wrap" class="detail-modal-wrap">
			<div class="detail-modal-image-wrap">
				<a id="product_image_detail_prev" class="detail-modal-prev detail-navi-active" href="javascript:void(0)"><span>前へ</span></a>
				<div class="detail-modal-large-box">
					<span id="zoom_">
						<img class="defaultImage" id="product_image_detail"  src="<?=$product->main_image?>">
						<img class="defaultImage" id="product_image_detail_anime" src="<?$product->main_image?>">
					</span>
				</div>
				<a id="product_image_detail_next" class="detail-modal-next detail-navi-active" href="javascript:void(0)"><span>次へ</span></a>
			</div>
			<div class="detail-modal-thum-wrap" id="detailThumbBox">
				<?php foreach ($gallery_images as $key => $gallery_image) { ?>
					<div class="detail-modal-thum-box product_image_detail_thumbnail modal-thum-on" data-index="<?=$key?>" >
						<div class="detail-modal-thum">
							<img src="<?=$gallery_image?>">
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>