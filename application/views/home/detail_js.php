<script type="text/javascript" src="<?=base_url()?>assets/js/jquery/jquery.raccoonDialog.js" charset="Shift_JIS"></script>
<script type="text/javascript">
	$(function() {
		$(".quantity_button").click(function(){
			var clickID = $(this).attr('id');
			var res = clickID.split('_');
			var cur_val = parseInt($("#input_" + res[1]).val());
			if (res[2] == 'up') {
				cur_val++;
			} else {
				if (cur_val > 1)
					cur_val--;
			}
			$("#input_" + res[1]).val(cur_val);
		});
	});
</script>
<script type="text/javascript"> //reviews
	function explain(documentId, displayId) {
		document.getElementById(documentId).onmouseover = function() {
			tid = setTimeout("exDisplayOn('" + displayId + "')", 0);
		}

		document.getElementById(documentId).onmouseout = function() {
			document.getElementById(displayId).style.display = "none";
			clearTimeout(tid);
		}
		document.getElementById(documentId).onclick = function() {
			if(document.getElementById(displayId).style.display=="block"){
				document.getElementById(displayId).style.display = "none";
				clearTimeout(tid);
			}
			else{
				tid = setTimeout("exDisplayOn('" + displayId + "')", 0);
			}
		}
	}

	function exDisplayOn(displayId) {
		document.getElementById(displayId).style.display = "block";
	}
	explain('ex<?=$company_info['user_id']?>','ex_display<?=$company_info['user_id']?>')
</script>

<script type="text/javascript">
	$(function() {
		var openId ="";
		$(".dl-infobtn-mdl").click(function(){
			openId = $(this).attr("id").replace("dl-infobtn-","").replace("-btm","");
			$("[id^='dl-" + openId + "-info-mdl']").show();
			$("#co-fullh-mdl-mdlbg").show();
			$(".co-fullh-mdl-content").scrollTop(0);
		});
		$("#co-fullh-mdl-mdlbg,.co-fullh-mdl-wrap .co-fullh-mdl-cls").click(function(){
			$("#co-fullh-mdl-mdlbg").hide();
			$("[id^='dl-" + openId + "-info-mdl']").hide();
		});
	});
</script>

<script type="text/javascript" src="<?=base_url()?>assets/js/raccoon_switch_image-1.0.2.js" charset="Shift_JIS"></script>
<script type="text/javascript">
	(function($) {
		var imagePaths = <?=json_encode($gallery_images)?>;
		var currentIndex = 0;
		
		changePreview(currentIndex);

		function changeIndex(index) {
			if(index < 0) {
				currentIndex = imagePaths.length - 1;
			} else if(index >= imagePaths.length) {
				currentIndex = 0;
			} else {
				currentIndex = index;
			}
		}
		function changePreview(index) {
			if (imagePaths.length == 0)
				return;
			changeIndex(index);
			refreshPreview();
		}
		function refreshPreview() {
			$('#product_image_preview_anime').hide();
			$('#product_image_preview').attr("src", imagePaths[currentIndex]);
			$('.thum-image-selected').not('#thumbnail_' + currentIndex).addClass('thum-image-vertical').removeClass('thum-image-selected');
			$('#thumbnail_' + currentIndex).addClass('thum-image-selected').removeClass('thum-image-vertical');
		}
		function animatePreview(index) {
			if(isBusy) return;
			var prevIndex = currentIndex;
			changeIndex(index);
			if(prevIndex == currentIndex) return;

			isBusy = true;

			var DURATION = 200;
			var distance = $('#largeImageParentBox').width();
			if(prevIndex > index) {
				distance *= -1;
			}
			$('#product_image_preview_anime').show();
			var $preview = $('#product_image_preview');
			animation(
				$preview
				,{"transform" : "translateX(" + -distance + "px)"}
				, DURATION);
			setTimeout(function(){
				$preview.css({"transform" : "translateX(" + distance + "px)", "transition" : ''}).attr("src", imagePaths[currentIndex]);
				setTimeout(function(){
					animation(
						$preview
						,{"transform" : "translateX(0px)"}
						, DURATION);
					setTimeout(refreshPreview, DURATION + 100);
				}, 50);
			}, DURATION + 5);

		}

		$(".product_image_preview_thumbnail").bind('click mouseover', function(){
			changePreview($(this).data('index'));
		});

		$("#product_image_prev").bind('click', function(){
			changePreview(currentIndex - 1);
		});

		$("#product_image_next").bind('click', function(){
			changePreview(currentIndex + 1);
		});

		var X = 0;
		var Y = 1;

		function calcDist(touches) {
			var dist = 0;
			for(var i = 0; i < touches.length - 1; i++) {
				for(var j = i + 1; j < touches.length; j++) {
					var tmpDist = 
						Math.sqrt(
							Math.pow(touches[i].pageX - touches[j].pageX, 2) +
							Math.pow(touches[i].pageY - touches[j].pageY, 2));
					if(tmpDist > dist) {
						dist = tmpDist;
					}
				}
			}
			return dist;
		}

		function calcCenterPos(touches) {
			var x = 0;
			var y = 0;
			for(var i = 0; i < touches.length; i++) {
				x += touches[i].pageX;
				y += touches[i].pageY;
			}
			return [x / touches.length, y / touches.length];
		}

		(function(){
			var moveStartPos;
			var currentPos;
			var touchCount;
			reset();

			function reset(){
				moveStartPos = null;
				touchCount = null;
			}

			$('#largeImageParentBox').unbind('touchmove');
			$('#largeImageParentBox').bind('touchmove', function(evt) {
				currentPos = calcCenterPos(evt.originalEvent.targetTouches);
				if(moveStartPos == null || touchCount != evt.originalEvent.targetTouches.length) {
					moveStartPos = currentPos;
				}
				touchCount = evt.originalEvent.targetTouches.length;
			});
			$('#largeImageParentBox').unbind('touchend');
			$('#largeImageParentBox').bind('touchend', function(evt) {
				if(currentPos && Math.abs(currentPos[X] - moveStartPos[X]) > $('#largeImageParentBox').width() / 5) {
					if(currentPos[X] < moveStartPos[X]) {
						animatePreview(currentIndex + 1);
					} else {
						animatePreview(currentIndex - 1);
					}
				}
				reset();
			});
		})();

		function showDetail() {
			if (imagePaths.length == 0)
				return;
			$('.exp-detail-fxregist').hide();
			refreshDetail();
			$('#pdeatil-image-modal').raccoonDialog();
			$('#pdeatil-image-modal').unbind();
			$('#pdeatil-image-modal').bind('touchstart touchmove touchend', function(evt){
				if($(evt.target).is(".detail-modal-large-box")) {
					evt.preventDefault();
				}
			})
			$('#pdeatil-image-modal .co-tooltip-cancel').unbind();
			$('#pdeatil-image-modal').bind('click', function(event) {
				if ($('#pdeatil-image-modal').css('display') == 'block') {
	  				if (!$(event.target).closest('#pdeatil-large-img-wrap').length) {
						$('#pdeatil-image-modal').raccoonDialog('close');
						$(window).unbind('resize', refreshDetailSize);
					}
	  			}
			});
			refreshDetailSize();
			$(window).bind('resize', refreshDetailSize);

			var $modalThumnail = $('.detail-modal-thum-box');

			$modalThumnail.unbind('click');
			$modalThumnail.bind('click', function(){
				changeDetail($(this).data('index'));
			});
	
			$("#product_image_detail_prev").unbind('click');
			$("#product_image_detail_prev").bind('click', function(){
				changeDetail(currentIndex - 1);
			});

			$("#product_image_detail_next").unbind('click');
			$("#product_image_detail_next").bind('click', function(){
				changeDetail(currentIndex + 1);
			});
			
			$('#product_image_detail').unbind('click')
			$('#product_image_detail').bind('click', function(evt){
				if(isBusy) return;
				if(zoomed) {
					detailImageTransform = {"scale" : 1};
					refreshDetail();
				} else {
					detailImageTransform = {"scale" : 3.2};
					zoomed = true;
					pcMove(evt);
					$('#product_image_detail').css({"transform" : getDetailImageTransform()});
				}
			});

			$(".product_image_detail_thumbnail").bind('click mouseover', function(){
				changeDetail($(this).data('index'));
			});

			function pcMove(evt) {
				if(!zoomed) return;
				if(isSmartPhone) return;

				var leftOver = $('#product_image_detail').width() / detailImageTransform['scale'] * ((detailImageTransform['scale'] - 1) / 2);
				detailImageTransform["translateX"] = (evt.offsetX / $('#product_image_detail').width() - 0.5) * -2 * leftOver;

				var topOver = $('#product_image_detail').height() / detailImageTransform['scale'] * ((detailImageTransform['scale'] - 1) / 2);
				detailImageTransform["translateY"] = (evt.offsetY / $('#product_image_detail').height() - 0.5) * -2 * topOver;
				
				$('#product_image_detail').css({"transform" : getDetailImageTransform()});
			}
			$('#product_image_detail').unbind('mousemove');
			$('#product_image_detail').bind('mousemove', function(evt){
				pcMove(evt);
			});

			(function(){
				var moveStartPos;
				var pinchStartDist;
				var touchCount;
				reset();

				function reset(){
					moveStartPos = null;
					pinchStartDist = null;
				}

				$('#product_image_detail').unbind('touchstart');
				$('#product_image_detail').bind('touchstart', function(evt) {
					moveStartPos = calcCenterPos(evt.originalEvent.targetTouches);
				})
				$('#product_image_detail').unbind('touchmove');
				$('#product_image_detail').bind('touchmove', function(evt) {  
					evt.preventDefault();
					if(isBusy) return;
					
					
					var movedPos = calcCenterPos(evt.originalEvent.targetTouches);
					if(touchCount != evt.originalEvent.targetTouches.length) {
						reset();
					}
					if(moveStartPos) {
						if(!detailImageTransform["translateX"]) detailImageTransform["translateX"] = 0;
						detailImageTransform["translateX"] += movedPos[X] - moveStartPos[X];
						
						if (zoomed) {
							if(!detailImageTransform["translateY"]) detailImageTransform["translateY"] = 0;
							detailImageTransform["translateY"] += movedPos[Y] - moveStartPos[Y];
						}
					}
					moveStartPos = movedPos;
					touchCount = evt.originalEvent.targetTouches.length;
					
					
					if(evt.originalEvent.changedTouches.length >= 2) {
						
						var dist = calcDist(evt.originalEvent.changedTouches);
						if(pinchStartDist) {
							detailImageTransform["scale"] -= ((pinchStartDist - dist) / $('#product_image_detail').width());
							if(detailImageTransform["scale"] < 1) {
								detailImageTransform["scale"] = 1;
							}
							if(detailImageTransform["scale"] > 2) {
								detailImageTransform["scale"] = 2;
							}
						}
						pinchStartDist = dist;
						zoomed = true;
						
					}
					$('#product_image_detail').css({"transform" : getDetailImageTransform()});
				})
				$('#product_imagle_detail').unbind('touchend');
				$('#product_image_detail').bind('touchend', function(evt) {
					if(event.targetTouches.length == 0) {
						reset();

						var x = null;
						var y = null;
						var move_size = 50;						
						var $detailBox = $('#pdeatil-image-modal .detail-modal-image-wrap');
						var $image = $('#product_image_detail');
						var imageWidth = $image.width() * detailImageTransform['scale'];
						var leftOver = $image.width() / detailImageTransform['scale'] * ((detailImageTransform['scale'] - 1) / 2) - ($detailBox.width() - $image.width()) / 2;
						var leftMargin = detailImageTransform["translateX"] - leftOver;
						var rightMargin = -1 * detailImageTransform["translateX"] - leftOver;
						var move = detailImageTransform["translateX"];

						if(move > 0 && move_size < move_size) {
							if(leftMargin < 0) {
								if(rightMargin > -1 * leftMargin) {
									x = detailImageTransform["translateX"] - leftMargin;
								} else {
									x = -1 * leftOver;
								}
							}
						
						} else if( move > move_size && !zoomed) {
							animateDetail(currentIndex - 1);
							return;
						}
						
						if(move < 0 && move > -move_size) {
							if (rightMargin < 0) {
								if(leftMargin > -1 * rightMargin) {
									x = detailImageTransform["translateX"] + rightMargin;
								} else {
									x = leftOver;
								}
							}
						
						} else if(move < -move_size && !zoomed) {
							animateDetail(currentIndex + 1);
							return;
						}
						
						var imageHeight = $image.height() * detailImageTransform['scale'];
						var topOver = $image.height() / detailImageTransform['scale'] * ((detailImageTransform['scale'] - 1) / 2) - ($detailBox.height() - $image.height()) / 2;
						var topMargin = detailImageTransform["translateY"] - topOver;
						var bottomMargin = -1 * detailImageTransform["translateY"] - topOver;
						if(bottomMargin > 0 && topMargin < 0) {
							if(bottomMargin > -1 * topMargin) {
								y = detailImageTransform["translateY"] - topMargin;
							} else {
								y = -1 * topOver;
							}
						}
						if(topMargin > 0 && bottomMargin < 0) {
							if(topMargin > -1 * bottomMargin) {
								y = detailImageTransform["translateY"] + bottomMargin;
							} else {
								y = topOver;
							}
						}

						if(x !== null || y !== null) {
							if(x !== null) detailImageTransform["translateX"] = x;
							if(y !== null) detailImageTransform["translateY"] = y;
							animation($('#product_image_detail'), {"transform" : getDetailImageTransform()}, 300);
						}
					}
				})
			})();
		}
		$('#largeImageParentBox').bind('click', showDetail);
		
		function animation($target, css, duration) {
			$target.css({"transition" : "all " + duration + "ms ease-in-out"}).css(css);
			setTimeout(function(){
				isBusy = false;
				$target.css({"transition" : ''});
			}, duration);
		}
		var THRESHOLD_SMARTPHONE = 940;
		var isSmartPhone;		
		function refreshDetailSize(){
			var winW = $(window).width();
			var dtlMdlLrgBoxMarg = $("#pdeatil-image-modal #detailThumbBox").outerHeight();
			var dtlMdlLrgBox = $(window).height() - dtlMdlLrgBoxMarg;
			isSmartPhone = winW <= THRESHOLD_SMARTPHONE;
			if(isSmartPhone) {
				$("#pdeatil-image-modal").css({"left" : 0});
			}
			$('#modalDialogMask').css({"width" : winW + "px","z-index" : 1});
		}

		var isBusy = false;
		var zoomed = false;
		function refreshDetail(){
			refreshDetailThumnail();
			$('#product_image_detail_anime').hide();	
			$('#product_image_detail')
				.attr("src", getDetailImagePath(currentIndex))
				.attr("style", "border-width: 0px; margin: auto; top: 0; bottom: 0; left: 0; right: 0; position: absolute;");
			isBusy = false;
			zoomed = false;
			detailImageTransform = {"scale" : 1}
		}
		function refreshDetailThumnail(){
			var $modalThumnail = $('.detail-modal-thum-box');
			$modalThumnail.not(':eq(' + currentIndex + ')').removeClass('modal-thum-on');
			$modalThumnail.eq(currentIndex).addClass('modal-thum-on');
		}
		function animateDetail(index) {
			if(isBusy) return;
			var prevIndex = currentIndex;
			changeIndex(index);
			if(prevIndex == currentIndex) return;

			isBusy = true;

			var DURATION = 500;
			var distance = $('#pdeatil-image-modal').width();
			if(prevIndex > index) {
				distance *= -1;
			}
			$('#product_image_detail_anime')
				.attr("src", getDetailImagePath(currentIndex))
				.css({"left" : distance + "px", "right" : (distance * -1) + "px"})
				.animate({"left" : "0px", "right" : "0px"}, DURATION);
			$('#product_image_detail_anime').show();
			$('#product_image_detail')
				.css({"left" : "0px"})
				.animate({"left" : (distance * -1) + "px", "right" : distance + "px"}, DURATION,
				refreshDetail);
			
			changePreview(index);
		}
		function getDetailImagePath(index) {
			return imagePaths[currentIndex];
		}
		function changeDetail(index) {
			if(isSmartPhone) {
				animateDetail(index);
			} else {
				changeIndex(index);
				refreshDetail();
			}
		}
		
		var detailImageTransform = {};
		var PX_PROPERTIES = ["translateX", "translateY"];
		if(!PX_PROPERTIES.includes) {
			
			PX_PROPERTIES.includes = function(arg) {
				for(var i = 0; i < PX_PROPERTIES.length; i++) {
					if(PX_PROPERTIES[i] == arg) return true;
				}
				return false;
			}
		}
		function getDetailImageTransform() {
			var array = [];
			for(property in detailImageTransform) {
				array.push(property + "(" + detailImageTransform[property] + (PX_PROPERTIES.includes(property) ? "px" : "") + ")");
			}
			return array.join(' ');
		}
	})(jQuery);
</script>