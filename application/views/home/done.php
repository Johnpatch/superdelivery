<?php require_once('partials/company_info.php'); ?>
<?php require_once('partials/company_info_dls.php'); ?>
<div id="co-fullh-mdl-mdlbg"></div>
<div class="co-container">
	<div class="co-w750 co-mt40">
		<div class="step-info step2">
			<div class="step-txt"><p>申請</p><p class="now-step">完了</p></div>
		</div>
		<div class="co-tac">
			<div class="co-p20 co-fs16">申請が完了しました。</div>
			<div class="co-notice-nobrdr-area">
				<div class="co-tac">
					プラタの卸価格閲覧条件と<span class="co-dib">貴社の店舗情報が合致しました。</span>
					<p class="co-b co-mt5 co-fs17">卸価格を公開しましたので、<span class="co-dib">すぐに購入可能です。</span></p>
					<div class="co-btn co-btn-blue co-btn-s co-btn-page">
						<span><a class="co-wsnrml" href="<?=site_url()?>home/product_list?user_id=<?=$company_info['user_id']?>"><?=$company_info['username']?>の商品リストへ</a></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="recommendBox" class="recommend-box-wrap trade-apply-recm co-w750" style="display: block;">
	<div class="recommend-box-dl">
		<input type="checkbox" name="moreRecommend" id="more-recommend-btn01">
		<p class="recommend-ttl">この企業に申請した人は下記企業も<span class="co-dib">見ています</span></p>
		<?php foreach ($related_products as $related_product) { ?>
		<div id="recommendItem" class="recommend-box-area">
			<div class="recommend-item-box">
				<div class="recommend-item-img">
					<a href="<?=site_url()?>home/good_detail/<?=$related_product->id?>">
						<img class="recommend-img" title="<?=$related_product->name?>" src="<?=$related_product->main_image?>">
					</a>
					<div class="add-to-wishlist wishlist_multi prelogin-wishlist <?=$related_product->is_wished==1?'added-wish ':''?>" id="pd_<?=$related_product->id?>_<?=$related_product->user_id?>">
						<span class="add-wish-icon" ng-click="wishlist(<?=$related_product->id?>, <?=$related_product->user_id?>)"></span>
					</div>
				</div>
				<div class="status" style="height: 20px;">&nbsp;</div>
				<div class="recommend-item-name">
					<a href="<?=site_url()?>home/good_detail/<?=$related_product->id?>"><?=$related_product->name?></a>
				</div>
				<div class="item-dealer">
					<div class="recommend-dealer-name"><a href="<?=site_url()?>home/product_list?user_id=<?=$related_product->user_id?>"><?=$related_product->username?></a></div>
				</div>
			</div>
		</div>
		<label for="more-recommend-btn01" class="more-recommend">もっと見る</label>
		<?php } ?>
	</div>
</div>
<div class="push"></div>