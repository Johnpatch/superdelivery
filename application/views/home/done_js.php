<script type="text/javascript" src="<?=base_url()?>assets/js/jquery/jquery.raccoonDialog.js" charset="Shift_JIS"></script>

<script type="text/javascript"> //reviews
	function explain(documentId,displayId) {
		document.getElementById(documentId).onmouseover = function() {
			tid = setTimeout("exDisplayOn('" + displayId + "')", 0);
		}

		document.getElementById(documentId).onmouseout = function() {
			document.getElementById(displayId).style.display = "none";
			clearTimeout(tid);
		}
		document.getElementById(documentId).onclick = function() {
			if(document.getElementById(displayId).style.display=="block"){
				document.getElementById(displayId).style.display = "none";
				clearTimeout(tid);
			}
			else{
				tid = setTimeout("exDisplayOn('" + displayId + "')", 0);
			}
		}
	}

	function exDisplayOn(displayId) {
		document.getElementById(displayId).style.display = "block";
	}
	explain('ex<?=$company_info['user_id']?>','ex_display<?=$company_info['user_id']?>')
</script>

<script type="text/javascript">
	$(function() {
		var openId ="";
		$(".dl-infobtn-mdl").click(function(){
			openId = $(this).attr("id").replace("dl-infobtn-","").replace("-btm","");
			$("[id^='dl-" + openId + "-info-mdl']").show();
			$("#co-fullh-mdl-mdlbg").show();
			$(".co-fullh-mdl-content").scrollTop(0);
		});
		$("#co-fullh-mdl-mdlbg,.co-fullh-mdl-wrap .co-fullh-mdl-cls").click(function(){
			$("#co-fullh-mdl-mdlbg").hide();
			$("[id^='dl-" + openId + "-info-mdl']").hide();
		});
	});
</script>