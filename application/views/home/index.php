<div id="tiles-index-main" class="co-container">
	<div id="co-liquid-middle">
		<div id="co-liquid-middle-main" style="width: 1347px;">
			<div class="pr-contents-area pre-pr-area">
				<div id="solicitation-area">
					<h2 class="solicitation-txt">
						<span class="sub-solicitation-txt">トレンドアイテムから定番商品まで</span>
						<span class="main-solicitation-txt">豊富なバリエーションの商品を<span class="co-dib">1点から仕入れ</span></span>
						<span class="solicitation-sd">事業者向け卸・仕入れサイト<span class="co-dib">スーパーデリバリー</span></span>
					</h2>
					<?php if ($this->identity->is_guest()) {?>
					<div class="solicitation-link">
						<div class="solicitation-regist"><div class="co-btn co-btn-s co-btn-red co-btn-page"><span><a href="<?=site_url()?>auth/signup" rel="external" class="ui-link"><?=_lang('btn_register')?></a></span></div></div>
					</div>
					<?php } ?>
				</div>
				<div id="sd-data">
					<dl class="sd-data-box sd-data-item">
						<dt class="sd-data-head">掲載<span>商品数</span></dt>
						<dd class="sd-data-num">
							<span class="sd-data-count"><?=$product_count?></span>
							<span class="sd-data-count-static"><?=$product_count?></span>
							<span class="sd-data-unit">商品</span>
						</dd>
					</dl><dl class="sd-data-box sd-data-dealer">
						<dt class="sd-data-head">出展<span>企業数</span></dt>
						<dd class="sd-data-num">
							<span class="sd-data-count"><?=$brand_count?></span>
							<span class="sd-data-count-static"><?=$brand_count?></span>
							<span class="sd-data-unit">社</span>
						</dd>
					</dl>
				</div>
			</div>
			<!-- <div id="feature-wrap" class="feature-wrap-top-shadow">
				<h3 class="cont-title">特集<span class="cont-title-en">FEATURED ITEMS</span></h3>
				<div class="feature-box">
					<a class="feature-link co-img-over" href="javascript:;">
						<div class="feature-image">
							<img src="<?=base_url()?>assets/img/pages/home/new_1.jpg" alt="DIYグッズ" title="DIYグッズ">
						</div>
						<div class="feature-txt-area" style="height: 101px;">
							<div class="feature-row">
								<p class="feature-end"><span>6/19</span>まで</p>
								<div class="feature-info">
									<p class="feature-ttl">DIYグッズ</p>
									<p class="feature-txt">女性にも人気のDIY。手軽に始められるDIYグッズをピックアップしましたので、店内にDIYグッズコーナーを作ってみてはい...</p>
								</div>
							</div>
						</div>
						<div class="feature-new">NEW</div>
					</a>
					<a class="feature-link co-img-over"  href="javascript:;"><div class="feature-image"><img src="<?=base_url()?>assets/img/pages/home/new_2.jpg" alt="MENS　ビジネスアイテム" title="MENS　ビジネスアイテム"></div><div class="feature-txt-area" style="height: 101px;"><div class="feature-row"><p class="feature-end"><span>6/14</span>まで</p><div class="feature-info">
								<p class="feature-ttl">MENS　ビジネスアイテム</p>
								<p class="feature-txt">バッグや靴、ペン等の父の日ギフトにも最適な、男性におすすめのビジネスアイテムをご紹介します。ギフトとしての提案にもおすす...</p>
							</div></div></div><div class="feature-new">NEW</div>
					</a>
					<a class="feature-link co-img-over" href="javascript:;"><div class="feature-image"><img src="<?=base_url()?>assets/img/pages/home/new_3.jpg" alt="新作サンダル" title="新作サンダル"></div><div class="feature-txt-area" style="height: 101px;"><div class="feature-row"><p class="feature-end"><span>6/12</span>まで</p><div class="feature-info">
									<p class="feature-ttl">新作サンダル</p>
									<p class="feature-txt">新作サンダルのご紹介です。フラットサンダルや、サボサンダル、コンフォートサンダルなど人気の新作をご紹介いたします。</p>
								</div></div></div>
						</a>
						<a class="feature-link co-img-over" href="javascript:;">
							<div class="feature-image">
							<img src="<?=base_url()?>assets/img/pages/home/new_4.jpg" alt="デニムアイテム" title="デニムアイテム"></div><div class="feature-txt-area" style="height: 101px;"><div class="feature-row"><p class="feature-end"><span>6/10</span>まで</p><div class="feature-info">
										<p class="feature-ttl">デニムアイテム</p>
										<p class="feature-txt">日本製のこだわりデニムから、プチプラアイテムまで。トレンドが継続するデニム素材は、幅広い年代にご提案いただけるアイテムで...</p>
									</div></div></div>
						</a>
						<a class="feature-link co-img-over"  href="javascript:;"><div class="feature-image"><img src="<?=base_url()?>assets/img/pages/home/new_5.jpg" alt="冷感ファッション" title="冷感ファッション"></div><div class="feature-txt-area" style="height: 101px;"><div class="feature-row"><p class="feature-end"><span>6/5</span>まで</p><div class="feature-info">
											<p class="feature-ttl">冷感ファッション</p>
											<p class="feature-txt">接触冷感など、夏を快適に過ごすファッションアイテムのご提案。じめじめとした夏に思いっきりおしゃれを楽しむためのインナーや...</p>
										</div></div></div>
						</a>
						<a class="feature-link co-img-over"  href="javascript:;"><div class="feature-image"><img src="<?=base_url()?>assets/img/pages/home/new_6.jpg" alt="ハロウィン・クリスマス予約" title="ハロウィン・クリスマス予約"></div><div class="feature-txt-area" style="height: 101px;"><div class="feature-row"><p class="feature-end"><span>6/24</span>まで</p><div class="feature-info">
												<p class="feature-ttl">ハロウィン・クリスマス予約</p>
												<p class="feature-txt">ハロウィン・クリスマス商材はこの時期から先行販売が始まります。このタイミングで売り切れてしまう商品も多数ありますので、実...</p>
											</div></div></div>
						</a>
				</div>
			</div> -->
			<!-- <div class="sd-special">
				<div class="sd-special-wrap"> 
					<div id="freeshipping-top">
						<a  href="javascript:;"> 
							<img src="<?=base_url()?>assets/img/pages/home/freeshipping.jpg" alt="" style="width: 100%; height: 100%; padding: 0;">
						</a>
					</div>
					<div id="shoprac-top">
						<a  href="javascript:;">
							<img src="<?=base_url()?>assets/img/pages/home/bntr.jpg" alt="" style="width: 100%; height: 100%; padding: 0;">
						</a>
					</div>
				</div>
			</div> -->
			<div id="ranking-top">
				<h3 class="cont-title"><span class="co-pt-only">売れ筋</span>ランキング<span class="cont-title-en">RANKING</span>
					<!-- <a class="index-more-link" href="javascript:;"><span class="more-link-arw">もっと<span class="co-pt-only">売れ筋ランキングを</span>見る</span></a> -->
				</h3>
				<div class="ranking-box">
					<ul>
						<?php foreach($ranking_products as $ranking_product) { ?>
						<li><a href="<?=site_url()?>home/good_detail/<?=$ranking_product->id?>">
							<img src="<?=$ranking_product->main_image?>" title="<?=$ranking_product->name?>" style="width: 100%; height: auto;">
						</a></li>
						<?php } ?>
					</ul>
					<!-- <div class="period">集計期間：2019/05/20&nbsp;-&nbsp;2019/05/26</div> -->
				</div>
			</div>
			<div id="pickUpDealer-static">
				<h3 class="cont-title">注目ブランド<span class="cont-title-en">SPOTLIGHT</span></h3>
				<?php 
				foreach ($top_brands as $key => $top_brand) {
					if ($key % 4 == 0) {
						echo "<div class='pud-dealer-imgwrap'>";
					} ?>
					<a href="<?=site_url()?>home/product_list?brand_id=<?=$top_brand->id?>" class="pud-img-box" style="margin: 0;" >
						<img src="<?=$this->utils->brand_image_url($top_brand->id, $top_brand->image)?>" class="pud-dealer-img">
						<h3 class="pu-dealer-name"><?=$top_brand->name?></h3>
						<p><?=$top_brand->description?></p>
					</a>
					<?php
					if ($key % 4 == 3) { 
						echo "</div>";
					}
				} ?>
			</div>
			<div id="newdealer-area">
				<h3 class="cont-title">
					新規出展企業<span class="cont-title-en">NEW VENDORS</span>
					<!-- <a href="javascript:;" class="index-more-link"><span class="more-link-arw"><?=_lang('all')?>の新規出展企業を見る</span></a> -->
				</h3>
				<div class="newdealer-box">
					<div class="newdealer-btn">
						<div class="co-btn co-btn-wht" id="newDealerId-bk" style="display: none;">
							<span><a href="javascript:void(0);"><img src="<?=base_url()?>assets/img/pages/home/beforepage.gif" alt=""></a></span>
						</div>
						<div class="co-btn co-btn-dsbl" style="" id="newDealerId-bk-dsbl">
							<span><span><img alt="" src="<?=base_url()?>assets/img/pages/home/beforepage.gif"></span></span></div>
						</div>
						<div class="newdealer-info-area flex-box-anim" id="newDealerId-list" back-disable-id="newDealerId-bk-dsbl" forward-disable-id="newDealerId-fw-dsbl" back-id="newDealerId-bk" forward-id="newDealerId-fw" max-cols="8" min-space="8" side-space="0" fix-space="200" fill-dummy="" style="margin-left: 0px; margin-right: 0px; zoom: 1; width: 483px;">
							<div class="flex-box-parts-wrap" style="display: inline-block; margin-left: 0px; width: 7224px;">
								<?php	foreach ($new_products as $new_product) { ?>
								<div class="newdealer-info" style="margin-left: 0px; margin-right: 119px;">
									<div class="exhibit-date"><?=$this->utils->print_md($new_product->date)?></div>
									<a href="<?=site_url()?>home/good_detail/<?=$new_product->id?>" class="co-img-over newdealer-img-box">
										<img class="thumb_sml" style="width: 100%; height: 100%;" src="<?=$new_product->main_image?>">
										<img class="thumb_nml" style="width: 100%; height: 100%;" src="<?=$new_product->main_image?>">
									</a>
									<a  href="<?=site_url()?>home/good_detail/<?=$new_product->id?>" class="newdealer-name"><?=$new_product->name?></a>
								</div>
								<?php } ?>
							</div>
						</div>
						<div class="newdealer-btn">
							<div class="co-btn co-btn-wht" id="newDealerId-fw">
								<span><a href="javascript:void(0);"><img src="<?=base_url()?>assets/img/pages/home/nextpage.gif" alt=""></a></span>
							</div>
							<div class="co-btn co-btn-dsbl" style="display:none" id="newDealerId-fw-dsbl">
								<span><span><img alt="" src="<?=base_url()?>assets/img/pages/home/nextpage.gif"></span></span>
							</div>
						</div>
					</div>
				</div>
				<!-- <h3 class="cont-title co-tac">インフォメーション<span class="cont-title-en">INFORMATION</span></h3> -->
				<!-- <div id="information-wrap">
					<div class="round-info"><a href="javascript:;"><img src="<?=base_url()?>assets/img/pages/home/info_r_storeopening_01.jpg" alt="開業前にご入会" title="スーパーデリバリーは開業前でも仕入れ可能です！開業準備中の仕入れや、店舗什器・備品まで、あなたの新規開業を徹底サポート！ ">
						<p>開業前にご入会</p>
					</a><a href="javascript:;" target="_blank"><img src="<?=base_url()?>assets/img/pages/home/info_r_sdmedia_00.png" alt="SUPER DELIVERY MEDIA">
						<p>SUPER DELIVERY MEDIA</p>
					</a></div>
					<div class="shop-useful">
						<p class="shop-useful-title">提携サービス</p>
						<ul>
							<li class="useful-cont">
								<a v>
									<img src="<?=base_url()?>assets/img/pages/home
									/info_mini_square_05.jpg" alt="クレジット決済を導入しませんか？「Square」">
								</a>
							</li><li class="useful-cont">
								<a href="javascript:;">
									<img src="<?=base_url()?>assets/img/pages/home
									/info_mini_ubiregi_01.jpg" alt="iPad POSレジ 「ユビレジ」">
								</a>
							</li>
						</ul>
					</div>
				</div> -->
			</div>
		</div>
	</div>