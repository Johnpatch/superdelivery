<script type="text/javascript">
	var number = new RaccoonNumber();
	
	$(document).ready(function(){
		var maxInterval = 1000;
		var startTime = new Date().getTime();

		createCounter(<?=$brand_count?>, "sd-data-dealer");
		createCounter(<?=$product_count?>, "sd-data-item");

		function createCounter(count, id){
			var counter = $("." + id + " .sd-data-num .sd-data-count");
			var ua = UserAgent.getOS().name;

			if(ua.indexOf('Windows') != -1 || ua.indexOf('Mac') != -1){
				var timer = setInterval(function() {
					var countNumber = Math.floor(count * (new Date().getTime() - startTime) / maxInterval);
					if (countNumber >= count){
						countNumber = count;
						clearInterval(timer);
					}
					counter.text(number.commaFormat(countNumber));
				}, 4);
			} else {
				counter.text(number.commaFormat(count));
			}
		};
	});

	function setWidth(id){
		var counter = $("." + id + " .sd-data-num .sd-data-count");
		var dummyCounter = $("." + id + " .sd-data-num .sd-data-count-static");

		counter.css("width", dummyCounter.css("width"));
	};
</script>