<div class="co-pankuzu-list">
	<a href="<?=site_url()?>"><?=_lang('to_top')?></a>
	<span><?=_lang('login')?></span>
</div>
<div id="jsp-tiles-certification-c" ng-controller="loginCtrl">
	<div class="cont-login co-container">
		<div class="login-box">
			<div class="wrap">
				<div class="ttl"><?=_lang('login')?></div>
				<div class="input-ttl">メールアドレスまたはID</div>
				<div class="input-area">
					<input type="text" name="identification" maxlength="100" value="" class="input-id co-fs18 trim-input-id" ng-model="username" placeholder="username">
				</div>
				<div class="input-ttl">パスワード</div>
				<div class="input-area">
					<input type="password" name="password" value="" maxlength="30" class="input-pass co-fs18 trim-input-pass" ng-model="password" placeholder="password">
				</div>
				<div class="co-mt20">
					<div class="co-btn co-btn-red co-btn-m co-btn-page">
						<span>
							<img src="<?=base_url()?>assets/img/common/icon/icon_btn_page_00.png" alt="">
							<input type="button" value="<?=_lang('login')?>" ng-click="submitLogin()">
						</span>
					</div>
				</div>
				
			</div>
		</div>
		<div class="regist-box">
			<div class="ttl">
				まだ会員登録をしていない方へ
			</div>
			<div class="about">
				<div class="co-mb5">スーパーデリバリーは<br>事業者向け卸・仕入れ専用サイトです。</div>
				<div class="co-dib co-tal">
					<p class="co-mb5">・卸価格は会員にのみ公開</p>
					<p class="co-mb5">・多くの商品が1点から購入可能</p>
				</div>
			</div>
			<div class="regist-box-btn">
				<div class="co-btn co-btn-red co-btn-s co-btn-page">
					<span><a href="<?=site_url()?>auth/signup"><?=_lang('btn_register')?></a></span>
				</div>
			</div>
		</div>
	</div>
</div>