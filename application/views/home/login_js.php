<script type="text/javascript">
	
angular.module('homeApp')
.controller('loginCtrl', function($scope, $http) {
	$scope.username = ''
	$scope.password = '';
	$scope.submitLogin = function() {
		if ($scope.username == '' || $scope.password == '') {
			alert('Inpt your username and password correctly!');
			return;
		}
		$http({
			method : 'post',
			dataType: 'json',
			url : $scope.serverUrl + 'api/post_login',
			data : 'posted_data=' + 
				encodeURIComponent(JSON.stringify({
					username: $scope.username,
					password: $scope.password
				})),
			headers : {'Content-Type': 'application/x-www-form-urlencoded'}
		}).then(function(res) {
			if (res.data == 0) {	// ERROR_NONE
				window.location.href = $scope.serverUrl;
			} else if (res.data == 1) {
				alert('There is such username.');
			} else if (res.data == 2) {
				alert('Password is incorrect!');
			} else if (res.data == 3) {
				alert('This account is not allowed!');
			}
		});	
	}

});

</script>