<div id="dl-trade-<?=$company_info['user_id']?>-info-mdl" class="co-fullh-mdl-wrap co-container">
	<div class="co-fullh-mdl-ttl">
		<?=_lang('terms_and_conditions')?><div class="co-fullh-mdl-cls"></div>
	</div>
	<div class="co-fullh-mdl-content">
		<div class="dl-trade-info-mdl-ttl">販売規制について</div>
		<div class="dl-info-mdl-txt">
			<?=$company_info['company_informations'][1]?>
		</div>
	</div>
</div>
<div id="dl-payship-<?=$company_info['user_id']?>-info-mdl" class="co-fullh-mdl-wrap">
	<div class="co-fullh-mdl-ttl">
		送料・決済方法<div class="co-fullh-mdl-cls"></div>
	</div>
	<div class="co-fullh-mdl-content">
		<div id="dealer-carriage" class="co-container">
			<?=$company_info['company_informations'][2]?>
		</div>
	</div>
</div>
<div id="dl-about-<?=$company_info['user_id']?>-info-mdl" class="co-fullh-mdl-wrap">
	<div class="co-fullh-mdl-ttl">
		企業情報<div class="co-fullh-mdl-cls"></div>
	</div>
	<div class="co-fullh-mdl-content">
		<div id="dealer-carriage" class="co-container">
			<?=$company_info['company_informations'][3]?>
		</div>
	</div>
</div>