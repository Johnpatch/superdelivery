<div id="dl-info-area">
	<div class="dl-info-box">
		<div class="dl-name">
			<a class="dl-name-txt" href="<?=site_url()?>home/product_list?user_id=<?=$company_info['user_id']?>"><?=$company_info['username']?></a>
		</div>
		<div class="dl-info">
			<div class="dl-infobtn">
				<ul>
					<li><a id="dl-infobtn-trade-<?=$company_info['user_id']?>" class="dl-infobtn-mdl"><?=_lang('terms_and_conditions')?></a></li> <!-- terms and conditions -->
					<li><a id="dl-infobtn-payship-<?=$company_info['user_id']?>" class="dl-infobtn-mdl">送料・決済方法</a></li> <!-- shipping and payment method -->
					<li><a id="dl-infobtn-about-<?=$company_info['user_id']?>" class="dl-infobtn-mdl co-001g">企業情報</a></li> <!-- enterprise information -->
				</ul>
				<div class="co-container dealer-satisfaction-wrap">
					<div style="position:relative; z-index:3;">
						<div class="dealer-satisfaction" style="display:inline; vertical-align:text-bottom;" id="ex197802">
						<div class="co-satisfy-star-box">
							<span class="co-satisfy-star-bar" style="width:<?=4.6/5*100?>%;"></span>
							<span class="co-satisfy-star"></span>
						</div>&nbsp;4.6
						<span class="co-fs11 records">（52件）<img src="<?=base_url()?>assets/img/pages/detail/popup-link-01.gif" alt="" class="co-pb2"></span>
							<div class="dealer-satisfaction-score" style="left:0;" id="ex_display197802">
								<div class="satisfaction-score-tbl">
									<div class="co-satisfy-star-container">
										<div>
											<span class="point">商品説明</span>
											<div class="co-satisfy-star-box">
												<span class="co-satisfy-star-bar" style="width:90%;"></span>
												<span class="co-satisfy-star"></span>
											</div>
											4.5
										</div>
										<div>
											<span class="point">実物一致度</span>
											<div class="co-satisfy-star-box">
												<span class="co-satisfy-star-bar" style="width:94%;"></span>
												<span class="co-satisfy-star"></span>
											</div>
											4.7
										</div>
										<div>
											<span class="point">価格・品質</span>
											<div class="co-satisfy-star-box">
												<span class="co-satisfy-star-bar" style="width:92%;"></span>
												<span class="co-satisfy-star"></span>
											</div>
											4.6
										</div>
									</div>
									<div class="co-satisfy-star-container">
										<div>
											<span class="point">納品状況</span>
											<div class="co-satisfy-star-box">
												<span class="co-satisfy-star-bar" style="width:92%;"></span>
												<span class="co-satisfy-star"></span>
											</div>
											4.6
										</div>
										<div>
											<span class="point">担当者対応</span>
											<div class="co-satisfy-star-box">
												<span class="co-satisfy-star-bar" style="width:92%;"></span>
												<span class="co-satisfy-star"></span>
											</div>
											4.6
										</div>
										<div>
											<span class="point">在庫精度</span>
											<div class="co-satisfy-star-box">
												<span class="co-satisfy-star-bar" style="width:98%;"></span>
												<span class="co-satisfy-star"></span>
											</div>
											4.9
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if ($this->identity->is_logined() && $product->p_status != 1) { ?>
	<div class="dl-trade-info">
		<div class="dl-trade-info-txt">
			<?=$company_info['username']?>&nbsp;の卸価格をご覧になる場合は、申請（無料）を行ってください。
			<br>※「申請」は卸価格を見るためのお手続です。必ずしも購入をする必要はありません。
		</div>
		<?php if ($product->p_status == -1) { ?>
		<div class="dl-trade-info-btn">
			<div class="co-btn co-btn-red co-btn-s co-btn-page"><span>
				<a href="<?=site_url()?>home/apply/<?=$product->id?>">卸価格を見る（申請画面へ）</a>
			</span></div>
		</div>
		<?php } ?>
	</div>
	<?php } ?>
</div>
