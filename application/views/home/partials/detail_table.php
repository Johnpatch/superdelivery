<div class="prodact-detail-action-box btm-fix-btns" style="text-align: right; bottom: 0px;">
	<div class="add-to-wishlist-set">
		<div class="co-btn co-btn-wht co-btn-ss to-wishlist-btn <?=$product->is_wished==1?'wish-set-added':''?>" id="pgd_<?=$product->id?>_<?=$product->user_id?>"><span>
			<a ng-click="wishlist(<?=$product->id?>, <?=$product->user_id?>)"><?=_lang('btn_add2wishlist')?></a>
		</span></div>
	</div>
	<div class="co-btn co-btn-red co-btn-m addCartItem_dummy"><span>
		<a href="javascript:;" ng-click="add2cart(<?=$product->id?>, <?=$product->user_id?>)"><?=_lang('btn_add2cart')?></a></span>
	</div>
</div>
<div class="product-information-box pre-set">
	<div class="info-list-wrap">
		<table cellspacing="0" cellpadding="0" class="set-list">
			<tbody>
				<tr class="co-pc-only">
					<th class="border-r back-color th-set-number">セット <br>番号</th>
					<th class="border-r back-color">内訳 <br>（メーカー <span class="text-newline">品番）</span></th>
					<th colspan="2" class="border-r back-color">
						価格（税抜）
						<a href="#" target="_blank"><img src="<?=base_url()?>assets/img/pages/detail/question.gif" class="vmiddle" alt=""></a>
					</th>
					<th class="back-color">数量</th>
				</tr>
				<?php foreach ($product_details as $key=>$product_detail) { ?>
				<tr class="ts-tr02">
					<td rowspan="3" class=" co-align-center co-pc-only border-rt border-b"><?=$product_detail->product_number?></td>
					<td rowspan="3" class="border-rt co-align-left border-b td-set-detail">
						<div><?=$product_detail->quantity?>&nbsp;点
							<div class="co-mt3 co-pc-only">(<?=$product_detail->barcode?>)</div>
						</div>
					</td>
					<td class="maker-product-price co-pc-only">
						<span>参考上代</span>
					</td>
					<td class="border-rt td-price01 co-pc-only co-pr5">
						オープンプライス
					</td>
					<td class="co-ts-only ts-td-col01">
						<div class="set-num">
							<span>1セット
								<span class="co-dib co-fs12">（<?=$product_detail->quantity?>点）：&nbsp;</span>
							</span>
							<span class="maker-wholesale-set-price">
								<span class="cmp-price co-fcred">¥<?=$product_detail->current_price?></span>
								<?php if ($product_detail->current_price != $product_detail->wholesale_price) { ?>
								<span class="list-price">¥<?=$product_detail->wholesale_price?></span>
								<?php } ?>
							</span>
						</div>
						<div class="ts-price-wrap">
							<table>
								<tbody><tr class="maker-product-price">
									<td>
										<span class="text-newline">
											参考上代
										</span>
									</td><td class="td-price01">：オープンプライス
									</td>
								</tr>
								<tr class="maker-wholesale-price">
									<td>
										卸単価
									</td>
									<td class="td-price02">：
										<span class="cmp-price">¥<?=$product_detail->current_price * $product_detail->quantity?></span>
									</td>
								</tr>
							</tbody></table>
						</div>
					</td>
					<td rowspan="3" class="border-t border-b td-quantities ts-td-col02">
						<div id="quantities_wrap_1">
							<div class="purchase-count co-dib">
								<div>
									<div class="quantity-button">
										<a class="quantity_button" id="input_<?=$product_detail->id?>_down" style="cursor: pointer" href="javascript:void(0)"><img src="<?=base_url()?>assets/img/pages/detail/minus.png" alt="-"></a>
									</div>
									<div>
										<input type="text" pattern="[0-9]*" name="quantities[]" id="input_<?=$product_detail->id?>" maxlength="9" size="3" onfocus="this.select()" value="0" class="lot-input co-ime-disabled quantities">
									</div>
									<div class="quantity-button">
										<a class="quantity_button" id="input_<?=$product_detail->id?>_up" style="cursor: pointer" href="javascript:void(0)"><img src="<?=base_url()?>assets/img/pages/detail/plus.png" alt="+"></a>
									</div>
								</div>
							</div>
						</div>
						<span id="error_quantities_wrap_1"></span>
						<div id="errors_1" class="errors" style="display: none">
							<p class="fo-error-exclamation"><span class="text-newline"></span></p>
						</div>
					</td>
					<td class="tr-last co-ts-only">
						<table><tbody><tr>
							<td>
								<span>SD品番：<?=$product->sku?><?=$product_detail->product_number?></span>
								<span> / メーカー品番：<?=$product_detail->barcode?></span>
							</td>
						</tr></tbody></table>
					</td>
				</tr>
				<tr class="co-pc-only">
					<td class="maker-wholesale-price">
						卸単価
					</td>
					<td class="border-r co-align-right co-pr5 co-pl5 co-pb5 td-price02">
						<?php if ($product_detail->current_price != $product_detail->wholesale_price) { ?>
						<p class="list-price"><span>¥ <?=$product_detail->wholesale_price?></span></p>
						<?php } ?>
						<p class="cmp-price">¥ <?=$product_detail->current_price?></p>
						<input type="hidden" id="cp4_<?=$product_detail->id?>" value="<?=$product_detail->current_price * $product_detail->quantity?>">
					</td>
				</tr>
				<tr class="co-pc-only">
					<td class="set-num">1 セット <span class="text-newline">(<?=$product_detail->quantity?>&nbsp;点)</span></td>
					<td class="maker-wholesale-set-price co-align-right">
						<span class="cmp-price">¥ <?=$product_detail->current_price * $product_detail->quantity?></span>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>
<div class="prodact-detail-action-box btm-fix-btns" style="text-align: right; bottom: 0px;">
	<div class="add-to-wishlist-set">
		<div class="co-btn co-btn-wht co-btn-ss to-wishlist-btn <?=$product->is_wished==1?'wish-set-added':''?>" id="pgd_<?=$product->id?>_<?=$product->user_id?>"><span>
			<a ng-click="wishlist(<?=$product->id?>, <?=$product->user_id?>)"><?=_lang('btn_add2wishlist')?></a>
		</span></div>
	</div>
	<div class="co-btn co-btn-red co-btn-m addCartItem_dummy"><span>
		<a href="javascript:;" ng-click="add2cart(<?=$product->id?>, <?=$product->user_id?>)"><?=_lang('btn_add2cart')?></a></span>
	</div>
</div>
