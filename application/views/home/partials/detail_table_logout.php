<div class="product-information-box pre-set">
	<div class="info-list-wrap">
		<table cellspacing="0" cellpadding="0" class="set-list">
		<tbody>
			<tr class="co-pc-only">
				<th class="border-r back-color th-set-number">セット<br>番号</th>
				<th class="border-r back-color">内訳<br>（メーカー<span class="text-newline">品番）</span></th>
				<th class="border-r back-color">数量</th>
				<th class="border-r back-color">
					小売価格
					<a href="#" target="_blank"><img src="<?=base_url()?>assets/img/pages/detail/question.gif" class="vmiddle" alt=""></a>
				</th>
				<th class="back-color">卸価格</th>
			</tr>
			<?php foreach ($product_details as $key=>$product_detail) { ?>
			<tr class="ts-tr02">
				<td class="co-align-center border-rt border-b co-pc-only">
					<?=$product_detail->product_number?>
				</td>
				<td class="border-rt border-b td-set-detail">
					<div>
						<?=$product_detail->breakdown?>
						<div class="co-mt3 co-pc-only">(<?=$product_detail->barcode?>)</div>
						<div class="co-fcgray td-jan">JAN:<?=$product_detail->jan?></div>
					</div>
				</td>
				<td class="border-rt co-align-center co-pc-only">
					<span class="text-newline">
						<?=$product_detail->quantity?>&nbsp;点
					</span>
				</td>
				<td class="border-rt maker-product-price">
					<div class="float-left">メーカー希望小売価格（税抜）</div>
					<div class="float-right">
						<span class="text-newline co-ts-only"></span>
						<!-- ¥<?=$product_detail->price?> -->
						オープンプライス
					</div>
				</td>
				<td class="stock-info co-align-center border-t">
					<span class="co-ts-only co-dib">卸価格は</span>会員のみ公開
				</td>
			</tr>
			<?php } ?>
		</tbody>
		</table>
	</div>
</div>
<div class="prodact-detail-action-box btm-fix-btns" style="text-align: right; bottom: 0px;">
	<div>仕入れには会員登録が必要です。</div>
		<div class="add-to-wishlist-set">
			<div class="co-btn co-btn-wht co-btn-ss to-wishlist-btn <?=$product->is_wished==1?'wish-set-added':''?>" id="pgd_<?=$product->id?>_<?=$product->user_id?>"><span>
				<a ng-click="wishlist(<?=$product->id?>, <?=$product->user_id?>)"><?=_lang('btn_add2wishlist')?></a>
			</span></div>
		</div>
		<div class="co-btn co-btn-red co-btn-s co-btn-page"><span><a href="<?=site_url()?>auth/signup"><?=_lang('btn_register')?></a></span></div>
		<div class="co-clear"></div>
	</div>
	<div class="co-mr10 co-mb10 co-pc-only co-tar">
		すでに会員の方は<a href="<?=site_url()?>auth/login" class="co-b"><?=_lang('login')?></a>
	</div>
</div>