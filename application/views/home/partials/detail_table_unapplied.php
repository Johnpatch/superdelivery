<div class="product-information-box pre-set">
	<div class="info-list-wrap">
		<table cellspacing="0" cellpadding="0" class="set-list">
		<tbody>
			<tr class="co-pc-only">
				<th class="border-r back-color th-set-number">セット<br>番号</th>
				<th class="border-r back-color">内訳<br>（メーカー<span class="text-newline">品番）</span></th>
				<th class="border-r back-color">数量</th>
				<th class="border-r back-color">
					価格（税抜）
					<a href="#" target="_blank"><img src="<?=base_url()?>assets/img/pages/detail/question.gif" class="vmiddle" alt=""></a>
				</th>
				<th class="back-color">在庫状況</th>
			</tr>
			<?php foreach ($product_details as $key=>$product_detail) { ?>
			<tr class="ts-tr02">
				<td class="co-align-center border-rt border-b co-pc-only">
					<?=$product_detail->product_number?>
				</td>
				<td class="border-rt border-b td-set-detail">
					<div>
						<?=$product_detail->breakdown?>
					</div>
				</td>
				<td class="border-rt co-align-center co-pc-only">
					<span class="text-newline">
						<?=$product_detail->quantity?>&nbsp;点
					</span>
				</td>
				<td class="border-rt maker-product-price">
					<div class="float-left">メーカー希望小売価格（税抜）</div>
					<div class="float-right">
						<span class="text-newline co-ts-only"></span>
						オープンプライス
					</div>
				</td>
				<td class="stock-info co-align-center border-t">
					会員のみ公開
				</td>
			</tr>
			<?php } ?>
		</tbody>
		</table>
	</div>
</div>
<div class="prodact-detail-action-box btm-fix-btns" style="text-align: right; bottom: 0px;">
	<div></div>
		<div class="add-to-wishlist-set">
			<div class="co-btn co-btn-wht co-btn-ss to-wishlist-btn <?=$product->is_wished==1?'wish-set-added':''?>" id="pgd_<?=$product->id?>_<?=$product->user_id?>"><span>
				<a ng-click="wishlist(<?=$product->id?>, <?=$product->user_id?>)"><?=_lang('btn_add2wishlist')?></a>
			</span></div>
		</div>
		<?php if($company_info['company_status'] == -1) { ?>
			<div class="co-btn co-btn-ss co-btn-red pdtl-set-apply">
				<span>
					<a href="<?=site_url()?>home/apply?user_id=<?=$company_info['user_id']?>"><?=_lang('btn_go2apply_short')?></a>
				</span>
			</div>
		<?php } else { ?>
			卸価格:（申請）
		<?php } ?>
		<div class="co-clear"></div>
	</div>
</div>