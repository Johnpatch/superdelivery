<div id="dealerlist-dealerbox" class="co-container">
	<?php 	require_once('product_list_group_common.php'); ?>
	<div class="pre-dealerlist dealerlist-inner">
		<?php foreach ($filtered_products as $key => $f_product) { ?>
		<div class="dealer-eachbox">
			<div class="info-dealerbox co-clf">
				<div class="info-dealerstatus">
					<p class="info-dealername">
						<a href="<?=site_url()?>home/product_list?user_id=<?=$f_product['user_info']['user_id']?>"><?=$f_product['user_info']['username']?></a>
					</p>
					<div class="info-dealermark">
						<a href="javascript:;" class="info-markshipping">
							送料無料
							<span class="dealer-mark-smartd">※沖縄・離島を除く</span>
							<span class="info-marktxt">送料無料<br>※沖縄・離島を除く</span>
						</a>
					</div>
					<div class="info-dealersatis">
						<div class="co-container dealer-satisfaction-wrap">
							<div style="position:relative; z-index:3;">
								<div class="dealer-satisfaction" style="display:inline; vertical-align:text-bottom;" id="ex<?=$f_product['user_info']['user_id']?>">
								<div class="co-satisfy-star-box">
									<span class="co-satisfy-star-bar" style="width:<?=4.6/5*100?>%;"></span>
									<span class="co-satisfy-star"></span>
								</div>&nbsp;4.6
								<span class="co-fs11 records">（52件）<img src="<?=base_url()?>assets/img/pages/detail/popup-link-01.gif" alt="" class="co-pb2"></span>
									<div class="dealer-satisfaction-score" style="left:0;" id="ex_display<?=$f_product['user_info']['user_id']?>">
										<div class="satisfaction-score-tbl">
											<div class="co-satisfy-star-container">
												<div>
													<span class="point">商品説明</span>
													<div class="co-satisfy-star-box">
														<span class="co-satisfy-star-bar" style="width:90%;"></span>
														<span class="co-satisfy-star"></span>
													</div>
													4.5
												</div>
												<div>
													<span class="point">実物一致度</span>
													<div class="co-satisfy-star-box">
														<span class="co-satisfy-star-bar" style="width:94%;"></span>
														<span class="co-satisfy-star"></span>
													</div>
													4.7
												</div>
												<div>
													<span class="point">価格・品質</span>
													<div class="co-satisfy-star-box">
														<span class="co-satisfy-star-bar" style="width:92%;"></span>
														<span class="co-satisfy-star"></span>
													</div>
													4.6
												</div>
											</div>
											<div class="co-satisfy-star-container">
												<div>
													<span class="point">納品状況</span>
													<div class="co-satisfy-star-box">
														<span class="co-satisfy-star-bar" style="width:92%;"></span>
														<span class="co-satisfy-star"></span>
													</div>
													4.6
												</div>
												<div>
													<span class="point">担当者対応</span>
													<div class="co-satisfy-star-box">
														<span class="co-satisfy-star-bar" style="width:92%;"></span>
														<span class="co-satisfy-star"></span>
													</div>
													4.6
												</div>
												<div>
													<span class="point">在庫精度</span>
													<div class="co-satisfy-star-box">
														<span class="co-satisfy-star-bar" style="width:98%;"></span>
														<span class="co-satisfy-star"></span>
													</div>
													4.9
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div> <!-- stars view -->
				</div>
			</div>
			<div class="info-dealeritemnum">
				<a href="javascript:;">
					<span class="itemnum-search"><?=$f_product['user_info']['bap_cnt']?></span>
					<span class="itemnum-unit"> 件</span>
				</a>
				<span class="itemnum-slash">
					<span class="co-m5">/</span>
					全<?=count($f_product['products'])?>件</span>
			</div>	
			<div class="dealer-itembox" id="itembox-contents-<?=$f_product['user_info']['user_id']?>">
				<div style="display:none;" class="product_count" id="product_count_<?=$f_product['user_info']['user_id']?>"><?=count($f_product['products'])?></div>
				<input type="text" name="product_divs_name_<?=$f_product['user_info']['user_id']?>" class="product_divs" id="product_divs_<?=$f_product['user_info']['user_id']?>" style="display:none;" >
				<div class="dealer-itembox-inner item-scroll-box" dealercode="<?=$f_product['user_info']['user_id']?>" id="itembox-inner-<?=$f_product['user_info']['user_id']?>">
					<div class="itembtn-back">
						<div class="retrace_step" style="display:none;">
							<div class="item-btnback"></div>
						</div>
						<div class="not_retrace_step" style="display:block;">
							<div class="item-btnback-dsbl"></div>
						</div>
					</div>
					<div class="item-box-area item-scroll-area">
					<?php foreach ($f_product['products'] as $product) { ?>
						<div class="itembox-parts visible-on" ng-show="has_available_options('<?=$product->options?>')">
							<div class="item-img-box wish-dvs-jdg item-wish">
								<a href="<?=site_url()?>home/good_detail/<?=$product->id?>">
									<img class="item-img" title="<?=$product->name?>" src="<?=$product->main_image?>" border="0">
								</a>
								<div class="add-to-wishlist  prelogin-wishlist <?=$product->is_wished==1?'added-wish ':''?>" id="pd_<?=$product->id?>_<?=$product->user_id?>">
									<span class="add-wish-icon" ng-click="wishlist(<?=$product->id?>, <?=$product->user_id?>)"></span>
								</div>
							</div>
							<div class="item-name">
								<a href="<?=site_url()?>home/good_detail/<?=$product->id?>"><?=$product->name?></a>
							</div>
							<?php
								if ($product->p_status == -1) 
									include('list_cata_logout.php'); 
								else if ($product->p_status == 1)
									include('list_cata.php'); 
								else
									include('list_cata_unapplied.php'); 
							?>
						</div>
					<?php } ?>
					</div>
					<div class="itembtn-next" style="margin-left: 0;">
						<div class="advance_step" style="display:block;">
							<div class="item-btnnext"></div>
						</div>
						<div class="loading_step" style="display:none;">
							<div class="item-lodingbtn"></div>
						</div>
						<div class="not_advance_step" style="display:none;">
							<div class="item-btnnext-dsbl"></div>
						</div>
					</div>
					<?php if ($f_product['user_info']['company_status'] == -1) { ?>
					<div style="float: right;">
						<div class="co-btn co-btn-red co-btn-s co-btn-page"><span>
							<a href="<?=site_url()?>home/apply?user_id=<?=$f_product['user_info']['user_id']?>"><?=_lang('btn_go2apply')?></a>
						</span></div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php } ?>
		<div id="co-bottom-genre-wrap" class="co-ts-only">
			<p class="co-bottom-genre-title">商品ジャンル</p>
		</div>
	</div>
</div>