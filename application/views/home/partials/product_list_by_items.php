<div id="productlist-productbox" class="co-cotainer">
	<?php if ($user_id == 0) {
		require_once('product_list_group_common.php');
	} ?>
	<div class="co-clf item-box-area" style="font-size:82%;">
		<?php foreach ($products as $product) { ?>
		<div class="itembox-parts" ng-show="has_available_options('<?=$product->options?>')">
			<div class="item-img-box wish-dvs-jdg item-wish">
				<a href="<?=site_url()?>home/good_detail/<?=$product->id?>">
					<img class="item-img" alt="<?=$product->name?>" title="<?=$product->name?>" src="<?=$product->main_image?>">
				</a>
				<div class="add-to-wishlist  prelogin-wishlist <?=$product->is_wished==1?'added-wish ':''?>" id="pd_<?=$product->id?>_<?=$product->user_id?>">
					<span class="add-wish-icon" ng-click="wishlist(<?=$product->id?>, <?=$product->user_id?>)"></span>
				</div>
			</div>
			<div class="item-name">
				<a href="<?=site_url()?>home/good_detail/<?=$product->id?>">
					<?=$product->name?>
				</a>
			</div>
			<?php
				if ($this->identity->is_guest()) 
					include('list_cata_logout.php'); 
				else if ($product->p_status == 1)
					include('list_cata.php'); 
				else
					include('list_cata_unapplied.php'); 
			?>
			<div class="item-dealer-name">
				<a href="<?=site_url()?>home/product_list?user_id=<?=$product->user_id?>"><?=$product->username?></a>
			</div>
		</div>
		<?php } ?>
	</div>
	<div class="listorder-box co-clf notupper-order">
		<div class="page-nav-area">
			<span class="page-nav-numtxt">1 ～ <?=count($products)?>件
				<span class="co-dib">（全<?=count($products)?> 件）</span>
			</span>
		</div>
	</div>
	<div id="co-bottom-genre-wrap" class="co-ts-only">
		<p class="co-bottom-genre-title">商品ジャンル</p>
	</div>
</div>