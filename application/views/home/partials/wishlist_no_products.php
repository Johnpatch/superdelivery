<div class="non-wish-product">
	<p class="first-sentence">
		<img src="<?=base_url()?>assets/img/wishlist/first_sentence_heart_00.png"><br><span class="middle-span">登録されている商品が<span>ありません</span></span><br>以下の画面でハートマークをクリックするとこのページにリストアップされます。</p>
	<div class="second-sentence co-clf">
		<p class="left-box"><span>【商品一覧】</span><br><img src="<?=base_url()?>assets/img/wishlist/second_sentence_productlist_00.jpg" class="co-pc-only"><img src="<?=base_url()?>assets/img/wishlist/index_sp_00.png" class="co-ts-only"></p>
		<p class="right-box"><span>【商品詳細】</span><br><img src="<?=base_url()?>assets/img/wishlist/second_sentence_productdetail_00.jpg" class="co-pc-only"><img src="<?=base_url()?>assets/img/wishlist/detail_sp_00.png" class="co-ts-only"></p>
	</div>
</div>