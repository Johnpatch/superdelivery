<form name="wishlistForm" id="wishlistForm">
	<div class="smd-listorder-area">
		<div class="smd-listorder-box co-container co-clf co-ts-only">
			<div class="chk-del-wrap co-clf">
				<a href="javascript:void(0);" class="delete-mode-button toggle-delete-mode">選択して削除</a>
				<a href="javascript:void(0);" class="delete-mode-cancel-button toggle-delete-mode">キャンセル</a>
			</div>
		</div>
	</div>
	<div class="smd-listorder-box co-container co-pc-only co-clf">
		<div class="wishlist-option-box co-clf">
			<div class="chk-wrap"><a class="all-select-button" href="javascript:;">
				<?=_lang('select_all')?>
			</a></div>
		</div>
	</div>
	<div class="item-box-area">
		<?php foreach ($products as $key => $product) { ?>
		<div class="itembox-out-line co-container itembox itembox-parts wishlist_product" id="wishlist_product_<?=$product->id?>" data-wishlist-code="<?=$product->id?>" data-product-name="<?=$product->name?>">
			<div class="item-img-box">
				<a href="<?=site_url()?>home/good_detail/<?=$product->id?>" class="wishlist-dealer-image-box">
					<img class="item-img" src="<?=$product->main_image?>" title="<?=$product->name?>">
				</a>
				<span class="wishlist-select-icon-clear toggle-delete-check" data-proceed_code="<?=$product->id?>" style="display: inline;"><span class="remove-check-txt">選択して削除</span><span></span></span>
				<span class="wishlist-select-icon-check toggle-delete-check" data-proceed_code="<?=$product->id?>" style="display: none;"><span></span></span>
				<input type="hidden" value="" id="proceed_code_<?=$product->id?>" name="proceed_code_<?=$product->id?>">
			</div>
			<div class="productname item-name">
				<div class="title">
					<a href="<?=site_url()?>home/good_detail/<?=$product->id?>"><?=$product->name?></a>
				</div>
			</div>
			<div class="dealer-status item-dealer">
				<!-- <div class="price item-price-box">
					<div class="price item-price">
						<span class="trade-status-large discount">¥ 14 </span><span class="trade-status-bold discount"><span class="slash">/ </span><span class="lot">point to</span></span>
					</div>
					<span class="list-price">¥ 15 <span class="unit">/ point to</span></span>
				</div> -->
				<div class="dealername dn-hfixed item-dealer-name">
					<a href="<?=site_url()?>home/product_list?user_id=<?=$product->user_id?>"><?=$product->username?></a>
				</div>
				<div class="wishlist-memo-area">
					<div class="memo-contents" data-proceed_code="<?=$product->id?>">
						<div class="pencil-icon"><img src="<?=base_url()?>/assets/img/wishlist/wishlist_icon_pencil_00.png" alt=""></div>
						<p class="sentence">
							<span><?=empty($memoes[$product->id])?'MEMO':$memoes[$product->id]?></span>
						</p>
					</div>
					<div class="memo-operation">
						<textarea class="memo-input" maxlength="40" id="memo_<?=$product->id?>"><?=empty($memoes[$product->id])?'':$memoes[$product->id]?></textarea>
						<span class="memo-check" data-proceed_code="<?=$product->id?>"></span>
						<span class="memo-clear" data-proceed_code="<?=$product->id?>"></span>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
	<div class="co-clear"></div>
	<div class="listorder-box notupper-order co-clf">
		<div class="page-nav-area">
			<span class="page-nav-numtxt">1 ～ <?=count($products)?> 件 (全 <?=count($products)?> 件)</span><span class="page-nav-prev co-ts-only">Forward</span><span class="co-ts-only last-page-num">1</span><span class="page-nav-next co-ts-only">next</span>
		</div>
	</div>
	<!-- <div class="list-bottom-useful">
		<div class="co-btn co-btn-wht co-btn-ss co-btn-page"><span><a href="https://www.superdelivery.com/p/alert/form.do">Register new arrival alert</a></span></div>
	</div> -->
</form>