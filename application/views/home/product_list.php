<?php if ($user_id == 0) { ?>
<div class="co-pankuzu-list co-pc-only">
	<a href="<?=site_url()?>"><?=_lang('to_top')?></a>
	<span><?=_lang('product_list')?></span>
</div>
<?php } else {
	require_once('partials/company_info.php');
	require_once('partials/company_info_dls.php');
} ?>
<div id="co-plist-layout-outer" class="plist-has-pankuzu" ng-controller="listCtrl">
	<div class="jsp-layout-base-layout">
		<div class="co-plist-layout-wrap">
			<div class="co-plist-layout-parts search-box-parts">
				<div class="co-product-refine">
					<div class="search-box-ttl co-clf">
						<div class="search-box-ttl-txt">出品日</div>
					</div>
					<ul class="refine-search-list">
						<li class="search-list-txt">
							<a href="<?=site_url()?>home/product_list?date=<?=$this->utils->get_current_date()?>">今日</a>
						</li>
						<li class="search-list-txt">
							<a href="<?=site_url()?>home/product_list?date=<?=$this->utils->get_date_by_adding_days(-1)?>"><?=$this->utils->get_date_by_adding_days(-1)?></a>
						</li>
						<li class="search-list-txt">
							<a href="<?=site_url()?>home/product_list?date=<?=$this->utils->get_date_by_adding_days(-2)?>"><?=$this->utils->get_date_by_adding_days(-2)?></a>
						</li>
					</ul>
					<div class="search-box-ttl co-clf">
						<div class="search-box-ttl-txt"><?=_lang('category')?></div>
					</div>
					<ul class="refine-search-list">
						<li class="search-list-txt">
							<span class="search-list-slct">
								<?=$my_category['name']?>
								<!-- &nbsp;(<?=$my_category['product_count']?>) -->
							</span>
						</li>
					</ul>
					<ul id="psgil" class="refine-search-list subrefine-list">
						<?php foreach ($sub_category_list as $key => $sub_category) { 
							if ($sub_category['product_count'] > 0) { ?>
							<li class="search-list-txt">

								<?php if ($sub_category['leaf'] == 1) { ?>
								<span class="search-list-slct"><?=$sub_category['name']?></span>
								<?php } else { ?>
								<a href="<?=site_url()?>home/product_list?category_id=<?=$sub_category['id']?>"><?=$sub_category['name']?>
									<?php if ($user_id == 0) {
										echo '&nbsp;('. $sub_category['product_count']. ')';
									} ?>
								</a>
								<?php } ?>
							</li>
						<?php } } ?>
					</ul>
					<?php if ($user_id == 0) { ?>
					<form id="attrSearchForm" name="attrSearchForm">
						<?php 
						foreach ($options as $key => $option) { 
							if ($key == 0 || $options[$key-1]->option_id != $option->option_id) {
						?>
							<div style="margin:0 auto 5px;padding:0 8px;">
								<div style="line-height:200%; padding:3px 0 0 0;">
									<div style="float:left;font-weight:bold;"><?=$option->group_name?></div>
									<div style="clear:both;"></div>
								</div>
								<ul style="margin:3px 0;padding: 0;list-style:none;">
						<?php } ?>
									<li><label>
										<input type="checkbox" ng-click="change_option(<?=$option->id?>)">
										&nbsp;<?=$option->name?>
									</label></li>
							<?php if ($key+1 == count($options) || ($options[$key+1]->option_id != $option->option_id)) { ?>
								</ul>
							</div>
							<?php } ?>
						<?php } ?>
					</form>
					<?php } ?>
					<?php if ($user_id > 0) { ?>
					<div class="search-box-ttl co-clf">
						<div class="search-box-ttl-txt"><?=_lang('custom_category')?></div>
					</div>
					<ul class="refine-search-list">
						<?php foreach ($my_custom_categories as $key => $my_c_category) { ?>
							<li class="search-list-txt">
								<span class="search-list-slct">
									<a href="<?=site_url()?>home/product_list?custom_category=<?=$my_c_category->id?>">
										<?=$my_c_category->name?>
									</a>
								</span>
							</li>
						<?php } ?>
					</ul>
					<div class="search-box-ttl co-clf">
						<div class="search-box-ttl-txt"><?=_lang('brand')?></div>
					</div>
					<ul class="refine-search-list">
						<?php foreach ($my_brands as $key => $m_brand) { ?>
							<li class="search-list-txt">
								<span class="search-list-slct">
									<a href="<?=site_url()?>home/product_list?brand_id=<?=$m_brand->id?>">
										<?=$m_brand->name?>
									</a>
								</span>
							</li>
						<?php } ?>
					</ul>
					<?php } ?>
				</div>
			</div>
			<div class="co-plist-layout-parts">
				<?php if ($group_by == 1 && $user_id == 0) {
					require_once('partials/product_list_by_companies.php');
				 } else { 
					require_once('partials/product_list_by_items.php');
				}?>
			</div>
		</div>
	</div>
</div>