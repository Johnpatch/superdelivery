<script type="text/javascript" src="<?=base_url()?>assets/js/productlist_product_slide.js" charset="Shift_JIS"></script>
<script type="text/javascript">
	$(function() {
		var openId ="";
		$(".dl-infobtn-mdl").click(function(){
			openId = $(this).attr("id").replace("dl-infobtn-","").replace("-btm","");
			$("[id^='dl-" + openId + "-info-mdl']").show();
			$("#co-fullh-mdl-mdlbg").show();
			$(".co-fullh-mdl-content").scrollTop(0);
		});
		$("#co-fullh-mdl-mdlbg,.co-fullh-mdl-wrap .co-fullh-mdl-cls").click(function(){
			$("#co-fullh-mdl-mdlbg").hide();
			$("[id^='dl-" + openId + "-info-mdl']").hide();
		});
	});
</script>
<script type="text/javascript">
// $(function(){
// 	var listrefine = $("#smd-listrefine-area .smd-listrefine");
// 	var ordarChange = $("#smd-listrefine-area .smd-listrefine .smd-order-change");
// 	var searchBox = $("#smd-listrefine-area .smd-listrefine .list-searchbox");
// 	var refPos = 0;
// 	ordarChange.css("display","block");
// 	searchBox.css("display","none");
// 	$(window).scroll(function () {
// 		var refFirstPos =  $("#smd-listrefine-area").offset().top;
// 		var refineStopPos = $("#footer-area-common3").offset().top;
// 		var Pos = $(this).scrollTop();
// 		if ( Pos > refFirstPos && refineStopPos > Pos ) {
// 			listrefine.addClass("refine-fix");
// 			ordarChange.css("display","none");
// 			searchBox.css("display","block");
// 			var refineHeight = listrefine.outerHeight();
// 						if (Pos > refPos) {
// 				if ($(window).scrollTop() >= 100) {
// 					listrefine.css("top", "-" + refineHeight + "px");
// 				}
// 			} else {
// 				listrefine.css("top", "0px");
// 			}
// 		} else {
// 			listrefine.removeClass("refine-fix");
// 			ordarChange.css("display","block");
// 			searchBox.css("display","none");
// 		}
// 		refPos = Pos;
// 	});
// });

function explain(documentId, displayId) {
	document.getElementById(documentId).onmouseover = function() {
		tid = setTimeout("exDisplayOn('" + displayId + "')", 0);
	}

	document.getElementById(documentId).onmouseout = function() {
		document.getElementById(displayId).style.display = "none";
		clearTimeout(tid);
	}
	document.getElementById(documentId).onclick = function() {
		if(document.getElementById(displayId).style.display=="block"){
			document.getElementById(displayId).style.display = "none";
			clearTimeout(tid);
		}
		else{
			tid = setTimeout("exDisplayOn('" + displayId + "')", 0);
		}
	}
}
function exDisplayOn(displayId) {
	document.getElementById(displayId).style.display = "block";
}

<?php if ($group_by == 1 && isset($filtered_products) && count($filtered_products) > 0) { ?>
var dealerCodes_str = '<?=$user_ids?>';
var productSlideProperty = {
	"displayCount" : 5,
	"thresholdCount" : 5 * 2,
	"sizeByPage" : 30 ,
	"dealerCodes" : dealerCodes_str,
	"isSwipeEnabled": true
};

var dealerCodes = dealerCodes_str.split(",");
for(i=0;i<dealerCodes.length;i++){
	dealerCode=dealerCodes[i];
	// $('#ex'+dealerCode).jqueryMenulayer({
	// 	content : '#ex_display'+dealerCode,
	// 	closeButton : '#satisfy-close'+dealerCode
	// });
	explain('ex' + dealerCode, 'ex_display' + dealerCode)

	$('#dealer-cam-link'+dealerCode).jqueryMenulayer({
		content : '#dealer-cam'+dealerCode,
		closeButton : '#dealer-cam-close'+dealerCode
	});
}

jQuery(document).ready(function() {
	productSlide(productSlideProperty);
});

<?php } else if (isset($company_info['user_id'])) { ?>
	explain('ex<?=$company_info['user_id']?>','ex_display<?=$company_info['user_id']?>')

<?php } ?>

angular.module('homeApp')
.controller('listCtrl', function($scope) {

	$scope.options = [];
	$scope.change_option = function(option_id) {
		var option_index = $scope.options.indexOf(option_id);
		if (option_index < 0) {
			$scope.options.push(option_id);
		} else {
			$scope.options = $scope.options.slice(option_id, 1);
		}
	}

	$scope.has_available_options = function(product_option) {
		if ($scope.options.length == 0)
			return true;
		var ret_value = false;
		angular.forEach($scope.options, function(option, key) {
			if (product_option.search(option) >= 0)
				ret_value = true;
		});
		return ret_value;
	}

	$scope.change_group_by = function(group_by_type) {
		var pos = document.location.href.search('group_by_type');
		var current_href;
		if (pos > 0) {
			current_href = document.location.href.substr(0, pos-1);
		} else {
			current_href = document.location.href;
		}
		if (current_href.search('=') <=0) {
			document.location = current_href + "?group_by_type=" + group_by_type;
		} else {
			document.location = current_href + "&group_by_type=" + group_by_type;
		}
	}
});
</script>

