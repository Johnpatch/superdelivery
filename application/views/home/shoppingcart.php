<div class="co-pankuzu-list co-pc-only">
	<a href="<?=site_url()?>"><?=_lang('to_top')?></a>
	<span><?=_lang('shoppingcart')?></span>
</div>
<div class="co-tal co-container co-pc-only">
	<div class="co-title1 contents-title">
		<span><?=_lang('shoppingcart')?></span>
	</div>
</div>
<?php if (count($cart_products) == 0) {
	echo _lang('no_data');
} ?>
<?php foreach ($cart_products as $c_product) { ?>
<div id="order-shopping-cart">
	<div id="jsp-tiles-shopping-c" class="co-container">
		<div class="cart-wrapper">
			<div class="checkout-area fee-kind-wrap co-clf right-checkout">
				<table><tbody>
					<tr class="items">
						<th><div>商品小計</div></th>
						<td><div>¥ <?=$this->utils->MakeBigNumStyle($c_product['user']->totalPrice)?></div></td>
					</tr>
					<tr class="shipping-cost fee-mobile-none">
						<th><div>消費税</div></th>
						<td><div>¥ 0</div></td>
					</tr>
					<tr class="shipping-cost">
						<th><div>送料見込み（税込）</div></th>
						<td><div>¥ 0</div></td>
					</tr>
				</tbody></table>
				<table><tbody>
					<tr class="thetotal-price">
						<th><div class="totalprice">総合計</div></th>
						<td><div class="price">¥ <?=$this->utils->MakeBigNumStyle($c_product['user']->totalPrice)?></div></td>
					</tr>
				</tbody></table>
				<div class="checkout-button">
					<div class="co-btn co-btn-red co-btn-m co-btn-page" id="checkout">
						<span><a href="<?=site_url()?>home/order?user_id=<?=$c_product['user']->id?>"><span class="co-dib">ご注文画面へ</span></a></span>
					</div>
				</div>
			</div>
			<div class="cart-dealer-wrapper co-clf" id="dc_84326">
				<div class="co-clf company-name-area">
					<div class="company-name dealerinfo-markbox"><a href="<?=site_url()?>home/product_list?user_id=<?=$c_product['user']->id?>"><?=$c_product['user']->username?></a></div>
				</div>
				<div class="dealer-th">
					<div class="th item-detail">商品情報</div>
					<div class="th lot">セット数</div>
					<div class="th price">商品代金</div>
					<div class="th tax">消費税</div>
				</div>
				<?php foreach ($c_product['products'] as $product) { ?>
				<div class="product-detail co-clf">
					<div id="psh_4212606_1">
						<div class="item-detail-td td co-clf">
							<div class="img-box">
								<div class="co-img-center-wrap">
									<div class="co-img-center">
										<div class="co-img-center-inner">
											<a href="<?=site_url()?>home/good_detail/<?=$product->product_id?>">
												<img src="<?=$this->utils->main_image_url($product->product_id, $product->main_image)?>" border="0" alt="<?=$product->name?>">
											</a>
										</div>
									</div>
								</div>
							</div>
							<div class="product-name">
								<a href="<?=site_url()?>home/good_detail/<?=$product->id?>">
									<?=$product->name?>&nbsp;:&nbsp;<?=$product->quantity?>&nbsp;点
								</a>
							</div>
							<div class="sd-number">
								<?=$product->sku. '-'. $product->product_number?>
								<span>(Manufacturer part number: usb053)</span>
							</div>
							<div class="setDetail-deliveryDate-box">
								<div>
									Breakdown: <?=$product->breakdown?>
									<span class="co-pc-only">&nbsp;</span>
									<div class="delivery-td">
										Shipping Date: Next Business Day
									</div>
								</div>
							</div>
						</div>
						<div class="co-clf lot-price-wrap">
							<div class="td lot-td">
								<div>
									<div class="lot-td-inner" style="width: 100%;">
										<p>1セット（<?=$product->quantity?>点）</p>
										<p>
											<span class="co-fcred">¥&nbsp;<?=$this->utils->MakeBigNumStyle($product->r_price * $product->quantity)?></span>
											&nbsp;×
										</p>
									</div>
								</div>
								<div class="lot-input-area">
									<div>
										<div class="quantity-button">
											<a class="onMouse" onclick="addNumber('quantity_<?=$product->id?>', -1);" style="cursor: pointer" href="javascript:void(0)"><img src="<?=base_url()?>assets/img/pages/detail/minus.png" alt="-"></a>
										</div>
										<div>
											<input type="text" pattern="[0-9]*" name="quantities[]" id="quantity_<?=$product->id?>" size="3" onfocus="this.select()" value="<?=$product->cart_qty?>" class="lot-input co-ime-disabled">
										</div>
										<div class="quantity-button">
											<a class="onMouse" onclick="addNumber('quantity_<?=$product->id?>', 1);" style="cursor: pointer" href="javascript:void(0)"><img src="<?=base_url()?>assets/img/pages/detail/plus.png" alt="+"></a>
										</div>
									</div>
									<div class="reload-button">
										<a class="onMouse" ng-click="changeCartQty(<?=$product->user_id?>, <?=$product->id?>)" href="javascript:void(0);">更新</a>
									</div>
								</div>
								<div class="co-clf wishlist-del-buttons wish-dvs-jdg">
									<div class="add-to-wishlist  prelogin-wishlist <?=$product->is_wished==1?'added-wish ':''?>" id="pd_<?=$product->product_id?>_<?=$product->user_id?>" ng-click="wishlist(<?=$product->product_id?>, <?=$product->user_id?>)">
										<span class="add-wish-icon"></span>
									</div>
									<div class="delete-link">
										<a class="delete-item" href="javascript:void(0)" id="delete_4212606_1_84326" ng-click="removeFromCart(<?=$product->id?>, <?=$product->user_id?>)"><span class="add-del-txt">削除</span><span class="add-del-icon"></span></a>
									</div>
								</div>
							</div>
							<div class="price-td td">
								<div class="co-clf">
									<span class="cmp-price co-fcred">¥&nbsp;<?=$this->utils->MakeBigNumStyle($product->r_price *$product->quantity * $product->cart_qty)?></span>
									<span class="co-ts-only">→</span>
									<span class="list-price"></span>
								</div>
							</div>
							<div class="td tax-td">
								<span class="co-ts-only">（消費税）</span>¥ 0
							</div>
						</div>
					</div>
				</div>
				<?php } ?>
				<div class="box-table-last">
					<div class="settlement co-clf">
						<table>
							<tbody>
								<tr class="items">
									<th><span class="co-fr">商品小計</span></th>
									<td class="" style="width:6em;">¥ <?=$this->utils->MakeBigNumStyle($c_product['user']->totalPrice)?></td>
								</tr>
								<tr class="shipping-cost fee-mobile-none">
									<th><span class="co-fr">消費税</span></th>
									<td><div>¥ 0</div></td>
								</tr>
								<tr class="shipping-cost fee-mobile-none">
									<th><div style="position:relative;"><div class="co-tooltip-onmouse co-tooltip-onmouse-right co-fr parentheses-margin-ts"><a class="co-popup-link-03">送料見込み（税込）</a></div></div></th>
									<td class="">¥ 0</td>
								</tr>
								<tr class="thetotal-price">
									<th><div class="text co-vamid"><span class="co-fr">総合計</span></div></th>
									<td class="co-b">¥ <?=$this->utils->MakeBigNumStyle($c_product['user']->totalPrice)?></td>
								</tr>
							</tbody>
						</table>
						<div class="dealer-order">
							<div class="delete-dealer function-delete-item">
								<a class="onMouse delete-dealer-cart" href="javascript:void(0);" ng-click="removeSubAllFromCart(<?=$product->user_id?>)">
									<p>この企業の商品を削除</p>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="bottom-checkout co-ts-only">
				<div class="checkout-area co-clf fee-kind-wrap">
					<table>
						<tbody>
							<tr class="items">
								<th><div>商品小計</div></th>
								<td><div>¥ <?=$this->utils->MakeBigNumStyle($c_product['user']->totalPrice)?></div></td>
							</tr>
							<tr class="shipping-cost fee-mobile-none">
								<th><div>消費税</div></th>
								<td><div>¥ 0</div></td>
							</tr>
							<tr class="shipping-cost fee-mobile-none">
								<th><div>送料見込み（税込）</div></th>
								<td><div>¥ 0</div></td>
							</tr>
						</tbody>
					</table>
					<table>
						<tbody>
							<tr class="thetotal-price">
								<th><div class="totalprice">総合計</div></th>
								<td><div class="price co-fcred">¥ <?=$this->utils->MakeBigNumStyle($c_product['user']->totalPrice)?></div></td>
							</tr>
						</tbody>
					</table>
					<div class="checkout-button">
						<div class="co-btn co-btn-red co-btn-m co-btn-page" id="checkout2">
							<span><a href="javascript:void(0);"><span class="co-dib">ご注文画面へ</span></a></span>
						</div>
					</div>
				</div>
				<div class="sp-tab-notice">
					<span>※送料見込みについて</span><br>
					「標準の配送先」の住所を元に算出されています。商品情報に別途送料の記載がある場合は、またはキャンペーンなどを実施している場合はそちらが優先されます。また、納期が異なる商品をご注文の場合、発送ごとに送料が発生する場合がございます。
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>