<div class="box1214 co-pankuzu">
	<a href="<?=site_url()?>"><?=_lang('to_top')?></a>
	&gt;<?=_lang('tradinglist')?>
</div>
<div class="co-container">
	<div class="co-title1">
		<span><?=_lang('tradinglist')?></span>
		<div class="request-all">申請できる企業：<span class="bigred">1245社</span>
		<a href="https://www.superdelivery.com/p/memberManage/registStore/edit.do" class="co-001g co-ml5">取引企業をもっと増やす</a></div>
	</div>
</div>
<div id="jsp-tiles-tradinglist-list">
	<div class="tradinglist-serch-wrap">
		<div align="center">
			<div class="jsp-layout-base-layout">
				<table width="100%" cellpadding="0" cellspacing="0">
            		<tbody><tr>
            		 	<td align="left" valign="top" width="220">
							<div class="co-product-refine">
								<div class="search-box-ttl co-clf">
									<div class="search-box-ttl-txt"><?=_lang('category')?></div>
								</div>
								<ul class="refine-search-list">
									<li class="search-list-txt">
										<span class="search-list-slct">
											<?=$my_category['name']?>
											<!-- &nbsp;(<?=$my_category['product_count']?>) -->
										</span>
									</li>
								</ul>
								<ul id="psgil" class="refine-search-list subrefine-list">
									<?php foreach ($sub_category_list as $key => $sub_category) {  ?>
										<li class="search-list-txt">

											<?php if ($sub_category['leaf'] == 1) { ?>
											<span class="search-list-slct"><?=$sub_category['name']?></span>
											<?php } else { ?>
											<a href="<?=site_url()?>home/product_list?category_id=<?=$sub_category['id']?>"><?=$sub_category['name']?>
											</a>
											<?php } ?>
										</li>
									<?php  } ?>
								</ul>
            		 		</div>
            		 	</td>
               			<td align="left" valign="top">
                  			<div align="center">
								<div class="tradinglist-serch-wrap">
									<div class="listorder-box upper-order co-clf">
										<div class="listorder-slct">
											<select name="sort" onchange="location.href=this.options[this.selectedIndex].value;">
												<option value="/p/tradinglist/search.do?ot=o">注文回数順（過去1年）</option>
												<option value="/p/tradinglist/search.do?ot=ta">取引開始日順</option>
												<option value="/p/tradinglist/search.do?ot=p">商品追加日順</option>
												<option selected="selected">お気に入り順</option>
											</select>
										</div>
										<div class="page-nav-area">
											<span class="page-nav-numtxt">
												1 ～ 6社目（全6社）</span>
										</div>
									</div>
                  					<div>
										<div class="co-container">
											<form action="https://www.superdelivery.com/p/tradinglist/edit.do" method="post" name="tradingListForm" id="tradingListForm">
												<?php foreach ($trading_companies as $t_company) { ?>
												<div class="tradinglist-delaer-wrap co-cf">
													<div class="box01 co-cf">
														<div class="tradinglist-dealer-name">
															<div class="mark">
																<a class="onMouse" href="javascript:toggleStar(<?=$t_company->id?>)">
																	<img id="star_on_<?=$t_company->id?>" src="<?=base_url()?>assets/img/pages/tradinglist/favorite_on.gif" alt="" style="display: none;">
																	<img id="star_off_<?=$t_company->id?>" src="<?=base_url()?>assets/img/pages/tradinglist/favorite_off.gif" alt="">
																	<input type="hidden" id="star_mark_<?=$t_company->id?>" name="star_mark_<?=$t_company->id?>" value="0">
																</a>
															</div>
															<a  href="<?=site_url()?>home/product_list?user_id=<?=$t_company->id?>"><?=$t_company->username?>（<?=$t_company->product_count?>件）</a>
														</div>
														<div id="dealerinfo-markbox" class="cpmark-box"><div class="dealer-satisfaction co-pl10 co-vamid" style="display:inline;" id="ex<?=$t_company->id?>">
															<div class="co-satisfy-star-box"><span class="co-satisfy-star-bar" style="width:88%;"></span><span class="co-satisfy-star"></span></div>&nbsp;4.4（27件）<img src="<?=base_url()?>assets/img/pages/tradinglist/popup-link-01.gif" alt="" style="vertical-align:inherit;">
															<div class="dealer-satisfaction-score" id="ex_display<?=$t_company->id?>" style="display: none;">
																<div class="satisfaction-score-tbl">
																	<div class="co-satisfy-star-container">
																		<div>
																			<span class="point">商品説明</span>
																			<div class="co-satisfy-star-box">
																				<span class="co-satisfy-star-bar" style="width:94%;"></span>
																				<span class="co-satisfy-star"></span>
																			</div>
																			4.7
																		</div>
																		<div>
																			<span class="point">実物一致度</span>
																			<div class="co-satisfy-star-box">
																				<span class="co-satisfy-star-bar" style="width:92%;"></span>
																				<span class="co-satisfy-star"></span>
																			</div>
																			4.6
																		</div>
																		<div>
																			<span class="point">価格・品質</span>
																			<div class="co-satisfy-star-box">
																				<span class="co-satisfy-star-bar" style="width:92%;"></span>
																				<span class="co-satisfy-star"></span>
																			</div>
																			4.6
																		</div>
																	</div>
																	<div class="co-satisfy-star-container">
																		<div>
																			<span class="point">納品状況</span>
																			<div class="co-satisfy-star-box">
																				<span class="co-satisfy-star-bar" style="width:90%;"></span>
																				<span class="co-satisfy-star"></span>
																			</div>
																			4.5
																		</div>
																		<div>
																			<span class="point">担当者対応</span>
																			<div class="co-satisfy-star-box">
																				<span class="co-satisfy-star-bar" style="width:94%;"></span>
																				<span class="co-satisfy-star"></span>
																			</div>
																			4.7
																		</div>
																		<div>
																			<span class="point">在庫精度</span>
																			<div class="co-satisfy-star-box">
																				<span class="co-satisfy-star-bar" style="width:66%;"></span>
																				<span class="co-satisfy-star"></span>
																			</div>
																			3.3
																		</div>
																	</div>
																</div>
															</div>
														</div></div>
														<div class="co-clear"></div>
													</div>
													<div class="box02 co-clf">
														<div class="tradinglist-dealer-image">
															<a class="item-image-box" href="<?=site_url()?>home/good_detail/<?=$t_company->first_product->id?>"><img src="<?=$this->utils->main_image_url($t_company->first_product->id, $t_company->first_product->main_image);?>" alt="カネ三商店" title="カネ三商店" style="padding-top: 6px; padding-bottom: 6px; width: 120px; height: auto;"></a>
														</div>
														<div class="cp-topic">
															<span class="campaign-topic">キャンペーン開催中</span>
														</div>
														<table cellspacing="0" cellpadding="0" class="tips" summary="">
															<tbody><tr>
																<th class="co-b">注文回数：</th>
																<td>0回</td>
																<th class="co-b">取引開始日：</th>
																<td>2019/7/17</td>
																<th class="co-b">商品追加日：</th>
																<td>
																	2019/7/12
																	<span class="new">47</span>商品
																</td>
															</tr>
														</tbody></table>
														<div class="co-ml20">
															<p class="notice-headline">お知らせ</p>
														</div>
														<div class="co-ml20">
															<div class="co-tooltip-onclick-wrap" style="z-index: 100;">
																<div class="news-box"><a class="co-popup-link-02" href="javascript:void(0)">新作商品順次追加中！</a></div>
																<div class="co-tooltip-onclick" id="dealer-news<?=$t_company->id?>_1" style="width: 500px;left: 0;">
																	<div class="co-tooltip-cancel" id="dealer-news-close<?=$t_company->id?>_1">×</div>
																	<div class="co-tooltip-text" style="margin-top: 1em;word-wrap: break-word;">
																		新作食器、新作置物を随時追加中です！ぜひご覧いただけますと幸いです。
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<?php } ?>
											</form>
										</div>
                  					</div>
                  					<div class="listorder-box notupper-order co-clf">
										<div class="page-nav-area">
											<span class="page-nav-numtxt">
												1 ～ 6社目（全6社）
											</span>
										</div>
									</div>
									<div class="list-bottom-useful">
										<a href="https://www.superdelivery.com/p/alert/form.do" class="onMouse"><img class="onMouseOver" src="<?=base_url()?>assets/img/pages/tradinglist/alert_new_buttom_06_on.gif" alt="新着入荷アラートを登録する"><img class="onMouseOut" src="<?=base_url()?>assets/img/pages/tradinglist/alert_new_buttom_06.gif" alt="新着入荷アラートを登録する"></a>
									</div>
                  				</div>
							</div>
						</td>
            		</tr></tbody>
            	</table>
			</div>
		</div>
	</div>
</div>
<div class="push"></div>