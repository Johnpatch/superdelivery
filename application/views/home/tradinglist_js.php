<script type="text/javascript">
var openedId="";
function explain(documentId,displayId) {
	if(isMobi){
		document.getElementById(documentId).onclick = function() {
			if(document.getElementById(displayId).style.display=="block"){
				document.getElementById(displayId).style.display = "none";
				openedId="";
			}
			else{
				if(openedId!=""){
					document.getElementById(openedId).style.display = "none";
				}
				tid = setTimeout("exDisplayOn('" + displayId + "')", 0);
				openedId=displayId;
			}
		}

	}
	else{
		document.getElementById(documentId).onmouseover = function() {
			tid = setTimeout("exDisplayOn('" + displayId + "')", 0);
		}

		document.getElementById(documentId).onmouseout = function() {
			document.getElementById(displayId).style.display = "none";
			clearTimeout(tid);
		}
	}
}

function exDisplayOn(displayId) {
	document.getElementById(displayId).style.display = "block";

	var number = displayId.match(/[0-9]+/);
}

function toggleStar(dealer_code) {
	var starMark = document.getElementById("star_mark_" + dealer_code);
	var overImg;
	var outImg;
	if (starMark.value == "1") {
		starMark.value = "0";
		document.getElementById("star_on_" + dealer_code).style.display = "none";
		document.getElementById("star_off_" + dealer_code).style.display = "inline";
	} else if (starMark.value == "0") {
		starMark.value = "1";
		document.getElementById("star_on_" + dealer_code).style.display = "inline";
		document.getElementById("star_off_" + dealer_code).style.display = "none";
	}

	document.getElementById("star_mark").value = starMark.value;
	document.getElementById("target_dealer_code").value = dealer_code;

	doAction();
}

function doAction() {
	var form = document.getElementById("tradingListForm");
	form.target = "iframe1";
	form.submit();
}
</script>