<div ng-controller="wishlistCtrl">
	<div class="co-pankuzu-list co-pc-only">
		<a href="<?=site_url()?>"><?=_lang('to_top')?></a>
		<span><?=_lang('wishlist')?></span>
	</div>
	<div class="co-container">
		<div class="co-title1"><span><?=_lang('wishlist')?></span></div>
	</div>
	<div class="jsp-layout-base-layout">
		<div class="co-plist-layout-wrap">
			<div class="co-plist-layout-parts search-box-parts">
				<div class="co-product-refine">
					<div class="search-box-ttl co-clf">
						<div class="search-box-ttl-txt"><?=_lang('category')?></div>
					</div>
					<ul class="refine-search-list">
						<li class="search-list-txt">
							<span class="search-list-slct">
								<?=$my_category['name']?>
								<!-- &nbsp;(<?=$my_category['product_count']?>) -->
							</span>
						</li>
					</ul>
					<ul id="psgil" class="refine-search-list subrefine-list">
						<?php
						foreach ($sub_category_list as $key => $sub_category) { ?>
							<li class="search-list-txt">
								<?php if ($sub_category['leaf'] == 1) { ?>
								<a href="<?=site_url()?>home/product_list?category_id=<?=$sub_category['id']?>"><?=$sub_category['name']?>
									&nbsp;(<?=$sub_category['product_count']?>)</a>
								<?php } else { ?>
								<a href="<?=site_url()?>home/product_list?category_id=<?=$sub_category['id']?>"><?=$sub_category['name']?>&nbsp;(<?=$sub_category['product_count']?>)</a>
								<?php } ?>
							</li>
						<?php } ?>
					</ul>
				</div>
			</div>
			<div class="co-plist-layout-parts" id="jsp-tiles-wishlist-c">
				<div id="productlist-productbox" class="co-cotainer">
					<div id="smd-listrefine-area" class="co-ts-only">
						<div class="smd-listrefine">
							<div class="list-searchbox" style="display: none;">
								<form class="searchbox">
									<input type="search" placeholder="キーワード・企業名" name="word" value="" class="search-text" ng-model="mobile_search_word">
									<div class="search-button">
										<input value="" type="submit" ng-click="wishlist_search_submit()">
									</div>
								</form>
							</div>
							<a class="smd-order-refine">絞り込み</a>
							<a class="smd-order-change"><?=_lang('custom_category')?></a>
						</div>
					</div>
					<?php
						if (count($products) > 0) {
							require_once('partials/wishlist_products.php');
						} else {
							require_once('partials/wishlist_no_products.php');
						}
					?>
				</div>
			</div>
		</div>
	</div>
	<div class="push"></div>

	<div class="wishtlist-fixed-delete-button" style="position: fixed;">
		<a href="javascript:void(0);" class="delete-mode-cancel-button toggle-delete-mode co-ts-only">キャンセル </a>
		<span>削除する</span>
	</div>
	<div class="co-order-type-mdl co-ts-only" id="rwd_memo_modal" style="display: none;">
		<p class="co-order-type-mdl-ttl">MEMO <span class="co-order-type-mdl-cls memo_modal_cancel">×</span></p>
		<div class="co-order-type-mdl-content">
			<div class="mdl-content-wrap">
				<div class="image-box-wrap">
					<div class="image-box-inner co-clf">
						<div class="large-image-absolute" id="memo_modal_thumbnail"></div>
					</div>
				</div>
				<p id="memo_modal_product_name"></p>
			</div>
			<div class="mdl-content-wrap mdl-memo-area memo-operation">
				<div>
					<textarea id="memo_modal_memo_input" class="memo-input" maxlength="40"></textarea>
					<p>※ Please enter within 40 characters</p>
				</div>
			</div>
		</div>
		<div class="co-order-type-mdl-footer">
			<div class="co-btn co-btn-m co-btn-wht"><span class="mdl-memo-clear memo_modal_cancel"><a>Cancel</a></span></div><!--
			--><div class="co-btn co-btn-m co-btn-blue"><span class="mdl-memo-check" id="memo_modal_apply"><a>OK</a></span></div>
		</div>
	</div>
	<div class="co-order-type-mdl co-ts-only" id="rwd_search_box" style="display: none;">
		<p class="co-order-type-mdl-ttl">絞り込み search <span class="co-order-type-mdl-cls">×</span></p>
		<div class="co-order-type-mdl-content">
			<ul class="co-order-type-list">
				<li>
					<div class="co-order-type-word"><input type="text" name="word" id="mdl_word_sp" value="" placeholder="商品名、企業名など"></div>
					<div class="co-mt10">
						<div class="co-order-type-except">Exclusion word:</div><!--
					--><div class="co-order-type-input">
							<input type="text" id="mdl_exw_sp" name="exw" value="" placeholder="半角スペースで複数指定できます">
						</div>
					</div>
				</li>
				<li class="co-order-radio-list">
				<div class="co-order-type-cap">Publication status</div><!--
				--><ul class="co-order-type-input">
						<li class="co-order-radio"><input type="radio" name="ex" value="" id="exhibit_all" class="exhibit_status" checked=""><label for="exhibit_all">all</label></li><!--
						--><li class="co-order-radio"><input type="radio" name="ex" value="ex" id="exhibit" class="exhibit_status"><label for="exhibit">Being published</label></li><!--
						--><li class="co-order-radio"><input type="radio" name="ex" value="nex" id="notexhibit" class="exhibit_status"><label for="notexhibit">Posting end</label></li>
					</ul>
				</li>
				<li class="co-order-type-hasnext co-order-type-select genre-title" id="genre_row" style="display:none;">
					<div class="co-order-type-cap">Genre</div>
					<div id="selected_genre_name" class="co-order-type-input co-pl5" style=""><span><?=_lang('all')?></span><span class=""></span></div>
				</li>
				
					<li class="co-order-check-list">
						<div class="co-order-type-cap">Preferential conditions</div><!--
					--><ul class="co-order-type-input">
							<li class="co-order-check"><input type="checkbox" id="detailed_cmp_sp" name="cp" value="on"><label for="detailed_cmp_sp">During the campaign</label></li>
							<li class="co-order-check"><input type="checkbox" id="detailed_bg_sp" name="ph" value="t"><label for="detailed_bg_sp">Products bought in the past</label></li>
						</ul>
					</li>
				
			</ul>
		</div>

		<!--ジャンルLv1-->
		<div class="co-order-type-mdl genre-area" data-genre-code="all" style="display:none;">
			<p class="co-order-type-mdl-ttl"><span class="co-order-type-mdl-back" data-parent-code="null">To one before</span> genre</p>
			<div class="co-order-type-mdl-content">
				<ul class="co-order-genre-list">
					<li class="genre-tree terminal" data-genre-code="all" data-genre-name="<?=_lang('all')?>" data-parent-code="all" data-parent-name="">
						<div class="co-order-type-cap">all</div></li>
					<?php foreach ($products as $key => $product) { ?>
					<li class="co-order-type-hasnext genre-tree" data-genre-code="<?=$product->id?>"><div class="co-order-type-cap"><?=$product->name?></div></li>
					<?php } ?>
				</ul>
			</div>
		</div>
		
		
		
			<div class="co-order-type-mdl genre-area" data-genre-code="5134" style="display:none;">
				<p class="co-order-type-mdl-ttl"><span class="co-order-type-mdl-back" data-parent-code="all">1つ前へ</span><?=_lang('category')?></p>
				<div class="co-order-type-mdl-content">
					<ul class="co-order-genre-list">
						<li class="genre-tree terminal" data-genre-code="5134" data-genre-name="生活雑貨" data-parent-code="all" data-parent-name="">
							<div class="co-order-type-cap">生活雑貨<?=_lang('all')?></div>
						</li>
						<li class="genre-tree terminal" data-genre-code="2866" data-genre-name="日用品" data-parent-code="5134" data-parent-name="生活雑貨"><div class="co-order-type-cap">日用品</div></li>
					</ul>
				</div>
			</div>
		

		<div class="co-order-type-mdl-footer">
			<a href="javascript:void(0);" id="rwd_search_box_reset_btn">条件をリセット</a><!--
		--><div class="co-btn co-btn-m co-btn-wht"><span><a href="javascript:void(0);" id="rwd_search_box_search_btn">検索する</a></span></div>
		</div>
	</div>
	<div class="co-order-type-mdl co-ts-only" id="rwd_search_box_sort" style="display: none;">
		<p class="co-order-type-mdl-ttl">並び替え<span class="co-order-type-mdl-cls">×</span></p>
		<div class="co-order-type-mdl-content">
			<ul class="co-order-radio-list">
				<?php foreach ($custom_categories as $key => $custom_category) { ?>
				<li class="co-order-radio">
					<input type="radio" name="order_sort" id="order_sort_added<?=$custom_category->id?>" value="<?=$custom_category->id?>">
					<label for="order_sort_added<?=$custom_category->id?>"><?=$custom_category->name?>&nbsp;(<?=$custom_category->product_count?>)</label>
				</li>
				<?php } ?>
				<!-- <li class="co-order-radio"><input type="radio" name="order_sort" id="order_sort_name" value="/p/wishlist/search.do?o=cn&amp;cc=all"><label for="order_sort_name">企業名五十音順</label></li>
				
					<li class="co-order-radio"><input type="radio" name="order_sort" id="order_sort_orderdate" value="/p/wishlist/search.do?o=od&amp;cc=all"><label for="order_sort_orderdate">注文日順</label></li>
					<li class="co-order-radio"><input type="radio" name="order_sort" id="order_sort_low" value="/p/wishlist/search.do?o=pa&amp;cc=all"><label for="order_sort_low">卸価格が安い順</label></li>
					<li class="co-order-radio"><input type="radio" name="order_sort" id="order_sort_high" value="/p/wishlist/search.do?o=pd&amp;cc=all"><label for="order_sort_high">卸価格が高い順</label></li> -->
				
			</ul>
		</div>
	</div>
</div>