<script type="text/javascript">
	
$(function(){
	var listrefine = $("#smd-listrefine-area .smd-listrefine");
	var ordarChange = $("#smd-listrefine-area .smd-listrefine .smd-order-change");
	var searchBox = $("#smd-listrefine-area .smd-listrefine .list-searchbox");
	var refPos = 0;
	ordarChange.css("display","block");
	searchBox.css("display","none");
	$(window).scroll(function () {
		var refFirstPos =  $("#smd-listrefine-area").offset().top;
		var refineStopPos = $("#footer-area-common3").offset().top;
		var Pos = $(this).scrollTop();
		if ( Pos > refFirstPos && refineStopPos > Pos ) {
			listrefine.addClass("refine-fix");
			ordarChange.css("display","none");
			searchBox.css("display","block");
			var refineHeight = listrefine.outerHeight();
						if (Pos > refPos) {
				if ($(window).scrollTop() >= 100) {
					listrefine.css("top", "-" + refineHeight + "px");
				}
			} else {
				listrefine.css("top", "0px");
			}
		} else {
			listrefine.removeClass("refine-fix");
			ordarChange.css("display","block");
			searchBox.css("display","none");
		}
		refPos = Pos;
	});
});

angular.module('homeApp')
.controller('wishlistCtrl', function($scope, $http) {
	$scope.wishlist_init = function() {
		var WishlistSearch = {};

		WishlistSearch.Wishlist = (function() {
			var Wishlist = function(options) {
				this.wishlistForm = $("#wishlistForm");
				this.productbox = $("#productlist-productbox");
				this.openSearch = $(".smd-order-refine");
				this.openSort = $(".smd-order-change");
				this.deleteMode = $(".toggle-delete-mode");
				this.deleteBtn = $(".wishtlist-fixed-delete-button");
				this.allSelect = $(".all-select-button");
				this.cartArea = $("#cart-area");
				this.cartTabArea = $("#cart-area-tab");

				this.modeToggleClass = "wishlist_delete_mode";
				this.allSelectToggleClass = "all-select-button-active";

				this.isSpMode = options.isSpMode;
				this.editAction = options.actionPath + "/edit.do";
				this.deleteAction = options.actionPath + "javascript:;";

				this.memoModal = new WishlistMemoModal();
				this.searchModal = new WishlistSearchModal(options);
				this.wishlistProducts = $(".wishlist_product").map(function() {
					return new WishlistProduct($(this).data("wishlist-code"), $(this).data("product-name"));
				});

				this.initEvent(this.isSpMode);
			};

			Wishlist.prototype.initEvent = function(isSpMode) {
				var clickEvent = "click";

				if (isSpMode) {
					// sp i‚èž‚Ý
					this.openSearch.bind(clickEvent, $.proxy(this.openSearchModal, this, "search"));
					// sp •À‚×‘Ö‚¦
					this.openSort.bind(clickEvent, $.proxy(this.openSearchModal, this, "sort"));
					// sp ‘I‘ð‚µ‚Äíœ
					this.deleteMode.bind(clickEvent, $.proxy(this.toggleDeleteMode, this));
				} else {
					// pc ‚·‚×‚Ä‘I‘ð
					this.allSelect.bind(clickEvent, $.proxy(this.toggleAllSelect, this));
					// pc ƒƒ‚•Û‘¶
					this.productbox.delegate(".memo-check", clickEvent, $.proxy(this.saveMemo, this));
					// pc ƒƒ‚ƒLƒƒƒ“ƒZƒ‹
					this.productbox.delegate(".memo-clear", clickEvent, $.proxy(this.cancelMemoEdit, this));
					// pc ¤•i‚Ìƒ{ƒbƒNƒXŠO‚ÌƒNƒŠƒbƒN
					$(document).bind(clickEvent, $.proxy(this.saveForDocumentClick, this));
				}
				// pc sp íœ‚·‚é
				this.deleteBtn.bind(clickEvent, $.proxy(this.executeDelete, this));
				// pc sp íœƒ`ƒFƒbƒN
				this.productbox.delegate(".toggle-delete-check", clickEvent, $.proxy(this.switchDeleteCheck, this));
				// pc sp MEMO
				this.productbox.delegate(".memo-contents", clickEvent, $.proxy(this.editMemo, this));
				// pc sp ƒƒ‚•Û‘¶ƒŠƒNƒGƒXƒg
				$(document).bind("/wishlist/edit", $.proxy(this.executeMemoEdit, this));
			};

			Wishlist.prototype.resetEvent = function(isSpMode) {
				if ((this.isSpMod && isSpMode) || (!this.isSpMode && !isSpMode)) {
					return;
				}
				var clickEvent = "click"
				this.isSpMode = isSpMode;

				if (isSpMode) {
					this.openSearch.bind(clickEvent, $.proxy(this.openSearchModal, this, "search"));
					this.openSort.bind(clickEvent, $.proxy(this.openSearchModal, this, "sort"));
					this.deleteMode.bind(clickEvent, $.proxy(this.toggleDeleteMode, this));

					this.allSelect.unbind(clickEvent, this.toggleAllSelect);
					this.productbox.undelegate(".memo-check", clickEvent, this.saveMemo);
					this.productbox.undelegate(".memo-clear", clickEvent, this.cancelMemoEdit);
					$(document).unbind("click", this.saveForDocumentClick);
				} else {
					this.allSelect.bind(clickEvent, $.proxy(this.toggleAllSelect, this));
					this.productbox.delegate(".memo-check", clickEvent, $.proxy(this.saveMemo, this));
					this.productbox.delegate(".memo-clear", clickEvent, $.proxy(this.cancelMemoEdit, this));
					$(document).bind(clickEvent, $.proxy(this.saveForDocumentClick, this));

					this.openSearch.unbind(clickEvent, this.openSearchModal);
					this.openSort.unbind(clickEvent, this.openSearchModal);
					this.deleteMode.unbind(clickEvent, this.toggleDeleteMode);
				}
			};

			Wishlist.prototype.getWishlist = function(wishlistCode) {
				return $.grep(this.wishlistProducts, function(v) {
					return v.wishlistCode == wishlistCode;
				})[0];
			};

			Wishlist.prototype.isDeleteMode = function() {
				return this.productbox.hasClass(this.modeToggleClass);
			};

			Wishlist.prototype.openSearchModal = function(type, e) {
				e.stopPropagation();
				if (!this.isDeleteMode()) {
					this.searchModal.open(type);
				}
			};

			Wishlist.prototype.editMemo = function(e) {
				e.stopPropagation();
				var wishlist = this.getWishlist($(e.currentTarget).data("proceed_code"));
				if (!this.isSpMode) {
					// ‘¼‚É•ÒW’†‚Ìƒƒ‚‚ª‚ ‚éŽž‚Í•Û‘¶‚·‚éB
					this.saveEditingMemo();
					wishlist.editMemo();
				} else {
					this.memoModal.open(wishlist);
				}
			};

			Wishlist.prototype.saveMemo = function(e) {
				e.stopPropagation();
				var wishlist = this.getWishlist($(e.currentTarget).data("proceed_code"));
				wishlist.saveMemo();
			};

			Wishlist.prototype.cancelMemoEdit = function(e) {
				e.stopPropagation();
				var wishlist = this.getWishlist($(e.currentTarget).data("proceed_code"));
				wishlist.cancelMemo();
			};

			Wishlist.prototype.toggleDeleteMode = function(e) {
				e.stopPropagation();
				e.preventDefault();
				this.productbox.toggleClass(this.modeToggleClass);
				this.setDeletedAll(this.productbox.hasClass(this.modeToggleClass));
				// íœƒ‚[ƒh‚ÌŽž‚Í‰æ–Ê‰º•”‚ÌƒJ[ƒg‚ðŒ©‚é‚ð•\Ž¦‚µ‚È‚¢
				if (this.cartArea.find(".cart-box").is(":visible")) {
					this.cartArea.find(".cart-box.fix-cart").toggle(!this.isDeleteMode());
				}
				if (this.cartTabArea.find(".cart-box").is(":visible")) {
					this.cartTabArea.find(".cart-box.fix-cart").toggle(!this.isDeleteMode());
				}
			};

			Wishlist.prototype.toggleAllSelect = function(e) {
				e.stopPropagation();
				e.preventDefault();
				$(e.target).toggleClass(this.allSelectToggleClass);
				this.setDeletedAll($(e.target).hasClass(this.allSelectToggleClass));
			};

			Wishlist.prototype.setDeletedAll = function(deleted) {
				if (!this.isDeleteMode()) {
					$.each(this.wishlistProducts, function(i, v) {
						v.switchDeleteCheck(deleted)
					});
				}
				this.deleteBtn.toggle(deleted);
			};

			Wishlist.prototype.switchDeleteCheck = function(e) {
				e.stopPropagation();
				e.preventDefault();

				var proceedCode = $(e.currentTarget).data("proceed_code"),
					deleted = $(e.currentTarget).hasClass("wishlist-select-icon-clear"),
					wishlist = this.getWishlist(proceedCode);

				wishlist.switchDeleteCheck(deleted);

				if (!this.isDeleteMode()) {
					var checked = $.grep(this.wishlistProducts, function(v) {
						return v.deleted;
					});
					this.deleteBtn.toggle(checked.length > 0);
				}
			};

			Wishlist.prototype.saveForDocumentClick = function(e) {
				// ƒƒ‚•ÒWó‘Ô‚Åƒƒ‚ˆÈŠO‚Ì—v‘f‚ðƒNƒŠƒbƒN‚µ‚½‚ç•Û‘¶BƒŠƒ“ƒN‚Ìê‡‚Í•Û‘¶‚µ‚È‚¢
				if ($.inArray(e.target.tagName, ["A", "TEXTAREA"]) == -1) {
					this.saveEditingMemo();
				}
			};

			Wishlist.prototype.saveEditingMemo = function() {
				$.each(this.wishlistProducts, function(i, v) {
					if (v.editing) {
						v.saveMemo();
					}
				});
			};

			Wishlist.prototype.executeMemoEdit = function(e, data) {
				var wishlistProduct = data.wishlistProduct;
				if (wishlistProduct.changed) {
					$("#memo").val(wishlistProduct.currentMemo);
					$("#target_code").val(wishlistProduct.wishlistCode);
					$("#mode").val("memo");

					this.wishlistForm.attr({ target: "iframe1", action: this.editAction });
					this.wishlistForm.submit();
				}
			};

			Wishlist.prototype.executeDelete = function(e) {
				e.stopPropagation();
				var deleteProducts = $.map(this.wishlistProducts, function(v, i) {
					v.setProceedCodeVal();
					return v.deleted ? v : null;
				});
				if (deleteProducts.length > 0) {
					$scope.deleteProducts(deleteProducts);
				}
			};

			return Wishlist;
		})();

		var WishlistProduct = (function() {
			var WishlistProduct = function(wishlistCode, productName) {
				this.wishlistProductArea = $("#wishlist_product_" + wishlistCode);
				this.proceedCodeEl = $("#proceed_code_" + wishlistCode);
				this.memoEl = this.wishlistProductArea.find(".memo-input");

				this.wishlistCode = wishlistCode;
				this.productName = productName;
				this.currentMemo = this.memoEl.val();
				this.deleted = false;
				this.editing = false;
				this.changed = false;
				this.selectIconClear = true;
				this.selectIconCheck = false;
				this.memoContent = false;
				this.memoOperation = false;
			};

			WishlistProduct.prototype.getEl = function(selector) {
				return this.wishlistProductArea.find(selector);
			};

			WishlistProduct.prototype.setProceedCodeVal = function() {
				this.proceedCodeEl.val(this.deleted ? "1" : "");
			};

			WishlistProduct.prototype.switchDeleteCheck = function(deleted) {
				this.setDeleted(deleted);
				this.refreshSelectIcon();
			};

			WishlistProduct.prototype.setDeleted = function(deleted) {
				this.deleted = deleted;
				if (deleted) {
					this.selectIconClear = false;
					this.selectIconCheck = true;
				} else {
					this.selectIconClear = true;
					this.selectIconCheck = false;
				}
			};

			WishlistProduct.prototype.refreshSelectIcon = function() {
				this.wishlistProductArea.find(".wishlist-select-icon-clear").toggle(this.selectIconClear).end()
										.find(".wishlist-select-icon-check").toggle(this.selectIconCheck);
			};

			WishlistProduct.prototype.editMemo = function() {
				this.switchMemoEdit(true);
				this.memoEl.focus();
			};

			WishlistProduct.prototype.applyMemo = function(memoVal) {
				this.memoEl.val($.trim(memoVal));
				this.saveTrigger();
			};

			WishlistProduct.prototype.saveMemo = function() {
				var memo = $.trim(this.memoEl.val());
				this.saveTrigger();
				this.switchMemoEdit(false);
				if (this.changed) {
					this.wishlistProductArea.find(".memo-contents").addClass("memo-saved").delay(100).queue(function() {
						$(this).removeClass("memo-saved").dequeue();
					});
				}
			};

			WishlistProduct.prototype.saveTrigger = function() {
				var memo = $.trim(this.memoEl.val());
				this.changed = this.currentMemo != memo;
				if (this.changed) {
					this.currentMemo = memo;
					this.wishlistProductArea.find(".wishlist-memo-area").find(".sentence span").text(memo || "MEMO");
				}
				$scope.saveMemo(this.wishlistCode, memo);
			};

			WishlistProduct.prototype.cancelMemo = function() {
				this.memoEl.val(this.currentMemo);
				this.switchMemoEdit(false);
			};

			WishlistProduct.prototype.switchMemoEdit = function(isEdit) {
				if (isEdit) {
					this.memoContent = false;
					this.memoOperation = true;
					this.editing = true;
				} else {
					this.memoContent = true;
					this.memoOperation = false;
					this.editing = false;
				}
				this.refreshMemoArea();
			};

			WishlistProduct.prototype.refreshMemoArea = function() {
				this.wishlistProductArea.find(".memo-contents").toggle(this.memoContent).end()
										.find(".memo-operation").toggle(this.memoOperation);
			};

			return WishlistProduct;
		})();

		var WishlistSearchModal = (function() {
			var WishlistSearchModal = function(options) {
				this.topArea = $("#top");
				this.searchBoxArea = $("#rwd_search_box");
				this.searchBoxSortArea = $("#rwd_search_box_sort");
				this.searchBtn = $("#rwd_search_box_search_btn");
				this.resetBtn = $("#rwd_search_box_reset_btn");
				this.genreArea = $("#genre_row");
				this.selectedGenreName = $("#selected_genre_name");

				this.selectedGenre = null;
				this.searchAction = options.actionPath + "/search.do";
				this.currentGenre = options.cc;
				this.currentOrder = options.o;

				this.init();
				this.bindEvent();
			};

			WishlistSearchModal.prototype.init = function() {
				this.genreArea.toggle(this.isExhibited());
				var current = this.searchBoxArea.find(".genre-tree.terminal").filter('[data-genre-code="' + this.currentGenre + '"]');
				this.moveGenre({
					code: current.data("genre-code"),
					name: current.data("genre-name"),
					parentCode: current.data("parent-code"),
					parentName: current.data("parent-name"),
					terminal: true
				});
			};

			WishlistSearchModal.prototype.bindEvent = function() {
				var self = this,
					clickEvent = "click";

				$(".exhibit_status").bind("change", function(e) {
					self.genreArea.toggle($(this).val() == "ex");
				});

				$(".genre-title").bind(clickEvent, function(e) {
					self.showGenre(self.selectedGenre != null ? self.selectedGenre.parentCode : "all");
				});

				self.searchBoxArea.delegate(".genre-tree", clickEvent, function(e) {
					self.moveGenre({
						code: $(this).data("genre-code"),
						name: $(this).data("genre-name"),
						parentCode: $(this).data("parent-code"),
						parentName: $(this).data("parent-name"),
						terminal: $(this).hasClass("terminal")
					});
				});

				self.searchBoxArea.delegate(".genre-area .co-order-type-mdl-back", clickEvent, function(e) {
					self.showParent($(this).data("parent-code"));
				});

				self.searchBoxSortArea.delegate(".co-order-radio", "change", function(e) {
					self.order(e);
				});

				self.searchBtn.bind(clickEvent, function(e) {
					self.search();
				});

				self.resetBtn.bind(clickEvent, function(e) {
					e.preventDefault();
					self.resetCondition();
				});

				self.searchBoxArea.find(".co-order-type-mdl-cls").bind(clickEvent, function(e) {
					e.preventDefault();
					self.close(self.searchBoxArea);
				});

				self.searchBoxSortArea.find(".co-order-type-mdl-cls").bind(clickEvent, function(e) {
					e.preventDefault();
					self.close(self.searchBoxSortArea);
				});
			};

			WishlistSearchModal.prototype.isExhibited = function() {
				return $(".exhibit_status:checked").val() == "ex";
			};

			WishlistSearchModal.prototype.open = function(type) {
				// this.topArea.hide();
				if (type == "search") {
					this.searchBoxArea.css('top', ($(window).height())).show().animate({ "top": 0 }, 250);
				} else if (type == "sort") {
					this.searchBoxSortArea.css('top', ($(window).height())).show().animate({ "top": 0 }, 250);
				}
			};

			WishlistSearchModal.prototype.close = function(modal) {
				modal.hide();
				this.topArea.show();
			};

			WishlistSearchModal.prototype.moveGenre = function(genre) {
					if (genre.terminal) {
					this.selectGenre(genre);
				} else {
					this.showGenre(genre.code);
				}
			};

			WishlistSearchModal.prototype.selectGenre = function(genre) {
				this.selectedGenre = genre;
				if (genre.parentName != "") {
					this.selectedGenreName.show().children().first().text(genre.parentName).end().last().addClass("co-order-type-arw").text(genre.name);
				} else {
					this.selectedGenreName.show().children().first().text(genre.name).end().last().removeClass("co-order-type-arw").text("");
				}
				this.slidePanel({ direction: "left", hideEl: this.searchBoxArea.find(".genre-area:visible") });
			};

			WishlistSearchModal.prototype.showGenre = function(genreCode) {
				this.slidePanel({
					direction: "right",
					hideEl: this.searchBoxArea.find(".genre-area:visible"),
					showEl: this.searchBoxArea.find('.genre-area[data-genre-code="' + genreCode + '"]')
				});
			};

			WishlistSearchModal.prototype.showParent = function(parentCode) {
				if (parentCode != null) {
					this.slidePanel({
						direction: "left",
						hideEl: this.searchBoxArea.find(".genre-area:visible"),
						showEl: this.searchBoxArea.find('.genre-area[data-genre-code="' + parentCode + '"]')
					});
				} else {
					this.slidePanel({
						direction: "left",
						hideEl: this.searchBoxArea.find(".genre-area:visible")
					});
				}
			};

			WishlistSearchModal.prototype.slidePanel = function(elements) {
				var $hideEl = elements.hideEl,
					$showEl = elements.showEl,
					hideDistance = elements.direction == "left" ? "100%" : "-100%",
					showDistance = elements.direction == "left" ? "-100%" : "100%";

				if ($hideEl) {
					$hideEl.animate({ 'left': hideDistance }, 250, function() {
						$(this).hide();
					});
				}
				if ($showEl) {
					$showEl.css('left', showDistance).show().animate({ 'left': '0%' }, 250);
				}
			};

			WishlistSearchModal.prototype.order = function(e) {
				e.stopPropagation();
				setTimeout(function() {
					$scope.sub_cate_click($(e.target).val());
				}, 50);
			};

			WishlistSearchModal.prototype.search = function() {
				var conditions = {};
				this.searchBoxArea.find("input").each(function() {
					var $el = $(this),
						type = $el.attr("type");

					if (type == "text" && $el.val() != "") {
						conditions[$el.attr("name")] = $el.val();
					} else if (type == "radio" || type == "checkbox") {
						if ($el.is(":checked") && $el.val() != "") {
							conditions[$el.attr("name")] = $el.val();
						}
					}
				});
				conditions["o"] = this.currentOrder;
				conditions["cc"] = this.isExhibited() && this.selectedGenre != null ? this.selectedGenre.code : "all";
			
				try {
					if (conditions['word'].length < 1) {
						alert('Please input keyword!');
						return;
					}
				} catch(err) {
					alert('Please input keyword!');
					return;
				}
				var detail_data = encodeURIComponent(JSON.stringify({
					category: 0,
					keyword: conditions['word'],
					exclude_keyword: conditions['exw'],
					from_cost: 0,
					to_cost: 0,
				}));
				window.location.href = $scope.serverUrl + 'home/search?detail_search_data=' + detail_data;
			};

			WishlistSearchModal.prototype.resetCondition = function() {
				this.searchBoxArea.find("input:text").val("").end().find("input:checkbox").prop("checked", false);
				this.selectedGenre = null;
				this.genreArea.hide();
				this.selectedGenreName.hide().children().empty();
				$("#exhibit_all").prop("checked", true);
			};

			return WishlistSearchModal;
		})();

		var WishlistMemoModal = (function() {
			var WishlistMemoModal = function() {
				this.modalMemoArea = $("#rwd_memo_modal");
				this.topArea = $("#top");
				this.applyBtn = $("#memo_modal_apply");
				this.productName = $("#memo_modal_product_name");
				this.thumbnail = $("#memo_modal_thumbnail");
				this.memo = $("#memo_modal_memo_input");

				this.wishlistProduct = null;
				this.scroll = 0;

				this.bindEvent();
			};

			WishlistMemoModal.prototype.bindEvent = function() {
				var self = this,
					clickEvent = "click";

				self.applyBtn.bind(clickEvent, function(e) {
					e.stopPropagation();
					self.wishlistProduct.applyMemo(self.memo.val());
					self.close();
				});

				self.modalMemoArea.find(".memo_modal_cancel").bind(clickEvent, function(e) {
					e.stopPropagation();
					self.close();
				});

				self.modalMemoArea.bind(clickEvent, function(e) {
					if ($(e.target).attr("id") != "memo_modal_memo_input") {
						self.memo.blur();
					}
				});
			};

			WishlistMemoModal.prototype.open = function(wishlistProduct) {
				if (!this.wishlistProduct || this.wishlistProduct.wishlistCode != wishlistProduct.wishlistCode) {
					this.wishlistProduct = wishlistProduct;
					this.thumbnail.empty().append(wishlistProduct.getEl(".wishlist-dealer-image-box").children().clone());
					this.productName.text(wishlistProduct.productName);
				}
				this.memo.val(wishlistProduct.currentMemo);
				this.scroll = $(window).scrollTop();
				// this.topArea.hide();
				this.modalMemoArea.css('top', ($(window).height())).show().animate({ "top": 0 }, 250);
			};

			WishlistMemoModal.prototype.close = function() {
				this.modalMemoArea.hide();
				this.topArea.show();
				$(window).scrollTop(this.scroll);
			};

			return WishlistMemoModal;
		})();

		var wishlist = new WishlistSearch.Wishlist({
			actionPath: '/p/wishlist',
			isSpMode: $("#header-sp-cont").is(":visible"),
			cc: 'all',
			o: 'ad'
		});

		var currentWidth = $(window).width(),
			resizeWidth = null,
			resizeTimer = null;

		$(window).bind("orientationchange resize", function() {
			if (resizeTimer) {
				clearTimeout(resizeTimer);
			}
			resizeTimer = setTimeout(function() {
				resizeWidth = $(window).width();
				if (currentWidth != resizeWidth) {
					wishlist.resetEvent($("#header-sp-cont").is(":visible"));
					currentWidth = $(window).width();
				}
			}, 200);
		});

		$(window).scroll(function() {
			var pushHeight = 60;
			var pushPos = $('.push').offset().top + pushHeight;
			var winBottom = $(this).scrollTop() + $(this).height() - pushHeight;
			if (winBottom <= pushPos) {
				$('.wishtlist-fixed-delete-button').css('position', 'fixed');
			} else if (winBottom > pushPos) {
				$('.wishtlist-fixed-delete-button').css('position', 'relative');
			}
		});
	}
	$scope.wishlist_init();

	$scope.deleteProducts = function(d_products) {
		var del_list = [];
		for (var i = d_products.length - 1; i >= 0; i--) {
			del_list.push(d_products[i].wishlistCode);
		}
		$http({
			method : 'post',
			dataType: 'json',
			url : $scope.serverUrl + 'api/post_del_wishlists',
			data : 'posted_data=' + 
				encodeURIComponent(JSON.stringify({
					del_list: del_list
				})),
			headers : {'Content-Type': 'application/x-www-form-urlencoded'}
		}).then(function(res) {
			window.location.reload();
		});
	}

	$scope.wishlist_search_submit = function() {
		$scope.submit_search(0, $scope.mobile_search_word);
	}

	$scope.saveMemo = function(product_id, memo) {
		$http({
			method : 'post',
			dataType: 'json',
			url : $scope.serverUrl + 'api/post_save_memo',
			data : 'posted_data=' + 
				encodeURIComponent(JSON.stringify({
					product_id: product_id,
					memo: memo
				})),
			headers : {'Content-Type': 'application/x-www-form-urlencoded'}
		}).then(function(res) {
		});
	}
});
</script>