<script type='text/javascript' src='<?=base_url()?>assets/js/angular.js'></script>

<script>
angular.module('homeApp', [])
.controller('homeCtrl', function($scope, $http) {
	
	$scope.serverUrl = '<?=site_url()?>';
	$scope.current_product_id = 0;
	$scope.r_number = new RaccoonNumber();

	$scope.prepareCategories = function() {
		var categories = <?=isset($categories)?$categories:'{}'?>;
		var last_lv1_category = null;
		var last_lv2_category = null;
		var top_categories_count = 0;
		angular.forEach(categories, function(category, key) {
			var cate_arrange = category['arrange'];
			var cate_href = $scope.serverUrl + "home/product_list?category_id=" + category['id'];
			if (cate_arrange.length == 4) {
				$(".genre-list-box").append("<li class='genre-list-head'><a href="+ cate_href+ " class='genre-list-lv1' id='lv1_"+ category['id']+ "'>"+ category['name']+ "</a></li>");
				last_lv1_category = category;
				top_categories_count++;

				// for mobile responsive
				var append_data = 
					"<h3>" + category['name'] + "</h3>" +
					"<ul class='slide-sub-genre' id='lv1_slide_ul" +category['id'] + "'>";
				$("#slide-genre-box").append(append_data);
				// for mobile responsive in bottom
				var trigger_key = key + 1;
				var append_data =
					"<p class='bottom-genre-lv1'><label for='bottom-genre-trigger" + trigger_key + "'>" +
						category['name'] +
					"</label></p>" +
					"<input type='checkbox' name='bottom-genre-trigger' id='bottom-genre-trigger" +trigger_key + "' class='bottom-genre-trigger'>" +
					"<ul class='bottom-sub-genre' id='bottom_lv1ul" + category['id'] + "'>";
				$("#co-bottom-genre-wrap").append(append_data);
			} else if (cate_arrange.length == 8) {
				var parent_id = "#lv1_" + category['parent_id'];
				if ($(parent_id).siblings().length == 0) {
					var after_data =
					 	"<div class='genre-sub-list'>" +
					 		"<div class='row'>" +	
					 			"<ul class='genre-list-lv2' id='lv2ul_" + category['parent_id'] + "'>" +
					 			"</ul>" +
					 			"<div class='genre-list-lv3-box' id='lv2div_" + category['parent_id'] + "'>" + 
					 			"</div>" +
					 		"</div>" + 
					 	"</div>";
					$(parent_id).after(after_data);
				}
				$("#lv2ul_" + category['parent_id']).append("<li><a href=" + cate_href + ">"+ category['name']+ "<a></li>");
				last_lv2_category = category;

				// for mobile responsive
				var append_data = 
					"<li><a href='" + cate_href + "''>" +
						"<div>" + category['name'] + "</div>" +
					"</a></li>";
				$("#lv1_slide_ul" + category['parent_id']).append(append_data);
				// for mobile responsive in bottom
				var append_data =
					"<li><a href=" + cate_href +"><div>" +
						category['name'] + 
					"</div></a></li>";
				$("#bottom_lv1ul" + category['parent_id']).append(append_data);
			} else {
				var parent_id = "#lv2div_" + last_lv1_category['id'];
				var display_style = "";
				if ($(parent_id).children().length == 0) {
					display_style = "block;";
				} else {
					display_style = "none;";
				}
				if ($(parent_id).find("ul#lv3ul_" + category['parent_id']).length == 0) {
					var last_lv2_cat_href = $scope.serverUrl + "home/product_list?category_id=" + last_lv2_category['id'];
					var append_data = 
						"<div class='genre-list-lv3' style='display:" + display_style + "'>" +
							"<p class='genre-lv3-head'>" + 
								"<a href=" + last_lv2_cat_href + ">" +
									last_lv2_category['name'] +
								"</a>" + 
							"</p>" + 
							"<ul id='lv3ul_"+ category['parent_id']+ "'>" +
							"</ul>" +
						"</div>";
					$(parent_id).append(append_data);
				} 
				$("#lv3ul_" + category['parent_id']).append("<li><a href=" + cate_href + ">"+category['name']+"</a></li>")
			}
		});
		$("#search_box_genre").val(<?=isset($custom_category_id)?$custom_category_id:0?>);
		var top_category_index = 0;
		var half_categories_count =  (top_categories_count * 0.5).toFixed(0);
		angular.forEach(categories, function(category, key) {
			if (category['parent_id'] == 0) {
				top_category_index++;
				var lv2ul_id = "#lv2div_" + category['id'];
				if (top_category_index <= half_categories_count) {
					$(lv2ul_id).parent().parent().addClass('open-left');
				} else {
					$(lv2ul_id).parent().parent().addClass('open-right');
				}
				if ($(lv2ul_id).length > 0) {
					if ($(lv2ul_id).children().length == 0) {
						$("#lv1_" + category['id']).parent().addClass('genre-book');
					}
				}
			}
		});
	}

	$scope.prepareCategories();

	$scope.searching_word = "<?=isset($keyword)?$keyword:''?>";
	$scope.mobile_search_word = $scope.searching_word;

	$scope.sub_cate_click = function(cate_id) {
		$scope.submit_search(cate_id, '');
	}

	$scope.search = function() {
		$scope.submit_search($("#search_box_genre").val(), $scope.searching_word);
	}

	$scope.mobile_search_submit = function() {
		$scope.submit_search(0, $scope.mobile_search_word);
	}

	$scope.custom_cate_click = function(cate_id) {
		$scope.submit_search(cate_id);
	}

	$scope.submit_search = function(search_category, searching_word) {
		if (searching_word == '') {
			if (search_category == 0) {
				window.location.href = $scope.serverUrl + 'home/product_list';
			} else {
				window.location.href = $scope.serverUrl + 'home/search?custom_category=' + search_category;
			}
		} else {
			if (search_category == 0) {
				window.location.href = $scope.serverUrl + 'home/search?keyword=' + searching_word;
			} else {
				window.location.href = $scope.serverUrl + 'home/search?keyword=' + searching_word + '&custom_category=' + search_category;
			}
		}
	}

	$scope.detail_search_view = false;
	$scope.detail_keyword = '';
	$scope.detail_exclude_keyword = '';
	$scope.from_cost = 0;
	$scope.to_cost = 0;

	$scope.detail_search = function() {
		$scope.detail_search_view = !$scope.detail_search_view;
		if ($scope.detail_search_view) {
			jQuery(".detailed-search-box").show();
		} else {
			jQuery(".detailed-search-box").hide();

		}
	}

	$scope.detail_search_start = function() {
		if ($scope.from_cost > $scope.to_cost) {
			alert('Input cost correctly');
			return;
		}

		var detail_data = encodeURIComponent(JSON.stringify({
			category: $("#detailed_search_box_genre").val(),
			keyword: $scope.detail_keyword,
			exclude_keyword: $scope.detail_exclude_keyword,
			from_cost: $scope.from_cost,
			to_cost: $scope.to_cost,
		}));
		window.location.href = $scope.serverUrl + 'home/search?detail_search_data=' + detail_data;
	}

	$scope.startWritingReview = function() {
		$scope.isReviewWritingShow = !$scope.isReviewWritingShow;
	}

	$scope.c_ratingChange = function() {
		$("#rating_img").empty();
		for (var i = $scope.c_rating; i > 0; i--) {
			$("#rating_img").append("<img src='<?=base_url()?>assets/img/grade/score_star_00.png' style='width: 12px; height: 12px;'>");
		}
	}

	$scope.sendingComment = function() {
		$http({
			method : 'post',
			dataType: 'json',
			url : $scope.serverUrl + 'api/post_comment',
			data : 'posted_data=' + 
				encodeURIComponent(JSON.stringify({
					product_id: $("#current_product_id").val(),
					rating: $scope.c_rating,
					content: $scope.c_content
				})),
			headers : {'Content-Type': 'application/x-www-form-urlencoded'}
		}).then(function(res) {
			if (res.data == -2) {
				alert('You must login first!');
			} else 	if (res.data == 1) {
				window.location.reload();
			}
		});	
	}

	$scope.wishlist = function (product_id, owner_id) {
		$http({
			method : 'post',
			dataType: 'json',
			url : $scope.serverUrl + 'api/post_wishlist',
			data : 'posted_data=' + 
				encodeURIComponent(JSON.stringify({
					product_id: product_id,
					owner_id: owner_id
				})),
			headers : {'Content-Type': 'application/x-www-form-urlencoded'}
		}).then(function(res) {
			if (res.data == 1) {
				var tag_id = 'pd_' +product_id +'_' +owner_id;
				var wish_tags = $("[id='" + tag_id + "']");
				for (var i = wish_tags.length - 1; i >= 0; i--) {
					var wish_tag = $(wish_tags[i]);
					if (wish_tag.hasClass('added-wish')) {
						wish_tag.removeClass('added-wish');
					} else {
						wish_tag.addClass('added-wish');
					}
				}

				var tag_id = 'pgd_' +product_id +'_' +owner_id;
				var wish_tags = $("[id='" + tag_id + "']");
				for (var i = wish_tags.length - 1; i >= 0; i--) {
					var wish_tag = $(wish_tags[i]);
					if (wish_tag.hasClass('wish-set-added')) {
						wish_tag.removeClass('wish-set-added');
					} else {
						wish_tag.addClass('wish-set-added');
					}
				}
			} else if (res.data == -2)
				alert('You must login first!')
			else	
				alert('Your operation is failed!');
		});	
	}

	$scope.removeFromCart = function(product_detail_id, user_id) {
		$http({
			method : 'post',
			dataType: 'json',
			url : $scope.serverUrl + 'api/post_removeProduct',
			data : 'posted_data=' + 
				encodeURIComponent(JSON.stringify({
					product_detail_id: product_detail_id,
					user_id: user_id
				})),
			headers : {'Content-Type': 'application/x-www-form-urlencoded'}
		}).then(function(res) {
			window.location.reload();
		});	
	}

	$scope.removeSubAllFromCart = function(user_id) {
		$http({
			method : 'post',
			dataType: 'json',
			url : $scope.serverUrl + 'api/post_removeSubAllProduct',
			data : 'posted_data=' + 
				encodeURIComponent(JSON.stringify({
					user_id: user_id
				})),
			headers : {'Content-Type': 'application/x-www-form-urlencoded'}
		}).then(function(res) {
			window.location.reload();
		});	
	}

	$scope.changeCartQty = function(user_id, product_detail_id, changed_amount) {
		var changed_amount = $('#quantity_' + product_detail_id).val();
		$http({
			method : 'post',
			dataType: 'json',
			url : $scope.serverUrl + 'api/post_change_cartQty',
			data : 'posted_data=' + 
				encodeURIComponent(JSON.stringify({
					user_id: user_id,
					product_detail_id: product_detail_id,
					changed_qty: changed_amount
				})),
			headers : {'Content-Type': 'application/x-www-form-urlencoded'}
		}).then(function(res) {
			window.location.reload();
		});	
	}

	$scope.carts = {};
	$scope.my_total_price = 0;

	$scope.updateCartData = function() {
		$http({
			method : 'post',
			dataType: 'json',
			url : $scope.serverUrl + 'api/get_cart',
			headers : {'Content-Type': 'application/x-www-form-urlencoded'}
		}).then(function(res) {
			var cart = res.data;
			$scope.carts = cart;
			for (x in $scope.carts) {
				if (Number($scope.carts[x].total_price) > 0)
					$scope.my_total_price += Number($scope.carts[x].total_price);
			}
		});
	}

	$("#totalAmount").hide();
	$scope.updateCartData();
	$("#totalAmount").show();



	$scope.add2cart = function(product_id, user_id) {
		var my_carts = new Array();
		var cnts = $("[maxlength='9']");
		for (var i = cnts.length - 1; i >= 0; i--) {
			var input_tag = $(cnts[i]);
			var input_val = input_tag.val();
			var product_detail_id = input_tag.attr('id').split('_')[1];
			var current_price = $('#cp4_' + product_detail_id).val();
			if (input_val > 0) {
				my_carts.push([product_id, product_detail_id, input_val, current_price]);
			}
		}
		if (my_carts.length == 0) {
			alert('Please choose quantity.');
			return;
		}
		$http({
			method : 'post',
			dataType: 'json',
			url : $scope.serverUrl + 'api/post_add2cart',
			data : 'posted_data=' + 
				encodeURIComponent(JSON.stringify({
					user_id: user_id,
					carts: my_carts
				})),
			headers : {'Content-Type': 'application/x-www-form-urlencoded'}
		}).then(function(res) {
			$scope.updateCartData();
			// alert('This product is added to your cart!');
			// window.location.reload();
		});	
	}
});
</script>