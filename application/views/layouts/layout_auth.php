<!DOCTYPE html>
<html lang="ja" style="height: 100%;" ng-app="signupApp" >
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="robots" content="noodp,noydir,index,follow">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<title>事業者専用の卸・仕入れサイト【スーパーデリバリー】</title>
	<meta name="robots" content="noodp,noydir,index,follow">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content="卸,問屋,卸売,卸し,卸問屋,仕入れ,仕入,卸販売">
	<meta name="description" content="スーパーデリバリーの公式サイト。日本全国のメーカー・問屋が商品を卸価格で販売する事業者専用の卸・仕入れサイト。小売店の仕入れ、事業者の備品・販促品調達を効率化。アパレル・雑貨を中心に幅広いジャンルのメーカー・問屋と取引きが始まります。">
	<link rel="SHORTCUT ICON" href="<?=base_url()?>assets/img/sd.ico">
	<style type="text/css">
		@import url("<?=base_url()?>assets/css/retailer-part-001.css?1555908269000");
		@import url("<?=base_url()?>assets/css/unique.css?1559008984000");
		@import url("<?=base_url()?>assets/css/common.css?1559008984000");
		@import url("<?=base_url()?>assets/css/form.css?1559008984000");
		@import url("<?=base_url()?>assets/css/header.css?1559008984000");
		@import url("<?=base_url()?>assets/css/footer.css?1555908269000");
		@import url("<?=base_url()?>assets/css/rwd.css?1559008984000");
	</style>	
	<?php echo $css_content; ?>
</head>
<body ng-controller="signupCtrl">
	<?php echo $content; ?>
</body>
<?php 	require_once('signup_ajs.php'); ?>
</html>