<!DOCTYPE html>
<html lang="en" class="translated-ltr" ng-app="homeApp">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="robots" content="noodp,noydir,index,follow">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<title><?=$title_bar?></title>
	<link rel="SHORTCUT ICON" href="<?=base_url()?>assets/img/sd.ico">
	<style type="text/css">
		@import url("<?=base_url()?>assets/css/retailer-part-001.css?1555908269000");
		@import url("<?=base_url()?>assets/css/unique.css?1559008984000");
		@import url("<?=base_url()?>assets/css/common.css?1559008984000");
		@import url("<?=base_url()?>assets/css/form.css?1559008984000");
		@import url("<?=base_url()?>assets/css/header.css?1559008984000");
		@import url("<?=base_url()?>assets/css/footer.css?1555908269000");
		@import url("<?=base_url()?>assets/css/rwd.css?1559008984000");
	</style>
	<?php echo $css_content; ?>
</head>
<body ng-controller="homeCtrl">
	<?php 	require_once('partials/slide-menu.php'); ?>
	<div id="top">
		<div id="header-fixd-content" class="wrapper fixed-header">
			<?php 	
				require_once('partials/header-area.php');
				require_once('partials/h-coupon-bar.php');
				echo $content; 
			?>
		</div>
		<?php 	require_once('partials/footer-area.php'); ?>
	</div>
</body>
<?php 	require_once('home_ajs.php'); ?>

<script type="text/javascript" src="<?=base_url()?>assets/js/dc.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/f.txt"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/ytc.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/sp-cooperate.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/jquery/jquery-1.6.4.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/common.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/image_centering.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/mobile_ua.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/utm_getter.js"></script>

<script type="text/javascript">
	jQuery(function ($) {
		var fixHeaderLayout = function() {
			if($("#header-common #header-simple").length) { 
				$("#header-fixd-content.fixed-header").css("padding-top", 0);
			}
			if (isSmartDevice() && isTransPathTarget()){ 
				$("#header-area-common").css("position", "relative");
				$("#header-fixd-content").css("padding-top", 0);
				$("#header-btm-common").css({"position":"relative", "top":0});
			} else {
				var headerHeight = $("#header-area-common").outerHeight();
				var headerPadding = parseInt(headerHeight);
				$("#header-fixd-content").css("padding-top", headerPadding);
			}
		}
		fixHeaderLayout();
		$(window).resize(function(){ fixHeaderLayout(); });
	});
</script>

<script type="text/javascript" src="<?=base_url()?>assets/js/flexible-box.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/flexible-box-animate.js"></script>

<script type="text/javascript" src="<?=base_url()?>assets/js/element_main.js" charset="UTF-8" ></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/slide_menu.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/jquery/jquery.menu-aim.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/jquery/jquery.menulayer.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/jquery/jquery.header.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/jquery/jquery.header.loggedin.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/jquery/jquery.genre_navi.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/sdGetShoppingCart.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function() {
		$.genreAnimation();
	});
</script>
<script type="text/javascript" src="<?=base_url()?>assets/js/jquery/jquery.autocomplete.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/encode.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/jquery/jqueryWatermaker.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/search_box_liquid.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/search_box_rwd.js"></script>

<script type="text/javascript">
$(function() {
	$("#rwd-searchWordTextWrap").jqueryWatermaker();

	$("#rwd_header_word").autocomplete("/t/suggest/search.do", {
		genreSelector : "#rwd-search_box_genre",
		onItemSelect : function() {
			textSearchBox.search({
				formId : "rwd-searchbox-form",
				keywordId : "rwd_header_word",
				genreCode : $("#rwd-search_box_genre").val()
			});
		}
	});

	textSearchBox.setTopText();

	$("#rwd-search_box_genre").change(function() {
		textSearchBox.setTopText();
  	});

	$("#rwd-searchbox-button").click(function() {
		textSearchBox.search({
			formId : "rwd-searchbox-form",
			keywordId : "rwd_header_word",
			genreCode : $("#rwd-search_box_genre").val()
		});
	});
});
</script>
<script type="text/javascript" src="<?=base_url()?>assets/js/raccoon_product.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/raccoon_number.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/useragent.js"></script>

<script type="text/javascript">
	$(function() {
		function setEqualHeight(columns){
			var tallestcolumn = 0;
			columns.each(
			function(){
				currentHeight = $(this).height();
				if(currentHeight > tallestcolumn){
					tallestcolumn = currentHeight;
				}
			});
			columns.height(tallestcolumn);
		}
		var fTxtH = function() {
			$(".feature-txt-area").css("height","auto");
			setEqualHeight($(".feature-txt-area"));
		}
		fTxtH();
		$(window).resize(function() { fTxtH(); })
	});
</script>
<?php 	echo $js_content; ?>
</html>