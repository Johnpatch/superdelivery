<div id="footer-area-common3" class="co-container">
	<div class="up">
		<div class="wrap co-clf">
			<div class="left-column">
				<p class="title">スーパーデリバリーについて</p>
				<ul class="menu">
					<li class="line-h"><a href='javascript:;' target="_blank"><?=_lang('guide')?></a></li>
					<li><a href='javascript:;'>サイトマップ</a></li>
					<li><a href='javascript:;'>プライバシー・<span class="sp-br">ステートメント</span></a></li>
					<li><a target="_blank" href='javascript:;'>会社概要</a></li>
				</ul>
				<a class="export-button" href="<?=site_url()?>">SUPER DELIVERY<span class="pc-br"> International</span></a>
			</div>
			<div class="right-wrap-column co-clf">
				<div class="center-column">
					<p class="title">商品を卸したいメーカー様へ</p>
					<ul class="menu">
						<li class="invitation">
							<a href="<?=site_url()?>admin"><img src="<?=base_url()?>assets/img/pages/home/for_maker_invitation_icon00.png" alt=""><span>出展のご案内</span></a>
						</li>
						<div class="txtlink"><a href='javascript:;' class="co-001w"><span>海外販路を開拓したいメーカー様へ</span></a></div>
						<li><a class="seminar" href='javascript:;'><img src="<?=base_url()?>assets/img/pages/home/for_maker_seminar_icon00.png" alt=""><span>EC販路開拓セミナー</span></a></li>
					</ul>
				</div>
				<div class="right-column">
					<p class="title">SNS</p>
					<div class="links-wrap">
						<a target="_blank" href='javascript:;'><img class="twitter" src="<?=base_url()?>assets/img/pages/home/twitter_icon00.png" alt=""></a>
						<a target="_blank" href='javascript:;'><img class="facebook" src="<?=base_url()?>assets/img/pages/home/facebook_icon00.png" alt=""></a>
					</div>
					<img src="<?=base_url()?>assets/img/pages/home/award_logo_footer02.png" class="award-nsa" alt="第1回 日本サービス大賞 地方創生大臣賞">
				</div>
			</div>
		</div>
	</div>
	<div class="footer-down">
		<div class="inner">
			<p class="main-title">ラクーングループのサービス</p>
			<div class="column-all">
				<div class="column01">
					<p class="title">ECおよびEC関連</p>
					<div class="dmain-wrap">
						<a class="dmain dmain-sd" href='javascript:;' target="_blank"><span class="dmain-name">スーパーデリバリー</span><span class="dmain-text">卸・仕入れサイト</span></a>
						<ul class="service service-sd">
							<li><a href='javascript:;' target="_blank">国内向けサービス</a></li>
							<li><a href='javascript:;' target="_blank">海外向けサービス<span>（SD export）</span></a></li>
						</ul>
					</div>
					<div class="dmain-wrap">
						<a class="dmain dmain-corec" href='javascript:;' target="_blank"><span class="dmain-name">COREC</span><span class="dmain-text">クラウド受注・発注システム</span></a>
					</div>
					<div class="dmain-wrap">
						<a class="dmain dmain-sdf" href='javascript:;' target="_blank"><span class="dmain-name">SDファクトリー</span><span class="dmain-text">メーカーと工場のマッチングサービス</span></a>
					</div>
				</div>
				<div class="column02">
					<p class="title">決済</p>
					<ul>
						<li class="dmain-wrap"><a class="dmain dmain-paid" href='javascript:;' target="_blank"><span class="dmain-name">Paid</span><span class="dmain-text">BtoB後払い決済</span></a></li>
					</ul>
				</div>
				<div class="column03">
					<p class="title">保証</p>
					<div class="dmain-wrap">
						<a class="dmain dmain-trgr" href='javascript:;' target="_blank"><span class="dmain-name">T&amp;G売掛保証</span><span class="dmain-text">売掛金・売掛債権の課題解決</span></a>
					</div>
					<div class="dmain-wrap">
						<a class="dmain dmain-uriho" href='javascript:;' target="_blank"><span class="dmain-name">URIHO</span><span class="dmain-text">ネット完結型の売掛保証</span></a>
					</div>
					<div class="dmain-wrap">
						<a class="dmain dmain-yachin" href='javascript:;' target="_blank"><span class="dmain-name">事業用家賃保証</span><span class="dmain-text">事業用物件に特化した家賃債務保証</span></a>
					</div>
					<div class="dmain-wrap">
						<a class="dmain dmain-alemo" href='javascript:;' target="_blank"><span class="dmain-name">ALEMO賃貸保証</span><span class="dmain-text">賃貸物件の家賃債務保証</span></a>
					</div>
				</div>
			</div>
		</div>
		<p class="copyright">スーパーデリバリーは個人情報を暗号化して送信するSSLに対応しています。<br><span class="under">（C）2002 RACCOON HOLDINGS, Inc.</span></p>
	</div>
</div>