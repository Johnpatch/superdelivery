<div id="header-area-common" class="co-container rwd-header fixed-head">
	<div class="co-clf">
		<div class="header-l-wrap">
			<div class="header-logo">
				<a href="<?=site_url()?>"><img src="<?=base_url()?>assets/img/common/logo/logo_header.gif" alt="スーパーデリバリー" class="co-vabot"></a>
			</div>
		</div>
		<div class="header-r-wrap-member">
			<div class="header-menu co-clf">
				<?php if ($this->identity->is_guest()) { ?>
				<h1><?=$title_bar?></h1>
				<div class="pre-google-trans-wrap">
					<div class="r-menu">
						<ul>
							<li class="pre-h-help">
								<a href="<?=site_url()?>"><?=_lang('guide')?></a>
							</li>
							<li class="pre-h-wishlist">
								<a href="<?=site_url()?>home/wishlist"><?=_lang('wishlist')?></a>
							</li>	
							<li>
								<a href="<?=site_url()?>" class="change-lang to-en">International Ver.<img src="<?=base_url()?>assets/img/pages/home/link_icon.png" class="co-ml10"></a>
							</li>
						</ul>
					</div>
				</div>
				<?php } else { ?>
				<div class="l-menu">
					<ul>
						<li>
							<a href="javascript:;">
								<span class="header-count" id="alert-count" style="display: none;"></span>入荷アラート</a>
						</li>
						<li>
							<a href="<?=site_url()?>home/tradinglist"><?=_lang('tradinglist')?></a>
						</li>
						<li>
							<a href="<?=site_url()?>home/wishlist"><?=_lang('wishlist')?></a>
						</li>	
						<li>
							<a href="javascript:;"><?=_lang('buyhistory')?></a>
						</li>	
						<li><a href="javascript:;">メッセージBOX</a></li>
					</ul>
				</div>
				<div id="header-member-panel-area">
					<div id="headerMemberPanel" class="r-panel" style="">
						<span class="com-name" style="width: auto;"><?=$this->identity->user()->username?></span>
						<span class="com-name-last">&nbsp;</span>
						<span class="header-count" id="message-count" style="display: none;"></span>
					</div>
					<div class="panel-contents" id="member-panel-contents" style="display: none;">
						<div class="panel-content-news" id="panel-content-news" style="display: block;">
							<div class="co-attention2">お知らせ</div>
						</div>
						<div class="panel-content-info">
							<div class="co-clf co-tar">
								<p class="co-tal co-b co-mb0">お客様番号：<?=$this->identity->user()->id?></p>
								<div id="raccoon-rank" class="rank-white">ホワイト会員</div>
								<div id="point-num" class="point-num">
									<a href="#member/manage/point/order_history">0&nbsp;ポイント</a>
								</div>
							</div>
							<div class="shop-info co-clf">
								<div class="co-img-center-wrap shop-photo">
									<div class="co-img-center">
										<div class="co-img-center-inner">
											<a href="#p/memberManage/registStore/edit.do" class="co-img-over"><span><img id="member-panel-store-photo" src="<?=base_url()?>assets/img/header/shop_no_image01.gif" alt=""></span></a>
										</div>
									</div>
								</div>
								<div class="link-txt">
									<ul>
										<li><a href="#/p/memberManage/registStore/edit.do" class="co-001g">会員情報の変更</a></li>
										<li><a href="#/p/memberManage/settlement/first.do" class="co-001g">決済方法の登録・変更</a></li>
										
										<li><a href="#/p/memberManage/listMailMagazine/search.do" class="co-001g">メルマガの設定</a></li>
										<li><a href="#/p/memberManage/changePassword/search.do" class="co-001g">パスワードの変更</a></li>
										<li><a href="#/p/memberManage/posregi/status.do" class="co-001g">POSレジの設定</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel-content-logout">
							<div class="loggedin-help-howto"><a href="#/p/contents/guide/help/" target="_blank"><span><?=_lang('guide')?></span></a></div>
							<a href="<?=site_url()?>auth/logout"><?=_lang('logout')?></a>
						</div>
					</div>
				</div>
				<?php } ?>
			</div>
			<div class="header-search co-clf">
				<div class="l-search <?=$this->identity->is_logined()?'logged-in-search':''?>">
					<div class="search-wrap" style="width: 801px; margin-left: -413px;">
						<form class="searchbox" id="searchbox-form" name="searchWord">
							<input id="dpsl_current_condition_pre" type="hidden" value="true">
							<input id="p_action" type="hidden" value="psl">
							<span style="display: none; font-size: 12px; letter-spacing: -1px;" id="hidden-span"><?=_lang('all')?></span>
							<span class="searchbox-select-wrap" id="select-parent" style="width: 157px;">
								<span class="searchbox-select-top" id="select-top" style="width: 130px;"><?=_lang('all')?></span>
								<span class="searchbox-select-down" id="select-down"></span>
								<select class="searchbox-select" id="search_box_genre" style="width: 157px;" >
									<option value='0'><?=_lang('all')?></option>
									<?php foreach ($custom_categories as$custom_category) {
										echo "<option value='$custom_category->id'>$custom_category->name</option>";
									} ?>
								</select>
							</span>
							<div id="searchbox-field-wrap" class="searchbox-field-wrap co-clf" style="width: 539px;">
								<div class="searchbox-field">
									<div class="searchbox-select-width" id="searchbox-select" style="padding-left: 167px;">
										<div id="searchWordTextWrap" class="watermark liquid">
											<span style="display: inline;">キーワード・企業名</span>
											<input type="text" class="searchbox-word search_key ac_input" autocomplete="off" ng-model="searching_word">
										</div>
										<div class="detailed-search-btn" ng-click="detail_search()">+詳細検索</div>
										<div class="detailed-search-box" style="display: none;">
											<div class="arr"><img src="<?=base_url()?>assets/img/pages/home/detailed_search_arr.png" alt=""></div>
											<table>
												<tbody><tr class="genre-select">
													<th>検索範囲</th>
													<td>
														<select id="detailed_search_box_genre">
															<option value="0">すべて</option>
																<?php foreach ($custom_categories as$custom_category) {
																	echo "<option value='$custom_category->id'>$custom_category->name</option>";
																} ?>
														</select>
													</td>
												</tr>
												<tr class="keyword">
													<th>キーワード</th>
													<td>
														<input class="search-word" type="text" id="detailed_word" value="" placeholder="キーワード・企業名" ng-model="detail_keyword">
														<div class="co-mt10">除外キーワード<span class="co-tooltip-onmouse co-ml0"><a class="co-popup-link-03" href="javascript:void(0);"><span class="co-tal inner-text" style="width:300px;">スペースで区切ることで複数指定できます。<br>例）AAA BBB</span></a></span>：<input type="text" name="exw" id="detailed_exclude_word" value="" ng-model="detail_exclude_keyword">&nbsp;を除く</div>
													</td>
												</tr>
												<tr class="price detailed-condition">
													<th class="co-pt15">価格</th>
													<td>
														<div><span>上代</span>
															<input type="number" id="detailed_rb" name="rb" value="" ng-model="from_cost">円～<input type="number" id="detailed_rc" name="rc" value="" ng-model="to_cost">円
														</div>
													</td>
												</tr>
												<tr class="commitment detailed-condition">
													<th class="co-pt5">こだわり条件</th>
													<td>
														<div class="co-clf">
															<label for="detailed_db" class="exclude-dpsl"><input id="detailed_db" type="checkbox" name="db" value="1">代引き対応</label>
															<label for="detailed_oi"><input id="detailed_oi" type="checkbox" name="oi" value="1">1点販売あり</label>
															<label for="detailed_ri" class="exclude-dpsl"><input id="detailed_ri" type="checkbox" name="ri" value="1">画像転載可</label>
															<label for="detailed_is"><input id="detailed_is" type="checkbox" name="is" value="1">在庫あり</label>
														</div>
													</td>
												</tr>
											</tbody></table>
											<div class="button-area">
												<div class="button"><a href="javascript:void(0);" ng-click="detail_search_start()" id="detailed_search_button">検索する</a></div>
												<a class="reset-btn" href="javascript:void(0);" ng-click="detail_search()" id="detailed_reset_button">条件をリセット</a>
											</div>
										</div>
									</div>
								</div>
								<div class="searchbox-button" id ="searchbox-button">
									<input class="search-button" type="button" ng-click="search()">
								</div>
							</div>
						</form>
						<?php if ($this->identity->is_logined()) { ?>
						<div class="cart-price-area" id="cart-price-area">
							<table cellspacing="0" cellpadding="0" border="0">
								<tbody><tr>
									<td id="cart2" class="exists">
										<a href="<?=site_url()?>home/shoppingcart">カート</a>
									</td>
									<td valign="middle" class="price-btn-area" id="price-btn-area">
										<div class="price-box" id="toggle_price_box">
											<div class="bg-price1"></div>
											<div class="bg-price2"></div>
											<p class="nowprice">
												<span>
													<a href="javascript:void(0);" ><span id="totalAmount" class="in-cart" style="display: none;">¥{{r_number.commaFormat(my_total_price)}}</span></a>
												</span>
											</p>
										</div>
										<div class="price-detail-box" id="price-detail-box-id">
											<div class="detail-inner" id="detail-inner">
												<table cellspacing="0" cellpadding="0" border="0" id="price-table" class="price-table" ng-if="my_total_price > 0">
													<tbody ng-repeat="cart in carts">
														<tr>
															<td class="td01-company ">
																<a href="<?=site_url()?>home/product_list?user_id={{cart.user_id}}">{{cart.username}}</a>
															</td>
															<td class="td02 td02-price ">
																¥{{r_number.commaFormat(cart.total_price)}}
															</td>
														</tr>
														<tr>
															<td colspan="3" class="td02 td02-price2 ">
																送料無料
															</td>
														</tr>
													</tbody>
												</table>
												<div id="rest-of-table" class="rest-of-table">
													<img src="<?=base_url()?>assets/img/pages/tradinglist/pricebox_resttable.gif" ng-show="my_total_price > 0">
												</div>
											<!-- 	<div id="cartItem_none_text" class="sd_ship_text" style="display: none;">
													現在、ショッピングカートの中は空です。
												</div> -->
												<div id="sd_ship_systemError" class="sd_ship_text" ng-show="my_total_price==0">
													エラーが発生しました。
												</div>
												<!-- <div id="sd_ship_logOutError" class="sd_ship_text" style="display: none;">
													読み込みに失敗しました。<br>
													ページを再度読み込み直してください。
												</div> -->
												<div class="btn-fromprice-tocart" style="">
													<a href="<?=site_url()?>home/shoppingcart">
														<img border="0" onmouseout="this.src='<?=base_url()?>assets/img/common/header/btn_fromprice_tocart.gif'" onmouseover="this.src='<?=base_url()?>assets/img/common/header/btn_fromprice_tocart_on.gif'" alt="カートへ" src="<?=base_url()?>assets/img/common/header/btn_fromprice_tocart.gif">
													</a>
												</div>
											</div>
										</div>
									</td>
								</tr>
							</tbody></table>
						</div>
						<div id="point-num1" class="point-area">
							<span>利用可能ポイント</span><br>
							<a href="https://www.superdelivery.com/member/manage/point/order_history">0&nbsp;pt</a>
						</div>
						<?php } ?>
					</div>
					<?php if ($this->identity->is_guest()) { ?>
						<div class="pre-btn-area">
							<div class="co-btn co-btn-red co-btn-s co-btn-page"><span><a href="<?=site_url()?>auth/signup"><?=_lang('btn_register')?></a></span></div>
							<a href="<?=site_url()?>auth/login" class="log-btn"><?=_lang('login')?></a>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
	<div id="loading-contents">
		<div id="genre-list-area">
			<ul class="genre-list-box">
			</ul>
		</div>
	</div>
</div>

<div id="header-sp-cont" style="">
	<div class="solicit-txt">
		<p><?=$title_bar?></p>
	</div>
	<?php if ($this->identity->is_guest()) { ?>
	<div id="header-main" class="co-clf">
		<div id="sub-menu-btn">
			<div class="first-line"></div>
			<div class="mid-line"></div>
			<div class="last-line"></div>
			<span><?=_lang('menu')?></span>
		</div><div class="head-logo"><a href="<?=site_url()?>"><img alt="SUPER DELIVERY" src="<?=base_url()?>assets/img/common/logo/logo_rwd.png"></a></div><div class="right-cont">
					<a href="<?=site_url()?>auth/login" class="sp-login-box"><?=_lang('login')?></a></div>
	</div>
	<?php } else { ?>
	<div id="header-main" class="co-clf">
		<div id="sub-menu-btn" style="cursor: pointer;">
				<div class="first-line"></div>
				<div class="mid-line"></div>
				<div class="last-line"></div>
				<span>MENU</span>
			</div><div class="head-logo"><a href="<?=site_url()?>"><img src="<?=base_url()?>assets/img/common/logo/logo_rwd.png"></a></div><div id="cart-area" class="right-cont cart-in h-sp-cart">				
			</div>
	</div>
	<ul id="header-nav">
		<li class="nav-alert"><a href="javascript:;"><span>入荷アラート</span></a></li>
		<li class="nav-trading"><a href="<?=site_url()?>home/tradinglist"><span><?=_lang('tradinglist')?></span></a></li>
		<li class="nav-wish"><a href="<?=site_url()?>home/wishlist"><span><?=_lang('wishlist')?></span></a></li>
		<li class="nav-history"><a href="javascript:;"><span><?=_lang('buyhistory')?></span></a></li>
		<li class="nav-msg"><a href="javascript:;"><span>メッセージBOX</span></a></li><li id="cart-area-tab" class="right-cont cart-in h-tab-cart">
			<div class="cart-box">
				<a id="cartest" href="<?=site_url()?>home/shoppingcart">
					<span class="cart-price">¥{{r_number.commaFormat(my_total_price)}}</span>
					<p><?=_lang('shoppingcart')?></p>
				</a>
			</div>
			<div class="fix-cart cart-box" style="bottom: -140px;">
				<a id="cartest" href="<?=site_url()?>home/shoppingcart">
					<span class="cart-price">¥{{r_number.commaFormat(my_total_price)}}</span>
					<p><?=_lang('shoppingcart')?></p>
				</a>
			</div>
		</li>
	</ul>
	<div id="point-num2" class="header-point-area other">
		<span>利用可能ポイント:</span>
		<a href="javascript:;">0&nbsp;pt</a>
	</div>
	<?php }?>
</div>	