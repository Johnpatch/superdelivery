<div id="slide-menu" style="">
	<p class="slide-genre-head co-mt0">キーワード検索</p>
	<div class="h-search-pre">
		<form class="searchbox" id="slide-searchbox-form">
			<div id="slide-searchbox-field-wrap" class="searchbox-field-wrap co-clf">
				<div class="searchbox-field">
					<div class="searchbox-select-width" id="slide-searchbox-select" style="padding-left: 205px;">
						<div id="slide-searchWordTextWrap" class="watermark liquid">
							<span style="display: inline;">キーワード・企業名</span>
							<input type="text" class="searchbox-word search_key ac_input" autocomplete="off" name="word" id="slide_word" ng-model="mobile_search_word">
							<input type="text" style="display: none;">
						</div>
					</div>
				</div>
				<div id="slide-searchbox-button" class="searchbox-button">
					<input class="search-button" id="slide-search-button" value="" type="submit" ng-click="mobile_search_submit()">
				</div>
			</div>
			<input type="hidden" name="so" value="score">
			<input type="hidden" name="vi" value="1">
		</form>
	</div>
	<div class="pre-sp-help-howto">
		<a href="javascript:;" target="_blank"><span><?=_lang('guide')?></span></a>
	</div>
	<div class="pre-sp-wishlist">
		<a href="<?=site_url()?>home/wishlist"><span><?=_lang('wishlist')?></span></a>
	</div>
	<div class="pre-sp-wishlist">
		<a href="<?=site_url()?>home/shoppingcart"><span><?=_lang('shoppingcart')?></span></a>
	</div>
	<p class="slide-genre-head">商品ジャンル</p>
	<div id="slide-genre-box">
	</div>
</div>