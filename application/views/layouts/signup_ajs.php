<script type='text/javascript' src='<?=base_url()?>assets/js/angular.min.js'></script>

<script>
angular.module('signupApp', [])
.controller('signupCtrl', function($scope, $http) {

	$scope.serverUrl = "<?=site_url()?>";

	$scope.email = '';
	$scope.username = '';
	$scope.password = '';

	$scope.startRegistration = function() {
		if ($scope.email =='' || $scope.username == '' || $scope.password == '') {
			alert('Please input your basic information!');
			return;
		}

		$http({
			method : 'post',
			dataType: 'json',
			url : $scope.serverUrl + "api/post_registration",
			data : "posted_data=" + 
				encodeURIComponent(JSON.stringify({
					email: $scope.email,
					username: $scope.username,
					password: $scope.password
				})),
			headers : {'Content-Type': 'application/x-www-form-urlencoded'}
		}).then(function(res) {
			alert('You have registered a new account! Please wait for administrator to allow your account!');
			window.location.href = $scope.serverUrl;
		});
	}
});
</script>