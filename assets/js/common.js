/*出展企業情報ウィンドウ*/

function  collect(url) {
	var w = window;
	var destination = "width=720,height=620,left=0,top=0,resizable=yes,scrollbars=yes,toolbar=yes,location=yes,menubar=yes,status=yes";
	if((w == window) || w.closed) { w = open(url,'superdetail',destination);w.focus();} else {w.focus();}
	return(false);
}

function browserLanguage(){
	return window.navigator.userLanguage
	|| window.navigator.language
	|| window.navigator.browserLanguage;
}

function isBrowserLanguageForeign(){
	return !/^ja/.test(browserLanguage());
}

/*出展企業情報ウィンドウNEW*/
function  collect4(url, target) {
	var w = open(url, target);
	w.focus();
	return false;
}

/*POPUPウィンドウ*/

function  collect2(url) {
	var w = window;
	var destination = "width=400,height=400,left=0,top=0,resizable=yes,scrollbars=yes";
	if((w == window) || w.closed) { w = open(url,'superdetail',destination);w.focus();} else {w.focus();}
	return(false);
}

/*verisign用ウィンドウ*/

function  collect3(url) {
	var w = window;
	var destination = "width=500,height=450,left=0,top=0,resizable=yes,scrollbars=yes";
	if((w == window) || w.closed) { w = open(url,'superdetail',destination);w.focus();} else {w.focus();}
	return(false);
}

/*出展企業情報お知らせタイトル用ウィンドウ*/

function  collect_dealerintroduction(code) {
	collect('/j/do/clickDealerIntroduction?code=' + code);
	return(false);
}

/**/

function check_all(){
	var i,e;
	i = 0;
	f = document.clickProductDetail;//フォームの名前
	e = f.elements.length;

for (i = 0; i < e; i++) {
    f.elements[i].checked = true;
	}
}

function nocheck_all(){
	var i,e;
	i = 0;
	f = document.clickProductDetail;//フォームの名前
	e = f.elements.length;

for (i = 0; i < e; i++) {
    f.elements[i].checked = false;
	}
}

/*　ページＴＯＰへリンク （新バージョン）2008/7/30 */

var getBrowserHeight = function(){
	if (window.innerHeight) {
		return window.innerHeight;
	}
	else if (document.documentElement && document.documentElement.clientHeight != 0) {
		return document.documentElement.clientHeight;
	}
	else if (document.body) {
		return document.body.clientHeight;
	}
	else {
		return 0;
	}
}
var funcMoveNavi = function(){
	var scroll_value = document.body.scrollTop || document.documentElement.scrollTop;
	var browser_height = getBrowserHeight();

	if (Number(scroll_value) != NaN && Number(browser_height) != NaN && browser_height != 0){
		document.getElementById("navi").style.top = "" + (browser_height + scroll_value - 80) + "px";
		document.getElementById("navi").style.display = "block";
	}
}

/* 日付取得 */
var getDateFunction = function(){
	mydate=new Date();
	Mo=mydate.getMonth()+1+"/";
	Da=mydate.getDate()+"";
	Day=mydate.getDay();
	Day2=new Array(7);
	Day2[0]="日";Day2[1]="月";Day2[2]="火";
	Day2[3]="水";Day2[4]="木";Day2[5]="金";
	Day2[6]="土";
	return (""+Mo+Da+"("+Day2[Day]+")");
}

/* iPhone・iPad */
var agent = navigator.userAgent;
var isMobi=false;
if(agent.match(/iPhone|iPad/)){
	isMobi=true;

	/* iPhone・iPad端末のみCSS読み込み */
	var link = document.createElement('link');
	link.href = '/css/mobile_ua.css';
	link.rel = 'stylesheet';
	link.type = 'text/css';
	var h = document.getElementsByTagName('head')[0];
	h.appendChild(link);
}
/* スマホ、iPadなどタッチパネル全般 */
/* iPhone,iPad,iPod,Android(モバイル&タブレット) */
var isTouchPanel = function() {
	return (agent.indexOf('iPhone') > 0 || agent.indexOf('iPad') > 0 || agent.indexOf('iPod') > 0 || agent.indexOf('Android') > 0);
};

/* モダンブラウザ判定 */
var isModernBrowser = function() {
	return typeof window.getSelection === "function";
};

/*以下廃止予定------------------------------------------*/

function winopen2(loc) {
pjs=window.open(loc,'Newpage','width=400,height=300,left=0,top=90,resizable=yes,scrollbars=yes,menubar=no'); }

/*ページの上へリンクの位置*/
function iniFunc(){}

  /**
   * フォーム送信関数(2重送信対応)
   * @param form			フォーム要素
   * @param anchor			リンク要素(オプション)
   * @param disableClass	操作不可CSS(オプション)
   */
  function submitForm(form, anchor, disableClass) {
	  if (!this._anchors) this._anchors = [];
	  for (var i=0, l=this._anchors.length; i<l; ++i) {
		  if (anchor === this._anchors[i]) return false;
	  }
	  this._anchors.push(anchor);
	  form.submit();
	  if (anchor && disableClass) {
		  var classes = anchor.className.split(' ');
		  classes.push(disableClass);
		  anchor.className = classes.join(' ');
	  }
	  return false;
  }

var al1efie11 = "<input type='hidden' name='al1efie11' value=''>";
function submitForIE11(form) {
	form.append(al1efie11).appendTo(document.body).submit();
}