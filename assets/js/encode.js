var htmlEncode = function(strings) {
	return strings.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/[ \t]/g, "&nbsp;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#39;").replace(/\n/g, "<br>");
};
var htmlDecode = function(strings) {
	return strings.replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&nbsp;/g, " ").replace(/&gt;/g, ">").replace(/&copy;/g, "ｩ");
};