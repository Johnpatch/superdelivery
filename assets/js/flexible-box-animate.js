/*
 * flexboxとの違い
 * ・ 移動がアニメーションになっています。
 * ・ 段組はできません。
 * ・ ボタンのdisableの制御がclassの設定だけでなくID指定で出し分け出来ます
 *   forward-disable-id
 *   back-disable-id
 * ・ disableの時、アニメーションで行き止まり感をだします。
 * 使用法の違い
 * ・ FrexBoxにしたい要素に”flex-box-anim”を設定してください。（FlexBoxは”flex-box”）
 * ・ 親要素とパーツの間に”flex-box-parts-wrap”を挟んでください。
*/

//パブリック用オブジェクト
var flexboxAnimUtil = {};

//名前空間を限定
( function(){

//真ん中のサイズ制御
var parentDiv = "#co-liquid-middle";
var centerFlexDiv = "#co-liquid-middle-main";
var flexClass = "flex-box-anim";

//固定のDIVとフレキシブルなDIV
var flexDivs = ["#co-liquid-middle-main","#co-liquid-top","#co-liquid-bottom"];
var fixDivs = ["#co-liquid-middle-left","#co-liquid-middle-right"];

///////////////////////////////////

///////////////////////////////////////////////////////////////////
//FbaPropertiesクラス
///////////////////////////////////////////////////////////////////

var FbaProperties = (function (){
	function FbaProperties(target){
		var flexBoxDiv = $(target);
		this.rows = 1;
		this.rowSpace = 1;
		this.sideSpace = -1;
		this.fixSpace = 0;
		this.minSpace = 1;
		this.fillDummy = true;
		this.spaceFillType = "margin";
		this.baseSize =-1;
		this.maxCols =-1;
		this.endAnim = true;

		if(flexBoxDiv.attr("fix-space")){
			this.fixSpace = flexBoxDiv.attr("fix-space") -0;
		}
		if(flexBoxDiv.attr("side-space")){
			this.sideSpace = flexBoxDiv.attr("side-space") -0;
		}
		if(flexBoxDiv.attr("rows")){
			this.rows = flexBoxDiv.attr("rows") -0;
		}
		if(flexBoxDiv.attr("row-space")){
			this.rowSpace = flexBoxDiv.attr("row-space") -0;
		}
		if(flexBoxDiv.attr("min-space")){
			this.minSpace = flexBoxDiv.attr("min-space") -0;
		}
		if(flexBoxDiv.attr("fill-dummy")){
			if( flexBoxDiv.attr("fill-dummy") == "false" ) {
				this.fillDummy = false
			};
		}
		if(flexBoxDiv.attr("max-cols")){
			this.maxCols = flexBoxDiv.attr("max-cols") -0;
		}
		if(flexBoxDiv.attr("base-width")){
			this.baseSize=flexBoxDiv.attr("base-width")-0;
		}
		if(flexBoxDiv.attr("fill-space")){
			if( flexBoxDiv.attr("fill-space") == "width" ) {
				this.spaceFillType = "width";
			};
		}
		if(flexBoxDiv.attr("forward-disable-class")){
			this.fwdDisableClass = flexBoxDiv.attr("forward-disable-class");
			this.selector_fwdDisableClass = "."+this.fwdDisableClass;
		}
		if(flexBoxDiv.attr("back-disable-class")){
			this.bkDisableClass = flexBoxDiv.attr("back-disable-class");
			this.selector_bkDisableClass = "."+this.bkDisableClass;
		}
		if(flexBoxDiv.attr("forward-id")){
			this.fwdBtId = flexBoxDiv.attr("forward-id");
		}
		if(flexBoxDiv.attr("back-id")){
			this.bkBtId = flexBoxDiv.attr("back-id");
		}
		if(flexBoxDiv.attr("ignore-late")){
			this.ignoreLate = flexBoxDiv.attr("ignore-late");
		}

		if(flexBoxDiv.attr("forward-disable-id")){
			this.fwdDisableButton = $("#"+flexBoxDiv.attr("forward-disable-id"));
		}
		if(flexBoxDiv.attr("back-disable-id")){
			this.bkDisableButton = $("#"+flexBoxDiv.attr("back-disable-id"));
		}
		if(flexBoxDiv.attr("no-end-anim")){
			this.endAnim = false;
		}

	}
	return FbaProperties;
})();

/////////////////////////////////////////////////////////////////
//FlexBoxクラス
///////////////////////////////////////////////////////////////////

var flexBoxIdSeq =0;
var FlexBoxAnim = (function (){
	function FlexBoxAnim(target,parentid){
		// 表示しないパーツを指定
		this.forceHiddenClass = "nodisp";
		this.selector_forceHiddenClass = "."+this.forceHiddenClass;
		this.flexBoxDiv = $(target);
		this.flexPartsWrap = this.flexBoxDiv.children(".flex-box-parts-wrap");
		this.parts =this.getParts();
		this.fbaProperty = new FbaProperties(this.flexBoxDiv);
		this.parentDivId=parentid;
		this.currentPage=0;
		this.pageWidth=0;
		this.pageNum=0;
		this.isMoving = false;
		if(this.flexBoxDiv.attr("id")){
			this.flexBoxId = "#"+this.flexBoxDiv.attr("id");
		}
		else{
			this.flexBoxId = "FB_"+flexBoxIdSeq++;
		}

		fbaProperty=this.fbaProperty;
		//ボタンを設定
		// 右へ
		var self = this;
		if(fbaProperty.fwdBtId){
			this.fwbt = $("#"+fbaProperty.fwdBtId);
			this.fwbt.click(function(){
				self.doSlideRight();
			});
		}
		// 左へ
		if(fbaProperty.bkBtId){
			this.bkbt = $("#"+fbaProperty.bkBtId);
			this.bkbt.click(function(){
				self.doSlideLeft();
			});
		}
		if(fbaProperty.fwdDisableButton){
			fbaProperty.bkDisableButton.click(function(){self.doSlideLeftEnd();})
		}
		if(fbaProperty.bkDisableButton){
			fbaProperty.fwdDisableButton.click(function(){self.doSlideRightEnd();})
		}
		var sideSpace = fbaProperty.sideSpace;
		if(sideSpace!=-1){
			var sideSpaceLeft = sideSpace;
			if(navigator.userAgent.indexOf("MSIE 6.0")!=-1) {
				sideSpaceLeft = Math.floor(sideSpaceLeft/2);
			}
			// 両サイドのスペースが設定されている場合、設定。
			this.flexBoxDiv.css({
				"margin-left":sideSpaceLeft,
				"margin-right":sideSpace,
				"zoom":1
			});
		}
	}
	return FlexBoxAnim;
})();

///////////////////////////////////
//ボタン制御
///////////////////////////////////
/*
 * 右にパーツが有るか
 */
FlexBoxAnim.prototype.isForwardPartsExists	= function (){
	return this.currentPage < (this.pageNum -1)
}

/*
 * 左にパーツが有るか
 */
FlexBoxAnim.prototype.isPreviousPartsExists =function (){
	return 0 < this.currentPage
}

/*
 * 右に移動する
 */

FlexBoxAnim.prototype.doSlideRight = function (){
	if(this.isForwardPartsExists()==false){
		this.doSlideRightEnd();
		return;
	}
	if( this.isMoving ){
		return;
	}
	this.isMoving=true;
	var self=this;
  this.flexPartsWrap.animate(
  	{marginLeft: "-="+this.pageWidth+"px"},
  	{
  		duration:500,
  		complete: function(){
  			self.isMoving = false;
  		}
  	});
  this.currentPage++;
  this.controlButton();
}

/*
 * 左に移動する
 */
FlexBoxAnim.prototype.doSlideLeft = function(){
	if(this.isPreviousPartsExists()==false){
		this.doSlideLeftEnd();
		return;
	}
	if( this.isMoving ){
		return;
	}
	this.isMoving=true;
	var self=this;
  this.flexPartsWrap.animate(
  	{marginLeft: "+="+this.pageWidth+"px"},
  	{
  		duration:500,
  		complete: function(){
	  		self.isMoving = false;
  		}
  	});
  this.currentPage--;
  this.controlButton();
}

/*
 * 右に移動できない
 */
FlexBoxAnim.prototype.doSlideRightEnd = function(){
	if( this.fbaProperty.endAnim == false){
		return;
	}
  var move = 30;
  var target = this.flexPartsWrap;
  target.animate({marginLeft: "-="+move},300 , function(){
    target.animate({marginLeft: "+="+move},{duration:100 ,easing:"swing"} );
  });
};
/*
 * 左に移動できない
 */
FlexBoxAnim.prototype.doSlideLeftEnd = function(){
	if( this.fbaProperty.endAnim == false){
		return;
	}
  var move = 30;
  var target = this.flexPartsWrap;
  target.animate({marginLeft: "+="+move},300 , function(){
    target.animate({marginLeft: "-="+move},{duration:100 ,easing:"swing"} );
  });
};


FlexBoxAnim.prototype.getParts = function(){
	return this.flexPartsWrap.children("div")
}

/*
 * ボタンの制御
 */
FlexBoxAnim.prototype.controlButton = function(forceEnableBk,forceEnableFw){
	var forceBk = forceEnableBk || false;
	var forceFw = forceEnableFw || false;
	if(this.fbaProperty.fwdDisableClass){
		this.controlButtonByClass(forceBk,forceFw);
	}
	if(this.fbaProperty.fwdDisableButton){
		this.controlButtonById(forceBk,forceFw);
	}
}
/*
 * ボタンのクラスの制御
 */
FlexBoxAnim.prototype.controlButtonByClass = function(forceBk,forceFw){
	var disableFw=this.fbaProperty.fwdDisableClass;
	var disableBk=this.fbaProperty.bkDisableClass;

	var fwbt = this.fwbt;
	if(fwbt && disableFw ){
		if( forceFw || this.isForwardPartsExists() ){
			fwbt.removeClass(disableFw);
		}
		else{
			fwbt.addClass(disableFw);
		}
	}
	var bkbt = this.bkbt;
	if(bkbt && disableBk){
		if( forceBk || this.isPreviousPartsExists() ){
			bkbt.removeClass(disableBk);
		}
		else{
			bkbt.addClass(disableBk);
		}
	}
}
/*
 * ボタンの出し分け制御
 */
FlexBoxAnim.prototype.controlButtonById = function(forceBk,forceFw){
	var disableFw=this.fbaProperty.fwdDisableButton;
	var disableBk=this.fbaProperty.bkDisableButton;

	var fwbt = this.fwbt;
	if(fwbt && disableFw ){
		if( forceFw || this.isForwardPartsExists() ){
			fwbt.show();
			disableFw.hide();
		}
		else{
			fwbt.hide();
			disableFw.show();
		}
	}
	var bkbt = this.bkbt;
	if(bkbt && disableBk){
		if( forceBk || this.isPreviousPartsExists() ){
			bkbt.show();
			disableBk.hide();
		}
		else{
			bkbt.hide();
			disableBk.show();
		}
	}
}

FlexBoxAnim.prototype.disableButtons =function(){
	var disableFw=this.fbaProperty.fwdDisableClass;
	var disableBk=this.fbaProperty.bkDisableClass;


	var fwbt = this.fwbt;
	if(fwbt && disableFw ){
		fwbt.addClass(disableFw);
	}
	var bkbt = this.bkbt;
	if(bkbt && disableBk){
		bkbt.addClass(disableBk);
	}
}

/*
 * Flexの主処理。
 */
FlexBoxAnim.prototype.doFlex=function (_parentWidth){
	var flexSaba = 1;
	var flexBoxDiv = this.flexBoxDiv;
	var flexPartsWrap = this.flexPartsWrap;
	var forceHiddenClass = this.forceHiddenClass;
	var parentWidth =  $(this.parentDivId).innerWidth();

	if(navigator.userAgent.indexOf("MSIE 6.0")!=-1) {
		parentWidth =  _parentWidth;
	}

	parentWidth = 	parentWidth -flexSaba;

 	var sideSpaces = this.fbaProperty.sideSpace*2;

	flexBoxDiv.css("width",parentWidth-sideSpaces-this.fbaProperty.fixSpace);
	flexPartsWrap.css("margin-left","0");

	var parts =this.getParts();

	var partsCount =parts.length;
	var partsSize;
	if(this.fbaProperty.baseSize != -1){
		partsSize = this.fbaProperty.baseSize;
	}
	else{
		partsSize =parts.first().outerWidth(false);
	}
	var boxSize =this.flexBoxDiv.width();
	//console.log(" boxSize:"+boxSize+" parentWidth:"+parentWidth+" _parentWidth:"+parentWidth + " PID:" + this.parentDivId);

	//計算上の表示数を取得
	var dispNumObj= this.getDispNum();
	var dispNum= dispNumObj.dispNum;
	var space= dispNumObj.space - 0;

	var minSpace = this.fbaProperty.minSpace;

	if(this.fbaProperty.spaceFillType == "margin"){
		parts.css({
			"margin-left":0,
			"margin-right":space,
			"height":""
		});
	}
	else{
		var addSpace = Math.floor(minSpace/(dispNum-1));
		parts.css({
			"height":"",
			"width":(partsSize + space - minSpace),
			"margin-left":0,
			"margin-right":(minSpace+addSpace)
		});

	}

	//ページごとの設定
	this.pageNum = Math.ceil(partsCount / dispNum);
	this.pageWidth = (partsSize+space)*dispNum;
	flexPartsWrap.width(this.pageNum*this.pageWidth);
	this.currentPage = 0
	this.controlButton();
	var fbobj=this;
	if(navigator.userAgent.indexOf("MSIE 6.0")!=-1) {
		return;
	}
	//最大化から復元した場合に親のサイズ変更が間に合っていない場合がある。
	if(this.fbaProperty.ignoreLate != "true" && parentWidth != $(this.parentDivId).innerWidth()-flexSaba){
		fbobj.doFlex(_parentWidth);
	}
}

/*
 * 表示数を計算する
 */
FlexBoxAnim.prototype.getDispNum =function (){
	var minSpace = this.fbaProperty.minSpace;
	var maxCols = this.fbaProperty.maxCols;
	var boxSize =this.flexBoxDiv.width();
	var partsSize = 0;
	if(this.fbaProperty.baseSize != -1){
		partsSize = this.fbaProperty.baseSize;
	}
	else{
		var parts =this.getParts();
		partsSize =parts.first().outerWidth(false);
	}
	// 外枠がパーツのサイズを下回ったら1個。はみ出た部分は隠れる。
	if(boxSize < partsSize){
		dispNum = 1;
		return {"dispNum":dispNum,"space":minSpace};
	}

	var spaceRightAdjust = 0;
	//右も固定
	if(this.fbaProperty.spaceFillType == "margin"){
		spaceRightAdjust=1;
	}

	//表示数の計算
	var dispNum =Math.floor(boxSize / partsSize);

	// 最大列数を超えない
	if(dispNum > maxCols){
		dispNum = maxCols;
	}

	if(this.fbaProperty.fillDummy == false){
		var partsLen =this.getParts().size();
		dispNum = Math.min(partsLen,dispNum);
	}

	// 間隔が最小間隔より狭かったら1個減らす
	var spaceNum = dispNum-spaceRightAdjust; //　右側にマージン設定。
	var spaceTotal = boxSize - ( dispNum * partsSize );
	var space = Math.floor(spaceTotal/spaceNum);
	if(space<minSpace){
		dispNum=dispNum-1;
		spaceNum = dispNum-spaceRightAdjust;
		spaceTotal = boxSize - ( dispNum * (partsSize ) );
		space = Math.floor(spaceTotal/spaceNum);
	}
	return {"dispNum":dispNum,"space":space};
}

///////////////////////////////////////////////////////////////////
// FlexBoxクラスここまで
///////////////////////////////////////////////////////////////////

///////////////////////////////////
//サイズ制御
///////////////////////////////////
/*
* mainのサイズ制御
*/
function getBrowserWidth ( ) {
	return $("body").width();
}
/*
* Flex-boxパーツのリサイズ
*/
var flexBoxArray = new Array();
var otherflexBoxArray = new Array();
function resizeFlexDivs(){
	var fixWidth =0;
	//固定部分の幅を計算
	for(var i=0;i<fixDivs.length;i++){
		fixWidth = fixWidth + Math.floor($(fixDivs[i]).outerWidth(true));
	}
	var browserWidth = Math.floor(getBrowserWidth());
	var middleContentsWidth = browserWidth - fixWidth;
	var middleContentsMinWidthStr =$(centerFlexDiv).css("min-width");
	if(middleContentsMinWidthStr && /[0-9]*px$/.test(middleContentsMinWidthStr)){
		var middleContentsMinWidth=middleContentsMinWidthStr.match(/([0-9]*)px$/)[1];
		middleContentsWidth = Math.max(middleContentsWidth,middleContentsMinWidth);
	}

	middleContentsWidth = middleContentsWidth -2;

	$(centerFlexDiv).width(middleContentsWidth);

	for(var i=0;i<flexBoxArray.length;i++){
		var fb = flexBoxArray[i];
		if(fb.parentDivId == centerFlexDiv){
			fb.doFlex(middleContentsWidth);
		}
		else{
			var toSize = $(fb.parentDivId).width();
			var minWidthStr =$(fb.parentDivId).css("min-width");
			if(minWidthStr && /[0-9]*px$/.test(minWidthStr)){
				var minWidth=minWidthStr.match(/([0-9]*)px$/)[1];
				toSize = Math.max(browserWidth,minWidth);
			}

			fb.doFlex(toSize);
		}
	}
	for(var i=0;i<otherflexBoxArray.length;i++){
		var ofb = otherflexBoxArray[i];
		ofb(browserWidth,middleContentsWidth);
	}
}

//初期設定
var lastSize = 0;
var resizing = false;

jQuery(document).ready(function($){
	//初期設定

	for(var f=0;f<flexDivs.length;f++){
		var pdiv = flexDivs[f];
		$(pdiv).find("."+flexClass).each(function(){
			getFlexBox(this,pdiv);
		});
	}

	resizeFlexDivs();

	jQuery(window).resize(function(){
		resizeFlexDivs();
	});

});


function getFlexBox(target,pvid){
	var fb;
	if(flexBoxArray==undefined){
		flexBoxArray = new Array();
		fb = new FlexBoxAnim(target,pvid);
		flexBoxArray.push(fb);
		return fb;
	}
	for(var i=0;i<flexBoxArray.length;i++){
		if(flexBoxArray[i].flexBoxId == target){
			fb = flexBoxArray[i];
			return fb;
		}
	}
	fb = new FlexBoxAnim(target,pvid);
	flexBoxArray.push(fb);
	return fb;
}

function showFlexDivs(){
	for(var i=0;i<flexBoxArray.length;i++){
		var fb = flexBoxArray[i];
	}
}

flexboxAnimUtil.addFlex = function(flexFunction){
	if(jQuery.isFunction(flexFunction)){
		otherflexBoxArray.push(flexFunction);
	}

}


/*
* ボタンのクラスを強制的に設定
* forceEnableBkをtrueにすると戻る画像の有り無しにかかわらず、ボタンをONにします
* forceEnableFwをtrueにすると進む画像の有り無しにかかわらず、ボタンをONにします
*/
flexboxAnimUtil.refreshButtons = function(target,forceEnableBk,forceEnableFw){
	var fb = getFlexBox(target);
	if(fb){
		fb.controlButton(forceEnableBk,forceEnableFw);
	}
}

flexboxAnimUtil.disableButtons = function(target){
	var fb = getFlexBox(target);
	if(fb){
		fb.disableButtons();
	}
}


flexboxAnimUtil.isForwardExists = function(target){
	var fb = getFlexBox(target);
	if(fb){
		fb.isForwardExists(forceEnableBk,forceEnableFw);
	}
}
flexboxAnimUtil.isPreviousExists = function(target){
	var fb = getFlexBox(target);
	fb.isPreviousPartsExists(forceEnableBk,forceEnableFw);
}

/*
* 動的に追加した場合に初期化する
*/
flexboxAnimUtil.init = function(target,pdivd){
	var fb = getFlexBox(target,pdivd);
};

/*
* 強制的に再表示する。
* forceEnableBkをtrueにすると戻る画像の有り無しにかかわらず、ボタンをONにします
* forceEnableFwをtrueにすると進む画像の有り無しにかかわらず、ボタンをONにします
*/
flexboxAnimUtil.initFlex = function(target,forceEnableBk,forceEnableFw){
	var fb = getFlexBox(target);
	fb.doFlex();
}

})(); // flexboxAnimUtilの名前空間の終わり

