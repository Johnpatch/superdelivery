/*
 * 機能は２つ。
 *
 * １．#co-liquid-middleの#co-liquid-middle-mainのサイズを調整する
 * ２．#co-liquid-top,#co-liquid-middleの#co-liquid-middle-main, #co-liquid-bottomの中のflex-boxを制御する
 *
 * 注）ready後にflex-boxを追加した場合は、
 *   flexboxUtil.init([id])
 *   で初期化してください
 *   そのあと、
 *   flexboxUtil.initFlex([id])
 *   を実行すると、初期表示が正しくできます。
 *
 * ---------------------------------------------
 * flex-boxの制御
 * ---------------------------------------------
 * あ）表示幅に応じて、表示個数やその間隔を調整する。右端と左端のパーツは位置を固定します。
 * い）段組をする
 * う）左右送りボタンを制御する
 *
 * ---------------------------------------------
 *       パラメータ
 * ---------------------------------------------
 * max-cols="8" 	・・・　１行の表示個数
 * side-space="7" 	・・・flex-boxの両サイドのマージン。タグごと設定しなかった場合、もしくは、マイナス-1を指定した場合はflex-boxからはマージンを設定しない。（CSSの設定が活きる）
 * fix-space="7" 	・・・flex-boxのサイズを計算する際に固定値として引くサイズ。Bodyの幅から両サイドのボタンを引くなど
 * min-space="5" 	・・・flex-box内のDIVの最小間隔
 * base-width=""	・・・flex-box内のDIVの基準横幅。このサイズを元にいくつ入るか計算する。
 * fill-dummy="true" ・・・　１行の表示個数に満たなかった場合、スペースで調整するか、空白で埋めるか(デフォルトtrue)
 * fill-space="margin" ・・・　flex-box内のDIVの間をどう埋めるか(デフォルトmargin)。widthを設定した場合はDIVの横幅が変わり、min-spaceがマージンに設定される。
 *
 * --　段組用　　---------------------------------
 * rows="2"　		・・・段数
 * row-space="30"　	・・・段の間のスペース
 *
 * --　左右送りボタン用　　----------------------------
 * forward-id="bottomsId-fw" 				・・・右ボタンのID
 * forward-disable-class="r-btn-disabled"　 	・・・右ボタンを使用不可にする場合のクラス名
 *
 * back-id="bottomsId-bk" 					・・・左ボタンのID
 * back-disable-class="l-btn-disabled"		・・・左ボタンを使用不可にする場合のクラス名
 *
 * ※動的にDIVを追加削除した場合には、
 * 	flexboxUtil.init
 * 	flexboxUtil.refreshButtons
 * 	を呼んでください
 *
 */

//パブリック用オブジェクト
var flexboxUtil = {};

//名前空間を限定
( function(){

//真ん中のサイズ制御
var parentDiv = "#co-liquid-middle";
var centerFlexDiv = "#co-liquid-middle-main";
var flexClass = "flex-box";

//固定のDIVとフレキシブルなDIV
var flexDivs = ["#co-liquid-middle-main","#co-liquid-top","#co-liquid-bottom"];
var fixDivs = ["#co-liquid-middle-left","#co-liquid-middle-right"];

///////////////////////////////////

///////////////////////////////////////////////////////////////////
//FbPropertiesクラス
///////////////////////////////////////////////////////////////////

var FbProperties = (function (){
	function FbProperties(target){
		var flexBoxDiv = $(target);
		this.rows = 1;
		this.rowSpace = 1;
		this.sideSpace = -1;
		this.fixSpace = 0;
		this.minSpace = 1;
		this.fillDummy = true;
		this.spaceFillType = "margin";
		this.baseSize =-1;
		this.maxCols =-1;
		this.isSwipe = false; // スワイプ機能のON、OFF
		this.swipeBorderWidth = 670; // 画面幅がこれ以下になったらスワイプ用の処理を実行

		if(flexBoxDiv.attr("fix-space")){
			this.fixSpace = flexBoxDiv.attr("fix-space") -0;
		}
		if(flexBoxDiv.attr("side-space")){
			this.sideSpace = flexBoxDiv.attr("side-space") -0;
		}
		if(flexBoxDiv.attr("rows")){
			this.rows = flexBoxDiv.attr("rows") -0;
		}
		if(flexBoxDiv.attr("row-space")){
			this.rowSpace = flexBoxDiv.attr("row-space") -0;
		}
		if(flexBoxDiv.attr("min-space")){
			this.minSpace = flexBoxDiv.attr("min-space") -0;
		}
		if(flexBoxDiv.attr("fill-dummy")){
			if( flexBoxDiv.attr("fill-dummy") == "false" ) {
				this.fillDummy = false
			};
		}
		if(flexBoxDiv.attr("max-cols")){
			this.maxCols = flexBoxDiv.attr("max-cols") -0;
		}
		if(flexBoxDiv.attr("base-width")){
			this.baseSize=flexBoxDiv.attr("base-width")-0;
		}
		if(flexBoxDiv.attr("fill-space")){
			if( flexBoxDiv.attr("fill-space") == "width" ) {
				this.spaceFillType = "width";
			};
		}
		if(flexBoxDiv.attr("forward-disable-class")){
			this.fwdDisableClass = flexBoxDiv.attr("forward-disable-class");
			this.selector_fwdDisableClass = "."+this.fwdDisableClass;
		}
		if(flexBoxDiv.attr("back-disable-class")){
			this.bkDisableClass = flexBoxDiv.attr("back-disable-class");
			this.selector_bkDisableClass = "."+this.bkDisableClass;
		}
		if(flexBoxDiv.attr("forward-id")){
			this.fwdBtId = flexBoxDiv.attr("forward-id");
		}
		if(flexBoxDiv.attr("back-id")){
			this.bkBtId = flexBoxDiv.attr("back-id");
		}
		if(flexBoxDiv.attr("ignore-late")){
			this.ignoreLate = flexBoxDiv.attr("ignore-late");
		}
		if(flexBoxDiv.attr("is-swipe") && flexBoxDiv.attr("is-swipe") === 'true'){
			this.isSwipe = true;
		}
		if(flexBoxDiv.attr("swipe-border-width")){
			var _swipeBorderWidth = flexBoxDiv.attr("swipe-border-width") - 0;
			this.swipeBorderWidth = isNaN(_swipeBorderWidth) ? this.swipeBorderWidth : _swipeBorderWidth;
		}
	}
	return FbProperties;
})();

/////////////////////////////////////////////////////////////////
//FlexBoxクラス
///////////////////////////////////////////////////////////////////

var flexBoxIdSeq =0;
var FlexBox = (function (){
	function FlexBox(target,parentid){
		// 表示しないパーツを指定
		this.forceHiddenClass = "nodisp";
		this.selector_forceHiddenClass = "."+this.forceHiddenClass;
		this.flexBoxDiv = $(target);
		this.parts =this.flexBoxDiv.children('div');
		this.fbProperty = new FbProperties(this.flexBoxDiv);
		this.parentDivId=parentid;
		if(this.flexBoxDiv.attr("id")){
			this.flexBoxId = "#"+this.flexBoxDiv.attr("id");
		}
		else{
			this.flexBoxId = "FB_"+flexBoxIdSeq++;
		}

		fbProperty=this.fbProperty;
		//ボタンを設定
		// 右へ
		var self = this;
		if(fbProperty.fwdBtId){
			this.fwbt = $("#"+fbProperty.fwdBtId);
			this.fwbt.click(function(){
				self.doSlideRight();
			});
		}
		// 左へ
		if(fbProperty.bkBtId){
			this.bkbt = $("#"+fbProperty.bkBtId);
			this.bkbt.click(function(){
				self.doSlideLeft();
			});
		}


		var sideSpace = fbProperty.sideSpace;
		if(sideSpace!=-1){
			var sideSpaceLeft = sideSpace;
			if(navigator.userAgent.indexOf("MSIE 6.0")!=-1) {
				sideSpaceLeft = Math.floor(sideSpaceLeft/2);
			}
			// 両サイドのスペースが設定されている場合、設定。
			this.flexBoxDiv.css({
				"margin-left":sideSpaceLeft,
				"margin-right":sideSpace,
				"zoom":1
			});
		}
	}
	return FlexBox;
})();

///////////////////////////////////
//ボタン制御
///////////////////////////////////
/*
 * 右にパーツが有るか
 */
FlexBox.prototype.isForwardPartsExists	= function (){
	var parts =this.flexBoxDiv.children('div');
	var divParts =parts.filter(":hidden");
	var hiddenParts =divParts.filter(this.selector_forceHiddenClass);
	return (divParts.length-hiddenParts.length > 0);
}

/*
 * 左にパーツが有るか
 */
FlexBox.prototype.isPreviousPartsExists =function (){
	var prevHiddenParts =this.flexBoxDiv.children(this.selector_forceHiddenClass);
	return  (prevHiddenParts.length > 0);
}

/*
 * 右に移動する
 */
FlexBox.prototype.doSlideRight = function (){
	var cls=this.fbProperty.fwdDisableClass;
	if(cls != "" && this.fwbt.hasClass(cls)){
		return;
	}

	var parts =this.flexBoxDiv.children('div');
	var hiddenParts =parts.filter(this.selector_forceHiddenClass);
	var dispNum= this.getDispNum().dispNum;

	parts.removeClass(this.forceHiddenClass);
	parts.slice(0,hiddenParts.size()+dispNum).addClass(this.forceHiddenClass);
	this.doFlex();
}

/*
 * 左に移動する
 */
FlexBox.prototype.doSlideLeft = function(){
	var cls=this.fbProperty.bkDisableClass;
	if(cls != "" && this.bkbt.hasClass(cls)){
		return;
	}
	var parts =this.flexBoxDiv.children('div');
	var hiddenParts =parts.filter(this.selector_forceHiddenClass);
	var dispNum= this.getDispNum().dispNum;
	parts.removeClass(this.forceHiddenClass);
	if(hiddenParts.size()>dispNum){
		parts.slice(0,hiddenParts.size()-dispNum).addClass(this.forceHiddenClass);
	}
	this.doFlex();
}

/*
 * ボタンのクラスの制御
 */
FlexBox.prototype.controlButton = function(forceEnableBk,forceEnableFw){
	var forceBk = forceEnableBk || false;
	var forceFw = forceEnableFw || false;
	var disableFw=this.fbProperty.fwdDisableClass;
	var disableBk=this.fbProperty.bkDisableClass;

	var fwbt = this.fwbt;
	if(fwbt && disableFw ){
		if( forceFw || this.isForwardPartsExists() ){
			fwbt.removeClass(disableFw);
		}
		else{
			fwbt.addClass(disableFw);
		}
	}
	var bkbt = this.bkbt;
	if(bkbt && disableBk){
		if( forceBk || this.isPreviousPartsExists() ){
			bkbt.removeClass(disableBk);
		}
		else{
			bkbt.addClass(disableBk);
		}
	}

}
FlexBox.prototype.disableButtons =function(){
	var disableFw=this.fbProperty.fwdDisableClass;
	var disableBk=this.fbProperty.bkDisableClass;


	var fwbt = this.fwbt;
	if(fwbt && disableFw ){
		fwbt.addClass(disableFw);
	}
	var bkbt = this.bkbt;
	if(bkbt && disableBk){
		bkbt.addClass(disableBk);
	}
}


/*
 * Flexの主処理。
 */
FlexBox.prototype.doFlex=function (_parentWidth){
	var flexSaba = 1;
	var flexBoxDiv = this.flexBoxDiv;
	var forceHiddenClass = this.forceHiddenClass;
	var parentWidth =  $(this.parentDivId).innerWidth();

	// cssのmediaはスクロールバーの幅を含めた横幅。それに合わせてwindow.innerWidthを使う。
	var browserWidth = window.innerWidth;
	if(this.fbProperty.isSwipe && this.fbProperty.swipeBorderWidth >= browserWidth){
		var browserWidthSP = $(window).width();
		// スワイプモードのときはjsでつけたstyle属性を消す。!importantの削減。
		flexBoxDiv.attr('style','');
		flexBoxDiv.children('div').attr('style','');
		return;
	}

	if(navigator.userAgent.indexOf("MSIE 6.0")!=-1) {
		parentWidth =  _parentWidth;
	}

	parentWidth = 	parentWidth -flexSaba;

 	var sideSpaces = this.fbProperty.sideSpace*2;

	flexBoxDiv.css("width",parentWidth-sideSpaces-this.fbProperty.fixSpace);

	var parts =flexBoxDiv.children('div:not(.'+forceHiddenClass+')');

	flexBoxDiv.children('div.'+forceHiddenClass).hide();

	var partsCount =parts.length;
	var partsSize;
	if(this.fbProperty.baseSize != -1){
		partsSize = this.fbProperty.baseSize;
	}
	else{
		partsSize =parts.first().outerWidth(false);
	}
	var boxSize =this.flexBoxDiv.width();
	//console.log(" boxSize:"+boxSize+" parentWidth:"+parentWidth+" _parentWidth:"+parentWidth + " PID:" + this.parentDivId);

	//計算上の表示数を取得
	var dispNumObj= this.getDispNum();
	var dispNum= dispNumObj.dispNum;
	var space= dispNumObj.space - 0;

	var minSpace = this.fbProperty.minSpace;

	//一旦非表示
	parts.hide();

	if(this.fbProperty.spaceFillType == "margin"){
		parts.css({
			"margin-left":0,
			"margin-right":space,
			"height":""
		});
	}
	else{
		var addSpace = Math.floor(minSpace/(dispNum-1));
		parts.css({
			"height":"",
			"width":(partsSize + space - minSpace),
			"margin-left":0,
			"margin-right":(minSpace+addSpace)
		});

	}

	//段組
	var endPos = 0;
	var rows = this.fbProperty.rows;
	var rowSpace = this.fbProperty.rowSpace;
	for(var i=0;i<rows;i++){
		var startPos=i*dispNum;
		var endPos=(i+1)*dispNum;
		rowParts = parts.slice(startPos,endPos);
		rowParts.first().css({
			"margin-left":0
		});
		rowParts.last().css({
			"margin-right":0
		});

		//行間（2段目以降）
		if(i==0){
			rowParts.css("margin-top",0);
		}
		else{
			rowParts.css("margin-top",rowSpace);
		}
		rowParts.show();
		//高さ設定（最大に合わせる）
		var maxHeight = 0;
		for(j=0;j<rowParts.length;j++){
			maxHeight = Math.max(rowParts.eq(j).height(),maxHeight);
		}
		rowParts.height(maxHeight);
	}
	this.controlButton();
	var fbobj=this;
	if(navigator.userAgent.indexOf("MSIE 6.0")!=-1) {
		return;
	}
	//最大化から復元した場合に親のサイズ変更が間に合っていない場合がある。
	if(this.fbProperty.ignoreLate != "true" && parentWidth != $(this.parentDivId).innerWidth()-flexSaba){
		fbobj.doFlex(_parentWidth);
	}
}

/*
 * 表示数を計算する
 */
FlexBox.prototype.getDispNum =function (){
	var minSpace = this.fbProperty.minSpace;
	var maxCols = this.fbProperty.maxCols;
	var boxSize =this.flexBoxDiv.width();
	var partsSize = 0;
	if(this.fbProperty.baseSize != -1){
		partsSize = this.fbProperty.baseSize;
	}
	else{
		var parts =this.flexBoxDiv.children('div');
		partsSize =parts.first().outerWidth(false);
	}
	// 外枠がパーツのサイズを下回ったら1個。はみ出た部分は隠れる。
	if(boxSize < partsSize){
		dispNum = 1;
		return {"dispNum":dispNum,"space":minSpace};
	}

	var spaceRightAdjust = 0;
	//右も固定
	if(this.fbProperty.spaceFillType == "margin"){
		spaceRightAdjust=1;
	}

	//表示数の計算
	var dispNum =Math.floor(boxSize / partsSize);

	// 最大列数を超えない
	if(dispNum > maxCols){
		dispNum = maxCols;
	}

	if(this.fbProperty.fillDummy == false){
		var partsLen =this.flexBoxDiv.children('div').size();
		dispNum = Math.min(partsLen,dispNum);
	}

	// 間隔が最小間隔より狭かったら1個減らす
	var spaceNum = dispNum-spaceRightAdjust; //　右側にマージン設定。
	var spaceTotal = boxSize - ( dispNum * partsSize );
	var space = Math.floor(spaceTotal/spaceNum);
	if(space<minSpace){
		dispNum=dispNum-1;
		spaceNum = dispNum-spaceRightAdjust;
		spaceTotal = boxSize - ( dispNum * (partsSize ) );
		space = Math.floor(spaceTotal/spaceNum);
	}
	return {"dispNum":dispNum,"space":space};
}

///////////////////////////////////////////////////////////////////
// FlexBoxクラスここまで
///////////////////////////////////////////////////////////////////

///////////////////////////////////
//サイズ制御
///////////////////////////////////
/*
* mainのサイズ制御
*/
function getBrowserWidth ( ) {
	return $("body").width();
}
/*
* Flex-boxパーツのリサイズ
*/
var flexBoxArray = new Array();
var otherflexBoxArray = new Array();
function resizeFlexDivs(){
	var fixWidth =0;
	//固定部分の幅を計算
	for(var i=0;i<fixDivs.length;i++){
		fixWidth = fixWidth + Math.floor($(fixDivs[i]).outerWidth(true));
	}
	var browserWidth = Math.floor(getBrowserWidth());
	var middleContentsWidth = browserWidth - fixWidth;
	var middleContentsMinWidthStr =$(centerFlexDiv).css("min-width");
	if(middleContentsMinWidthStr && /[0-9]*px$/.test(middleContentsMinWidthStr)){
		var middleContentsMinWidth=middleContentsMinWidthStr.match(/([0-9]*)px$/)[1];
		middleContentsWidth = Math.max(middleContentsWidth,middleContentsMinWidth);
	}

	middleContentsWidth = middleContentsWidth -2;

	$(centerFlexDiv).width(middleContentsWidth);

	for(var i=0;i<flexBoxArray.length;i++){
		var fb = flexBoxArray[i];
		if(fb.parentDivId == centerFlexDiv){
			fb.doFlex(middleContentsWidth);
		}
		else{
			var toSize = $(fb.parentDivId).width();
			var minWidthStr =$(fb.parentDivId).css("min-width");
			if(minWidthStr && /[0-9]*px$/.test(minWidthStr)){
				var minWidth=minWidthStr.match(/([0-9]*)px$/)[1];
				toSize = Math.max(browserWidth,minWidth);
			}

			fb.doFlex(toSize);
		}
	}
	for(var i=0;i<otherflexBoxArray.length;i++){
		var ofb = otherflexBoxArray[i];
		ofb(browserWidth,middleContentsWidth);
	}
}

//初期設定
var lastSize = 0;
var resizing = false;

jQuery(document).ready(function($){
	//初期設定

	for(var f=0;f<flexDivs.length;f++){
		var pdiv = flexDivs[f];
		$(pdiv).find("."+flexClass).each(function(){
			getFlexBox(this,pdiv);
		});
	}

	resizeFlexDivs();

	jQuery(window).resize(function(){
		resizeFlexDivs();
	});

});


function getFlexBox(target,pvid){
	var fb;
	if(flexBoxArray==undefined){
		flexBoxArray = new Array();
		fb = new FlexBox(target,pvid);
		flexBoxArray.push(fb);
		return fb;
	}
	for(var i=0;i<flexBoxArray.length;i++){
		if(flexBoxArray[i].flexBoxId == target){
			fb = flexBoxArray[i];
			return fb;
		}
	}
	fb = new FlexBox(target,pvid);
	flexBoxArray.push(fb);
	return fb;
}

function showFlexDivs(){
	for(var i=0;i<flexBoxArray.length;i++){
		var fb = flexBoxArray[i];
	}
}

flexboxUtil.addFlex = function(flexFunction){
	if(jQuery.isFunction(flexFunction)){
		otherflexBoxArray.push(flexFunction);
	}

}


/*
* ボタンのクラスを強制的に設定
* forceEnableBkをtrueにすると戻る画像の有り無しにかかわらず、ボタンをONにします
* forceEnableFwをtrueにすると進む画像の有り無しにかかわらず、ボタンをONにします
*/
flexboxUtil.refreshButtons = function(target,forceEnableBk,forceEnableFw){
	var fb = getFlexBox(target);
	if(fb){
		fb.controlButton(forceEnableBk,forceEnableFw);
	}
}

flexboxUtil.disableButtons = function(target){
	var fb = getFlexBox(target);
	if(fb){
		fb.disableButtons();
	}
}


flexboxUtil.isForwardExists = function(target){
	var fb = getFlexBox(target);
	if(fb){
		fb.isForwardExists(forceEnableBk,forceEnableFw);
	}
}
flexboxUtil.isPreviousExists = function(target){
	var fb = getFlexBox(target);
	fb.isPreviousPartsExists(forceEnableBk,forceEnableFw);
}

/*
* 動的に追加した場合に初期化する
*/
flexboxUtil.init = function(target,pdivd){
	var fb = getFlexBox(target,pdivd);
};

/*
* 強制的に再表示する。
* forceEnableBkをtrueにすると戻る画像の有り無しにかかわらず、ボタンをONにします
* forceEnableFwをtrueにすると進む画像の有り無しにかかわらず、ボタンをONにします
*/
flexboxUtil.initFlex = function(target,forceEnableBk,forceEnableFw){
	var fb = getFlexBox(target);
	fb.doFlex();
}

})(); // flexboxUtilの名前空間の終わり

