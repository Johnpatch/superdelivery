/**
 * 指定したサイズのボックスに画像をセンタリングします。
 * 画像のサイズが取得できなかった場合、1秒おきに3回までリトライします。
 * curRetryは通常は省略してください。
 * 
 * 縦位置の指定：
 * padding-top・padding-bottomを設定してセンタリングします。
 * 横位置の指定：
 * padding-left・paddingn-rightを設定してセンタリングします。
 * 
 * @param im imgタグ本体
 * @param height imgタグの外側の縦幅。0を指定した場合は縦のセンタリングをしない。
 * @param width	imgタグの外側の設定したい横幅。0を指定した場合は横のセンタリングをしない。
 * @param curRetry	[省略推奨：初期値０]現在のリトライ回数
 */
function setImageCenter(im, height, width, curRetry) {
	var retry = curRetry || 0;
	var target = $(im);
	var img = new Image();
	img.src = target.attr('src');
	var imgHeight = img.height;
	var imgWidth = img.width;
	//サイズが取れない場合はリトライ
	if (imgHeight == 0 || imgWidth == 0) {
		//１秒後に再処理
		//３秒を超える場合はセンタリングを諦める
		if (retry < 3) {
			setTimeout(function() {
				retry = retry + 1;
				setImageCenter(im, height, width, retry);
			}, 1000)
		}
		return;
	}
	if (height != 0 && height != imgHeight) {
		var pad = Math.floor((height - imgHeight) / 2)+ "px"
		target.css(	{
			"padding-top": pad,
			"padding-bottom": pad
		});
	}
	if (width != 0 && width != imgWidth) {
		var pad = Math.floor((width - imgWidth) / 2) + "px"
		target.css({
			"padding-left":pad ,
			"padding-right": pad
		});
	}
}
