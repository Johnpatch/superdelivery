function activateSubmenu(selectedRow) {
	if( $(selectedRow).is(".cursor") == false){
		return;
	}

	var subMenuA = $(selectedRow).find(".list-text");
	$("#" + "sub-" + subMenuA[0].id).show();
	subMenuA.addClass("active-on");

	var dmyHeight = $(selectedRow).find(".dummy-rightbg").height();
	if(dmyHeight == 0) {
		$(".dummy-rightbg").css("height", $(".dummy-rightbg").parent().outerHeight() - 2);
	}
}

function deactivateSubmenu(selectedRow) {
	var subMenuA = $(selectedRow).find(".list-text");
	$("#" + "sub-" + subMenuA[0].id).hide();
	subMenuA.removeClass("active-on");
}

var exitTimer = "";

function exitGenre() {
	exitTimer = setTimeout(
			function(){
				$(".sub-genre").hide();
				$("#genre-navi-ul").find(".list-text").removeClass("active-on");
				exitTimer="";
			}
			,400);
}
function enterGenre() {
	if(exitTimer != ""){
		clearTimeout(exitTimer);
		exitTimer="";
	}
}

(function($){
$.genreAnimation = function() {
	$(".genre-list-lv2 li").each(function() {
		$(".genre-list-lv2 li").bind('mouseover', function() {
			$(".genre-list-lv2 li").removeClass("genre-lv2-on");
			$(this).addClass("genre-lv2-on");

			/* 繝ｬ繝吶Ν2縺ｫ蠢懊§縺溘Ξ繝吶Ν3繧定｡ｨ遉ｺ */
			var openGenreList = $(".genre-list-lv2 li").index(this);
			$(this).parent().next().find(".genre-list-lv3").each(function() {
				$(this).hide();
				$(".genre-list-lv3").eq(openGenreList).show();
			});
		});
	});
	$(".genre-list-lv1").each(function() {/* 繝ｬ繝吶Ν1繧ｸ繝｣繝ｳ繝ｫ繧檀over縺励◆繧牙ｸｸ縺ｫ譛蛻昴�繝ｬ繝吶Ν2繧ｸ繝｣繝ｳ繝ｫ縺ｮ繝ｬ繝吶Ν3縺悟�縺ｦ縺上ｋ */
		$(this).bind('mouseover', function() {
				$(this).next().find(".genre-list-lv2 li").removeClass("genre-lv2-on");
				$(this).next().find(".genre-list-lv2 li:first-child").addClass("genre-lv2-on");
				$(this).next().find(".genre-list-lv3").hide();
				$(this).next().find(".genre-list-lv3:first-child").show();
			
		});
	});
	/* 繝ｬ繝吶Ν1繧ｸ繝｣繝ｳ繝ｫhover譎ゅ�荳句ｱ､繧ｸ繝｣繝ｳ繝ｫ蜃ｺ迴ｾ繧帝≦繧峨○繧� */
	var genreOnTimer;
	$(".genre-list-head").hover(function() {
		var onGenre = $(this).index();
		genreOnTimer = setTimeout(function() {
							$(".genre-list-head").eq(onGenre).addClass("genre-on");
						}, 200);
	},function() {
		clearTimeout(genreOnTimer);
		$(".genre-list-head").removeClass("genre-on");
	});
}
})(jQuery);
