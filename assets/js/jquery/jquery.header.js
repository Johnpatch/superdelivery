jQuery(document).ready(function($) {
	// 商品ジャンル
	if (0 < $(".all-genre-btn").size()) {
		// スマホの場合はclick、それ以外はhover
		if (isTouchPanel()) {
			$("#all-genre-open").jqueryMenulayer(
				{
					contentsId: "genre-navi",
					handlerClass: "all-genre-btn-on",
					handlerRemoveClass: "all-genre-btn"
				}
			);
		} else {
			$("#all-genre-open").jqueryMenulayer(
				{
					contentsId: "genre-navi",
					handlerClass: "all-genre-btn-on",
					handlerRemoveClass: "all-genre-btn",
					hoverOpen: true,
					hoverHandlerId: "genre-menu",
					closeOnClickBody: false
				}
			);
		}
	}

	//20140811探しやすさタブボタン追加
	/*$(".genre-navi-list #genre-navi-ul a").each(function() {
		var namu = $(this).attr("href").split("/");
		if (namu[6].match(/^[0-9]+$/)) {
			$(this).attr("onclick","analyticsTracker._trackPageview('/analytics/header/genretree/" + "dealer" + "/" + namu[6] + "/');");
		}
	});*/

	var menuFlag = true;

	$("#genre-navi,[id$='-genre-id']").hover(function() {
		var menuFlag = true;
	}, function(){
		menuFlag = false;
	});

	$(window,document).click(function() {
		if (menuFlag == false) {
			if ($('#dealer-genre-id').is('.genre-close-function')) {
				$('#genre-navi').hide(0,function(){
					menuFlag = true;
					$("#dealer-genre-id").removeClass("dealer-genre-btn dealer-genre-btn-on").addClass("dealer-genre-btn-nrl");
					$("#product-genre-id").removeClass("product-genre-btn product-genre-btn-on").addClass("product-genre-btn-nrl");
				});
			}
		}
	});

	$('#genre-navi').click(function() {
		menuFlag = true;
	});

	$("[id$='-genre-id']").click(function() {
		menuFlag = true;
		var genreKind = $(this).attr("id").replace("-genre-id","");
		if(genreKind == 'dealer') {
			var genreNotKind = 'product';
		} else if(genreKind == 'product') {
			var genreNotKind = 'dealer';
		}

		//ボタン切り替え
		if($(this).is("." + genreKind + "-genre-btn" + "," + "." + genreKind + "-genre-btn-nrl")){
			$("#" + genreKind + "-genre-id").removeClass(genreKind + "-genre-btn-nrl "+ genreKind + "-genre-btn").addClass(genreKind + "-genre-btn-on");
			$("#" + genreNotKind + "-genre-id").removeClass(genreNotKind + "-genre-btn-nrl " + genreNotKind + "-genre-btn-on").addClass(genreNotKind + "-genre-btn");
			$('#genre-navi').show();
			$("#genre-navi ul li").hide();
			$("#genre-navi ul li").fadeIn(250);
		} else if ($('#dealer-genre-id').is('.genre-close-function')) {
			$('#genre-navi').hide();
			$("#" + genreKind + "-genre-id").removeClass(genreKind + "-genre-btn " + genreKind + "-genre-btn-on").addClass(genreKind + "-genre-btn-nrl");
			$("#" + genreNotKind + "-genre-id").removeClass(genreNotKind + "-genre-btn " + genreNotKind + "-genre-btn-on").addClass(genreNotKind + "-genre-btn-nrl");
		}

		//パラメータつきorなし
		$(".genre-navi-list #genre-navi-ul a").each(function(){
			var genreUrl = $(this).attr("href").split("?");
			var genreNum = $(this).attr("href").split("/");
			if (genreNum[6].match(/^[0-9]+$/)) {
				if($('#loading-contents').children().hasClass('genretree-analytics')) {
					$(this).attr("onclick","analyticsTracker._trackPageview('/analytics/header/genretree/" + genreKind + "/" + genreNum[6] + "/top/');");
				} else {
					$(this).attr("onclick","analyticsTracker._trackPageview('/analytics/header/genretree/" + genreKind + "/" + genreNum[6] + "/');");
				}
				if(genreKind == 'dealer') {
					$(this).attr("href",genreUrl[0]);
				} else if(genreKind == 'product') {
					$(this).attr("href",genreUrl[0] + "?vi=1");
				}
			}
		});
	});

	/*$("div:not('#genre-navi-content')").click(function(){
		console.log("aaa")
		if($("#company-genre-id").hasClass(".company-genre-btn-on")) {
			console.log("aaa")
			$('#genre-navi').css("display","none");
			$('#company-genre-id').removeClass("company-genre-btn company-genre-btn-on").addClass("company-genre-btn-nrl");
			$('#product-genre-id').removeClass("product-genre-btn product-genre-btn-on").addClass("product-genre-btn-nrl");
		}
	});*/

	// 検索窓
	$("#header_word").bind("focus", function() {
		$("#select-parent").addClass("searchbox-select-wrap-focus");
		$("#select-down").addClass("searchbox-select-down-focus");
		$("#searchbox-field-wrap").addClass("searchbox-field-wrap-focus");
	});

	$("#header_word").bind("blur", function() {
		$("#select-parent").removeClass("searchbox-select-wrap-focus");
		$("#select-down").removeClass("searchbox-select-down-focus");
		$("#searchbox-field-wrap").removeClass("searchbox-field-wrap-focus");
	});

	$("#search_box_genre").bind("focus", function() {
		$("#select-parent").addClass("searchbox-select-wrap-focus");
		$("#select-down").addClass("searchbox-select-down-focus");
		$("#searchbox-field-wrap").addClass("searchbox-field-wrap-focus");
	});

	$("#search_box_genre").bind("blur", function() {
		$("#select-parent").removeClass("searchbox-select-wrap-focus");
		$("#select-down").removeClass("searchbox-select-down-focus");
		$("#searchbox-field-wrap").removeClass("searchbox-field-wrap-focus");
	});

	$("#select-parent").mouseover(function(){
		$("#select-down").addClass("searchbox-select-down-hover");
		$("#searchbox-field-wrap").addClass("searchbox-field-wrap-hover");
	});

	$("#select-parent").mouseleave(function(){
		$("#select-down").removeClass("searchbox-select-down-hover");
		$("#searchbox-field-wrap").removeClass("searchbox-field-wrap-hover");
	});
});