(function($) {
	$(document).ready(function() {

		// $.getJSON("/p/header/get.do", {ts: new Date().getTime()}, function(json) {
		// 	/* お知らせ */
		// 	if (json.allMessageCount > 0) {
		// 		$("#panel-content-news").show();
		// 		var unReadMsgCount = json.unReadMessageCount;
		// 		if (unReadMsgCount > 0) {
		// 			if(unReadMsgCount > 99) {
		// 				unReadMsgCount = "99";
		// 			}
		// 			$("#message-count").html(unReadMsgCount).show();

		// 			$("#panel-content-news").append("<dl>");
		// 			var msgNumber = 0;
		// 			$.each(json.messages, function(i) {
		// 				$("#panel-content-news").append("<dt>" + json.messages[i]['publishDate'] + "</dt>");
		// 				$("#panel-content-news").append("<dd><a href=\"/p/r/msg/" + json.messages[i]['id'] + "\">"+ json.messages[i]['messageTitle'] + "</a></dd>");
		// 				msgNumber++;
		// 			})
		// 			$("#panel-content-news").append("</dl>");

		// 		} else {
		// 			$("#panel-content-news").append(
		// 				'<div class="no-news">未読のお知らせはありません。</div>'
		// 			);
		// 		}
		// 		$("#panel-content-news").append(
		// 			'<div class="find-all">' +
		// 				'<a class="co-001g" href="/p/do/message/list">一覧</a>' +
		// 			'</div>'
		// 		);
		// 	}

		// 	/* ポイント */
		// 	if(json.point != null) {
		// 		var num = new String(json.point).replace(/,/g, "");
		// 		while(
		// 			num != (
		// 				num = num.replace(/^(-?\d+)(\d{3})/, "$1,$2")
		// 			)
		// 		);
		// 		$("#point-num,#point-num1,#point-num2").find("a").prepend(num);
		// 	}
		// 	if(json.raccoonRank != null) {
		// 		var raccoonRank = "";
		// 		var raccoonRankStr = "";
		// 		if(json.raccoonRank == "White") {
		// 			raccoonRank = "rank-white";
		// 			raccoonRankStr = "ホワイト";
		// 		}
		// 		if(json.raccoonRank == "Bronze") {
		// 			raccoonRank = "rank-bronze";
		// 			raccoonRankStr = "ブロンズ";
		// 		}
		// 		if(json.raccoonRank == "Silver") {
		// 			raccoonRank = "rank-silver";
		// 			raccoonRankStr = "シルバー";
		// 		}
		// 		if(json.raccoonRank == "Gold") {
		// 			raccoonRank = "rank-gold";
		// 			raccoonRankStr = "ゴールド";
		// 		}
		// 		if(json.raccoonRank == "Platinum") {
		// 			raccoonRank = "rank-platinum";
		// 			raccoonRankStr = "プラチナ";
		// 		}
		// 		$("#raccoon-rank").addClass(raccoonRank);
		// 		$("#raccoon-rank").prepend(raccoonRankStr);
		// 	}

		// 	/* アラート */
		// 	var alertCount = json.alertCount;
		// 	if (alertCount > 0) {
		// 		if (alertCount >= 100) {
		// 			alertCount = "!";
		// 		}
		// 		$("#alert-count").html(alertCount).show();
		// 	}

		// 	/* 店舗画像 */
		// 	var storePhotoPath = json.storePhotoPath;
		// 	if (json.storePhotoPath != null) {
		// 		var protocol = "https:" == document.location.protocol ? "https:" : "http:";
		// 		$("#member-panel-store-photo").attr("src", protocol + "//www.superdelivery.com/ip/n/sa/120/120/www.superdelivery.com" + storePhotoPath);
		// 	} else {
		// 		$("#member-panel-store-photo").attr("src", "/img/common/other_parts/shop_no_image01.gif");
		// 	}
		// });

		/* カートパネル */
		// var cookieManager = new RaccoonCookie();
		// if (cookieManager.getCookie('cart') >= 1) {
		// 	jQuery('#cart2').addClass('exists');
		// }
		// スマホの場合はclick、それ以外はhover
		if (isTouchPanel()) {
			$("#toggle_price_box").jqueryMenulayer(
				{
					contentsId: "price-detail-box-id",
					handlerClass: "price-box-on",
					handlerRemoveClass: "price-box"
				}
			);
		} else {
			$("#toggle_price_box").jqueryMenulayer(
				{
					contentsId: "price-detail-box-id",
					handlerClass: "price-box-on",
					handlerRemoveClass: "price-box",
					hoverOpen: true,
					hoverHandlerId: "price-btn-area",
					closeOnClickBody: false
				}
			);
		}

		/* アカウントパネル */
		if (isTouchPanel()) {
			$("#headerMemberPanel").jqueryMenulayer(
				{
					contentsId: "member-panel-contents",
					handlerClass: "r-panel-on"
				}
			);
		} else {
			$("#headerMemberPanel").jqueryMenulayer(
				{
					contentsId: "member-panel-contents",
					handlerClass: "r-panel-on",
					hoverOpen: true,
					hoverHandlerId: "header-member-panel-area",
					closeOnClickBody: false
				}
			);
		}

		//アカウントパネルサイズ
		function PanelWidth() {
			var headerMenuWidth = $("#header-area-common .header-menu").width();
			var lmenuWidth = $("#header-area-common .l-menu").width();
			var cnameWidth = $("#headerMemberPanel .com-name").width();
			var panelContentsWidth = 245;

			var panelNameMaxWidth = headerMenuWidth - lmenuWidth - 70;

			if (cnameWidth > panelNameMaxWidth) {
				$("#headerMemberPanel .com-name").css("width", panelNameMaxWidth);
				$("#headerMemberPanel .com-name").addClass("cn-elps");
			}

			var memberpanelOWidth = $("#headerMemberPanel").outerWidth();
			if (memberpanelOWidth > panelContentsWidth) {
				$("#member-panel-contents").css("width", memberpanelOWidth);
			}
		}
		PanelWidth();

		$(window).resize(function() {
			$("#headerMemberPanel .com-name").css("width", "auto");
			$("#headerMemberPanel .com-name").removeClass("cn-elps");
			PanelWidth();
		});

	});
})(jQuery);