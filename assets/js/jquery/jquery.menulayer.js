/**
 * レイヤ表示
 * @param this					開くボタン要素
 * @param option.contentsId	開く要素ID
 * @param option.closeButton	閉じボタンID
 * @param option.handlerClass	開くボタン追加クラス
 * @param option.handlerRemoveClass	開くボタン削除クラス(要素をtoggleさせたいときに使用)
 * @param option.contentClass	開く要素追加クラス
 * @param option.closeOnClickBody	bodyクリックで閉じるを有効にするかどうか(デフォルトはtrue、hoverの場合は必ずfalseにする)
 * @param option.hoverOpen	hoverで開くかどうか(デフォルトはfalse、(注意!)hoverで開く場合はクリックでは開かない)
 * @param option.hoverHandlerId hoverで開閉する場合のhoverの範囲(開いたcontentsIdを触っている場合は閉じない、というパターンが多いので)
 * @return jQuery
 */
(function($){
	var Shim = {
		show : function (content) {},
		showRect : function (left, top, width, height) {},
		hide : function() {}
	};

	var closeOnClickBody = {
		_binded : false,
		bind : function() {
			var ev = 'ontouchstart' in window ? 'touchend' : 'click';
			if (!this._binded) {
				$('body').bind(ev, $.menuLayerAllClose);
				this._binded = true;
			}
		},
		unbind : function() {
			var ev = 'ontouchstart' in window ? 'touchend' : 'click';
			if (this._binded) {
				$('body').unbind(ev, $.menuLayerAllClose);
				this._binded = false;
			}
		}
	};

	var MenuLayer = function(){
		this.initialize.apply(this, arguments);
	};
	MenuLayer.instances = [];
	MenuLayer.prototype.initialize = function(handler, content, handlerCls, handlerRemoveCls, contentCls, closeOnClickBody){
		this.handler = handler;
		this.content = content;
		this.handlerCls = handlerCls;
		this.handlerRemoveCls = handlerRemoveCls;
		this.contentCls = contentCls;
		this.closeOnClickBody = (typeof closeOnClickBody === "undefined")? true : closeOnClickBody;
		this.is_open = false;
	};
	MenuLayer.prototype.open = function(event) {
		this._open(event);
//		Shim.show(this.content);
	};
	MenuLayer.prototype._open = function(event) {
		this.content.show();
		if (this.handlerRemoveCls) {
			this.handler.removeClass(this.handlerRemoveCls);
		}
		if (this.handlerCls) {
			this.handler.addClass(this.handlerCls);
		}
		if (this.contentCls) {
			this.content.addClass(this.contentCls);
		}
		closeOnClickBody.bind();
		this.is_open = true;
	};
	MenuLayer.prototype.close = function(){
		Shim.hide();
		this.content.hide();
		if (this.handlerRemoveCls) {
			this.handler.addClass(this.handlerRemoveCls);
		}
		if (this.handlerCls) {
			this.handler.removeClass(this.handlerCls);
		}
		if (this.contentCls) {
			this.content.removeClass(this.contentCls);
		}
		closeOnClickBody.unbind();
		this.is_open = false;
	};
	MenuLayer.prototype.isOpen = function() {
		return this.is_open;
	};
	var MenuLayerAutoPosition = function(){
		this.initialize.apply(this, arguments);
	};
	MenuLayerAutoPosition.prototype = new MenuLayer();
	MenuLayerAutoPosition.prototype.open = function(){
		MenuLayer.prototype._open.call(this);
		var offset = this.handler.position(),
			width = this.content.outerWidth(),
			height = this.content.outerHeight();
		offset.top += this.handler.outerHeight() - 1;
		//ウィンドウサイズからはみ出る場合はハンドラの右揃え
		if ((width + offset.left ) >= (window.innerWidth || document.body.clientWidth)) {
			offset.left -= width - this.handler.outerWidth();
		}
		this.content.css({
			left : offset.left + 'px',
			top : offset.top + 'px'
		});
		Shim.showRect(
				offset.left,
				offset.top,
				width,
				height);
	};
	$.fn.extend({
		jqueryMenulayer: function(option) {
			if (!option) option = {};
			if (option.contentsId) option.content = '#' + option.contentsId;
			var getContent,
				$closeBtn = $(option.closeButton);
			var MenuLayerClass = (option.position === 'auto')?
									MenuLayerAutoPosition : MenuLayer;
			if (typeof option.content == "string") {
				getContent = function() {
					return $(option.content);
				};
			} else {
				getContent = option.content
			}
			this.each(function(v, i){
				var $content = getContent.call(this);
				var menuLayer = new MenuLayerClass(
							$(this), $content, option.handlerClass, option.handlerRemoveClass, option.contentClass, option.closeOnClickBody);

				MenuLayer.instances.push(menuLayer);

				if (option.hoverOpen && option.hoverHandlerId) {
					// hover
					$("#" + option.hoverHandlerId).hover(
						function(event){
							if (!menuLayer.isOpen()) {
								event.stopPropagation();
								$.menuLayerAllClose();
								menuLayer.open();
							}
						},
						function() {
							if(menuLayer.isOpen()) {
								menuLayer.close();
							}
						}
					);
				} else {
					// クリック
					menuLayer.handler.click(function(event){
						if(menuLayer.isOpen()) {
							menuLayer.close();
						} else {
							event.stopPropagation();
							$.menuLayerAllClose();
							menuLayer.open();
						}
					});
				}

				if($closeBtn.size() > 0) {
					$closeBtn.click(function(event){
						$.each(MenuLayer.instances, function(){
							if (menuLayer === this) {
								menuLayer.close(event);
								return false;
							}
						})
					});
				}

				var ev = 'ontouchstart' in window ? 'touchend' : 'click';
				$content.bind(ev, function(event) {
					event.stopPropagation();
				});
			});

			return this;
		}
	});

	$.menuLayerAllClose = function() {
		$.each(MenuLayer.instances, function(){
			if (this.closeOnClickBody) {
				this.close();
			}
		});
	};
})(jQuery);
