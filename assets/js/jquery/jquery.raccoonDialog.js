(function($) {
	var documentWidth = "";
	var documentHeight = "";
	var contentsWidht = "";
	var contentsHeight = "";
	var modalDialogMaskId = "";
	var modalDialogMaskIframeId = "";
	var ie6Flag = 0;
	var iniFlag = true;
	var $contentSelector = "";
	if($.browser.msie && parseInt($.browser.version) == 6) {
		ie6Flag = 1;
	}

	$.fn.raccoonDialog = function(option) {
		var dt = "";

		$(this).css("display", "block");
		$contentSelector = $(this);

		if(!option || option == 'notCloseOpen') {
			dt = 'modal';
		} else {
			dt = option.dialogType;
		}

		if(option == 'close') {
			modalDialogMaskId.css("display", "none");
			$(this).css("display", "none");
			if(ie6Flag == 1) {
				modalDialogMaskIframeId.css("display", "none");
			}
			return false;
		}

		if(dt == 'modal') {
			if(iniFlag) {
				$("body").append('<div id="modalDialogMask"></div>');
				if(ie6Flag == 1) {
					$("body").append('<iframe width="0" height="0" frameborder="0" src="/blank.html" id="modalDialogMaskIframe"></iframe>');
				}

				modalDialogMaskId = $("#modalDialogMask");
				modalDialogMaskIframeId = $("#modalDialogMaskIframe");

				if(option != 'notCloseOpen') {
					modalDialogMaskId.bind("click", function() {
						$(this).css("display", "none");
						$contentSelector.css("display", "none");
						if(ie6Flag == 1) {
							modalDialogMaskIframeId.css("display", "none");
						}
					});
				}
				iniFlag = false;
			}

			modalDialogMaskId.css("display", "block");
			if(ie6Flag == 1) {
				modalDialogMaskIframeId.css("display", "inline");
			}
			resizeFunc();
		}

		$(document).keyup(function(e){
			if(e.keyCode == 27 && $contentSelector.css("display") == "block") {
				modalDialogMaskId.css("display", "none");
				$contentSelector.css("display", "none");
				if(ie6Flag == 1) {
					modalDialogMaskIframeId.css("display", "none");
				}
			}
		});
	}


	$(window).bind("resize", function() {
		if($contentSelector != "") {
			resizeFunc();
		}
	})

	$(window).bind("load", function() {
		if($contentSelector != "") {
			setTimeout(resizeFunc, 1000);
		}
	})

	var resizeFunc = function() {
		var documentWidth = $(document).width();
		var documentHeight = $(document).height();
		var windowWidth = $(window).width();

		modalDialogMaskId
		.css("width", documentWidth + "px")
		.css("height", documentHeight + "px");

		if(ie6Flag == 1) {
			modalDialogMaskIframeId
			.css("width", documentWidth + "px")
			.css("height", documentHeight + "px");
		}

		contentsWidht = $contentSelector.width();
		contentsHeight = $contentSelector.height();

		$contentSelector
		.css("left", (windowWidth - contentsWidht ) / 2 + "px");
		//.css("top", ($(window).height() - contentsHeight) / 2 + "px");
	}

})(jQuery);
