(function($){
	$.fn.jqueryWatermaker = function(config) {
		
		var span = $(this).children("span:first");
		var input = $(this).children("input:first");
		
		if(input.val() == "") {
			span.css("display","inline");
		} else {
			span.css("display","none");
		}
		
		span.click(function() {
			span.css("display","none");
			input.focus();
		});
		
		input.focus(function() {
			span.css("display","none");
		});
		
		input.blur(function() {
			span.css("display","none");
			if(input.val() == "") {
				span.css("display","inline");
			} else {
				span.css("display","none");
			}
		});
		return this;
	};
})(jQuery);