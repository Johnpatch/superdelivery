if(isMobi){
	/*
	 * iPad/iPhoneの場合、
	 * <input type=image> or <a><img/></a>を
	 * タップすると画像が消える。
	 * そもそもonmouseover/onmouseoutは反応しないので意味が無いため。
	 * onmouseover/onmouseoutをとる
	 * ontouchstart,ontouchend属性に振り替える
	 */
	$(document).ready(
		function(){
			// ipad2のみ
			$(":image").each(
				function(){
					$(this).removeAttr("onmouseover");
					$(this).removeAttr("onmouseout");
				}
			);
			// ipad、ipad2
			$("a > img").each(
				function(){
					$(this).removeAttr("onmouseover");
					$(this).removeAttr("onmouseout");
				}
			);
		}
	)
}
