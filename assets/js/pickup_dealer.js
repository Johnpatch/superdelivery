$(function() {
  var pudImgArea = $("#pickUpDealer .pud-dealer-imgarea");
  var pudSendWidth = null;
  var posMargin = null;
  var imgMax = 900;
  var pudConSize = function() {
          //蟾ｦ蜿ｳ縺ｮ逕ｻ蜒�20%縺壹▽莉･荳九�蝣ｴ蜷医�20%繧偵く繝ｼ繝�
          if(($(window).width() * 0.6) < imgMax) {
            /* 譫�邨�∩縺ｮ繝槭�繧ｸ繝ｳ */
            var pudSendWidth = $(".pud-img-send").outerWidth();
            posMargin = $(window).width() - pudSendWidth*2;
            pudImgArea.css("margin-left",-posMargin);
            /* 螟ｧ譫�縺ｮpadding縲√�繧ｸ騾√ｊ縺ｮ蟷�:css縺ｮ謖�ｮ壹ｒ豢ｻ縺九☆縺溘ａ縺ｫstyle繧堤┌蜉ｹ縺ｫ */
            $("#pickUpDealer").removeAttr("style");
            $(".pud-img-send").removeAttr("style");
            /* 逕ｻ蜒乗棧縺ｮ蟷� */
            $("#pickUpDealer .pud-img-box").each(function(){
              $(this).width(posMargin);
            });
          } else {
            var nonImgArea = ($(window).width() - imgMax)/2;
            /* 譫�邨�∩縺ｮ繝槭�繧ｸ繝ｳ */
            posMargin = $(window).width() - nonImgArea*2;
            pudImgArea.css("margin-left",-posMargin);
            /* 螟ｧ譫�縺ｮpadding */
            $("#pickUpDealer").css("padding-left",nonImgArea)
            /* 繝壹�繧ｸ騾√ｊ縺ｮ蟷� */
            $(".pud-img-send").width(nonImgArea);
            /* 逕ｻ蜒乗棧縺ｮ蟷� */
            $("#pickUpDealer .pud-img-box").each(function(){
              $(this).width(imgMax);
            });
          }
          /* 逕ｻ蜒城√ｊ縺ｮ鬮倥＆縺ｨ菴咏區 */
          var pudImgH = $(".pud-dealer-img").height();
          if(pudImgH == 0){
            setTimeout(pudConSize,200);
            return;
          }
          var pageSendH = $("#pickUpDealer .pud-img-send img").height();
          $("#pickUpDealer .pud-img-send").css({"padding-top":(pudImgH - pageSendH)/2,"height":pudImgH});
        }

  /* 繧ｹ繝ｩ繧､繝峨�蜍輔″繧呈�ｼ邏� */
  var pudSlideNext = function (moveTime,moveSpeed) {
          // 繧｢繝九Γ繝ｼ繧ｷ繝ｧ繝ｳ荳ｭ縺ｫ蜍輔°縺ｪ縺�ｈ縺�↓荳ｸ蝙九�繧ｿ繝ｳ辟｡蜉ｹ
          unbindMoveButtons();
          for (var i=0; i<moveTime; i++) {
            pudImgArea.animate({"margin-left": -(posMargin*2)}, moveSpeed , function(){
              $("#pickUpDealer .pud-img-box:first").insertAfter("#pickUpDealer .pud-img-box:last");
              $("#pickUpDealerSwitch .pud-switch:last").insertBefore("#pickUpDealerSwitch .pud-switch:first");
              pudImgArea.css("margin-left",-posMargin);
            });
          }
          $.when(pudImgArea).done(function () { //繧｢繝九Γ繝ｼ繧ｷ繝ｧ繝ｳ縺檎ｵゅｏ縺｣縺溘ｉ荳ｸ蝙九�繧ｿ繝ｳ譛牙柑縺ｫ
            bindMoveButtons();
          });
        }
  var pudSlideBefore = function (moveTime,moveSpeed) {
          unbindMoveButtons();
          for (var i=0; i<moveTime; i++) {
            pudImgArea.animate({"margin-left": -(posMargin*0)}, moveSpeed , function(){
              $("#pickUpDealer .pud-img-box:last").insertBefore("#pickUpDealer .pud-img-box:first");
              $("#pickUpDealerSwitch .pud-switch:first").insertAfter("#pickUpDealerSwitch .pud-switch:last");
              pudImgArea.css("margin-left",-posMargin);
            });
          }
          $.when(pudImgArea).done(function () {
            bindMoveButtons();
          });
        }

  /* 閾ｪ蜍暮√ｊ */
  var int = 5000;
  var pudSlide = null;
  var pudAutoNext = function() {
    pudSlideNext(1,400)
  };
  var pudAutoMoving = function () {
              pudSlide = setInterval(pudAutoNext, int);
              if( isTouchDevice() == false ){
                $(".pud-img-send,.pud-switch").hover(function() {
                  clearInterval(pudSlide);
                },function() {
                  clearInterval(pudSlide);
                  pudSlide = setInterval(pudAutoNext, int);
                });
              }
          };

  /* 蟾ｦ蜿ｳ縺ｮ遏｢蜊ｰ縺ｧ縺ｮ逕ｻ蜒城√ｊ */
  var pudSlideSpeed = 400;
  var pudNextMove = function() { pudSlideNext(1,pudSlideSpeed); };
  var pudBeforeMove = function() { pudSlideBefore(1,pudSlideSpeed); };
  /* 荳ｸ蝙九�繧ｿ繝ｳ縺ｮ逕ｻ蜒城√ｊ */
  var pudSwitchSpeed = 100;
  var pudSwitchMove = function() {
              var switchNum = $("#pickUpDealerSwitch .pud-switch").index(this);
              var switchOnNum = $("#pickUpDealerSwitch .switch-on").index();
              if(switchOnNum < switchNum) {
                var pudMoveTime = switchNum - switchOnNum;
                pudSlideNext(pudMoveTime,pudSwitchSpeed);
              } else if(switchOnNum > switchNum) {
                var pudMoveTime = switchOnNum - switchNum;
                pudSlideBefore(pudMoveTime,pudSwitchSpeed);
              }
            }

  var bindMoveButtons = function(){
    $("#pickUpDealer .img-send-next").bind("click",pudNextMove);
    $("#pickUpDealer .img-send-back").bind("click",pudBeforeMove);
    /* 繧ｿ繝�メ繝�ヰ繧､繧ｹ縺ｯ荳ｸ蝙九�繧ｿ繝ｳ繧ｫ繝�ヨ */
    if( isTouchDevice() == false ){
      $("#pickUpDealerSwitch .pud-switch").click(pudSwitchMove);
    }
  }

  var unbindMoveButtons = function(){
    $("#pickUpDealer .img-send-next").unbind("click",pudNextMove);
    $("#pickUpDealer .img-send-back").unbind("click",pudBeforeMove);
    /* 繧ｿ繝�メ繝�ヰ繧､繧ｹ縺ｯ荳ｸ蝙九�繧ｿ繝ｳ縺ｪ縺� */
    if( isTouchDevice() == false ){
      $("#pickUpDealerSwitch .pud-switch").unbind("click",pudSwitchMove);
    }
  }

  /* 繧ｹ繝ｯ繧､繝励�逕ｻ蜒城√ｊ */
  if( isTouchDevice() ){
    var startX=null;
    var pudImgW = $(".pud-dealer-img").width();
    var ignoreRange = pudImgW/4;

    pudImgArea.bind("touchstart",function(){
      startX = event.changedTouches[0].pageX;
      //閾ｪ蜍暮√ｊ蛛懈ｭ｢
      clearInterval(pudSlide);
    });
    pudImgArea.bind("touchend",function(){
      if(startX == null){
        return;
      }
      endX = event.changedTouches[0].pageX;
      diffX= endX - startX;
      if( Math.abs(diffX) < ignoreRange){
        //cancel;
      }
      else if( diffX > 0){
        pudBeforeMove();
      }
      else{
        pudNextMove();
      }
      startX=null;
      //閾ｪ蜍暮√ｊ繧�5遘貞ｾ後↓蜀埼幕
      setTimeout(
        function(){
          clearInterval(pudSlide);
          pudSlide = setInterval(pudAutoNext, int);
        }
        ,3000
      );
    });
  }

  pudConSize();
  pudAutoMoving();
  $(window).resize(function(){
    pudConSize();
    clearInterval(pudSlide);
    pudAutoMoving();
  });

  bindMoveButtons();
});
