var productSlide = function(property) {
	// 初期表示する商品数
	var displayCount = property.displayCount;
	// 商品を取得しに行く商品数閾値
	var thresholdCount = property.thresholdCount;
	// リクエスト毎に取得する商品数
	var sizeByPage = property.sizeByPage;
	// 企業コードのリスト
	var _dealerCodes = property.dealerCodes;
	var dealerCodes;

	property.en = property.en || false;
	property.isSwipeEnabled = property.isSwipeEnabled || false;
	property.swipeBorderWidth = property.swipeBorderWidth || 940; // 画面幅がこれ以下のときにスワイプが有効になる

	// 企業コードが0件だった場合に、splitの結果配列の要素が
	// 空の文字列で1件（[""]）になってしまう仕様に対応。
	if(_dealerCodes.length == 0){
		dealerCodes= new Array();
	}
	else{
		dealerCodes = _dealerCodes.split(",");
	}
	var currentDisplayCount;
	var urlParamGenre;
	var urlParamOther;

	this.setDisplayCount = function(){
		var contentsInner;
		if(dealerCodes.length != 0 ){
			contentsInner =$("#itembox-inner-"+dealerCodes[0]);
		}
		else{
			var listRoot = $("#jsp-tiles-productlist-c-dealer-product-list");
			contentsInner = listRoot.find("div[class^='itembox-contents-inner'],.dealer-itembox-inner");
		}

		var backBox = contentsInner.children("div.item-back-box,.itembtn-back");
		var nextBox = contentsInner.children("div.item-next-box,.itembtn-next");
		var wrapWidth = contentsInner.width() - backBox.width() - nextBox.width();
		var boxWidth = contentsInner.children("div.itembox-parts-wrap,.dealer-itemparts,.item-box-area").children("div.itembox-parts").outerWidth({margin: true});
		// 画面幅小→大に広げたときにスライドの右ボタンが有効化されるようにする
		if(property.isSwipeEnabled && window.innerWidth <= property.swipeBorderWidth){
			currentDisplayCount = 3;
		}else{
			currentDisplayCount = Math.floor(wrapWidth/boxWidth);
		}
		if (currentDisplayCount > displayCount) {currentDisplayCount = displayCount;}

	}
	setDisplayCount();

	

	//ロード用URLを生成
	function createUrl(dealerCode, page, property) {
	    var base = "/p/do/asyncDealerProductListSearch"
	    if (property.en) {
	        var base = "/en/do/asyncDealerProductListSearch"
	    }
		var urlElement = new Array();
		urlElement[0] = "?dc="+dealerCode;
		urlElement[1] = !urlParamGenre ? "cc=all" : "cc="+urlParamGenre;
		urlElement[2] = "pg="+page;
		if (urlParamOther) {
			urlElement[3] = urlParamOther
		}
		return base + urlElement.join("&");
	}

	for(var i=0;i<dealerCodes.length;i++){
		var dealerCode = dealerCodes[i];

		// 準備
		var contentsInner =$("#itembox-inner-"+dealerCode);
	

		// 格納済み商品情報があればcontentsInnerへ
    	var preRead = $("#product_divs_" + dealerCode).val();
		if (preRead!="") {
			contentsInner.html(preRead);
		}

		var nextBox = contentsInner.children("div.item-next-box,.itembtn-next");
		var backBox = contentsInner.children("div.item-back-box,.itembtn-back");

		// 初期表示数の設定 (add)
		var partsWrap = contentsInner.children("div.itembox-parts-wrap,.dealer-itemparts,.item-box-area");
		var itemPars = partsWrap.children("div.itembox-parts");
		var visibleItems = itemPars.filter("div.visible-on");


		if(currentDisplayCount < visibleItems.length){
			$(visibleItems).slice(currentDisplayCount).each(function(){
				$(this).addClass("visible-rear").removeClass("visible-on");
			});
		}

		// 次を見るボタンの制御
		var productCount = $("#product_count_"+dealerCode).text();
    	if(productCount <= currentDisplayCount) {
			nextBox.find(".advance_step").hide();
			nextBox.find(".not_advance_step").show();
    	}

		backBox.children("div.retrace_step").bind("click", function() {

			var contentsInner = $(this).parents("div[class^='itembox-contents-inner'],.dealer-itembox-inner");
			var dealerCode = contentsInner.attr("dealerCode");

			var partsWrap = contentsInner.children("div.itembox-parts-wrap,.dealer-itemparts,.item-box-area");

			var itemPars = partsWrap.children("div.itembox-parts");
			var visibleItems = itemPars.filter("div.visible-on");
			var invisibleItems = itemPars.filter("div.visible-pre");

			var notRetraceStep = $(this).siblings("div.not_retrace_step");
			var productDivs = $("#product_divs_"+dealerCode);

			$(visibleItems).each(function(){
				$(this).addClass("visible-rear").removeClass("visible-on");
			});
			var invisibleItemsNum = invisibleItems.length;
			var toVisibleStartPos = invisibleItemsNum - currentDisplayCount;

			if(toVisibleStartPos<0){
				//　残りが表示数より少ない
				invisibleItems.each(function(){
					$(this).removeClass("visible-pre").addClass("visible-on");
				});
				//　Rearから足す
				var invisibleItemsRear= itemPars.filter("div.visible-rear").slice(0,Math.abs(toVisibleStartPos));
				invisibleItemsRear.each(function(){
					$(this).removeClass("visible-rear").addClass("visible-on");
				});
			}
			else{
				//　invisibleItemsの最後から表示数分
				invisibleItems.slice(toVisibleStartPos).each(function(){
					$(this).removeClass("visible-pre").addClass("visible-on");
				});
			}


			// back box
			var backBox = contentsInner.children("div.item-back-box,.itembtn-back");
			var retraceStep = backBox.children("div.retrace_step");
			var notRetraceStep = backBox.children("div.not_retrace_step");

			invisibleItems = partsWrap.children("div.visible-pre");

			if (invisibleItems.length==0) {
				retraceStep.hide();
				notRetraceStep.show();
			}
			else{
				retraceStep.show();
				notRetraceStep.hide();
			}

			// next box
			var nextBox = contentsInner.children("div.item-next-box,.itembtn-next");
			var notAdvanceStep = nextBox.children("div.not_advance_step");
			var advanceStep = nextBox.children("div.advance_step");

			notAdvanceStep.hide();
			advanceStep.show();

			// backup
			productDivs.val(contentsInner.html());

		});

		nextBox.children("div.advance_step").bind("click", function() {
			
			var contentsInner = $(this).parents("div[class^='itembox-contents-inner'],.dealer-itembox-inner");
			var dealerCode = contentsInner.attr("dealerCode");

			var advanceStep = $(this);

			// siblings　parts box
			var partsWrap = contentsInner.children("div.itembox-parts-wrap,.dealer-itemparts,.item-box-area");
			var nextBox = contentsInner.children("div.item-next-box,.itembtn-next");
			var backBox = contentsInner.children("div.item-back-box,.itembtn-back");

			// ---　child partsWrap
			var itemPars = partsWrap.children("div.itembox-parts").not('.sp-list-loading');
			var visibleItems = itemPars.filter("div.visible-on");
			var invisibleItems = itemPars.filter("div.visible-rear");
			var lastItem = itemPars.last();

			// back box
			var retraceStep = backBox.children("div.retrace_step");
			var notRetraceStep = backBox.children("div.not_retrace_step");

			// next box
			var notAdvanceStep = nextBox.children("div.not_advance_step");
			var loadingStep = nextBox.children("div.loading_step");
			var page = nextBox.children("input.page_num");

			// contents-inner's simblings
			var productCount = $("#product_count_"+dealerCode).text();
			var productDivs = $("#product_divs_"+dealerCode);

			var isNoMoreProducts = false;
			var maxPageNo = Math.ceil(productCount/sizeByPage);

			visibleItems.each(function() {
				$(this).addClass("visible-pre").removeClass("visible-on");
			});

			var emptyNum=0;
			if (invisibleItems.length < currentDisplayCount){
				emptyNum=currentDisplayCount-invisibleItems.length;
				invisibleItems.each(function(){
					$(this).removeClass("visible-rear").addClass("visible-on");
				});
			}
			else{
				invisibleItems.slice(0,currentDisplayCount).each(function(){
					$(this).removeClass("visible-rear").addClass("visible-on");
				});
			}

			// 見えているやつと後ろに隠れているものの合計がthresholdCount以下かつ最終ページ以下なら続きの商品を取りに行く
			// if(visibleItems.length + invisibleItems.length <= thresholdCount && page.val() <= maxPageNo) {
			if(visibleItems.length + invisibleItems.length <= thresholdCount) {
				$(this).hide();
				loadingStep.hide();
				notAdvanceStep.show();
				$("#product_divs_"+dealerCode).val(advanceStep.parents("div[class^='itembox-contents-inner'],.dealer-itembox-inner").html());
				// loadingStep.show();
				// var repurl = createUrl(dealerCode, page.val(), property);
				// page.val(parseInt(page.val())+1);
				// $.ajax({
				// 	type: "GET",
				// 	url: repurl,
				// 	dataType: 'html',
				// 	success: function(data) {
				// 		data = $.trim(data);
				// 		if (data) {
				// 			var partsData = $(data);
				// 			if(emptyNum>0){
				// 				partsData.filter(".itembox-parts").slice(0,emptyNum).each(function(){
				// 					$(this).removeClass("visible-rear").addClass("visible-on");
				// 				});
				// 			}
				// 			partsData.find("a.add-wishlist-action").bind("click", function(){addWishList(this);});
				// 			lastItem.after(partsData);
				// 		}
				// 		//再検索して判断
				// 		var itemParsAdd = partsWrap.children("div.itembox-parts");
				// 		var visibleItemsAdd = itemParsAdd.filter("div.visible-on");
				// 		var invisibleItemsAdd = itemParsAdd.filter("div.visible-rear");
				// 		loadingStep.hide();
				// 		if ((visibleItemsAdd.length - currentDisplayCount) + invisibleItemsAdd.length > 0) {
				// 			advanceStep.show();
				// 		} else {
				// 			notAdvanceStep.show();
				// 		}
				// 		$("#product_divs_"+dealerCode).val(contentsInner.html());
				// 	},
				// 	error: function(XMLHttpRequest, textStatus, errorThrown) {
				// 		loadingStep.hide();
				// 		notAdvanceStep.show();
				// 		$("#product_divs_"+dealerCode).val(advanceStep.parents("div[class^='itembox-contents-inner'],.dealer-itembox-inner").html());
				// 	}
				// });
			} else {
			// } else if (page.val() > maxPageNo){
				isNoMoreProducts = true;
			}

			notRetraceStep.hide();
			retraceStep.show();

			//再検索して判断
			var itemParsRe = partsWrap.children("div.itembox-parts");
			var visibleItemsRe = itemParsRe.filter("div.visible-on");
			var invisibleItemsRe = itemParsRe.filter("div.visible-rear");
			if (invisibleItemsRe.length == 0 && visibleItemsRe.length <= currentDisplayCount && isNoMoreProducts) {
				advanceStep.hide();
				notAdvanceStep.show();
			}
			$("#product_divs_"+dealerCode).val(contentsInner.html());

		});
	}

	$(window).resize(function(){
		setDisplayCount();
	});	
};