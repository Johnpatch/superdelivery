var RaccoonCookie = function() {
	this.cookieExpiry = 90;
	this.cookiePath = '/';

	this.setCookie = function(name, value) {
		var now = new Date();
		now.setTime(now.getTime() + this.cookieExpiry * 24 * 60 * 60 * 1000 );
		var dayIndex = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
		var monthIndex = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

		document.cookie = "" + escape(name) + "=" + escape(value) + ";"
			+ (this.cookieExpiry > 0 ? " expires=" + dayIndex[now.getDay()] + ', ' + now.getDate() + '-' + monthIndex[now.getMonth()] + '-' + now.getFullYear() + ' 00:00:00 GMT' + ";" : "")
			+ " path=" + this.cookiePath;
	};
	
	this.getCookie = function(name) {
		var cookies = document.cookie.split("; ");
		for (var i = 0; i < cookies.length; ++i) {
			var aCookie = cookies[i].split("=");
			if (unescape(aCookie[0]) == name) {
				return unescape(aCookie[1]);
			}
		}
		return null;
	};
	
	this.getCookies = function() {
		var cookies = [];
		if (document.cookie.length <= 0) {
			return cookies;
		}

		var cookiesText = document.cookie.split("; ");
		for (var i = 0; i < cookiesText.length; ++i) {
			var aCookie = cookiesText[i].split("=");
			cookies.push({name: unescape(aCookie[0]), value: unescape(aCookie[1])});
		}
		return cookies;
	};
	
	this.clearCookie = function(name) {
		document.cookie = "" + escape(name) + "=; expires=Sat, 01-Jan-2000 00:00:00 GMT; path=" + this.cookiePath;
	};
};
