var RaccoonNumber = function() {
	this.commaFormat = function(target) {
		target = '' + target;
		while (target != (target = target.replace(/(\d+)(\d\d\d)/, '$1,$2'))) {}
		return target;
	};

	this.formatToCurrencyJP = function(target){
		return "JPY " + number.commaFormat(target);
	};

	this.formatToTargetCurrency = function(currency, target){
		return currency + " " + number.commaFormat(target);
	};
};
