var RaccoonProduct = function() {
	this.getDetailUrl = function(code) {
		if (location.pathname.indexOf('/p/r/pd_d/') == 0) {
			return '/p/r/pd_d/' + code + '/';
		}
		else if (location.pathname.indexOf('/p/r/pd_r/') == 0) {
			return '/p/r/pd_r/' + code + '/';
		}
		else {
			return '/p/r/pd_p/' + code + '/';
		}
	};
};
