var RaccoonProductHistoryItem = function() {
	this.code = null;

	this.serializeSeparator = ':';

	this.loadSerialized = function(serialized) {
		var splitedData = serialized.split(this.serializeSeparator, 7);
		this.code = splitedData[0] - 0;
	};

	this.loadValue = function(code) {
		this.code = code - 0;
	};

	this.serialize = function() {
		return '' + this.code;
	};

	this.getThumbnailPath = function(code) {
		var longCode = '0000000' + this.code;
		return '/product_image/' + (longCode.slice(-7).substring(0, 3) - 0) + '/' + (longCode.slice(-7).substring(3, 4) - 0) + '/' + code + '_s_0.jpg';
	};
};

var RaccoonProductHistory = function(maxProducts) {
	this.setCookie = function(name, value) {
		var now = new Date();
		now.setTime(now.getTime() + this.cookieExpiry * 24 * 60 * 60 * 1000 );
		var dayIndex = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
		var monthIndex = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

		document.cookie = "" + escape(name) + "=" + escape(value) + "; expires="
			+ dayIndex[now.getDay()] + ', ' + now.getDate() + '-' + monthIndex[now.getMonth()] + '-' + now.getFullYear() + ' 00:00:00 GMT'
			+ "; path=" + this.cookiePath;
	};

	this.getCookie = function(name) {
		var cookies = document.cookie.split("; ");
		for (var i = 0; i < cookies.length; ++i) {
			var aCookie = cookies[i].split("=");
			if (unescape(aCookie[0]) == name) {
				return unescape(aCookie[1]);
			}
		}
		return null;
	};

	this.clearCookie = function(name) {
		document.cookie = "" + escape(name) + "=; expires=Sat, 01-Jan-2000 00:00:00 GMT; path=" + this.cookiePath;
	};

	this.add = function(product) {
		var newProducts = new Array();
		newProducts[newProducts.length] = product;
		for (var i = 0; i < this.products.length && newProducts.length <= this.maxProducts;  ++i) {
			if (product.code != this.products[i].code) {
				newProducts[newProducts.length] = this.products[i];
			}
		}

		this.products = newProducts;
	};

	this.store = function() {
		var serializedData = new Array();
		var serializedBytes = 0;
		for (var i = 0; i < this.products.length && i <= this.maxProducts; ++i) {
			serializedBytes += escape(this.products[i].serialize().replace(this.cookieSeparator, this.cookieEscapedSeparator)).length + this.cookieSeparator.length;
			if (serializedBytes > this.maxBytes) {
				break;
			}
			serializedData[serializedData.length] = this.products[i].serialize().replace(this.cookieSeparator, this.cookieEscapedSeparator);
		}
		this.setCookie(this.cookieName, serializedData.join(this.cookieSeparator));
	};

	this.contains = function(code) {
		for (var i = 0; i < this.products.length && i <= this.maxProducts; ++i) {
			if (this.products[i].code == code) {
				return true;
			}
		}

		return false;
	};

	this.maxProducts = maxProducts;
	this.products = new Array();
	this.cookieName = '_j_recent';
	this.cookiePath = '/';
	this.cookieExpiry = 90;
	this.cookieSeparator = ';;';
	this.cookieEscapedSeparator = ';；';
	this.maxBytes = 2000;

	if (this.getCookie(this.cookieName)) {
		var products = this.getCookie(this.cookieName).split(/;;(?!;)/);
		for (var i = 0; i < products.length && i <= this.maxProducts; ++i) {
			var product = new RaccoonProductHistoryItem();
			product.loadSerialized(products[i].replace(this.escape + this.cookieSeparator, this.cookieSeparator));
			this.products[this.products.length] = product;
		}
	}
};

var flexboxUtil = flexboxUtil || {};
var RaccoonRecentProduct = (function($, flexboxUtil) {

	var isForeign = false;

	var recentProduct = function(productCodes) {
		var recentProductCodes = productCodes,
			index = -1,
			exists = recentProductCodes.length > 0;

		function saveRequested(data) {
			index = $.inArray(data.checkedCode, recentProductCodes);
			exists = data.existsNext;
		}

		function getProductCodes() {
			return exists ? recentProductCodes.slice(index + 1) : [];
		}

		// ajaxで取得するproductが残っているかどうか
		function existsProduct() {
			return exists;
		}

		function isEmpty() {
			return index == -1;
		}

		return {
			getProductCodes : getProductCodes,
			existsProduct : existsProduct,
			saveRequested : saveRequested,
			isEmpty : isEmpty
		};
	};

	function requestRecentProduct(params, callback) {
		if (!params || !callback) {
			return;
		}

		isForeign = params.isForeign || false;

		params.productHistory = params.productHistory || {};
		params.recentItemsId = params.recentItemsId || "recent-items";
		params.initFlex = params.initFlex || false;
		params.ignoreFirst = params.ignoreFirst || false;
		params.sp = params.sp || false;
		params.recentDiv = params.recentDiv || "btm-recent-chk-common";
		params.parentDiv = params.parentDiv || "recent-items-wrap";
		// trueにすると、最初にgetProductしたときにすべての商品を取得する
		params.fetchAllProducts = params.fetchAllProducts || false;

		if (params.productHistory.products) {
			params.recentProductCodes = $.map(params.productHistory.products, function(v, i) {
				if ((params.ignoreFirst && i == 0) || isNaN(v.code)) {
					return null;
				}
				return v.code;
			});
		}

		var product = recentProduct(params.recentProductCodes);
		var recentControl = (function() {
			var viewId = "#" + params.recentItemsId,
				clicked = false;

			function initialize() {
				var viewItem = $(viewId),
					buttonId = viewItem.attr("forward-id");

				// 海外版、ログイン前、商品詳細はハートマークのwishlistの実装はwishlist.jsにまとめる
				//(すぐ戻せるように(10/10)、不要だったら消す)if(!isForeign && !viewItem.hasClass("prelogin-wishlist") && !$("#jsp-tiles-productdetail-c").hasClass('add-wishlist-detail-page')) {
				if(!isForeign && !$("#jsp-tiles-productdetail-c").hasClass('add-wishlist-detail-page')) {
					viewItem.delegate(".add-to-wishlist", "click", addWishlist);
				}

				if (buttonId) {
					$("#" + buttonId).click(function() {
						if (!clicked) {
							clicked = true;
							getRecentProducts();
						}
					});
				}

				if (params.initFlex) {
					flexboxUtil.init(viewId,"#"+params.parentDiv);
				}

				if (product.existsProduct()) {
					$("#" + params.recentDiv).show();
				}

				$(window).resize(function() {
					if (product.existsProduct()) {
						flexboxUtil.refreshButtons(viewId, false, true);
					}
				});
			}

			function handleResult(data) {
				var list = data.recentProductList;
				if (list.length != 0) {
					$.each(list, function(i, v) {
						v.productName = h(v.productName);
						v.viewName = h(v.viewName);
						v.status = (function(v) {
							var status = "";
							if (v.tradeCondition == "TRADABLE") {
								status = '<a href="/p/do/apply?code=' + v.dealerCode + '">卸価格を見る<br>（申請画面へ）<\/a>';
							} else if (v.tradeCondition == "APPLIED") {
								status = '<span class="co-fcgray">申請中<\/span>';
							} else {
								status = '<span class="co-fcgray">申請を受け付けていません<\/span>';
							}
							return status;
						})(v);
						v.isWish = ($.inArray(v.tradeCondition, ["TRADABLE", "PRICE_OPEN", "APPLIED", "TRADING"]) != -1);
					});

					callback(list);
					product.saveRequested(data);
					flexboxUtil.initFlex(viewId);

					if (product.existsProduct()) {
						flexboxUtil.refreshButtons(viewId, false, true);
						clicked = false;
					}

					// getRecentProducts() の product.getProductCodes() で
					// 商品コードが取れなくなるまで再帰的に実行する
					if(params.fetchAllProducts) getRecentProducts();
				} else {
					if (product.isEmpty()) {
						$("#" + params.recentDiv).hide();
					}
				}
			}

			function handleResultSp(data) {
				var list = data.recentProductList;
				if (list.length != 0) {
					callback(list);
				}
			}

			function h(value) {
				value = value || "";
				return value.replace(/&/g,"&amp;")
							.replace(/"/g,"&quot;")
							.replace(/'/g,"&#039;")
							.replace(/</g,"&lt;")
							.replace(/>/g,"&gt;");
			}

			function fetchRemote(productCodes) {
				var contextPath = "/p";
				if (isForeign){
					contextPath = "/en";
				}
				var jqXHR = $.ajax({
					type: "GET",
					url: contextPath + "/recentProduct/get.do",
					cache: false,
					data: {
						productCodes : productCodes
					}
				});
				return jqXHR;
			}

			function getRecentProducts() {
				var productCodes = product.getProductCodes();
				if (productCodes.length != 0) {
					fetchRemote(productCodes).success(function (data) {
						if (!params.sp) {
							handleResult(data);
						} else {
							handleResultSp(data);
						}
					});
				}
			}

			return {
				init : initialize,
				getRecentProducts : getRecentProducts
			};
		})();

		$(function () {
			if (!params.sp) {
				recentControl.init();
				var recentDiv = $("#" + params.recentDiv),
					target = $(window),
					border = recentDiv.offset().top - target.height(),
					isDelay = border > 0,
					trigger = false;

				if (isDelay) {
					target.scroll(function() {
						if (!trigger && (target.scrollTop() > border)) {
							trigger = true;
							recentControl.getRecentProducts();
						}
					});
				} else {
					recentControl.getRecentProducts();
				}
			} else {
				recentControl.getRecentProducts();
			}
	    });
	}

	function addWishlist() {
		var contextPath = "/p";
		var recentItem = $(this),
			productCode = recentItem.attr("id").replace("recent_product_code_", "");

		if(recentItem.hasClass("async-processing")) {
			return false;
		}

		recentItem.addClass("async-processing");

		if(!recentItem.hasClass("added-wish")) {
			recentItem.addClass("wish-loading");
		} else {
			recentItem.removeClass("added-wish");
			recentItem.find(".add-whis-txt").css("display", "none");
		}
		
		var loadingTimeId = "";
		$.ajax({
			type: "GET",
			cache : false,
			url: contextPath + "/wishlist/manipulateAsync.do",
			data : {
				p : productCode
			}
		}).success(function(msg) {
			loadingTimeId = setInterval(function() {
				recentItem.removeClass("wish-loading");
				if(msg.status == "add_success"){
					recentItem.removeClass("wish-error");
					recentItem.addClass("added-wish");
					recentItem.find(".add-whis-txt").html("追加しました");
					setTimeout(function(){ recentItem.find(".add-whis-txt").fadeOut(500); },1500);
					recentItem.removeClass("async-processing");
				} else if(msg.status == "delete_success") {
					recentItem.removeClass("wish-error");
					recentItem.find(".add-whis-txt").text("検討中に追加");
					recentItem.find(".add-whis-txt").css("display", "block");
					recentItem.removeClass("async-processing");
				} else if(msg.status == "orverflow") {
					recentItem.addClass("wish-error");
					recentItem.find(".add-whis-txt").html("追加できません<br>再度押してください");
					recentItem.removeClass("async-processing");
				} else {
					recentItem.addClass("wish-error");
					recentItem.find(".add-whis-txt").css("display", "block");
					recentItem.find(".add-whis-txt").html("システムエラーが<br>発生しました");
					recentItem.removeClass("async-processing");
				}
				clearInterval(loadingTimeId);
			}, 500);
		}).error(function(xhr, status, err) {
			recentItem.removeClass("wish-loading");
			recentItem.addClass("wish-error");
			recentItem.find(".add-whis-txt").css("display", "block");
			recentItem.find(".add-whis-txt").html("システムエラーが<br>発生しました");
			recentItem.removeClass("async-processing");
		});
	}

	return {
		requestRecentProduct : requestRecentProduct
	};

})(jQuery, flexboxUtil);


var flexboxAnimUtil = flexboxAnimUtil || {};
var RaccoonTopRecentProduct = (function($, flexboxAnimUtil) {

	var recentProduct = function(productCodes) {
		var recentProductCodes = productCodes,
			index = -1,
			exists = recentProductCodes.length > 0;

		function saveRequested(data) {
			index = $.inArray(data.checkedCode, recentProductCodes);
			exists = data.existsNext;
		}

		function getProductCodes() {
			return exists ? recentProductCodes : [];
		}

		function existsProduct() {
			return exists;
		}

		function isEmpty() {
			return index == -1;
		}

		return {
			getProductCodes : getProductCodes,
			existsProduct : existsProduct,
			saveRequested : saveRequested,
			isEmpty : isEmpty
		};
	};

	function requestRecentProduct(params, callback) {
		if (!params || !callback) {
			return;
		}

		params.productHistory = params.productHistory || {};
		params.recentItemsId = params.recentItemsId || "recent-items";
		params.initFlex = params.initFlex || false;
		params.recentDiv = params.recentDiv || "recent-item-area";
		params.parentDiv = params.parentDiv || "recent-item-box";

		if (params.productHistory.products) {
			params.recentProductCodes = $.map(params.productHistory.products, function(v, i) {
				if (isNaN(v.code)) {
					return null;
				}
				return v.code;
			});
		}

		var product = recentProduct(params.recentProductCodes);
		var recentControl = (function() {
			var viewId = "#" + params.recentItemsId,
				clicked = false;

			function initialize() {
				var viewItem = $(viewId),
					buttonId = viewItem.attr("forward-id");

				viewItem.delegate("a.add-wish-btn", "click", addWishlist);
				viewItem.delegate(".add-to-wishlist", "click", addWishlist);
				if (buttonId) {
					$("#" + buttonId).click(function() {
						if (!clicked) {
							clicked = true;
							getRecentProducts();
						}
					});
				}

				if (params.initFlex) {
					flexboxAnimUtil.init(viewId,"#"+params.parentDiv);
				}

				if (product.existsProduct()) {
					$("#" + params.recentDiv).show();
				}

				$(window).resize(function() {
					if (product.existsProduct()) {
						flexboxAnimUtil.refreshButtons(viewId, false, true);
					}
				});
			}

			function handleResult(data) {
				var list = data.recentProductList;
				if (list.length != 0) {
					$.each(list, function(i, v) {
						v.productName = h(v.productName);
						v.viewName = h(v.viewName);
						v.status = (function(v) {
							var status = "";
							if (v.tradeCondition == "TRADABLE") {
								status = '<a href="/p/do/apply?code=' + v.dealerCode + '">卸価格を見る(申請画面へ)<\/a>';
							} else if (v.tradeCondition == "APPLIED") {
								status = '<span class="co-fcgray">申請中<\/span>';
							} else {
								status = '<span class="co-fcgray">申請を受け付けていません<\/span>';
							}
							return status;
						})(v);
						v.isWish = ($.inArray(v.tradeCondition, ["TRADABLE", "PRICE_OPEN", "APPLIED", "TRADING"]) != -1);
					});

					callback(list);
					product.saveRequested(data);
					flexboxAnimUtil.initFlex(viewId);

					if (product.existsProduct()) {
						flexboxAnimUtil.refreshButtons(viewId, false, true);
						clicked = false;
					}
				} else {
					if (product.isEmpty()) {
						$("#" + params.recentDiv).hide();
					}
				}
			}

			function h(value) {
				value = value || "";
				return value.replace(/&/g,"&amp;")
							.replace(/"/g,"&quot;")
							.replace(/'/g,"&#039;")
							.replace(/</g,"&lt;")
							.replace(/>/g,"&gt;");
			}

			function fetchRemote(productCodes) {
				var contextPath = "/p";

				var jqXHR = $.ajax({
					type: "GET",
					url: contextPath + "/recentProduct/get.do",
					cache: false,
					data: {
						productCodes : productCodes,
						flexibleBoxType : "animate"
					}
				});
				return jqXHR;
			}

			function getRecentProducts() {
				var productCodes = product.getProductCodes();
				if (productCodes.length != 0) {
					fetchRemote(productCodes).success(function (data) {
						handleResult(data);
					});
				}
			}

			return {
				init : initialize,
				getRecentProducts : getRecentProducts
			};
		})();

		$(function () {
			recentControl.init();
			var recentDiv = $("#" + params.recentItemsId),
				target = $(window),
				border = recentDiv.offset().top - target.height(),
				isDelay = border > 0,
				trigger = false;

			if (isDelay) {
				if (!trigger &&  (target.scrollTop() > border)) {
					trigger = true;
					recentControl.getRecentProducts();
				}
				target.scroll(function() {
					if (!trigger && (target.scrollTop() > border)) {
						trigger = true;
						recentControl.getRecentProducts();
					}
				});
			} else {
				recentControl.getRecentProducts();
			}
	    });
	}

	function addWishlist() {
		var contextPath = "/p";
		var recentItem = $(this),
			productCode = recentItem.attr("id").replace("recent_product_code_", "")

		if(recentItem.hasClass("async-processing")) {
			return false;
		}

		recentItem.addClass("async-processing");

		if(!recentItem.hasClass("added-wish")) {
			recentItem.addClass("wish-loading");
		} else {
			recentItem.removeClass("added-wish");
			recentItem.find(".add-whis-txt").css("display", "none");
		}

		var loadingTimeId = "";
		$.ajax({
			type: "GET",
			cache : false,
			url: contextPath + "/wishlist/manipulateAsync.do",
			data : {
				p : productCode
			}
		}).success(function(msg) {
			loadingTimeId = setInterval(function() {
				recentItem.removeClass("wish-loading");
				if(msg.status == "add_success"){
					recentItem.removeClass("wish-error");
					recentItem.addClass("added-wish");
					recentItem.find(".add-whis-txt").html("追加しました");
					setTimeout(function(){ recentItem.find(".add-whis-txt").fadeOut(500); },1500);
					recentItem.removeClass("async-processing");
				} else if(msg.status == "delete_success") {
					recentItem.removeClass("wish-error");
					recentItem.find(".add-whis-txt").text("検討中に追加");
					recentItem.find(".add-whis-txt").css("display", "block");
					recentItem.removeClass("async-processing");
				} else if(msg.status == "orverflow") {
					recentItem.addClass("wish-error");
					recentItem.find(".add-whis-txt").html("追加できません<br>再度押してください");
					recentItem.removeClass("async-processing");
				} else {
					recentItem.addClass("wish-error");
					recentItem.find(".add-whis-txt").css("display", "block");
					recentItem.find(".add-whis-txt").html("システムエラーが<br>発生しました");
					recentItem.removeClass("async-processing");
				}
				clearInterval(loadingTimeId);
			}, 500);
		}).error(function(xhr, status, err) {
			recentItem.removeClass("wish-loading");
			recentItem.addClass("wish-error");
			recentItem.find(".add-whis-txt").css("display", "block");
			recentItem.find(".add-whis-txt").html("システムエラーが<br>発生しました");
			recentItem.removeClass("async-processing");
		});
	}

	return {
		requestRecentProduct : requestRecentProduct
	};

})(jQuery, flexboxAnimUtil);

