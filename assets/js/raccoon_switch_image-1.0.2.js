var RaccoonSwitchImage = function() {
	this.thumbImageBox = 'thumbImageBox';
	this.thumbImageBoxPrefix = 'thumbnail_';
	this.onclickTarget = 'largeImageParentBox';
	this.largeImageBox = 'largeImageBox';
	this.thumbnailBoxSize = 40;
	this.thumbnailSize = 40;
	this.largeImageSize = 456;
	this.initialized = false;
	this.nowIndex = null;
	this.largeImageTags = new Array();
	this.dummyImage = '/img/mark/clear.gif';
	this.onclickFunctions = [];

	this.getThumbnailBoxes = function() {
		var thumbnailBoxes = new Array();

		for (var i = 0; true; ++i) {
			if (document.getElementById(this.thumbImageBoxPrefix + i) == null) {
				break;
			}

			thumbnailBoxes[thumbnailBoxes.length] = document.getElementById(this.thumbImageBoxPrefix + i);
		}

		return thumbnailBoxes;
	};

	this.getThumbnails = function() {
		return document.getElementById(this.thumbImageBox).getElementsByTagName('img');
	};

	/*
	this.thumbnailStyleSelected = function(cssStyle) {
		cssStyle.borderColor = '#cc004b';
	};

	this.thumbnailStyleNotSelected = function(cssStyle) {
		cssStyle.borderColor = '#cccccc';
	};*/


	this.thumbnailStyleSelected = function(cssStyle) {

		(function($) {
			$(cssStyle).removeClass("thum-image-vertical");
			$(cssStyle).addClass("thum-image-selected");
		})(jQuery);



	};

	this.thumbnailStyleNotSelected = function(cssStyle) {

		(function($) {
			$(cssStyle).removeClass("thum-image-selected");
			$(cssStyle).addClass("thum-image-vertical");
		})(jQuery);

	};

	this.changeImage = function(index) {
		if (this.initialized == false || this.nowIndex == index) {
			return;
		}

		var thumbnails = this.getThumbnailBoxes();
		for (var i = 0; i < thumbnails.length; ++i) {
			this.thumbnailStyleNotSelected(thumbnails[i]);
		}
		this.thumbnailStyleSelected(thumbnails[index]);

		for (var i = 0; i < this.largeImageTags.length; ++i) {
			this.largeImageTags[i].style.display = 'none';
		}
		this.largeImageTags[index].style.display = 'block';
		this.nowIndex = index;

		if (this.onclickFunctions.length > 0) {
			try { // try for ie5.5
				document.getElementById(this.onclickTarget).style.cursor = 'pointer';
			} catch(e) {}
			document.getElementById(this.onclickTarget).onclick = this.onclickFunctions[index];
		}
	};

	this.onloadImage = function(index, loadImage, largeImage, thumbnail, onclickFunction) {

		if (typeof onclickFunction == 'function') {
			this.onclickFunctions[index] = onclickFunction;
		}

		if (loadImage.width > 0 && loadImage.height > 0) {
			largeImage.src = loadImage.src;
		}
		else {
			loadImage.width = 452;
			loadImage.height = 452;
			largeImage.style.backgroundImage = 'url(' + loadImage.src + ')';
			largeImage.style.backgroundRepeat = 'no-repeat';
			largeImage.src = this.dummyImage;
		}

		if (loadImage.width > this.largeImageSize || loadImage.height > this.largeImageSize) {
			if (loadImage.height >= loadImage.width) {
				largeImage.height = this.largeImageSize;
				largeImage.width = Math.floor(loadImage.width / loadImage.height * this.largeImageSize);
			}
			else {
				largeImage.height = Math.floor(loadImage.height / loadImage.width * this.largeImageSize);
				largeImage.width = this.largeImageSize;
			}
		}
		else {
			largeImage.height = loadImage.height;
			largeImage.width = loadImage.width;
		}
		largeImage.style.marginTop = "" + Math.floor((this.largeImageSize - largeImage.height) / 2) + "px";

		if (index != 0) {
			largeImage.style.display = 'none';
		}

		document.getElementById(this.largeImageBox).appendChild(largeImage);

		this.largeImageTags[index] = largeImage;

		thumbnail.src = loadImage.src;

		if (loadImage.width > this.thumbnailSize || loadImage.height > this.thumbnailSize) {
			if (loadImage.height >= loadImage.width) {
				thumbnail.height = this.thumbnailSize;
				thumbnail.width = Math.floor(loadImage.width / loadImage.height * this.thumbnailSize);
			}
			else {
				thumbnail.height = Math.floor(loadImage.height / loadImage.width * this.thumbnailSize);
				thumbnail.width = this.thumbnailSize;
			}
		}
		else {
			thumbnail.height = loadImage.height;
			thumbnail.width = loadImage.width;
		}
		thumbnail.style.marginTop = "" + Math.floor((this.thumbnailBoxSize - thumbnail.height) / 2) + "px";

		this.getThumbnailBoxes()[index].appendChild(thumbnail);
	};

	this.initializeImage = function() {
		this.initialized = true;
		this.changeImage(0);
	};
};
