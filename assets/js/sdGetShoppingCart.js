jQuery(document).ready(function() {
	$("#searchbox-form").css("paddingRight", "138px");
	$("#searchbox-button").css("right", "138px");

	getShoppingCart();
});

var getShoppingCart = function() {
	(function($) {
		var $detailInner = $("#detail-inner");
		var $sdShipText = $("#cartItem_none_text,#sd_ship_systemError");
		var $cartItemNoneText = $("#cartItem_none_text");
		var $sdShipSystemError = $("#sd_ship_systemError");
		var $restOfTable = $("#rest-of-table");
		var $totalAmount = $("#totalAmount");
		var $cartButton = $detailInner.find(".btn-fromprice-tocart");

		// $.ajax({
		// 	type: "GET",
		// 	url: "/p/do/shoppingCart/get",
		// 	dataType : 'html',
		// 	cache : false,
		// 	contentType: 'application/x-www-form-urlencoded; charset=utf-8',
		// 	success: function(data, status) {
		// 		// errorText
		// 		$sdShipText.hide();

		// 		if($("#price-table").size() > 0) {
		// 			$("#price-table").remove();
		// 		}

		// 		$detailInner.prepend(data);
		// 		var $priceTable = $("#price-table");

		// 		if($("#ajaxTotalAmountNumber").val() == 0) {
		// 			$('#cart2').removeClass('exists');
		// 			$totalAmount.removeClass('in-cart');
		// 			$priceTable.hide();
		// 			$cartItemNoneText.show();
		// 			$cartButton.hide();
		// 		} else {
		// 			$('#cart2').addClass('exists');
		// 			$totalAmount.addClass('in-cart');
		// 			$priceTable.show();
		// 			$cartItemNoneText.hide();
		// 			$cartButton.show();
		// 		}

		// 		if($("#ajaxEtcCount").val() > 5) {
		// 			$restOfTable.show();
		// 		} else {
		// 			$restOfTable.hide();
		// 		}

		// 		$totalAmount.text($("#ajaxTotalAmount").val());
		// 		var searchWrap = $("#header-area-common").find(".search-wrap");
		// 		var searchWrapWidth = searchWrap.width();
		// 		var cartPriceAreaWidth = searchWrap.find("#cart-price-area").width();

		// 		searchWrap.find(".searchbox-button").css("right", (cartPriceAreaWidth + 8) + "px");
		// 		searchWrap.find(".searchbox").css("padding-right", (cartPriceAreaWidth + 8) + "px");

		// 		if(typeof searchFieldWidth == "function") {
		// 			searchFieldWidth();
		// 		}
				
		// 		// RWD対応
		// 		if ($("#cart-area").length) {
		// 			var $cartArea = $("#cart-area");
		// 			var $cartAreaTab = $("#cart-area-tab");
		// 			if($("#ajaxTotalAmountNumber").val() == 0) {
		// 				if ($cartArea.hasClass("cart-in")) {
		// 					$cartArea.removeClass("cart-in");
		// 				}
		// 				if ($cartAreaTab.hasClass("cart-in")) {
		// 					$cartAreaTab.removeClass("cart-in");
		// 				}
		// 			} else {
		// 				$cartArea.find(".cart-price").html($("#ajaxTotalAmount").val());
		// 				$cartAreaTab.find(".cart-price").html($("#ajaxTotalAmount").val());
		// 				if (!$cartArea.hasClass("cart-in")) {
		// 					$cartArea.addClass("cart-in");
		// 				}
		// 				if (!$cartAreaTab.hasClass("cart-in")) {
		// 					$cartAreaTab.addClass("cart-in");
		// 				}
		// 			}
		// 		}
		// 	},
		// 	error: function() {
		// 	    $("#price-table").hide();
		// 		$sdShipSystemError.show();
		// 	}
		// });

	})(jQuery);
};