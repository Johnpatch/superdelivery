var wishlistSearchFlag = false;

function doFreeWordSearch() {
	var form = document.searchWord;
	var p_action = document.getElementById("p_action").value;

	// カタログビューからのサジェスト呼び出しの場合
	if(p_action != null && p_action.indexOf("cv") != -1) {
		form.word.value = document.getElementById("header_word").value + "（企業名）";
	}
	if($("#search_box_genre option:selected").attr("id") == 'wishlistSearch') {
		$("#searchbox-form").attr("action", "/p/wishlist/search.do");
		form.submit();
		return false;
	}
	if($("#search_box_genre option:selected").attr("id") == 'preloginWishlistSearch') {
		$("#searchbox-form").attr("action", "/p/prelogin/wishlist/search.do");
		form.submit();
		return false;
	}


	if(document.getElementById("header_word").value != "") {
		var d = new Date();
		var year = d.getFullYear();
		var month = d.getMonth();
		var date = d.getDate();
		var hh = d.getHours();
		var mm = d.getMinutes();
		var ss = d.getSeconds();
		var img = document.createElement("img");
		img.src = "/img/jsp/suggest.gif?word=" + encodeURIComponent(document.getElementById("header_word").value) + "&type=header" + "&time=" + year + (month + 1) + date + hh + mm + ss;
	}

	var genreValue = document.getElementById("search_box_genre").value;
	//選択に変更があった場合
	if (genreValue != "current_condition") {
		sendCurrentConditonParam(false);

		//サイト全体から
		if (genreValue == "all") {
			if (!document.getElementById("sb_freeword")) {
				var serchBoxGenre = document.createElement("input");
				serchBoxGenre.type = "hidden";
				serchBoxGenre.value = genreValue;
				serchBoxGenre.name = "sb";
				serchBoxGenre.id = "sb_freeword";
				form.appendChild(serchBoxGenre);
			}

			if(document.getElementsByName("vi")[0] != null) {
				document.getElementsByName("vi")[0].value = "1";
			}
			if(document.getElementsByName("so")[0] != null) {
				document.getElementsByName("so")[0].value = "score";
			}

			form.action = "/p/do/"+p_action+"/";
		}
		//ジャンル選択
		else {
			form.action = "/p/do/"+p_action+"/" + genreValue + "/";
		}

		// vi=3対策
		if(document.getElementsByName("vi")[0] != null) {
			if(document.getElementsByName("vi")[0] == "3") {
				document.getElementsByName("vi")[0].disabled = true;
			}
		}

	} else {
		sendCurrentConditonParam(true);
		var dpslPath = document.getElementById("dpsl_current_condition_action_path");
		if (dpslPath) {
			form.action = dpslPath.value;
		}
	}

	analyticsTracker._trackPageview('/analytics/search/header/' + form.word.value);

	var strUA = "";
	var d_time ="";
	strUA = navigator.userAgent.toLowerCase();
	if(strUA.indexOf("safari") != -1){
		d_time = 100;
	} else {
		d_time = 0;
	}

	setTimeout(function() {
		form.submit();
	}, d_time);

	return false;
}

function sendCurrentConditonParam(disabled) {
	var currentConditions = document.getElementById("current_condition");
	if (currentConditions && currentConditions.hasChildNodes()) {
		for (var i = 0, len = currentConditions.childNodes.length; i < len; i++) {
			if (currentConditions.childNodes[i].nodeType == 1) {
				currentConditions.childNodes[i].disabled = !disabled;
			}
		}
	}
}

function listKeyWordSearch(position) {

	var strUA = "";
	var d_time ="";
	strUA = navigator.userAgent.toLowerCase();

	if(document.getElementById(position + "_keyword").value != "") {
		var d = new Date();
		var year = d.getFullYear();
		var month = d.getMonth();
		var date = d.getDate();
		var hh = d.getHours();
		var mm = d.getMinutes();
		var ss = d.getSeconds();
		var img = document.createElement("img");
		img.src = "/img/jsp/suggest.gif?word=" + encodeURIComponent(document.getElementById(position + "_keyword").value) + "&type=header" + "&time=" + year + (month + 1) + date + hh + mm + ss;
	}

	if(strUA.indexOf("safari") != -1){
		d_time = 100;
	} else {
		d_time = 0;
	}

	setTimeout(function() {
		document.getElementById(position + "_keywordSearchForm").submit();
	}, d_time);
}

function agentIe(position) {
	var strUA = "";
	var d_time ="";
	strUA = navigator.userAgent.toLowerCase();

	if(strUA.indexOf("msie") != -1){
		if(document.getElementById(position + "_keyword").value != "") {
			var d = new Date();
			var year = d.getFullYear();
			var month = d.getMonth();
			var date = d.getDate();
			var hh = d.getHours();
			var mm = d.getMinutes();
			var ss = d.getSeconds();
			var img = document.createElement("img");
			img.src = "/img/jsp/suggest.gif?word=" + encodeURIComponent(document.getElementById(position + "_keyword").value) + "&type=header" + "&time=" + year + (month + 1) + date + hh + mm + ss;
		}
		document.getElementById(position + "_keywordSearchForm").submit();
	}
}

$(function(){
  $('#search_box_genre').change(function(){
	setSelectTopText();
  });
});

function searchBoxWidthPreLogin(){
	var winWidth = $(window).width();
	var swWidthNum = 1.53;
	var swMlNum = 3.2;
	var plusNum1 = Math.round((winWidth - 1024) / swWidthNum);
	var plusNum2 = Math.round(-(winWidth - 1024) / swMlNum);

	var swWidthMin = 634;
	var swWidthMax = 801;

	var swMlMin = -308;
	var swMlMax = -413;

	if (winWidth <= 1024) {
		$('#header-area-common .search-wrap').css({
			width: swWidthMin + 'px',
			marginLeft: swMlMin + 'px'
		});
	} else if(winWidth > 1024 && winWidth < 1280) {
		$('#header-area-common .search-wrap').css({
			width: (swWidthMin + plusNum1) + 'px',
			marginLeft: (swMlMin + plusNum2) + 'px'
		});
	} else if (winWidth >= 1280) {
		$('#header-area-common .search-wrap').css({
			width: swWidthMax + 'px',
			marginLeft: swMlMax + 'px'
		});
	}
}
function searchBoxWidthLoggedIn(){
	var winWidth = $(window).width();
	var swWidthNum = 1.4;
	var swMlNum = 2.81;
	var plusNum1 = Math.round((winWidth - 1024) / swWidthNum);
	var plusNum2 = Math.round(-(winWidth - 1024) / swMlNum);
	var plusNum3 = $('#header-area-common .cart-price-area').width() + 8;

	var swWidthMin = 634;
	var swWidthMax = 801;

	var swMlMin = -308;
	var swMlMax = -413;

	if (winWidth <= 1024) {
		$('#header-area-common .search-wrap').css({
			width: swWidthMin + 'px',
			marginLeft: swMlMin + 'px'
		});
	} else if(winWidth > 1024 && winWidth < 1280) {
		$('#header-area-common .search-wrap').css({
			width: (swWidthMin + plusNum1) + 'px',
			marginLeft: (swMlMin + plusNum2) + 'px'
		});
	} else if (winWidth >= 1280) {
		$('#header-area-common .search-wrap').css({
			width: swWidthMax + 'px',
			marginLeft: swMlMax + 'px'
		});
	}
}

function searchBoxWidth(){
	if($("#dpsl_current_condition_pre").size()==1){
		searchBoxWidthPreLogin();
	}
	else{
		searchBoxWidthLoggedIn();
	}
}

function setSelectTopText(){

	var selectTopMaxWidth=200;
	var selectTopMinWidth=130;
	var newSelectedText = $("#search_box_genre option:selected").text();
	var newSelectedTextOrg = $("#search_box_genre option:selected").text();
	if (newSelectedText == "") {
		newSelectedText = $('#select-top').text();
		newSelectedTextOrg = $('#select-top').text();
	}
	var hiddenSpan = $('#hidden-span');
	var selectTopSpan = $('#select-top');

	hiddenSpan.css({
		"font-size":selectTopSpan.css("font-size")
		,"letter-spacing":selectTopSpan.css("letter-spacing")
		});

	hiddenSpan.text(newSelectedText);

	//一文字削除
	var modflg= false;
	while(hiddenSpan.width() > selectTopMaxWidth){
		newSelectedText=newSelectedText.substring(0,newSelectedText.length-2);
		hiddenSpan.text(newSelectedText+"・・・");
		modflg= true;
	}
	$('#select-top').text(newSelectedText+(modflg?"・・・":"")).width(hiddenSpan.width());
	//console.log("selectTopSpan.width():"+selectTopSpan.width())
	if(selectTopSpan.width() < selectTopMinWidth){
		selectTopSpan.width(selectTopMinWidth);
	}
//	console.log("selectTopSpan.width():"+selectTopSpan.width())
//	console.log("hiddenSpan.width():"+hiddenSpan.width())
	if(navigator.userAgent.indexOf("MSIE 9.0")!=-1) {

	}


	var w =$('#select-top').outerWidth()+ $('#select-down').outerWidth();

	w = Math.max(w,selectTopMinWidth);

	$('#select-parent').width(w);
	$('#searchbox-select').css("padding-left", w+10 + "px");
	$('#search_box_genre').width(w);
}

function searchFieldWidth() {
	var searchWidth = $('#header-area-common .searchbox').width();
	$("#searchbox-field-wrap").css("width", (searchWidth - 11) + "px");
}

jQuery(document).ready(function() {
	// Widthを調整の上検索窓を出現させる
	searchBoxWidth();
	searchFieldWidth();
	$('#header-area-common .search-wrap').show();

	jQuery("#searchWordTextWrap").jqueryWatermaker();


	// 自動補完
	// jQuery('#header_word').autocomplete('/t/suggest/search.do', {
	// 	genreSelector : "#search_box_genre",
	// 	onItemSelect: function() {
	// 		DetailedSearchBox.suggestSearch();
	// 	}
	// });

	setSelectTopText();

	$(window).resize(function() {
		searchBoxWidth();
		searchFieldWidth();
	});
});