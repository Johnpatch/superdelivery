var textSearchBox = (function($) {
	function search(params) {
		var form = $("#" + params.formId),
			keyword = $("#" + params.keywordId),
			genreCode = params.genreCode || "all";

		if (keyword.val() != "") {
			var d = new Date();
			var year = d.getFullYear();
			var month = d.getMonth();
			var date = d.getDate();
			var hh = d.getHours();
			var mm = d.getMinutes();
			var ss = d.getSeconds();
			var img = document.createElement("img");
			img.src = "/img/jsp/suggest.gif?word=" + encodeURIComponent(keyword.val()) + "&type=header" + "&time=" + year + (month + 1) + date + hh + mm + ss;
		}

		if (genreCode == "all") {
			if (!form.find(".rwd_sb_freeword").get(0)) {
				$("<input>", {
					type : "hidden",
					name : "sb",
					value : "all"
				}).addClass("rwd_sb_freeword").appendTo(form);
			}
			form.attr("action", "/p/do/psl/");
		} else {
			form.attr("action", "/p/do/psl/" + genreCode + "/");
		}

		analyticsTracker._trackPageview('/analytics/search/header/' + keyword.val());
		var strUA = "";
		var d_time ="";
		strUA = navigator.userAgent.toLowerCase();
		if (strUA.indexOf("safari") != -1) {
			d_time = 100;
		} else {
			d_time = 0;
		}

		setTimeout(function() {
			form.submit();
		}, d_time);
	}

	function setTopText() {
		var selectTopMaxWidth = 200,
			selectTopMinWidth = 130;

		var newSelectedText = $("#rwd-search_box_genre option:selected").text(),
			hiddenSpan = $("#rwd-hidden-span"),
			selectTopSpan = $("#rwd-select-top");

		if (newSelectedText == "") {
			newSelectedText = selectTopSpan.text();
		}

		hiddenSpan.css({
			"font-size" : selectTopSpan.css("font-size")
			,"letter-spacing" : selectTopSpan.css("letter-spacing")
		}).text(newSelectedText);

		selectTopSpan.text(newSelectedText).width(hiddenSpan.width());
		if (selectTopSpan.width() < selectTopMinWidth) {
			selectTopSpan.width(selectTopMinWidth);
		}
	}

	return {
		search : search,
		setTopText : setTopText
	}

})(jQuery);
