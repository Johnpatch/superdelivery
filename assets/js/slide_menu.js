$(function(){
	var speed = 300;

	// 繝峨Ο繝ｯ繝ｼ繝｡繝九Η繝ｼ
	var menu = $('#slide-menu');
	var menuBtn = $('#sub-menu-btn');
	var body = $(document.body);
	var menuWidth = menu.width();

	menuBtn.bind('click', function(){
		body.toggleClass('open');
		if(body.hasClass('open')){
			menu.show()
			$('#header-fixd-content,#footer-area-common2,#footer-area-common3').animate({'left' : menuWidth }, speed);
		} else {
			$('#header-fixd-content,#footer-area-common2,#footer-area-common3').animate({'left' : 0 }, speed, function() {
				menu.hide()
			});
		}
	});

	// 繧ｸ繝｣繝ｳ繝ｫ繝｡繝九Η繝ｼ髢矩哩
	$("#slide-genre-box h3").click(function(){
		if(!$(this).hasClass("openHead")) {
			$(this).addClass("openHead");
			$(this).next("ul").show();
		} else {
			$(this).next("ul").hide();
			$(this).removeClass("openHead");
		}
	});

	// 繧ｫ繝ｼ繝亥崋螳�
	var boxPos = $("#header-sp-cont").offset().top + $("#header-sp-cont").outerHeight();

	var timer = false;
	$(window).bind('scroll resize touchmove', function() {
		if (timer !== false) {
			clearTimeout(timer);
		}
		timer = setTimeout(function() {
			if(!body.hasClass("open")) {
				var scrllPos = -$('html,body').offset().top;

				if (scrllPos > boxPos) {
					$(".cart-in .fix-cart.cart-box").not(":animated").animate({'bottom' : '-38px'}, speed);
				} else {
					$(".cart-in .fix-cart.cart-box").not(":animated").animate({'bottom' : '-140px'}, speed);
				}
			}
		}, 30);
	});

	$("#slide-searchWordTextWrap").jqueryWatermaker();
	$("#slide_word").autocomplete('/t/suggest/search.do', {
		onItemSelect : function() {
			textSearchBox.search({
				formId : "slide-searchbox-form",
				keywordId : "slide_word"
			});
		}
	});

	$("#slide-searchbox-button").click(function() {
		textSearchBox.search({
			formId : "slide-searchbox-form",
			keywordId : "slide_word"
		});
	});

});
