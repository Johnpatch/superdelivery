var UserAgent = {
	set : function(user_agent){
		this._user_agent = user_agent;
	},
	getOS : function(){
		var ret;
		if(ret = this._user_agent.match(/Windows NT (\d+\.\d*)/)){
			var os = new UserAgent.Component('Windows','NT '+ret[1],parseFloat(ret[1]));
			if(os.computeVersion < 5.1){
				os.version = '2000';
			}else if(os.computeVersion < 6.0){
				os.version = 'XP';
			}else if(os.computeVersion < 6.1){
				os.version = 'Vista';
			}else if(os.computeVersion <= 6.1){
				os.version = '7';
			}else if(os.computeVersion <= 6.3){
				os.version = '8';
			}else if(os.computeVersion <= 10.0){
				os.version = '10'
			}
			return os;
		}else if(ret = this._user_agent.match(/Windows (95|98|2000|Me|XP|CE])/)){
			var version = ret[1];
			var computeVersion = 0;
			if(this._user_agent.indexOf('; Win 9x 4.90') >= 0){
				version = 'Me';
			}
			if(version == 'XP'){
				computeVersion = 5.1;
			}
			return new UserAgent.Component('Windows', version, computeVersion);
		}else if(this._user_agent.indexOf('Android') > 0 && this._user_agent.indexOf('Mobile') == -1){
			var version,computeVersion;
			ret = this._user_agent.match(/Android ((\d+\.\d)(.\d)?)/);
			if (ret){
				version = ret[1];
				computeVersion = ret[2];
			} else{
				version = "";
				computeVersion = 1;
			}
			return new UserAgent.Component('Android(タブレット)', version, computeVersion);
		}else if(this._user_agent.indexOf('Android') > 0 && this._user_agent.indexOf('Mobile') > 0){
			var version,computeVersion;
			ret = this._user_agent.match(/Android ((\d+\.\d)(.\d)?)/);
			if (ret){
				version = ret[1];
				computeVersion = ret[2];
			} else{
				version = "";
				computeVersion = 1;
			}
			return new UserAgent.Component('Android', version, computeVersion);
		}else if(ret = this._user_agent.match(/(iPhone|iPod).*CPU (iPhone OS ((\d+_\d)(_\d)?))?.*Mobile/)){
			var version,computeVersion;
			if (ret[4]){
				version = ret[3].replace(/_/g, ".");
				computeVersion = ret[4].replace(/_/g, ".");
			} else{
				version = "";
				computeVersion = "1.0";
			}
			return new UserAgent.Component('iOS(' + ret[1] + ')', version, computeVersion);
		}else if(ret = this._user_agent.match(/iPad.*CPU OS ((\d+_\d)(_\d)?).*Mobile/)){
			return new UserAgent.Component('iOS(iPad)', ret[1].replace(/_/g, "."), ret[2].replace(/_/g, "."));
		}else if(ret = this._user_agent.match(/Mac OS X( (\d+([_\.]\d+)*))?/)){
			if(!ret[2] || ret[2] ==''){
				return new UserAgent.Component('Mac','OS X',10);
			}else{
				var v = ret[2].replace(/_/g,'.');
				return new UserAgent.Component('Mac','OS X ' + v, this._get_version(v));
			}
		}else if(ret = this._user_agent.match(/Linux/)){
			return new UserAgent.Component('Linux');
		}else{
			return new UserAgent.Component();
		}
	},
	getBrowser : function(){
		var name = null;
		var ret, ret2;
		if(ret = this._user_agent.match(/MSIE ((\d+\.\d*)b?(\d*))/)){ // before IE10
			if(ret[2] == '7.0' && (ret2 = this._user_agent.match(/ Trident\/(\d\.\d)/))){ // 互換表示
				if(ret2[1] == '4.0'){
					return new UserAgent.Component('Internet Explorer', '8.0 （互換）', 8.0);
				} else if(ret2[1] == '5.0'){
					return new UserAgent.Component('Internet Explorer', '9.0 （互換）', 9.0);
				} else if(ret2[1] == '6.0'){
					return new UserAgent.Component('Internet Explorer', '10.0 （互換）', 10.0);
				} else if(ret2[1] == '7.0'){
					return new UserAgent.Component('Internet Explorer', '11.0 （互換）', 11.0);
				}
			}
			return new UserAgent.Component('Internet Explorer', ret[1], parseFloat(ret[2]+ret[3]));
		}else if(ret = this._user_agent.match(/ Trident.* rv:((\d+\.\d*)b?(\d*))/)){ // IE11
			return new UserAgent.Component('Internet Explorer', ret[1], parseFloat(ret[2]+ret[3]));
		}else if(ret = this._user_agent.match(/Firefox\/(\d+(\.\w?\d+(pre)?)*)/)){
			return new UserAgent.Component('Firefox', ret[1], this._get_version(ret[1]));
		}else if(ret = this._user_agent.match(/Edge\/(\d+(\.\d+)*)/)) {
			return new UserAgent.Component('Edge',ret[1],this._get_version(ret[1]));
		}else if(ret = this._user_agent.match(/Chrome\/(\d+(\.\d+)*)/)){
			return new UserAgent.Component('Chrome', ret[1], this._get_version(ret[1]));
		}else if(ret = this._user_agent.match(/Android.*like Gecko\) Version\/((\d+\.\d)(.\d)?)/)){
			var browserName = 'Android Browser';
			if(this._user_agent.indexOf('Android') > 0 && this._user_agent.indexOf('Mobile') == -1){
				browserName += '(タブレット)';
			}
			return new UserAgent.Component(browserName, ret[1], ret[2]);
		}else if(ret = this._user_agent.match(/(iPhone|iPod|iPad).*Gecko\)( Version\/((\d+\.\d*)(.\d*)?)|) Mobile/)){
			var version,computeVersion;
			if(ret[4]){
				version = ret[3];
				computeVersion = ret[4];
			}else{
				version = "";
				computeVersion = 1;
			}
			return new UserAgent.Component('Safari(' + ret[1] + ')', version, computeVersion);
		}else if(ret = this._user_agent.match(/(Version\/(\d+(\.\d+)*))? Safari\/(\d+(\.\d+)*)/)){
			var version,computeVersion;
			if(ret[2]){
				version = ret[2];
				computeVersion = this._get_version(ret[2]);
			}else{
				var v = this._get_version(ret[4]);
				if(v >= 412){
					version = "2";
					computeVersion = 2;
				}else{
					version = "1";
					computeVersion = 2;
				}
			}
			return new UserAgent.Component('Safari', version, computeVersion);
		}else if(ret = this._user_agent.match(/Opera\/(\d+(\.\d+)*)/)){
			return new UserAgent.Component('Opera', ret[1], this._get_version(ret[1]));
		}else{
			return new UserAgent.Component();
		}
	},
	_get_version : function(str){
		var source = str.replace(/a|b|c|pre/,'').split('.'),
		version='',
		tkn='.';
		for(var i=0,l=source.length;i<l;++i){
			version += source[i] + tkn;
			tkn = '';
		}
		return parseFloat(version);
	}
};
UserAgent.Component = function(name,version,computeVersion){
	this.name = name;
	this.version = version;
	this.computeVersion = computeVersion;
};
UserAgent.Component.prototype.toString = function(){
	var name = this.name;
	if(this.version) name += ' ' + this.version;
	return  name;
};
UserAgent.Component.prototype.allow = function(ok, ng){
	//IE10を推奨環境から除く
	if(this.name == 'Internet Explorer' && this.version == 10) {
		return false;
	}
	return this.name == ok.name && this.computeVersion >= ok.computeVersion &&
			(!ng || (this.name == ng.name && this.computeVersion < ng.computeVersion));
}
UserAgent.Component.Unknown = new UserAgent.Component('Unknown');
UserAgent.set(navigator.userAgent);