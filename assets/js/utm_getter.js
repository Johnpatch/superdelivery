/**
GETパラメータにutmパラメータが含まれていたらCookieに保存する
 */

var UtmParamGetter = function(serverTime) {
	if (serverTime == null) {
		// 海外からのアクセスのために現地時間を日本時間に修正している
		var utcTime = Date.now();
		var jstTimeValue = utcTime + 1000 * 60 * 60 * 9;
		var jstTime = new Date();
		jstTime.setTime(jstTimeValue);
		this.serverTime = jstTime.getUTCFullYear() + "/" + (jstTime.getUTCMonth() + 1) + "/" + jstTime.getUTCDate() + " " + jstTime.getUTCHours() + ":" + jstTime.getUTCMinutes() + ":" + jstTime.getUTCSeconds();
	} else {
		this.serverTime = serverTime;
	}
	this.utmParamRegExp = /^[0-9a-zA-Z_]+$/;
};

UtmParamGetter.prototype.setCookie = function() {
	var arg = new Object();
	var urlParam = location.search.substring(1);
	if (urlParam) {
		var params = urlParam.split("&");
		for (var i = 0; i < params.length; i++) {
			var keyValue = params[i].split("=");
			arg[keyValue[0]] = keyValue[1];
		}
	}

	if (arg.utm_source != null && arg.utm_medium != null && arg.utm_campaign != null) {
		// 各utmパラメータは半角英数およびアンダーバーのみで15文字以下
		if (arg.utm_source.length <= 15 && arg.utm_medium.length <= 15 && arg.utm_campaign.length <= 15) {
			if (arg.utm_source.match(this.utmParamRegExp) && arg.utm_medium.match(this.utmParamRegExp) && arg.utm_campaign.match(this.utmParamRegExp)) {
				$.cookie.json = true;
				var utm_param = $.cookie('utm_param');
				if (utm_param == null) {
					utm_param = [{"utm_source": arg.utm_source, "utm_medium": arg.utm_medium, "utm_campaign": arg.utm_campaign, "access_date": this.serverTime}];
				} else {
					var new_utm_param = {"utm_source": arg.utm_source, "utm_medium": arg.utm_medium, "utm_campaign": arg.utm_campaign, "access_date": this.serverTime};
					utm_param.push(new_utm_param);
					// 保存するCookieは最大20件
					if (utm_param.length > 20) {
						utm_param.shift();
					}
				}
				$.cookie('utm_param', utm_param, {expires: 365, path: "/"});
			}
		}
	}
};